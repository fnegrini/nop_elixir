defmodule Run do

  def simulate(count) do

    my_sensor = Sensor.create()

    #Run "count" times
    run(my_sensor, count)

    true

  end

  def run(_my_sensor, 0) do
    # Stop condition
  end

  def run(my_sensor, count) do

    is_read = GenServer.call(my_sensor, :is_read, :infinity)

    is_activated = GenServer.call(my_sensor, :is_activated, :infinity)

    if (is_read == true) and !(is_activated == true) do
      GenServer.cast(my_sensor, :read)
      GenServer.cast(my_sensor, :deactivate)
    end

    run(my_sensor, count - 1)

  end
end

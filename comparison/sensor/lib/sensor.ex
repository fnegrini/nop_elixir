defmodule Sensor do
  use GenServer
  require Logger

  def create() do

    state = init_state()

    {:ok, pid} = GenServer.start_link(__MODULE__, state, name: __MODULE__)

    pid

  end

  defp init_state() do

    %{:is_read   => false,
      :is_activated => false}

  end

  @impl true
  def init(stack) do
    {:ok, stack}
  end

  @impl true
  def handle_call(:is_read, _from, state) do

    is_read = Map.get(state, :is_read)

    {:reply, is_read, state}

  end

  def handle_call(:is_activated, _from, state) do

    is_activated = Map.get(state, :is_activated)

    {:reply, is_activated, state}

  end

  @impl true
  def handle_cast(:activate, state) do
    state = Map.put(state, :is_read, false)
    state = Map.put(state, :is_activated, true)
    Logger.debug("Sensor was activated")
    {:noreply, state}
  end

  def handle_cast(:read, state) do
    state = Map.put(state, :is_read, true)
    Logger.debug("Sensor was readed")
    {:noreply, state}
  end

  def handle_cast(:deactivate, state) do
    state = Map.put(state, :is_activated, true)
    Logger.debug("Sensor was deactivated")
    {:noreply, state}
  end
end

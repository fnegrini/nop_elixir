
# Run in iex:
# c "./statistics/stat100x4000.exs"


{time_in_microseconds, list} =  :timer.tc(
  fn ->
    list = Cta.create_list(100, "test")
    Cta.simulate(list, 4000)
    Cta.wait_up_to_end_all_process(list)
    list
  end)

  IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
  IO.puts("The notification count is #{Cta.get_statistics_from_total(list)}")

defmodule Cta.Semaphore do
  use GenServer
  require Logger

  def init(state) do
    {:ok, state}
  end

  def start_link(name) do

  #States:
  # 0 => HorizontalGREEN
  # 1 => HorizontalYELLOW
  # 2 => HorizontalRED
  # 3 => VerticalGREEN
  # 4 => VerticalYELLOW
  # 5 => VerticalRED
  # 6 => HorizontalGREENFull
  # 7 => HorizontalYELLOWFull
  # 8 => VerticalGREENFull
  # 9 => VerticalYELLOWFull

    state = %{
      :atSeconds => 0,
      :atSemaphoreState => :VerticalRED,
      :atHVSS => 0,
      :atVVSS => 0,
    }

    state = reset_statistics_to_state(state)

    state = set_name(state, name)

    GenServer.start_link(__MODULE__, state)
  end

  defp set_name(state, name) do
    Map.put(state, :name, name)
  end

  defp get_name(state) do
    Map.get(state, :name)
  end

  defp get_statistics_from_state(state) do
    Map.get(state, :statistics)
  end

  defp add_statistics_to_state(state, count) do
    old = get_statistics_from_state(state)
    Map.put(state, :statistics, old + count)
  end

  defp reset_statistics_to_state(state) do
    Map.put(state, :statistics, 0)
  end

  def handle_call(:get_statistics, _from, state) do
    {:reply, get_statistics_from_state(state), state}
  end

  def handle_call(:get_name, _from, state) do
    {:reply, get_name(state), state}
  end

  def handle_call(:get_seconds, _from, state) do
    {:reply, Map.get(state, :atSeconds), state}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, Map.get(state, :atSemaphoreState), state}
  end

  def handle_call(:get_v_sensor, _from, state) do
    {:reply, Map.get(state, :atVVSS), state}
  end

  def handle_call(:get_h_sensor, _from, state) do
    {:reply, Map.get(state, :atHVSS), state}
  end

  def handle_cast(:reset_statistics, state) do
    {:noreply, reset_statistics_to_state(state)}
  end

  def handle_cast({:set_seconds, value}, state) do
    state = add_statistics_to_state(state, 1)
    state = Map.put(state, :atSeconds, value)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  def handle_cast({:inc_seconds, count}, state) do
    state = add_statistics_to_state(state, 1)
    state = inc_seconds(state, count)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  def handle_cast({:set_state, value}, state) do
    state = add_statistics_to_state(state, 1)
    state = Map.put(state, :atSemaphoreState, value)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  def handle_cast({:set_v_sensor, value}, state) do
    state = add_statistics_to_state(state, 1)
    state = Map.put(state, :atVVSS, value)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  def handle_cast({:set_h_sensor, value}, state) do
    state = add_statistics_to_state(state, 1)
    state = Map.put(state, :atHVSS, value)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  defp inc_seconds(state, count) do
    semSeconds = Map.get(state, :atSeconds)
    Map.put(state, :atSeconds, semSeconds + count)
  end

  defp reset_seconds(state) do
    Map.put(state, :atSeconds, 0)
  end

  defp change_state(newSemaState, state) do
    name = get_name(state)
    semState = Map.get(state, :atSemaphoreState)
    semSeconds = Map.get(state, :atSeconds)
    Logger.debug("Semaphore #{name} changed state from #{to_string(semState)} to #{to_string(newSemaState)} at seconds #{semSeconds}")
    Map.put(state, :atSemaphoreState, newSemaState)
  end

  defp control_semaphore_state(state) do
    semState = Map.get(state, :atSemaphoreState)
    semSeconds = Map.get(state, :atSeconds)
    verSensor = Map.get(state, :atVVSS)
    horSensor = Map.get(state, :atHVSS)
    check_state_by_second(semSeconds, semState, horSensor, verSensor, state)
  end

  #rlCBCL1
  defp check_state_by_second(2, :VerticalRED, _horSensor, _verSensor, state) do
    state = change_state(:HorizontalGREEN, state)
    reset_seconds(state)
  end

  #rlCBCL2
  defp check_state_by_second(38, :HorizontalGREEN, _horSensor, _verSensor, state) do
    state = change_state(:HorizontalYELLOW, state)
    reset_seconds(state)
  end

  #rlCBCL3
  defp check_state_by_second(30, :HorizontalGREENFull, _horSensor, _verSensor , state) do
    state = change_state(:HorizontalYELLOW, state)
    reset_seconds(state)
  end

  #rlCBCL4
  defp check_state_by_second(5, :HorizontalYELLOW, _horSensor, _verSensor, state) do
    state = change_state(:HorizontalRED, state)
    reset_seconds(state)
  end

  #rlCBCL5
  defp check_state_by_second(6, :HorizontalYELLOWFull, _horSensor, _verSensor, state) do
    state = change_state(:HorizontalRED, state)
    reset_seconds(state)
  end

  #rlCBCL6
  defp check_state_by_second(2, :HorizontalRED, _horSensor, _verSensor, state) do
    state = change_state(:VerticalGREEN, state)
    reset_seconds(state)
  end

  #rlCBCL7
  defp check_state_by_second(38, :VerticalGREEN, _horSensor, _verSensor, state) do
    state = change_state(:VerticalYELLOW, state)
    reset_seconds(state)
  end

  #rlCBCL8
  defp check_state_by_second(30, :VerticalGREENFull, _horSensor, _verSensor, state) do
    state = change_state(:VerticalYELLOW, state)
    reset_seconds(state)
  end

  #rlCBCL9
  defp check_state_by_second(5, :VerticalYELLOW, _horSensor, _verSensor, state) do
    state = change_state(:VerticalRED, state)
    reset_seconds(state)
  end

  #rlCBCL10
  defp check_state_by_second(6, :VerticalYELLOWFull, _horSensor, _verSensor, state) do
    state = change_state(:VerticalRED, state)
    reset_seconds(state)
  end

  #rlCBCL11
  defp check_state_by_second(seconds, :HorizontalGREEN, 1, _verSensor, state) when (seconds in (0..17)) do
    change_state(:HorizontalGREENFull, state)
  end

  #rlCBCL12
  defp check_state_by_second(seconds, :HorizontalGREEN, 2, _verSensor, state) when (seconds in (0..17)) do
    change_state(:HorizontalGREENFull, state)
  end

  #rlCBCL13
  defp check_state_by_second(seconds, :HorizontalGREEN, _horSensor, 1, state) when (seconds in (18..31)) do
    state = change_state(:HorizontalYELLOWFull, state)
    reset_seconds(state)
  end

  #rlCBCL14
  defp check_state_by_second(seconds, :HorizontalGREEN, _horSensor, 2, state) when (seconds in (18..31)) do
    state = change_state(:HorizontalYELLOWFull, state)
    reset_seconds(state)
  end

  #rlCBCL15
  defp check_state_by_second(seconds, :VerticalGREEN, _horSensor, 1, state) when (seconds in (0..17)) do
    change_state(:VerticalGREENFull, state)
  end

  #rlCBCL16
  defp check_state_by_second(seconds, :VerticalGREEN, _horSensor, 2, state) when (seconds in (0..17)) do
    change_state(:VerticalGREENFull, state)
  end

  #rlCBCL17
  defp check_state_by_second(seconds, :VerticalGREEN, 1, _verSensor, state) when (seconds in (18..31)) do
    state = change_state(:VerticalYELLOWFull, state)
    reset_seconds(state)
  end

  #rlCBCL17
  defp check_state_by_second(seconds, :VerticalGREEN, 2, _verSensor, state) when (seconds in (18..31)) do
    state = change_state(:VerticalYELLOWFull, state)
    reset_seconds(state)
  end

  defp check_state_by_second(_value, _semState, _horSensor, _verSensor, state) do
    state
  end

  def simulate(pid, seconds) do

    Enum.map(1..seconds,
      fn(_x) ->
        GenServer.cast(pid, {:inc_seconds, 1})
       end)

  end

end

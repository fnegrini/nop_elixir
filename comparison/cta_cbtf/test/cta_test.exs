defmodule CtaTest do
  use ExUnit.Case
  doctest Cta

  test "greets the world" do
    assert Cta.hello() == :world
  end

  test "Instance new semaphore" do
    {status, pid} = Cta.Semaphore.start_link("test")
    assert status == :ok
  end

  test "Getting statistics" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    assert GenServer.call(pid, :get_statistics) == 0
  end

  test "Getting name" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    assert GenServer.call(pid, :get_name) == "test"
  end

  test "Setting and Getting seconds attribute" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 10})
    assert GenServer.call(pid, :get_seconds) == 10
  end

  test "Setting and Getting state attribute" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_state, :HorizontalGREEN})
    assert GenServer.call(pid, :get_state) == :HorizontalGREEN
  end

  test "Testing inc seconds" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:inc_seconds, 1})
    assert GenServer.call(pid, :get_seconds) == 1

  end

  test "Testing vertical sensor" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_v_sensor, 1})
    assert GenServer.call(pid, :get_v_sensor) == 1

  end

  test "Testing horizontal sensor" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_h_sensor, 1})
    assert GenServer.call(pid, :get_h_sensor) == 1

  end

  test "Testing rule rlCBCL1" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    Cta.Semaphore.simulate(pid, 2)
    assert GenServer.call(pid, :get_state) == :HorizontalGREEN
  end

  test "Testing rule rlCBCL2" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    Cta.Semaphore.simulate(pid, 40)
    assert GenServer.call(pid, :get_state) == :HorizontalYELLOW
  end

  test "Testing rule rlCBCL4" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    Cta.Semaphore.simulate(pid, 45)
    assert GenServer.call(pid, :get_state) == :HorizontalRED
  end

  test "Testing rule rlCBCL6" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    Cta.Semaphore.simulate(pid, 47)
    assert GenServer.call(pid, :get_state) == :VerticalGREEN
  end

  test "Testing rule rlCBCL7" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    Cta.Semaphore.simulate(pid, 85)
    assert GenServer.call(pid, :get_state) == :VerticalYELLOW
  end

  test "Testing rule rlCBCL9" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    Cta.Semaphore.simulate(pid, 90)
    assert GenServer.call(pid, :get_state) == :VerticalRED
  end

  test "time count test" do

    {_status, pid} = Cta.Semaphore.start_link("test90")

    {time_in_microseconds, _ret_val} =  :timer.tc(
      fn ->
        Cta.Semaphore.simulate(pid, 90)
        Cta.wait_up_to_end_all_process([pid])
      end)

    IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
    IO.puts("The notification count is #{Cta.get_statistics_from_total([pid])}")

    assert true

  end

  test "Create list" do

    list = Cta.create_list(10, "test")

    assert length(list) == 10
  end

  test "Simulation list" do

    list = Cta.create_list(10, "test")

    Cta.simulate(list, 90)

    assert true
  end

end

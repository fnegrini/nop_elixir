
# Run in iex:
# c "./statistics/stat_sequential_100x4000.exs"


{time_in_microseconds, list} =  :timer.tc(
  fn ->
    {_status, pid} = Cta.Semaphore.start_link("Sequential")
    Cta.simulate_sequential(pid, 100, 4000)
    Cta.wait_up_to_end_all_process([pid])
    [pid]
  end)

  IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
  IO.puts("The notification count is #{Cta.get_statistics_from_total(list)}")

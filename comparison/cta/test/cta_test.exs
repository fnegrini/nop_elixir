defmodule CtaTest do
  use ExUnit.Case
  doctest Cta

  test "greets the world" do
    assert Cta.hello() == :world
  end

  test "Instance new semaphore" do
    {status, pid} = Cta.Semaphore.start_link("test")
    assert status == :ok
  end

  test "Getting statistics" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    assert GenServer.call(pid, :get_statistics) == 0
  end

  test "Getting name" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    assert GenServer.call(pid, :get_name) == "test"
  end

  test "Setting and Getting seconds attribute" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 10})
    assert GenServer.call(pid, :get_seconds) == 10
  end

  test "Setting and Getting state attribute" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_state, :HorizontalGREEN})
    assert GenServer.call(pid, :get_state) == :HorizontalGREEN
  end

  test "Testing rule RuleHorizontalGreen" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 2})
    assert GenServer.call(pid, :get_state) == :HorizontalGREEN

  end

  test "Testing rule RuleHorizontalYellow" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 40})
    GenServer.cast(pid, {:set_state, :HorizontalGREEN})
    assert GenServer.call(pid, :get_state) == :HorizontalYELLOW

  end

  test "Testing rule RuleHorizontalRed" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 45})
    GenServer.cast(pid, {:set_state, :HorizontalYELLOW})
    assert GenServer.call(pid, :get_state) == :HorizontalRed

  end

  test "Testing rule RuleVerticallGreen" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 47})
    GenServer.cast(pid, {:set_state, :HorizontalRed})
    assert GenServer.call(pid, :get_state) == :VerticalGREEN

  end

  test "Testing rule RuleVerticalYellow" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 85})
    GenServer.cast(pid, {:set_state, :VerticalGREEN})
    assert GenServer.call(pid, :get_state) == :VerticalYELLOW

  end

  test "Testing rule RuleVerticalRed" do
    {_status, pid} = Cta.Semaphore.start_link("test")
    GenServer.cast(pid, {:set_seconds, 90})
    GenServer.cast(pid, {:set_state, :VerticalYELLOW})
    assert GenServer.call(pid, :get_state) == :VerticalRED

  end

  test "90 seconds test" do
    {_status, pid} = Cta.Semaphore.start_link("test90")

    Cta.Semaphore.simulate(pid, 90)

    assert GenServer.call(pid, :get_state) == :VerticalRED

  end

  test "time count test" do

    {_status, pid} = Cta.Semaphore.start_link("test90")

    {time_in_microseconds, _ret_val} =  :timer.tc(
      fn ->
        Cta.Semaphore.simulate(pid, 90)
        Cta.wait_up_to_end_all_process([pid])
      end)

    IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
    IO.puts("The notification count is #{Cta.get_statistics_from_total([pid])}")

    assert true

  end

  test "Create list" do

    list = Cta.create_list(10, "test")

    assert length(list) == 10
  end

  test "Simulation list" do

    list = Cta.create_list(10, "test")

    Cta.simulate(list, 90)

    assert true
  end

end

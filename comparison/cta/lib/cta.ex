defmodule Cta do
  @moduledoc """
  Documentation for Cta.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Cta.hello()
      :world

  """
  def hello do
    :world
  end


  defp check_message_queue([]) do
    0 #List empty - all elements are queue empty
  end

  defp check_message_queue([pid | elements]) do

    info = Process.info(pid)

    count =
      if info[:message_queue_len] == 0 and info[:status] == :waiting do
        0
      else
        1
      end

    count + check_message_queue(elements)
  end

  defp check_message_queue_loop(elements) do

    if check_message_queue(elements) > 0 do

      :timer.sleep(10)

      check_message_queue_loop(elements)

    end

  end

  def wait_up_to_end_all_process(elements) do

    check_message_queue_loop(elements)

  end

  defp get_statistics([], total) do
    total
  end

  defp get_statistics([pid| elements], total) do
    subtotal = GenServer.call(pid, :get_statistics) + total
    get_statistics(elements, subtotal)
  end

  def get_statistics_from_total(elements) do
    get_statistics(elements, 0)
  end

  defp create_list_internal(0, _prefix, list) do
    list
  end

  defp create_list_internal(count, prefix, list) do

    {_status, pid} = Cta.Semaphore.start_link(prefix <> to_string(count))

    create_list_internal(count-1, prefix, list ++ [pid])

  end

  def create_list(count, prefix) do

    list = []

    create_list_internal(count, prefix, list)

  end

  def simulate([],_seconds) do
    #end of loop
  end

  def simulate([pid | semaphores], seconds) do

    Cta.Semaphore.simulate(pid, seconds)

    simulate(semaphores, seconds)

  end

  def simulate_sequential(pid, times, seconds) do
    Enum.map(1..times,
      fn(_x) ->
        Cta.Semaphore.simulate(pid, seconds)
       end
    )
  end
end

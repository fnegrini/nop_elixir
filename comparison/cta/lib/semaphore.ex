defmodule Cta.Semaphore do
  use GenServer
  require Logger

  def init(state) do
    {:ok, state}
  end

  def start_link(name) do

  #States:
  # 0 => HorizontalGREEN
  # 1 => HorizontalYELLOW
  # 2 => HorizontalRED
  # 3 => VerticalGREEN
  # 4 => VerticalYELLOW
  # 5 => VerticalRED

    state = %{
      :atSeconds => 0,
      :atSemaphoreState => :VerticalRED,
    }

    state = reset_statistics_to_state(state)

    state = set_name(state, name)

    GenServer.start_link(__MODULE__, state)
  end

  defp set_name(state, name) do
    Map.put(state, :name, name)
  end

  defp get_name(state) do
    Map.get(state, :name)
  end

  defp get_statistics_from_state(state) do
    Map.get(state, :statistics)
  end

  defp add_statistics_to_state(state, count) do
    old = get_statistics_from_state(state)
    Map.put(state, :statistics, old + count)
  end

  defp reset_statistics_to_state(state) do
    Map.put(state, :statistics, 0)
  end

  def handle_call(:get_statistics, _from, state) do
    {:reply, get_statistics_from_state(state), state}
  end

  def handle_call(:get_name, _from, state) do
    {:reply, get_name(state), state}
  end

  def handle_call(:get_seconds, _from, state) do
    {:reply, Map.get(state, :atSeconds), state}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, Map.get(state, :atSemaphoreState), state}
  end

  def handle_cast(:reset_statistics, state) do
    {:noreply, reset_statistics_to_state(state)}
  end

  def handle_cast({:set_seconds, value}, state) do
    state = add_statistics_to_state(state, 1)
    state = Map.put(state, :atSeconds, value)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  def handle_cast({:set_state, value}, state) do
    state = add_statistics_to_state(state, 1)
    state = Map.put(state, :atSemaphoreState, value)
    state = control_semaphore_state(state)
    {:noreply, state}
  end

  defp control_semaphore_state(state) do

    semState = Map.get(state, :atSemaphoreState)
    semSeconds = Map.get(state, :atSeconds)

    new_semState = check_state_by_second(semSeconds, semState)

    state =
      if (semState != new_semState) do
        name = get_name(state)
        Logger.debug("Semaphore #{name} changed state from #{to_string(semState)} to #{to_string(new_semState)} at seconds #{semSeconds}")
        Map.put(state, :atSemaphoreState, new_semState)
      else
        state
      end

    state
  end

  #RuleHorizontalGreen
  defp check_state_by_second(2, :VerticalRED) do
    :HorizontalGREEN
  end

  #RuleHorizontalYellow
  defp check_state_by_second(40, :HorizontalGREEN) do
    :HorizontalYELLOW
  end

  #RuleHorizontalRed
  defp check_state_by_second(45, :HorizontalYELLOW) do
    :HorizontalRed
  end

  #RuleVerticallGreen
  defp check_state_by_second(47, :HorizontalRed) do
    :VerticalGREEN
  end

  #RuleVerticalYellow
  defp check_state_by_second(85, :VerticalGREEN) do
    :VerticalYELLOW
  end

  #RuleVerticalYellow
  defp check_state_by_second(90, :VerticalYELLOW) do
    :VerticalRED
  end

  defp check_state_by_second(_value, semState) do
    semState
  end

  def simulate(pid, seconds) do

    Enum.map(1..seconds,
      fn(x) ->
        GenServer.cast(pid, {:set_seconds, rem(x-1,90) + 1})
       end)

  end

end

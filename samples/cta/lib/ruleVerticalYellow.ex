defmodule CTA.RuleVerticalYellow do
  use NOP.Element.Rule


  defp create_element_list([semaphore]) do
    #prSeconds semaphore_NOP.atSeconds == 85  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore, :atSeconds, :EQ, 85)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 3 {VerticalGREEN}
    prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore, :atSemaphoreState, :EQ, :VerticalGREEN)

    [prSeconds, prSemaphoreState]
    #[prSeconds]
  end

  defp create_instigation_list([semaphore]) do
    [{CTA.Semaphore_NOP, :mtVerticalTrafficLightYELLOW, [semaphore]}]
  end

end

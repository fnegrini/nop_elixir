defmodule CTA.RuleHorizontalYellow do
  use NOP.Element.Rule


  defp create_element_list([semaphore]) do
    #prSeconds semaphore_NOP.atSeconds == 40  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore, :atSeconds, :EQ, 40)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 0 {HorizontalGREEN}
    prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore, :atSemaphoreState, :EQ, :HorizontalGREEN)

    [prSeconds, prSemaphoreState]
    #[prSeconds]
  end

  defp create_instigation_list([semaphore]) do
    [{CTA.Semaphore_NOP, :mtHorizontalTrafficLightYELLOW, [semaphore]}]
  end

end

defmodule CTA.RuleHorizontalGreen do
  use NOP.Element.Rule


  defp create_element_list([semaphore]) do
    #prSeconds semaphore_NOP.atSeconds == 2  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore, :atSeconds, :EQ, 2)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 5 {VerticalRED}
    prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore, :atSemaphoreState, :EQ, :VerticalRED)

    [prSeconds, prSemaphoreState]
    #[prSeconds]
  end

  defp create_instigation_list([semaphore]) do
    [{CTA.Semaphore_NOP, :mtHorizontalTrafficLightGREEN, [semaphore]}]
  end

end

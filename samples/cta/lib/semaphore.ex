defmodule CTA.Semaphore_NOP do
  use NOP.Element.FBE

  #States:
  # 0 => HorizontalGREEN
  # 1 => HorizontalYELLOW
  # 2 => HorizontalRED
  # 3 => VerticalGREEN
  # 4 => VerticalYELLOW
  # 5 => VerticalRED

  defp int_attributes() do

    %{:atSeconds => 0, :atSemaphoreState => :VerticalRED}

  end

  defp change_state_internal(semaphore, new_state) do

    Logger.info("Semaphore changed state to #{new_state}")

    NOP.Service.FBE.set_attribute(semaphore, :atSemaphoreState, new_state)

  end

  def mtHorizontalTrafficLightGREEN(semaphore) do
    change_state_internal(semaphore, :HorizontalGREEN)
  end

  def mtHorizontalTrafficLightYELLOW(semaphore) do
    change_state_internal(semaphore, :HorizontalYELLOW)
  end

  def mtHorizontalTrafficLightRED(semaphore) do
    change_state_internal(semaphore, :HorizontalRED)
  end

  def mtVerticalTrafficLightGREEN(semaphore) do
    change_state_internal(semaphore, :VerticalGREEN)
  end

  def mtVerticalTrafficLightYELLOW(semaphore) do
    change_state_internal(semaphore, :VerticalYELLOW)
  end

  def mtVerticalTrafficLightRED(semaphore) do
    change_state_internal(semaphore, :VerticalRED)
  end

  def run_semaphore(semaphore, clock_count) do

    #Change semaphore seconds clock_count times a mod(90)
    Enum.map(1..clock_count,
      fn(x) ->
        NOP.Service.FBE.set_attribute(semaphore, :atSeconds, rem(x,90))
        #:timer.sleep(100)
       end)

  end

end

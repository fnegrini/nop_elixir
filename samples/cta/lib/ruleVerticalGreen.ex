defmodule CTA.RuleVerticallGreen do
  use NOP.Element.Rule


  defp create_element_list([semaphore]) do
    #prSeconds semaphore_NOP.atSeconds == 47  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore, :atSeconds, :EQ, 47)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 2 {HorizontalRED}
    prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore, :atSemaphoreState, :EQ, :HorizontalRED)

    [prSeconds, prSemaphoreState]
    #[prSeconds]
  end

  defp create_instigation_list([semaphore]) do
    [{CTA.Semaphore_NOP, :mtVerticalTrafficLightGREEN, [semaphore]}]
  end

end

defmodule CTA.Init do
  def init_semaphore(sufix) do

   semaphore = NOP.Service.FBE.create_fbe(CTA.Semaphore_NOP, "CTA.FBE.Semaphore_" <> sufix)

   NOP.Service.Rule.create_rule(CTA.RuleHorizontalGreen, "CTA.Rule.HorizontalGreen",  [semaphore])

   NOP.Service.Rule.create_rule(CTA.RuleHorizontalYellow, "CTA.Rule.HorizontalYellow",  [semaphore])

   NOP.Service.Rule.create_rule(CTA.RuleHorizontalRed, "CTA.Rule.HorizontalRed",  [semaphore])

   NOP.Service.Rule.create_rule(CTA.RuleVerticallGreen, "CTA.Rule.VerticalGreen",  [semaphore])

   NOP.Service.Rule.create_rule(CTA.RuleVerticalYellow, "CTA.Rule.VerticalYellow",  [semaphore])

   NOP.Service.Rule.create_rule(CTA.RuleVerticalRed, "CTA.Rule.VerticalRed",  [semaphore])

   semaphore

  end

  defp run_semaphore_list([], _clock_count) do

  end

  defp run_semaphore_list([semaphore | semaphore_list], clock_count) do

    Task.start(CTA.Semaphore_NOP, :run_semaphore, [semaphore, clock_count])

    run_semaphore_list(semaphore_list, clock_count)

  end

  def run_10x10(clock_count) do

    semaphore_list = []

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_02_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_03_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_04_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_05_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_06_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_07_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_08_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_09_10")]

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_01")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_02")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_03")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_04")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_05")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_06")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_07")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_08")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_09")]
    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_10_10")]

    run_semaphore_list(semaphore_list, clock_count)

  end

  def run_01x01(clock_count) do

    semaphore_list = []

    semaphore_list = semaphore_list ++ [CTA.Init.init_semaphore("Semaphore_01_01")]

    run_semaphore_list(semaphore_list, clock_count)

  end

end

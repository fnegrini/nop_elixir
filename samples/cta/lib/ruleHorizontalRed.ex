defmodule CTA.RuleHorizontalRed do
  use NOP.Element.Rule


  defp create_element_list([semaphore]) do
    #prSeconds semaphore_NOP.atSeconds == 45  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore, :atSeconds, :EQ, 45)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 1 {HorizontalYELLOW}
    prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore, :atSemaphoreState, :EQ, :HorizontalYELLOW)

    [prSeconds, prSemaphoreState]
    #[prSeconds]
  end

  defp create_instigation_list([semaphore]) do
    [{CTA.Semaphore_NOP, :mtHorizontalTrafficLightRED, [semaphore]}]
  end

end

defmodule NopCtaTest do
  use ExUnit.Case
  doctest NopCta

  test "greets the world" do
    assert NopCta.hello() == :world
  end

  test "Create first FBE" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    assert NOP.Service.FBE.get_name(fbe) == "NOP.element.MyFBE"
  end

  test "Create first Semaphore" do
    semaphore = NOP.Service.FBE.create_fbe(CTA.Semaphore_NOP, "NOP.FBE.Semaphore")

    assert NOP.Service.FBE.get_name(semaphore) == "NOP.FBE.Semaphore"
  end

  test "Init two semaphores" do
    semaphore1 = CTA.Init.init_semaphore("test1")

    semaphore2 = CTA.Init.init_semaphore("test2")

    NOP.Service.FBE.set_attribute(semaphore1, :atSeconds, 2)

    NOP.Service.FBE.set_attribute(semaphore1, :atSeconds, 2)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore1, :atSemaphoreState) == :HorizontalGREEN
  end

  test "Init Semaphore and test Rule RuleHorizontalGreen" do
    semaphore = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(semaphore, :atSeconds, 2)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore, :atSemaphoreState) == :HorizontalGREEN
  end

  test "Init Semaphore and test Rule RuleHorizontalYellow" do
    semaphore = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(semaphore, :atSeconds, 40)

    NOP.Service.FBE.set_attribute(semaphore, :atSemaphoreState, :HorizontalGREEN)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore, :atSemaphoreState) == :HorizontalYELLOW
  end

  test "Init Semaphore and test Rule RuleHorizontalRed" do
    semaphore = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(semaphore, :atSeconds, 45)

    NOP.Service.FBE.set_attribute(semaphore, :atSemaphoreState, :HorizontalYELLOW)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore, :atSemaphoreState) == :HorizontalRED
  end

  test "Init Semaphore and test Rule RuleVerticalGreen" do
    semaphore = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(semaphore, :atSeconds, 47)

    NOP.Service.FBE.set_attribute(semaphore, :atSemaphoreState, :HorizontalRED)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore, :atSemaphoreState) == :VerticalGREEN
  end

  test "Init Semaphore and test Rule RuleVerticalYellow" do
    semaphore = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(semaphore, :atSeconds, 85)

    NOP.Service.FBE.set_attribute(semaphore, :atSemaphoreState, :VerticalGREEN)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore, :atSemaphoreState) == :VerticalYELLOW
  end

  test "Init Semaphore and test Rule RuleVerticalRed" do

    semaphore = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(semaphore, :atSeconds, 0)

    NOP.Service.FBE.set_attribute(semaphore, :atSemaphoreState, :VerticalYELLOW)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(semaphore, :atSemaphoreState) == :VerticalRED
  end

end

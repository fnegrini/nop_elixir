# NopCta

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `nop_cta` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:nop_cta, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/nop_cta](https://hexdocs.pm/nop_cta).


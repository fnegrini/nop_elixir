defmodule NopEx.RlHNbHSignal do
  use NOP.Element.Rule

#rule rlHNbHSignal
#	condition
#	  subcondition cdHNbHSignal
#		 premise prHNbHSignal hn1.atHNbStep2_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbHSignal hn1.mtHNbHSignal();
#	  instigation inHNbHSignalS32 hn1.mtHNbStep2_4_Reset();
#	  instigation inHNbHSignalS33 hn1.mtHNbStep3_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbHSignal = NOP.Service.Premise.create_premise("NopEx.prHNbHSignal",
      hn1, :atHNbStep2_4, :EQ, true)

    cdHNbHSignal = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbHSignal", [prHNbHSignal], "NopEx.prHNbHSignal")

    [cdHNbHSignal]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbHSignal hn1.mtHNbHSignal();
      {NopEx.FHiddenNeuronB, :mtHNbHSignal, [hn1]},
      #	  instigation inHNbHSignalS32 hn1.mtHNbStep2_4_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_4_Reset, [hn1]},
      #	  instigation inHNbHSignalS33 hn1.mtHNbStep3_1_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_1_Set, [hn1]},
    ]

  end


end

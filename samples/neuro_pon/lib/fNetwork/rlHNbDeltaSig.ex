defmodule NopEx.RlHNbDeltaSig do
  use NOP.Element.Rule

#rule rlHNbDeltaSig
#	condition
#	  subcondition cdHNbDeltaSig
#		 premise prHNbDeltaSig hn1.atHNbStep3_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbDeltaSig0 hn1.mtHNbDeltaSigI0();
#	  instigation inHNbDeltaSig1 hn1.mtHNbDeltaSigI1();
#	  instigation inHNbDeltaSigS31 hn1.mtHNbStep3_1_Reset();
#	  instigation inHNbDeltaSigS32 hn1.mtHNbStep3_2_Set();
#	end_action
#end_rule
  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDeltaSig = NOP.Service.Premise.create_premise("NopEx.prHNbDeltaSig",
      hn1, :atHNbStep3_1, :EQ, true)

    cdHNbDeltaSig = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDeltaSig", [prHNbDeltaSig], "NopEx.prHNbDeltaSig")

    [cdHNbDeltaSig]

  end

  defp create_instigation_list([net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDeltaSig0 hn1.mtHNbDeltaSigI0();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaSigI0, [hn1, net]},
      #	  instigation inHNbDeltaSig1 hn1.mtHNbDeltaSigI1();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaSigI1, [hn1, net]},
      #	  instigation inHNbDeltaSigS31 hn1.mtHNbStep3_1_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_1_Reset, [hn1]},
      #	  instigation inHNbDeltaSigS32 hn1.mtHNbStep3_2_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_2_Set, [hn1]},
    ]

  end


end

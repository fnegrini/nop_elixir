defmodule NopEx.RlHNaDeltaBiasQtz do
  use NOP.Element.Rule

#rule rlHNaDeltaBiasQtz
#	condition
#	  subcondition cdHNaDeltaBiasQtz
#		 premise prHNaDeltaBiasQtz hn0.atHNaStep3_6 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaDeltaBiasQtz hn0.mtHNaDeltaBiasQtz();
#	  instigation inHNaDeltaBiasQtzS36 hn0.mtHNaStep3_6_Reset();
#	  instigation inHNaDeltaBiasQtzS37 hn0.mtHNaStep3_7_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDeltaBiasQtz = NOP.Service.Premise.create_premise("NopEx.prHNaDeltaBiasQtz",
      hn0, :atHNaStep3_6, :EQ, true)

    cdHNaDeltaBiasQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaDeltaBiasQtz", [prHNaDeltaBiasQtz], "NopEx.prHNaDeltaBiasQtz")

    [cdHNaDeltaBiasQtz]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDeltaBiasQtz hn0.mtHNaDeltaBiasQtz();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaBiasQtz, [hn0]},
      #	  instigation inHNaDeltaBiasQtzS36 hn0.mtHNaStep3_6_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_6_Reset, [hn0]},
      #	  instigation inHNaDeltaBiasQtzS37 hn0.mtHNaStep3_7_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_7_Set, [hn0]},
    ]

  end


end

defmodule NopEx.RlHNbDeltaBias do
  use NOP.Element.Rule

#rule rlHNbDeltaBias
#	condition
#	  subcondition cdHNbDeltaBias
#		 premise prHNbDeltaBias0 hn1.atHNbStep3_5 == true
#	 end_subcondition
#	end_condition
#	action
#	  instigation inHNbDeltaBias hn1.mtHNbDeltaBias();
#	  instigation inHNbDeltaBiasS35 hn1.mtHNbStep3_5_Reset();
#	  instigation inHNbDeltaBiasS36 hn1.mtHNbStep3_6_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDeltaBias0 = NOP.Service.Premise.create_premise("NopEx.prHNbDeltaBias0",
      hn1, :atHNbStep3_5, :EQ, true)

    cdHNbDeltaBias = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDeltaBias", [prHNbDeltaBias0], "NopEx.prHNbDeltaBias0")

    [cdHNbDeltaBias]

  end

  defp create_instigation_list([net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDeltaBias hn1.mtHNbDeltaBias();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaBias, [hn1, net]},
      #	  instigation inHNbDeltaBiasS35 hn1.mtHNbStep3_5_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_5_Reset, [hn1]},
      #	  instigation inHNbDeltaBiasS36 hn1.mtHNbStep3_6_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_6_Set, [hn1]},
    ]

  end


end

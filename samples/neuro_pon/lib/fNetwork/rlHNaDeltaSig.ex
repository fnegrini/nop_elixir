defmodule NopEx.RlHNaDeltaSig do
  use NOP.Element.Rule

#rule rlHNaDeltaSig
#	condition
#	  subcondition cdHNaDeltaSig
#		 premise prHNaDeltaSig hn0.atHNaStep3_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaDeltaSig0 hn0.mtHNaDeltaSigI0();
#	  instigation inHNaDeltaSig1 hn0.mtHNaDeltaSigI1();
#	  instigation inHNaDeltaSigS31 hn0.mtHNaStep3_1_Reset();
#	  instigation inHNaDeltaSigS32 hn0.mtHNaStep3_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDeltaSig = NOP.Service.Premise.create_premise("NopEx.prHNaDeltaSig",
      hn0, :atHNaStep3_1, :EQ, true)

      cdHNaDeltaSig = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaDeltaSig", [prHNaDeltaSig], "NopEx.prHNaDeltaSig")

    [cdHNaDeltaSig]

  end

  defp create_instigation_list([net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDeltaSig0 hn0.mtHNaDeltaSigI0();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaSigI0, [hn0, net]},
      #	  instigation inHNaDeltaSig1 hn0.mtHNaDeltaSigI1();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaSigI1, [hn0, net]},
      #	  instigation inHNaDeltaSigS31 hn0.mtHNaStep3_1_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_1_Reset, [hn0]},
      #	  instigation inHNaDeltaSigS32 hn0.mtHNaStep3_2_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_2_Set, [hn0]},
    ]

  end


end

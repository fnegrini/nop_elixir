defmodule NopEx.RlCalcONProd do
  use NOP.Element.Rule

#rule rlCalcONProd
#	condition
#	  subcondition cdCalcONProd
#		 premise prCalcONProdH0 hn0.atHNaStep1_5_ONStep == true and
#		 premise prCalcONProdH1 hn1.atHNbStep1_5_ONStep == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcONProd0 on0.mtONProdH0();
#	  instigation inCalcONProd1 on0.mtONProdH1();
#	  instigation inCalcONProdH0S15 hn0.mtHNaStep1_5_Reset();
#	  instigation inCalcONProdH1S15 hn1.mtHNbStep1_5_Reset();
#	  instigation inCalcONProdS12 on0.mtONStep1_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, hn1, _on0]) do

    prCalcONProdH0 = NOP.Service.Premise.create_premise("NopEx.prCalcONProdH0",
      hn0, :atHNaStep1_5_ONStep, :EQ, true)

    prCalcONProdH1 = NOP.Service.Premise.create_premise("NopEx.prCalcONProdH1",
      hn1, :atHNbStep1_5_ONStep, :EQ, true)

      cdCalcONProd = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcONProd", [prCalcONProdH0, prCalcONProdH1], "NopEx.prCalcONProdH0 and  NopEx.prCalcONProdH1")

    [cdCalcONProd]

  end

  defp create_instigation_list([_net, hn0, hn1, on0]) do

    [
      #	  instigation inCalcONProd0 on0.mtONProdH0();
      {NopEx.FOutputNeuron, :mtONProdH0, [on0, hn0]},
      #	  instigation inCalcONProd1 on0.mtONProdH1();
      {NopEx.FOutputNeuron, :mtONProdH1, [on0, hn1]},
      #	  instigation inCalcONProdH0S15 hn0.mtHNaStep1_5_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_5_Reset, [hn0]},
      #	  instigation inCalcONProdH1S15 hn1.mtHNbStep1_5_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_5_Reset, [hn1]},
      #	  instigation inCalcONProdS12 on0.mtONStep1_2_Set();
      {NopEx.FOutputNeuron, :mtONStep1_2_Set, [on0]},
    ]

  end


end

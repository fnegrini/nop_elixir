defmodule NopEx.RlCalcHNbSum do
  use NOP.Element.Rule

#rule rlCalcHNbSum
#	condition
#	  subcondition cdCalcHNbSum
#		 premise prCalcHNbSum hn1.atHNbStep1_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcHNbSum hn1.mtHNbSumProd();
#	  instigation inCalcHNbSumS12 hn1.mtHNbStep1_2_Reset();
#	  instigation inCalcHNbSumS13 hn1.mtHNbStep1_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prCalcHNbSum = NOP.Service.Premise.create_premise("NopEx.prCalcHNbSum",
      hn1, :atHNbStep1_2, :EQ, true)

    cdCalcHNbSum = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNbSum", [prCalcHNbSum], "NopEx.prCalcHNbSum")

    [cdCalcHNbSum]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
        #	  instigation inCalcHNbSum hn1.mtHNbSumProd();
        {NopEx.FHiddenNeuronB, :mtHNbSumProd, [hn1]},
        #	  instigation inCalcHNbSumS12 hn1.mtHNbStep1_2_Reset();
        {NopEx.FHiddenNeuronB, :mtHNbStep1_2_Reset, [hn1]},
        #	  instigation inCalcHNbSumS13 hn1.mtHNbStep1_3_Set();
        {NopEx.FHiddenNeuronB, :mtHNbStep1_3_Set, [hn1]},
    ]

  end


end

defmodule NopEx.RlCalcONTanH do
  use NOP.Element.Rule

#rule rlCalcONTanH
#	condition
#	  subcondition cdCalcONTanH
#		 premise prCalcONTanHS5 on0.atONStep1_5 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcONTanH on0.mtONTanH();
#	  instigation inCaclONTanHS15 on0.mtONStep1_5_Reset();
#	  instigation inCalcONTanHS21 on0.mtONStep2_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prCalcONTanHS5 = NOP.Service.Premise.create_premise("NopEx.prCalcONTanHS5",
      on0, :atONStep1_5, :EQ, true)

    cdCalcONTanH = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcONTanH", [prCalcONTanHS5], "NopEx.prCalcONTanHS5")

    [cdCalcONTanH]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inCalcONTanH on0.mtONTanH();
      {NopEx.FOutputNeuron, :mtONTanH, [on0]},
      #	  instigation inCaclONTanHS15 on0.mtONStep1_5_Reset();
      {NopEx.FOutputNeuron, :mtONStep1_5_Reset, [on0]},
      #	  instigation inCalcONTanHS21 on0.mtONStep2_1_Set();
      {NopEx.FOutputNeuron, :mtONStep2_1_Set, [on0]},
    ]

  end


end

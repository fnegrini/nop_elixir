defmodule NopEx.RlONErrDif do
  use NOP.Element.Rule

#rule rlONErrDif
#	condition
#	  subcondition cdONErrDif
#		 premise prONErrDif on0.atONStep2_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONErrDif on0.mtONErrDif();
#	  instigation inONErrDifS22 on0.mtONStep2_2_Reset();
#	  instigation inONErrDifS23 on0.mtONStep2_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONErrDif = NOP.Service.Premise.create_premise("NopEx.prONErrDif",
      on0, :atONStep2_2, :EQ, true)

    cdONErrDif = NOP.Service.Condition.create_condition(
      "NopEx.cdONErrDif", [prONErrDif], "NopEx.prONErrDif")

    [cdONErrDif]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONErrDif on0.mtONErrDif();
      {NopEx.FOutputNeuron, :mtONErrDif, [on0]},
      #	  instigation inONErrDifS22 on0.mtONStep2_2_Reset();
      {NopEx.FOutputNeuron, :mtONStep2_2_Reset, [on0]},
      #	  instigation inONErrDifS23 on0.mtONStep2_3_Set();
      {NopEx.FOutputNeuron, :mtONStep2_3_Set, [on0]},
    ]

  end


end

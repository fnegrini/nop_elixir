defmodule NopEx.RlHNaUpdWeight do
  use NOP.Element.Rule

#rule rlHNaUpdWeight
#	condition
#	  subcondition cdHNaUpdWeight
#		 premise prHNaUpdWeight hn0.atHNaStep3_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaUpdWeightI0 hn0.mtHNaUpdWeightI0();
#	  instigation inHNaUpdWeightI1 hn0.mtHNaUpdWeightI1();
#	  instigation inHNaUpdWeightS32 hn0.mtHNaStep3_4_Reset();
#	  instigation inHNaUpdWeightS33 hn0.mtHNaStep3_5_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaUpdWeight = NOP.Service.Premise.create_premise("NopEx.prHNaUpdWeight",
      hn0, :atHNaStep3_4, :EQ, true)

      cdHNaUpdWeight = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaUpdWeight", [prHNaUpdWeight], "NopEx.prHNaUpdWeight")

    [cdHNaUpdWeight]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaUpdWeightI0 hn0.mtHNaUpdWeightI0();
      {NopEx.FHiddenNeuronA, :mtHNaUpdWeightI0, [hn0]},
      #	  instigation inHNaUpdWeightI1 hn0.mtHNaUpdWeightI1();
      {NopEx.FHiddenNeuronA, :mtHNaUpdWeightI1, [hn0]},
      #	  instigation inHNaUpdWeightS32 hn0.mtHNaStep3_4_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_4_Reset, [hn0]},
      #	  instigation inHNaUpdWeightS33 hn0.mtHNaStep3_5_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_5_Set, [hn0]},
    ]

  end


end

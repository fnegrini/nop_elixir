defmodule NopEx.RlHNbUpdWeight do
  use NOP.Element.Rule

#rule rlHNbUpdWeight
#	condition
#	  subcondition cdHNbUpdWeight
#		 premise prHNbUpdWeight hn1.atHNbStep3_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbUpdWeightI0 hn1.mtHNbUpdWeightI0();
#	  instigation inHNbUpdWeightI1 hn1.mtHNbUpdWeightI1();
#	  instigation inHNbUpdWeightS32 hn1.mtHNbStep3_4_Reset();
#	  instigation inHNbUpdWeightS33 hn1.mtHNbStep3_5_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbUpdWeight = NOP.Service.Premise.create_premise("NopEx.prHNbUpdWeight",
      hn1, :atHNbStep3_4, :EQ, true)

    cdHNbUpdWeight = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbUpdWeight", [prHNbUpdWeight], "NopEx.prHNbUpdWeight")

    [cdHNbUpdWeight]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbUpdWeightI0 hn1.mtHNbUpdWeightI0();
      {NopEx.FHiddenNeuronB, :mtHNbUpdWeightI0, [hn1]},
      #	  instigation inHNbUpdWeightI1 hn1.mtHNbUpdWeightI1();
      {NopEx.FHiddenNeuronB, :mtHNbUpdWeightI1, [hn1]},
      #	  instigation inHNbUpdWeightS32 hn1.mtHNbStep3_4_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_4_Reset, [hn1]},
      #	  instigation inHNbUpdWeightS33 hn1.mtHNbStep3_5_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_5_Set, [hn1]},

    ]

  end


end

defmodule NopEx.RlONDerSquare do
  use NOP.Element.Rule

#rule rlONDerSquare
#	condition
#	  subcondition cdONDerSquare
#		 premise prONDerSquare on0.atONStep3_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONDerSquare on0.mtONDerSquare();
#	  instigation inONDerSquareS31 on0.mtONStep3_1_Reset();
#	  instigation inONDerSquareS32 on0.mtONStep3_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDerSquare = NOP.Service.Premise.create_premise("NopEx.prONDerSquare",
      on0, :atONStep3_1, :EQ, true)

    cdONDerSquare = NOP.Service.Condition.create_condition(
      "NopEx.cdONDerSquare", [prONDerSquare], "NopEx.prONDerSquare")

    [cdONDerSquare]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONDerSquare on0.mtONDerSquare();
      {NopEx.FOutputNeuron, :mtONDerSquare, [on0]},
      #	  instigation inONDerSquareS31 on0.mtONStep3_1_Reset();
      {NopEx.FOutputNeuron, :mtONStep3_1_Reset, [on0]},
      #	  instigation inONDerSquareS32 on0.mtONStep3_2_Set();
      {NopEx.FOutputNeuron, :mtONStep3_2_Set, [on0]},
    ]

  end


end

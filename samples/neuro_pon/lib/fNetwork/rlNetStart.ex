defmodule NopEx.RNetStart do
  use NOP.Element.Rule
  #rule rlNetStart

  #	condition
  #	  subcondition cdNetStart
  #		 premise prNetStartE net.atNumEpochs > 0 and
  #		 premise prNetStartS net.atNetStep1_1 == true
  #	  end_subcondition
  #	end_condition
  #	action
  #	  instigation inNetStartS11 net.mtNetStep1_1_Reset();
  #	  instigation inNetStartS21 net.mtNetStep3_1_Set();
  #	end_action
  #end_rule

  defp create_element_list([net, _hn0, _hn1, _on0]) do

    prNetStartE = NOP.Service.Premise.create_premise("NopEx.prNetStartE", net, :atNumEpochs, :GT, 0)

    prNetStartS = NOP.Service.Premise.create_premise("NopEx.prNetStartS", net, :atNetStep1_1, :EQ, true)

    cdNetStart = NOP.Service.Condition.create_condition(
      "NopEx.cdNetStart", [prNetStartE, prNetStartS], "NopEx.prNetStartE and NopEx.prNetStartS")

    [cdNetStart]

  end

  defp create_instigation_list([net, _hn0, _hn1, _on0]) do


    [
      #	  instigation inNetStartS11 net.mtNetStep1_1_Reset();
      {NopEx.FNetwork, :mtNetStep1_1_Reset, [net]},
      #	  instigation inNetStartS21 net.mtNetStep3_1_Set();
      {NopEx.FNetwork, :mtNetStep3_1_Set, [net]},
    ]

  end

end

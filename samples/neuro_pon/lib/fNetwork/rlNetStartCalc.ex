defmodule NopEx.RlNetStartCalc do
  use NOP.Element.Rule

  #rule rlNetStartCalc
  #	condition
  #	  subcondition cdNetStartCalc
  #		 premise prNetStartCalcR net.atReadTrainInputs < net.atTotTrainInputs and
  #		 premise prNetStartCalcS21 net.atNetStep3_1 == true
  #	  end_subcondition
  #	end_condition
  #	action
  #		instigation inStartCalcS21 net.mtNetStep3_1_Reset();
  #		instigation inStartCalcH0S11 hn0.mtHNaStep1_1_Set();
  #		instigation inStartCalcH1S11 hn1.mtHNbStep1_1_Set();
  #	end_action
  #end_rule
  defp create_element_list([net, _hn0, _hn1, _on0]) do

    prNetStartCalcR = NOP.Service.Premise.create_premise("NopEx.prNetStartCalcR",
      net, :atReadTrainInputs, :LT, net, :atTotTrainInputs)

    prNetStartCalcS21 = NOP.Service.Premise.create_premise("NopEx.prNetStartCalcS21",
      net, :atNetStep3_1, :EQ, true)

    cdNetStartCalc = NOP.Service.Condition.create_condition(
      "NopEx.cdNetStart", [prNetStartCalcR, prNetStartCalcS21], "NopEx.prNetStartCalcR and NopEx.prNetStartCalcS21")

    [cdNetStartCalc]

  end

  defp create_instigation_list([net, hn0, hn1, _on0]) do

    [
      #		instigation inStartCalcS21 net.mtNetStep3_1_Reset();
      {NopEx.FNetwork, :mtNetStep3_1_Reset, [net]},
      #		instigation inStartCalcH0S11 hn0.mtHNaStep1_1_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_1_Set, [hn0]},
      #		instigation inStartCalcH1S11 hn1.mtHNbStep1_1_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_1_Set, [hn1]},
    ]

  end


end

defmodule NopEx.RlHNaDerDif do
  use NOP.Element.Rule

#rule rlHNaDerDif
#	condition
#	  subcondition cdHNaDerDif
#		 premise prHNaDerDif hn0.atHNaStep2_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaDerDif hn0.mtHNaDerDif();
#	  instigation inHNaDerDifS32 hn0.mtHNaStep2_2_Reset();
#	  instigation inHNaDerDifS33 hn0.mtHNaStep2_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDerDif = NOP.Service.Premise.create_premise("NopEx.prHNaDerDif",
      hn0, :atHNaStep2_2, :EQ, true)

    cdHNaDerDif = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaDerDif", [prHNaDerDif], "NopEx.prHNaDerDif")

    [cdHNaDerDif]

  end

  defp create_instigation_list([net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDerDif hn0.mtHNaDerDif();
      {NopEx.FHiddenNeuronA, :mtHNaDerDif, [hn0, net]},
      #	  instigation inHNaDerDifS32 hn0.mtHNaStep2_2_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_2_Reset, [hn0]},
      #	  instigation inHNaDerDifS33 hn0.mtHNaStep2_3_Set();,
      {NopEx.FHiddenNeuronA, :mtHNaStep2_3_Set, [hn0]},
    ]

  end


end

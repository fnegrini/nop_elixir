defmodule NopEx.RlNetProdSumON do
  use NOP.Element.Rule

  #rule rlNetProdSumON
  #	condition
  #	  subcondition cdNetProdSumON
  #		 premise prNetProdSumON net.atNetStep2_1 == true
  #	  end_subcondition
  #	end_condition
  #	action
  #	  instigation inNetProdSumON net.mtNetProdSumON();
  #	  instigation inNetProdSumONS21 net.mtNetStep2_1_Reset();
  #	  instigation inNetProdSumONS43 on0.mtONStep4_3_Set();
  #	end_action
  #end_rule

  defp create_element_list([net, _hn0, _hn1, _on0]) do

    prNetProdSumON = NOP.Service.Premise.create_premise("NopEx.prNetProdSumON",
      net, :atNetStep2_1, :EQ, true)

    cdNetProdSumON = NOP.Service.Condition.create_condition(
      "NopEx.cdNetProdSumON", [prNetProdSumON], "NopEx.prNetProdSumON")

    [cdNetProdSumON]

  end

  defp create_instigation_list([net, _hn0, _hn1, on0]) do

  [
	  #	  instigation inNetProdSumON net.mtNetProdSumON();
      {NopEx.FNetwork, :mtNetProdSumON, [net, on0]},
	  #	  instigation inNetProdSumONS21 net.mtNetStep2_1_Reset();
      {NopEx.FNetwork, :mtNetStep2_1_Reset, [net]},
	  #	  instigation inNetProdSumONS43 on0.mtONStep4_3_Set();
      {NopEx.FOutputNeuron, :mtONStep4_3_Set, [on0]},
    ]

  end


end

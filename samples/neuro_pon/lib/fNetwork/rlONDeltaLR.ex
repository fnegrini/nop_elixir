defmodule NopEx.RlONDeltaLR do
  use NOP.Element.Rule

#rule rlONDeltaLR
#	condition
#	  subcondition cdONDeltaLR
#		 premise prONDeltaLR on0.atONStep4_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONDeltaLR0 on0.mtONDeltaLRH0();
#	  instigation inONDeltaLR1 on0.mtONDeltaLRH1();
#	  instigation inONDeltaLRS44 on0.mtONStep4_4_Reset();
#	  instigation inONDeltaLRS45 on0.mtONStep4_5_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDeltaLR = NOP.Service.Premise.create_premise("NopEx.prONDeltaLR",
      on0, :atONStep4_4, :EQ, true)

    cdONDeltaLR = NOP.Service.Condition.create_condition(
      "NopEx.cdONDeltaLR", [prONDeltaLR], "NopEx.prONDeltaLR")

    [cdONDeltaLR]

  end

  defp create_instigation_list([net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONDeltaLR0 on0.mtONDeltaLRH0();
      {NopEx.FOutputNeuron, :mtONDeltaLRH0, [on0, net]},
      #	  instigation inONDeltaLR1 on0.mtONDeltaLRH1();
      {NopEx.FOutputNeuron, :mtONDeltaLRH1, [on0, net]},
      #	  instigation inONDeltaLRS44 on0.mtONStep4_4_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_4_Reset, [on0]},
      #	  instigation inONDeltaLRS45 on0.mtONStep4_5_Set();
      {NopEx.FOutputNeuron, :mtONStep4_5_Set, [on0]},
    ]

  end


end

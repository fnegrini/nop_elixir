defmodule NopEx.RlHNbDerDif do
  use NOP.Element.Rule

#rule rlHNbDerDif
#	condition
#	  subcondition cdHNbDerDif
#		 premise prHNbDerDif hn1.atHNbStep2_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbDerDif hn1.mtHNbDerDif();
#	  instigation inHNbDerDifS32 hn1.mtHNbStep2_2_Reset();
#	  instigation inHNbDerDifS33 hn1.mtHNbStep2_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDerDif = NOP.Service.Premise.create_premise("NopEx.prHNbDerDif",
      hn1, :atHNbStep2_2, :EQ, true)

    cdHNbDerDif = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDerDif", [prHNbDerDif], "NopEx.prHNbDerDif")

    [cdHNbDerDif]

  end

  defp create_instigation_list([net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDerDif hn1.mtHNbDerDif();
      {NopEx.FHiddenNeuronB, :mtHNbDerDif, [hn1, net]},
      #	  instigation inHNbDerDifS32 hn1.mtHNbStep2_2_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_2_Reset, [hn1]},
      #	  instigation inHNbDerDifS33 hn1.mtHNbStep2_3_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_3_Set, [hn1]},
    ]

  end


end

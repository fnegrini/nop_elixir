defmodule NopEx.RlNetRaiseEpoch do
  use NOP.Element.Rule

#rule rlNetRaiseEpoch
#	condition
#	  subcondition cdNetRaiseEpoch
#		 premise prNetRaiseEpoch net.atReadTrainInputs >= net.atTotTrainInputs
#	  end_subcondition
#	end_condition
#	action
#	  instigation inNetRaiseEpoch net.mtNetRaiseEpoch();
#	  instigation inNetResetRTI net.mtNetReadTI_Reset();
#	  instigation inNetSetWIH00 hn0.mtHNaSetWIH00();
#	  instigation inNetSetWIH01 hn0.mtHNaSetWIH01();
#	  instigation inNetSetBIHa hn0.mtHNaSetBIHa();
#	  instigation inNetSetWIH10 hn1.mtHNbSetWIH10();
#	  instigation inNetSetWIH11 hn1.mtHNbSetWIH11();
#	  instigation inNetSetBIHb hn1.mtHNbSetBIHb();
#	  instigation inNetSetWHO0 on0.mtONSetWHO0();
#	  instigation inNetSetWHO1 on0.mtONSetWHO1();
#	  instigation inNetSetBHO on0.mtONSetBHO();
#	  instigation inNetErrorMed net.mtMedNetError();
#	  instigation inNetStepS11 net.mtNetStep1_1_Set();
#	  instigation inNetStepS31 net.mtNetStep3_1_Reset();
#	end_action
#end_rule

  defp create_element_list([net, _hn0, _hn1, _on0]) do

    prNetRaiseEpoch = NOP.Service.Premise.create_premise("NopEx.prNetRaiseEpoch",
      net, :atReadTrainInputs, :GE, net, :atTotTrainInputs)

    cdNetRaiseEpoch = NOP.Service.Condition.create_condition(
      "NopEx.cdNetRaiseEpoch", [prNetRaiseEpoch], "NopEx.prNetRaiseEpoch")

    [cdNetRaiseEpoch]

  end

  defp create_instigation_list([net, hn0, hn1, on0]) do

    [

      #   instigation inNetRaiseEpoch net.mtNetRaiseEpoch();
      {NopEx.FNetwork, :mtNetRaiseEpoch, [net]},
      #   instigation inNetResetRTI net.mtNetReadTI_Reset();
      {NopEx.FNetwork, :mtNetReadTI_Reset, [net]},
      #	  instigation inNetSetWIH00 hn0.mtHNaSetWIH00();
      {NopEx.FHiddenNeuronA, :mtHNaSetWIH00, [hn0]},
      #	  instigation inNetSetWIH01 hn0.mtHNaSetWIH01();
      {NopEx.FHiddenNeuronA, :mtHNaSetWIH01, [hn0]},
      #	  instigation inNetSetBIHa hn0.mtHNaSetBIHa();
      {NopEx.FHiddenNeuronA, :mtHNaSetBIHa, [hn0]},
      #	  instigation inNetSetWIH10 hn1.mtHNbSetWIH10();
      {NopEx.FHiddenNeuronB, :mtHNbSetWIH10, [hn1]},
      #	  instigation inNetSetWIH11 hn1.mtHNbSetWIH11();
      {NopEx.FHiddenNeuronB, :mtHNbSetWIH11, [hn1]},
      #	  instigation inNetSetBIHb hn1.mtHNbSetBIHb();
      {NopEx.FHiddenNeuronB, :mtHNbSetBIHb, [hn1]},
      #	  instigation inNetSetWHO0 on0.mtONSetWHO0();
      {NopEx.FOutputNeuron, :mtONSetWHO0, [on0]},
      #	  instigation inNetSetWHO1 on0.mtONSetWHO1();
      {NopEx.FOutputNeuron, :mtONSetWHO1, [on0]},
      #	  instigation inNetSetBHO on0.mtONSetBHO();
      {NopEx.FOutputNeuron, :mtONSetBHO, [on0]},
      #	  instigation inNetErrorMed net.mtMedNetError();
      {NopEx.FNetwork, :mtMedNetError, [net]},
      #	  instigation inNetStepS11 net.mtNetStep1_1_Set();
      {NopEx.FNetwork, :mtNetStep1_1_Set, [net]},
      #	  instigation inNetStepS31 net.mtNetStep3_1_Reset();
      {NopEx.FNetwork, :mtNetStep3_1_Reset, [net]},
    ]

  end


end

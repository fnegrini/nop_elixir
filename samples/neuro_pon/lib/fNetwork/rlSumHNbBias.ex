defmodule NopEx.RlSumHNbBias do
  use NOP.Element.Rule

#rule rlSumHNbBias
#	condition
#	  subcondition cdSumHNbBias
#		 premise prSumHNbBias hn1.atHNbStep1_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inSumHNbBias hn1.mtHNbSumBias();
#	  instigation inSumHNbBiasS13 hn1.mtHNbStep1_3_Reset();
#	  instigation inSumHNbBiasS14 hn1.mtHNbStep1_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prSumHNbBias = NOP.Service.Premise.create_premise("NopEx.prSumHNbBias",
      hn1, :atHNbStep1_3, :EQ, true)

    cdSumHNbBias = NOP.Service.Condition.create_condition(
      "NopEx.cdSumHNbBias", [prSumHNbBias], "NopEx.prSumHNbBias")

    [cdSumHNbBias]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inSumHNbBias hn1.mtHNbSumBias();
      {NopEx.FHiddenNeuronB, :mtHNbSumBias, [hn1]},
      #	  instigation inSumHNbBiasS13 hn1.mtHNbStep1_3_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_3_Reset, [hn1]},
      #	  instigation inSumHNbBiasS14 hn1.mtHNbStep1_4_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_4_Set, [hn1]},
    ]

  end


end

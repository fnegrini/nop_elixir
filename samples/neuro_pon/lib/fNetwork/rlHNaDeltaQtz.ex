defmodule NopEx.RlHNaDeltaQtz do
  use NOP.Element.Rule

#rule rlHNaDeltaQtz
#	condition
#	  subcondition cdHNaDeltaQtz
#		 premise prHNaDeltaQtz hn0.atHNaStep3_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaDeltaQtzI0 hn0.mtHNaDeltaQtzI0();
#	  instigation inHNaDeltaQtzI1 hn0.mtHNaDeltaQtzI1();
#	  instigation inHNaDeltaQtzS32 hn0.mtHNaStep3_3_Reset();
#	  instigation inHNaDeltaQtzS33 hn0.mtHNaStep3_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDeltaQtz = NOP.Service.Premise.create_premise("NopEx.prHNaDeltaQtz",
      hn0, :atHNaStep3_3, :EQ, true)

    cdHNaDeltaQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaDeltaQtz", [prHNaDeltaQtz], "NopEx.prHNaDeltaQtz")

    [cdHNaDeltaQtz]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDeltaQtzI0 hn0.mtHNaDeltaQtzI0();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaQtzI0, [hn0]},
      #	  instigation inHNaDeltaQtzI1 hn0.mtHNaDeltaQtzI1();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaQtzI1, [hn0]},
      #	  instigation inHNaDeltaQtzS32 hn0.mtHNaStep3_3_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_3_Reset, [hn0]},
      #	  instigation inHNaDeltaQtzS33 hn0.mtHNaStep3_4_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_4_Set, [hn0]},
    ]

  end


end

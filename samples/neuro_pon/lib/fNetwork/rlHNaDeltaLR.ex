defmodule NopEx.RlHNaDeltaLR do
  use NOP.Element.Rule

#rule rlHNaDeltaLR
#	condition
#	  subcondition cdHNaDeltaLR
#		 premise prHNaDeltaLR hn0.atHNaStep3_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaDeltaLRI0 hn0.mtHNaDeltaLRI0();
#	  instigation inHNaDeltaLRI1 hn0.mtHNaDeltaLRI1();
#	  instigation inHNaDeltaLRS32 hn0.mtHNaStep3_2_Reset();
#	  instigation inHNaDeltaLRS33 hn0.mtHNaStep3_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDeltaLR = NOP.Service.Premise.create_premise("NopEx.prHNaDeltaLR",
      hn0, :atHNaStep3_2, :EQ, true)

    cdCalcHNaProd = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNaProd", [prHNaDeltaLR], "NopEx.prHNaDeltaLR")

    [cdCalcHNaProd]

  end

  defp create_instigation_list([net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDeltaLRI0 hn0.mtHNaDeltaLRI0();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaLRI0, [hn0, net]},
      #	  instigation inHNaDeltaLRI1 hn0.mtHNaDeltaLRI1();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaLRI1, [hn0, net]},
      #	  instigation inHNaDeltaLRS32 hn0.mtHNaStep3_2_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_2_Reset, [hn0]},
      #	  instigation inHNaDeltaLRS33 hn0.mtHNaStep3_3_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_3_Set, [hn0]},
    ]

  end


end

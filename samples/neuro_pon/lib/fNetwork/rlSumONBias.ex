defmodule NopEx.RlSumONBias do
  use NOP.Element.Rule

#rule rlSumONBias
#	condition
#	  subcondition cdSumONBias
#		 premise prSumONBiasS4 on0.atONStep1_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inSumONBias on0.mtONSumBias();
#	  instigation inSumONBiasS14 on0.mtONStep1_4_Reset();
#	  instigation inSumONBiasS15 on0.mtONStep1_5_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prSumONBiasS4 = NOP.Service.Premise.create_premise("NopEx.prSumONBiasS4",
      on0, :atONStep1_4, :EQ, true)

    cdSumONBias = NOP.Service.Condition.create_condition(
      "NopEx.cdSumONBias", [prSumONBiasS4], "NopEx.prSumONBiasS4")

    [cdSumONBias]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inSumONBias on0.mtONSumBias();
      {NopEx.FOutputNeuron, :mtONSumBias, [on0]},
      #	  instigation inSumONBiasS14 on0.mtONStep1_4_Reset();
      {NopEx.FOutputNeuron, :mtONStep1_4_Reset, [on0]},
      #	  instigation inSumONBiasS15 on0.mtONStep1_5_Set();
      {NopEx.FOutputNeuron, :mtONStep1_5_Set, [on0]},
    ]

  end


end

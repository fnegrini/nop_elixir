defmodule NopEx.RlCalcHNaProd do
  use NOP.Element.Rule

#rule rlCalcHNaProd
#	condition
#	  subcondition cdCalcHNaProd
#		 premise prCalcHNaProd hn0.atHNaStep1_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcHNaP0 hn0.mtHNaProdI0();
#	  instigation inCalcHNaP1 hn0.mtHNaProdI1();
#	  instigation inCalcHNaS11 hn0.mtHNaStep1_1_Reset();
#	  instigation inCalcHNaS12 hn0.mtHNaStep1_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prCalcHNaProd = NOP.Service.Premise.create_premise("NopEx.prCalcHNaProd",
      hn0, :atHNaStep1_1, :EQ, true)

    cdCalcHNaProd = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNaProd", [prCalcHNaProd], "NopEx.prCalcHNaProd")

    [cdCalcHNaProd]

  end

  defp create_instigation_list([net, hn0, _hn1, _on0]) do

    [
      #	  instigation inCalcHNaP0 hn0.mtHNaProdI0();
      {NopEx.FHiddenNeuronA, :mtHNaProdI0, [hn0, net]},
      #	  instigation inCalcHNaP1 hn0.mtHNaProdI1();
      {NopEx.FHiddenNeuronA, :mtHNaProdI1, [hn0, net]},
      #	  instigation inCalcHNaS11 hn0.mtHNaStep1_1_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_1_Reset, [hn0]},
      #	  instigation inCalcHNaS12 hn0.mtHNaStep1_2_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_2_Set, [hn0]},
    ]

  end


end

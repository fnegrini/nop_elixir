defmodule NopEx.RlHNbDerSquare do
  use NOP.Element.Rule

#rule rlHNbDerSquare
#	condition
#	  subcondition cdHNbDerSquare
#		 premise prHNbDerSquare hn1.atHNbStep2_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbDerSquare hn1.mtHNbDerSquare();
#	  instigation inHNbDerSquareS31 hn1.mtHNbStep2_1_Reset();
#	  instigation inHNbDerSquareS32 hn1.mtHNbStep2_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDerSquare = NOP.Service.Premise.create_premise("NopEx.prHNbDerSquare",
      hn1, :atHNbStep2_1, :EQ, true)

    cdHNbDerSquare = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDerSquare", [prHNbDerSquare], "NopEx.prHNbDerSquare")

    [cdHNbDerSquare]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDerSquare hn1.mtHNbDerSquare();
      {NopEx.FHiddenNeuronB, :mtHNbDerSquare, [hn1]},
      #	  instigation inHNbDerSquareS31 hn1.mtHNbStep2_1_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_1_Reset, [hn1]},
      #	  instigation inHNbDerSquareS32 hn1.mtHNbStep2_2_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_2_Set, [hn1]},
    ]

  end


end

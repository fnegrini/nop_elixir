defmodule NopEx.RlHNaDerSquare do
  use NOP.Element.Rule

#rule rlHNaDerSquare
#	condition
#	  subcondition cdHNaDerSquare
#		 premise prHNaDerSquare hn0.atHNaStep2_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaDerSquare hn0.mtHNaDerSquare();
#	  instigation inHNaDerSquareS31 hn0.mtHNaStep2_1_Reset();
#	  instigation inHNaDerSquareS32 hn0.mtHNaStep2_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDerSquare = NOP.Service.Premise.create_premise("NopEx.prHNaDerSquare",
      hn0, :atHNaStep2_1, :EQ, true)

    cdHNaDerSquare = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaDerSquare", [prHNaDerSquare], "NopEx.prHNaDerSquare")

    [cdHNaDerSquare]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDerSquare hn0.mtHNaDerSquare();
      {NopEx.FHiddenNeuronA, :mtHNaDerSquare, [hn0]},
      #	  instigation inHNaDerSquareS31 hn0.mtHNaStep2_1_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_1_Reset, [hn0]},
      #	  instigation inHNaDerSquareS32 hn0.mtHNaStep2_2_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_2_Set, [hn0]},
    ]

  end


end

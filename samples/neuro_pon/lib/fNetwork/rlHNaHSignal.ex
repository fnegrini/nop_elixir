defmodule NopEx.RlHNaHSignal do
  use NOP.Element.Rule

#rule rlHNaHSignal
#	condition
#	  subcondition cdHNaHSignal
#		 premise prHNaHSignal hn0.atHNaStep2_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaHSignal hn0.mtHNaHSignal();
#	  instigation inHNaHSignalS32 hn0.mtHNaStep2_4_Reset();
#	  instigation inHNaHSignalS33 hn0.mtHNaStep3_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaHSignal = NOP.Service.Premise.create_premise("NopEx.prHNaHSignal",
      hn0, :atHNaStep2_4, :EQ, true)

    cdHNaHSignal = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaHSignal", [prHNaHSignal], "NopEx.prHNaHSignal")

    [cdHNaHSignal]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaHSignal hn0.mtHNaHSignal();
      {NopEx.FHiddenNeuronA, :mtHNaHSignal, [hn0]},
      #	  instigation inHNaHSignalS32 hn0.mtHNaStep2_4_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_4_Reset, [hn0]},
      #	  instigation inHNaHSignalS33 hn0.mtHNaStep3_1_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_1_Set, [hn0]},
    ]

  end


end

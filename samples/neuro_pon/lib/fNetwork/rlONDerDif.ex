defmodule NopEx.RlONDerDif do
  use NOP.Element.Rule

#rule rlONDerDif
#	condition
#	  subcondition cdONDerDif
#		 premise prONDerDif on0.atONStep3_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONDerDif on0.mtONDerDif();
#	  instigation inONDerDifS32 on0.mtONStep3_2_Reset();
#	  instigation inONDerDifS33 on0.mtONStep3_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDerDif = NOP.Service.Premise.create_premise("NopEx.prONDerDif",
      on0, :atONStep3_2, :EQ, true)

    cdONDerDif = NOP.Service.Condition.create_condition(
      "NopEx.cdONDerDif", [prONDerDif], "NopEx.prONDerDif")

    [cdONDerDif]

  end

  defp create_instigation_list([net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONDerDif on0.mtONDerDif();
      {NopEx.FOutputNeuron, :mtONDerDif, [on0, net]},
      #	  instigation inONDerDifS32 on0.mtONStep3_2_Reset();
      {NopEx.FOutputNeuron, :mtONStep3_2_Reset, [on0]},
      #	  instigation inONDerDifS33 on0.mtONStep3_3_Set();
      {NopEx.FOutputNeuron, :mtONStep3_3_Set, [on0]},
    ]

  end


end

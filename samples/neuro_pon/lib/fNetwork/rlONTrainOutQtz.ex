defmodule NopEx.RlONTrainOutQtz do
  use NOP.Element.Rule

#rule rlONTrainOutQtz
#	condition
#	  subcondition cdONTrainOutQtz
#		 premise prONTrainOutQtz on0.atONStep2_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONTrainOutQtz on0.mtONTrainOutQtz();
#	  instigation inONTrainOutQtzS21 on0.mtONStep2_1_Reset();
#	  instigation inONTrainOutQtzS22 on0.mtONStep2_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONTrainOutQtz = NOP.Service.Premise.create_premise("NopEx.prONTrainOutQtz",
      on0, :atONStep2_1, :EQ, true)

    cdONTrainOutQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdONTrainOutQtz", [prONTrainOutQtz], "NopEx.prONTrainOutQtz")

    [cdONTrainOutQtz]

  end

  defp create_instigation_list([net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONTrainOutQtz on0.mtONTrainOutQtz();
      {NopEx.FOutputNeuron, :mtONTrainOutQtz, [on0, net]},
      #	  instigation inONTrainOutQtzS21 on0.mtONStep2_1_Reset();
      {NopEx.FOutputNeuron, :mtONStep2_1_Reset, [on0]},
      #	  instigation inONTrainOutQtzS22 on0.mtONStep2_2_Set();
      {NopEx.FOutputNeuron, :mtONStep2_2_Set, [on0]},
    ]

  end


end

defmodule NopEx.RlNetNextInput do
  use NOP.Element.Rule

#rule rlNetNextInput
#	condition
#	  subcondition cdNetNextInput
#		 premise prNetNextLayerH0 hn0.atHNaStep3_8_NetStep == true and
#		 premise prNetNextLayerH1 hn1.atHNbStep3_8_NetStep == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inNetNextLayer net.mtNetNextInput();
#	  instigation inNetErrorSum net.mtSumNetError();
#	  instigation inNetNextLayerH0S38 hn0.mtHNaStep3_8_Reset();
#	  instigation inNetNextLayerH1S38 hn1.mtHNbStep3_8_Reset();
#	  instigation inNetNextLayerS31 net.mtNetStep3_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, hn1, _on0]) do

    prNetNextLayerH0 = NOP.Service.Premise.create_premise("NopEx.prNetNextLayerH0",
      hn0, :atHNaStep3_8_NetStep, :EQ, true)

    prNetNextLayerH1 = NOP.Service.Premise.create_premise("NopEx.prNetNextLayerH1",
      hn1, :atHNbStep3_8_NetStep, :EQ, true)

    cdNetNextInput = NOP.Service.Condition.create_condition(
      "NopEx.cdNetNextInput", [prNetNextLayerH0, prNetNextLayerH1], "NopEx.prNetNextLayerH0 and NopEx.prNetNextLayerH1")

    [cdNetNextInput]

  end

  defp create_instigation_list([net, hn0, hn1, on0]) do

  [
      #	  instigation inNetNextLayer net.mtNetNextInput();
      {NopEx.FNetwork, :mtNetNextInput, [net]},
      #	  instigation inNetErrorSum net.mtSumNetError();
      {NopEx.FNetwork, :mtSumNetError, [net, on0]},
      #	  instigation inNetNextLayerH0S38 hn0.mtHNaStep3_8_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_8_Reset, [hn0]},
      #	  instigation inNetNextLayerH1S38 hn1.mtHNbStep3_8_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_8_Reset, [hn1]},
      #	  instigation inNetNextLayerS31 net.mtNetStep3_1_Set();
      {NopEx.FNetwork, :mtNetStep3_1_Set, [net]},
    ]

  end


end

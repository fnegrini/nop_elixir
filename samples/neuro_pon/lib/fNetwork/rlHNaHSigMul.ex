defmodule NopEx.RlHNaHSigMul do
  use NOP.Element.Rule

#rule rlHNaHSigMul
#	condition
#	  subcondition cdHNaHSigMul
#		 premise prHNaHSigMul hn0.atHNaStep2_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaHSigMul hn0.mtHNaHSigMul();
#	  instigation inHNaHSigMulS32 hn0.mtHNaStep2_3_Reset();
#	  instigation inHNaHSigMulS33 hn0.mtHNaStep2_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaHSigMul = NOP.Service.Premise.create_premise("NopEx.prHNaHSigMul",
      hn0, :atHNaStep2_3, :EQ, true)

      cdHNaHSigMul = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaHSigMul", [prHNaHSigMul], "NopEx.prHNaHSigMul")

    [cdHNaHSigMul]

  end

  defp create_instigation_list([net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaHSigMul hn0.mtHNaHSigMul();
      {NopEx.FHiddenNeuronA, :mtHNaHSigMul, [hn0, net]},
      #	  instigation inHNaHSigMulS32 hn0.mtHNaStep2_3_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_3_Reset, [hn0]},
      #	  instigation inHNaHSigMulS33 hn0.mtHNaStep2_4_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_4_Set, [hn0]},
    ]

  end


end

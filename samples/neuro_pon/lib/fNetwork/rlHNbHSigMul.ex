defmodule NopEx.RlHNbHSigMul do
  use NOP.Element.Rule

#rule rlHNbHSigMul
#	condition
#	  subcondition cdHNbHSigMul
#		 premise prHNbHSigMul hn1.atHNbStep2_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbHSigMul hn1.mtHNbHSigMul();
#	  instigation inHNbHSigMulS32 hn1.mtHNbStep2_3_Reset();
#	  instigation inHNbHSigMulS33 hn1.mtHNbStep2_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbHSigMul = NOP.Service.Premise.create_premise("NopEx.prHNbHSigMul",
      hn1, :atHNbStep2_3, :EQ, true)

    cdHNbHSigMul = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbHSigMul", [prHNbHSigMul], "NopEx.prHNbHSigMul")

    [cdHNbHSigMul]

  end

  defp create_instigation_list([net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbHSigMul hn1.mtHNbHSigMul();
      {NopEx.FHiddenNeuronB, :mtHNbHSigMul, [hn1, net]},
      #	  instigation inHNbHSigMulS32 hn1.mtHNbStep2_3_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_3_Reset, [hn1]},
      #	  instigation inHNbHSigMulS33 hn1.mtHNbStep2_4_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_4_Set, [hn1]},
    ]

  end


end

defmodule NopEx.RlHNbUpdBias do
  use NOP.Element.Rule

#rule rlHNbUpdBias
#	condition
#	  subcondition cdHNbUpdBias
#		 premise prHNbUpdBias hn1.atHNbStep3_7 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbUpdBias hn1.mtHNbUpdBias();
#	  instigation inHNbUpdBiasS37 hn1.mtHNbStep3_7_Reset();
#	  instigation inHNbUpdBiasS38 hn1.mtHNbStep3_8_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbUpdBias = NOP.Service.Premise.create_premise("NopEx.prHNbUpdBias",
      hn1, :atHNbStep3_7, :EQ, true)

    cdHNbUpdBias = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbUpdBias", [prHNbUpdBias], "NopEx.prHNbUpdBias")

    [cdHNbUpdBias]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbUpdBias hn1.mtHNbUpdBias();
      {NopEx.FHiddenNeuronB, :mtHNbUpdBias, [hn1]},
      #	  instigation inHNbUpdBiasS37 hn1.mtHNbStep3_7_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_7_Reset, [hn1]},
      #	  instigation inHNbUpdBiasS38 hn1.mtHNbStep3_8_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_8_Set, [hn1]},
    ]

  end


end

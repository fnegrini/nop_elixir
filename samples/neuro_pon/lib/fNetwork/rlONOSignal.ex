defmodule NopEx.RlONOSignal do
  use NOP.Element.Rule

#rule rlONOSignal
#	condition
#	  subcondition cdONOSignal
#		 premise prONOSignal on0.atONStep3_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONOSignal on0.mtONOSignal();
#	  instigation inONOSignalS35 on0.mtONStep3_3_Reset();
#	  instigation inONOSignalS41 on0.mtONStep4_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONOSignal = NOP.Service.Premise.create_premise("NopEx.prONOSignal",
      on0, :atONStep3_3, :EQ, true)

    cdONOSignal = NOP.Service.Condition.create_condition(
      "NopEx.cdONOSignal", [prONOSignal], "NopEx.prONOSignal")

    [cdONOSignal]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONOSignal on0.mtONOSignal();
      {NopEx.FOutputNeuron, :mtONOSignal, [on0]},
      #	  instigation inONOSignalS35 on0.mtONStep3_3_Reset();
      {NopEx.FOutputNeuron, :mtONStep3_3_Reset, [on0]},
      #	  instigation inONOSignalS41 on0.mtONStep4_1_Set();
      {NopEx.FOutputNeuron, :mtONStep4_1_Set, [on0]},
    ]

  end


end

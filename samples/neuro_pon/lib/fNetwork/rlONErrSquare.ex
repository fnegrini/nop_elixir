defmodule NopEx.RlONErrSquare do
  use NOP.Element.Rule

#rule rlONErrSquare
#	condition
#	  subcondition cdONErrSquare
#		 premise prONErrSquare on0.atONStep2_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONErrSquare on0.mtONErrSquare();
#	  instigation inONErrSquareS22 on0.mtONStep2_3_Reset();
#	  instigation inONErrSquareS31 on0.mtONStep3_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONErrSquare = NOP.Service.Premise.create_premise("NopEx.prONErrSquare",
      on0, :atONStep2_3, :EQ, true)

    cdONErrSquare = NOP.Service.Condition.create_condition(
      "NopEx.cdONErrSquare", [prONErrSquare], "NopEx.prONErrSquare")

    [cdONErrSquare]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONErrSquare on0.mtONErrSquare();
      {NopEx.FOutputNeuron, :mtONErrSquare, [on0]},
      #	  instigation inONErrSquareS22 on0.mtONStep2_3_Reset();
      {NopEx.FOutputNeuron, :mtONStep2_3_Reset, [on0]},
      #	  instigation inONErrSquareS31 on0.mtONStep3_1_Set();
      {NopEx.FOutputNeuron, :mtONStep3_1_Set, [on0]},
    ]

  end


end

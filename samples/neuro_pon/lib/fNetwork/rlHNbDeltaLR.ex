defmodule NopEx.RlHNbDeltaLR do
  use NOP.Element.Rule

#rule rlHNbDeltaLR
#	condition
#	  subcondition cdHNbDeltaLR
#		 premise prHNbDeltaLR hn1.atHNbStep3_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbDeltaLRI0 hn1.mtHNbDeltaLRI0();
#	  instigation inHNbDeltaLRI1 hn1.mtHNbDeltaLRI1();
#	  instigation inHNbDeltaLRS32 hn1.mtHNbStep3_2_Reset();
#	  instigation inHNbDeltaLRS33 hn1.mtHNbStep3_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDeltaLR = NOP.Service.Premise.create_premise("NopEx.prHNbDeltaLR",
      hn1, :atHNbStep3_2, :EQ, true)

    cdHNbDeltaLR = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDeltaLR", [prHNbDeltaLR], "NopEx.prHNbDeltaLR")

    [cdHNbDeltaLR]

  end

  defp create_instigation_list([net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDeltaLRI0 hn1.mtHNbDeltaLRI0();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaLRI0, [hn1, net]},
      #	  instigation inHNbDeltaLRI1 hn1.mtHNbDeltaLRI1();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaLRI1, [hn1, net]},
      #	  instigation inHNbDeltaLRS32 hn1.mtHNbStep3_2_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_2_Reset, [hn1]},
      #	  instigation inHNbDeltaLRS33 hn1.mtHNbStep3_3_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_3_Set, [hn1]},
    ]

  end


end

defmodule NopEx.RlCalcHNbProd do
  use NOP.Element.Rule

#rule rlCalcHNbProd
#	condition
#	  subcondition cdCalcHNbProd
#		 premise prCalcHNbProd hn0.atHNbStep1_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcHNbP0 hn0.mtHNbProdI0();
#	  instigation inCalcHNbP1 hn0.mtHNbProdI1();
#	  instigation inCalcHNbS11 hn0.mtHNbStep1_1_Reset();
#	  instigation inCalcHNbS12 hn0.mtHNbStep1_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prCalcHNbProd = NOP.Service.Premise.create_premise("NopEx.prCalcHNbProd",
      hn1, :atHNbStep1_1, :EQ, true)

    cdCalcHNbProd = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNbProd", [prCalcHNbProd], "NopEx.prCalcHNbProd")

    [cdCalcHNbProd]

  end

  defp create_instigation_list([net, _hn0, hn1, _on0]) do

    [
      #	  instigation inCalcHNbP0 hn0.mtHNbProdI0();
      {NopEx.FHiddenNeuronB, :mtHNbProdI0, [hn1, net]},
      #	  instigation inCalcHNbP1 hn0.mtHNbProdI1();
      {NopEx.FHiddenNeuronB, :mtHNbProdI1, [hn1, net]},
      #	  instigation inCalcHNbS11 hn0.mtHNbStep1_1_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_1_Reset, [hn1]},
      #	  instigation inCalcHNbS12 hn0.mtHNbStep1_2_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_2_Set, [hn1]},
    ]

  end

end

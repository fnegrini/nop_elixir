defmodule NopEx.RlONDeltaBias do
  use NOP.Element.Rule

#rule rlONDeltaBias
#	condition
#	  subcondition cdONDeltaBias
#		 premise prONDeltaBias0 on0.atONStep4_7 == true
#	 end_subcondition
#	end_condition
#	action
#	  instigation inONDeltaBias on0.mtONDeltaBias();
#	  instigation inONDeltaBiasS47 on0.mtONStep4_7_Reset();
#	  instigation inONDeltaBiasS48 on0.mtONStep4_8_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDeltaBias0 = NOP.Service.Premise.create_premise("NopEx.prONDeltaBias0",
      on0, :atONStep4_7, :EQ, true)

    cdONDeltaBias = NOP.Service.Condition.create_condition(
      "NopEx.cdONDeltaBias", [prONDeltaBias0], "NopEx.prONDeltaBias0")

    [cdONDeltaBias]

  end

  defp create_instigation_list([net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONDeltaBias on0.mtONDeltaBias();
      {NopEx.FOutputNeuron, :mtONDeltaBias, [on0, net]},
      #	  instigation inONDeltaBiasS47 on0.mtONStep4_7_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_7_Reset, [on0]},
      #	  instigation inONDeltaBiasS48 on0.mtONStep4_8_Set();
      {NopEx.FOutputNeuron, :mtONStep4_8_Set, [on0]},
    ]

  end


end

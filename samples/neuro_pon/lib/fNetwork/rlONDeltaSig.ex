defmodule NopEx.RlONDeltaSig do
  use NOP.Element.Rule

#rule rlONDeltaSig
#	condition
#	  subcondition cdONDeltaSig
#		 premise prONDeltaSig on0.atONStep4_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONDeltaSigH0 on0.mtONDeltaSigH0();
#	  instigation inONDeltaSigH1 on0.mtONDeltaSigH1();
#	  instigation inONDeltaSigS43 on0.mtONStep4_3_Reset();
#	  instigation inONDeltaSigS44 on0.mtONStep4_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDeltaSig = NOP.Service.Premise.create_premise("NopEx.prONDeltaSig",
      on0, :atONStep4_3, :EQ, true)

    cdONDeltaSig = NOP.Service.Condition.create_condition(
      "NopEx.cdONDeltaSig", [prONDeltaSig], "NopEx.prONDeltaSig")

    [cdONDeltaSig]

  end

  defp create_instigation_list([_net, hn0, hn1, on0]) do

    [
      #	  instigation inONDeltaSigH0 on0.mtONDeltaSigH0();
      {NopEx.FOutputNeuron, :mtONDeltaSigH0, [on0, hn0]},
      #	  instigation inONDeltaSigH1 on0.mtONDeltaSigH1();
      {NopEx.FOutputNeuron, :mtONDeltaSigH1, [on0, hn1]},
      #	  instigation inONDeltaSigS43 on0.mtONStep4_3_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_3_Reset, [on0]},
      #	  instigation inONDeltaSigS44 on0.mtONStep4_4_Set();
      {NopEx.FOutputNeuron, :mtONStep4_4_Set, [on0]},
    ]

  end


end

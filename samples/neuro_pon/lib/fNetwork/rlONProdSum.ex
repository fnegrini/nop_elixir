defmodule NopEx.RlONProdSum do
  use NOP.Element.Rule

#rule rlONProdSum
#	condition
#	  subcondition cdONProdSum
#		 premise prONProdSum on0.atONStep4_1 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONSumProd0 on0.mtONProdSigH0();
#	  instigation inONSumProd1 on0.mtONProdSigH1();
#	  instigation inONSumProdS41 on0.mtONStep4_1_Reset();
#	  instigation inONSumProdS42 on0.mtONStep4_2_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONProdSum = NOP.Service.Premise.create_premise("NopEx.prONProdSum",
      on0, :atONStep4_1, :EQ, true)

    cdONProdSum = NOP.Service.Condition.create_condition(
      "NopEx.cdONProdSum", [prONProdSum], "NopEx.prONProdSum")

    [cdONProdSum]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONSumProd0 on0.mtONProdSigH0();
      {NopEx.FOutputNeuron, :mtONProdSigH0, [on0]},
      #	  instigation inONSumProd1 on0.mtONProdSigH1();
      {NopEx.FOutputNeuron, :mtONProdSigH1, [on0]},
      #	  instigation inONSumProdS41 on0.mtONStep4_1_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_1_Reset, [on0]},
      #	  instigation inONSumProdS42 on0.mtONStep4_2_Set();
      {NopEx.FOutputNeuron, :mtONStep4_2_Set, [on0]},
    ]

  end


end

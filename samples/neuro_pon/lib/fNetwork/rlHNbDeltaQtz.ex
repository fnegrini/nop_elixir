defmodule NopEx.RlHNbDeltaQtz do
  use NOP.Element.Rule

#rule rlHNbDeltaQtz
#	condition
#	  subcondition cdHNbDeltaQtz
#		 premise prHNbDeltaQtz hn1.atHNbStep3_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbDeltaQtzI0 hn1.mtHNbDeltaQtzI0();
#	  instigation inHNbDeltaQtzI1 hn1.mtHNbDeltaQtzI1();
#	  instigation inHNbDeltaQtzS32 hn1.mtHNbStep3_3_Reset();
#	  instigation inHNbDeltaQtzS33 hn1.mtHNbStep3_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDeltaQtz = NOP.Service.Premise.create_premise("NopEx.prHNbDeltaQtz",
      hn1, :atHNbStep3_3, :EQ, true)

    cdHNbDeltaQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDeltaQtz", [prHNbDeltaQtz], "NopEx.prHNbDeltaQtz")

    [cdHNbDeltaQtz]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDeltaQtzI0 hn1.mtHNbDeltaQtzI0();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaQtzI0, [hn1]},
      #	  instigation inHNbDeltaQtzI1 hn1.mtHNbDeltaQtzI1();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaQtzI1, [hn1]},
      #	  instigation inHNbDeltaQtzS32 hn1.mtHNbStep3_3_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_3_Reset, [hn1]},
      #	  instigation inHNbDeltaQtzS33 hn1.mtHNbStep3_4_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_4_Set, [hn1]},
    ]

  end


end

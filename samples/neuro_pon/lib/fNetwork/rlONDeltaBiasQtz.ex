defmodule NopEx.RlONDeltaBiasQtz do
  use NOP.Element.Rule

#rule rlONDeltaBiasQtz
#	condition
#	  subcondition cdONDeltaBiasQtz
#		 premise prONDeltaBiasQtz on0.atONStep4_8 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONDeltaBiasQtz on0.mtONDeltaBiasQtz();
#	  instigation inONDeltaBiasQtzS48 on0.mtONStep4_8_Reset();
#	  instigation inONDeltaBiasQtzS49 on0.mtONStep4_9_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDeltaBiasQtz = NOP.Service.Premise.create_premise("NopEx.prONDeltaBiasQtz",
      on0, :atONStep4_8, :EQ, true)

    cdONDeltaBiasQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdONDeltaBiasQtz", [prONDeltaBiasQtz], "NopEx.prONDeltaBiasQtz")

    [cdONDeltaBiasQtz]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONDeltaBiasQtz on0.mtONDeltaBiasQtz();
      {NopEx.FOutputNeuron, :mtONDeltaBiasQtz, [on0]},
      #	  instigation inONDeltaBiasQtzS48 on0.mtONStep4_8_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_8_Reset, [on0]},
      #	  instigation inONDeltaBiasQtzS49 on0.mtONStep4_9_Set();
      {NopEx.FOutputNeuron, :mtONStep4_9_Set, [on0]},
    ]

  end


end

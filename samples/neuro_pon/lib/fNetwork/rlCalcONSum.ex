defmodule NopEx.RlCalcONSum do
  use NOP.Element.Rule

#rule rlCalcONSum
#	condition
#	  subcondition cdCalcONSum
#		 premise prCalcONSum on0.atONStep1_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcONSum on0.mtONSumProd();
#	  instigation inCalcONSumS1 on0.mtONStep1_2_Reset();
#	  instigation inCalcONSumS3 on0.mtONStep1_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prCalcONSum = NOP.Service.Premise.create_premise("NopEx.prCalcONSum",
      on0, :atONStep1_2, :EQ, true)

    cdCalcONSum = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcONSum", [prCalcONSum], "NopEx.prCalcONSum")

    [cdCalcONSum]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inCalcONSum on0.mtONSumProd();
      {NopEx.FOutputNeuron, :mtONSumProd, [on0]},
      #	  instigation inCalcONSumS1 on0.mtONStep1_2_Reset();
      {NopEx.FOutputNeuron, :mtONStep1_2_Reset, [on0]},
      #	  instigation inCalcONSumS3 on0.mtONStep1_4_Set();
      {NopEx.FOutputNeuron, :mtONStep1_4_Set, [on0]},
    ]

  end


end

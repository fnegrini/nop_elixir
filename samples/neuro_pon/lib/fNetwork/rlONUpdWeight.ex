defmodule NopEx.RlONUpdWeight do
  use NOP.Element.Rule

#rule rlONUpdWeight
#	condition
#	  subcondition cdONUpdWeight
#		 premise prONUpdWeight on0.atONStep4_6 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONUpdWeight0 on0.mtONUpdWeightH0();
#	  instigation inONUpdWeight1 on0.mtONUpdWeightH1();
#	  instigation inONUpdWeight0S46 on0.mtONStep4_6_Reset();
#	  instigation inONUpdWeight0S47 on0.mtONStep4_7_Set();
#	end_action
#end_rule
  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONUpdWeight = NOP.Service.Premise.create_premise("NopEx.prONUpdWeight",
      on0, :atONStep4_6, :EQ, true)

    cdONUpdWeight = NOP.Service.Condition.create_condition(
      "NopEx.cdONUpdWeight", [prONUpdWeight], "NopEx.prONUpdWeight")

    [cdONUpdWeight]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONUpdWeight0 on0.mtONUpdWeightH0();
      {NopEx.FOutputNeuron, :mtONUpdWeightH0, [on0]},
      #	  instigation inONUpdWeight1 on0.mtONUpdWeightH1();
      {NopEx.FOutputNeuron, :mtONUpdWeightH1, [on0]},
      #	  instigation inONUpdWeight0S46 on0.mtONStep4_6_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_6_Reset, [on0]},
      #	  instigation inONUpdWeight0S47 on0.mtONStep4_7_Set();
      {NopEx.FOutputNeuron, :mtONStep4_7_Set, [on0]},
    ]

  end


end

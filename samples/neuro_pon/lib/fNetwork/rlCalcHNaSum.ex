defmodule NopEx.RlCalcHNaSum do
  use NOP.Element.Rule

#rule rlCalcHNaSum
#	condition
#	  subcondition cdCalcHNaSum
#		 premise prCalcHNaSum hn0.atHNaStep1_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcHNaSum hn0.mtHNaSumProd();
#	  instigation inCalcHNaSumS12 hn0.mtHNaStep1_2_Reset();
#	  instigation inCalcHNaSumS13 hn0.mtHNaStep1_3_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prCalcHNaSum = NOP.Service.Premise.create_premise("NopEx.prCalcHNaSum",
      hn0, :atHNaStep1_2, :EQ, true)

    cdCalcHNaSum = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNaSum", [prCalcHNaSum], "NopEx.prCalcHNaSum")

    [cdCalcHNaSum]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inCalcHNaSum hn0.mtHNaSumProd();
      {NopEx.FHiddenNeuronA, :mtHNaSumProd, [hn0]},
      #	  instigation inCalcHNaSumS12 hn0.mtHNaStep1_2_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_2_Reset, [hn0]},
      #	  instigation inCalcHNaSumS13 hn0.mtHNaStep1_3_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_3_Set, [hn0]},
    ]

  end


end

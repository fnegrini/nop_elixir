defmodule NopEx.FNetwork do
  use NOP.Element.FBE


  defp int_attributes() do
    %{
      :atNumEpochs => 0,
      :atTrainInput0 => 0,
      :atTrainInput1 => 0,
      :atTrainOutput => 0,
      :atReadTrainInputs => 0,
      :atTotTrainInputs => 0,
      :atProdSumON => 0,
      :atLR => 0,
      :atNetError => 0,
      :atNetStep1_1 => false,
      :atNetStep2_1 => false,
      :atNetStep3_1 => false,
      :atNumberOne => 16384,
    }
  end

  #method mtNetStep1_1_Reset(atNetStep1_1 = false)
  def mtNetStep1_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atNetStep1_1, false)
  end

  #method mtNetStep2_1_Reset(atNetStep2_1 = false)
  def mtNetStep2_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atNetStep2_1, false)
  end

  #method mtNetStep3_1_Reset(atNetStep3_1 = false)
  def mtNetStep3_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atNetStep3_1, false)
  end

  #method mtNetStep1_1_Set(atNetStep1_1 = true)
  def mtNetStep1_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atNetStep1_1, true)
  end

  #method mtNetStep2_1_Set(atNetStep2_1 = true)
  def mtNetStep2_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atNetStep2_1, true)
  end

  #method mtNetStep3_1_Set(atNetStep3_1 = true)
  def mtNetStep3_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atNetStep3_1, true)
  end

  #method mtNetReadTI_Reset(atReadTrainInputs = 0)
  def mtNetReadTI_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atReadTrainInputs, 0)
  end

  #method mtNetNextInput(atReadTrainInputs = atReadTrainInputs + 1)
  def mtNetNextInput(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atReadTrainInputs,  "@atReadTrainInputs + 1")
  end

  #method mtNetRaiseEpoch(atNumEpochs = atNumEpochs - 1)
  def mtNetRaiseEpoch(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atNumEpochs, "@atNumEpochs - 1")
  end

  #method mtNetProdSumON(atProdSumON = on0.atONProdSigQtzH0 + on0.atONProdSigQtzH1)
  def mtNetProdSumON(this, output) do
    #attributes = NOP.Service.FBE.get_attribute(output, [:atONProdSigQtzH0, :atONProdSigQtzH1])
    #sum =  attributes[:atONProdSigQtzH0] + attributes[:atONProdSigQtzH1]
    #NOP.Service.FBE.set_attribute(this, :atProdSumON, sum)
    NOP.Service.FBE.set_attribute_chain(output, "@atONProdSigQtzH0 + @atONProdSigQtzH1", {this, :atProdSumON, "@result"})

  end

  #method mtSumNetError(atNetError = atNetError + on0.atONError)
  def mtSumNetError(this, output) do
    #sum = NOP.Service.FBE.get_attribute(output, :atONError)
    #NOP.Service.FBE.set_attribute_expr(this, :atNetError, "@atNetError + " <> to_string(sum))

    NOP.Service.FBE.set_attribute_chain(output, "@atONError", {this, :atNetError, "@atNetError + @result"})
  end

  #method mtMedNetError(atNetError = atNetError / atTotTrainInputs)
  def mtMedNetError(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atNetError, "@atNetError / @atTotTrainInputs")
  end
end

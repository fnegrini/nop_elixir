defmodule NopEx.RlCalcHNaTanH do
  use NOP.Element.Rule

#rule rlCalcHNaTanH
#	condition
#	  subcondition cdCalcHNaTanH
#		 premise prCalcHNaTanH hn0.atHNaStep1_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcHNaTanH hn0.mtHNaTanH();
#	  instigation inCalcHNaTanHS14 hn0.mtHNaStep1_4_Reset();
#	  instigation inCalcHNaTanHS11 hn0.mtHNaStep1_5_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prCalcHNaTanH = NOP.Service.Premise.create_premise("NopEx.prCalcHNaTanH",
      hn0, :atHNaStep1_4, :EQ, true)

      cdCalcHNaTanH = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNaTanH", [prCalcHNaTanH], "NopEx.prCalcHNaTanH")

    [cdCalcHNaTanH]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inCalcHNaTanH hn0.mtHNaTanH();
      {NopEx.FHiddenNeuronA, :mtHNaTanH, [hn0]},
      #	  instigation inCalcHNaTanHS14 hn0.mtHNaStep1_4_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_4_Reset, [hn0]},
      #	  instigation inCalcHNaTanHS11 hn0.mtHNaStep1_5_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_5_Set, [hn0]},
    ]

  end


end

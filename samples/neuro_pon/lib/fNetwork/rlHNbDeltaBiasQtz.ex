defmodule NopEx.RlHNbDeltaBiasQtz do
  use NOP.Element.Rule

#rule rlHNbDeltaBiasQtz
#	condition
#	  subcondition cdHNbDeltaBiasQtz
#		 premise prHNbDeltaBiasQtz hn1.atHNbStep3_6 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNbDeltaBiasQtz hn1.mtHNbDeltaBiasQtz();
#	  instigation inHNbDeltaBiasQtzS36 hn1.mtHNbStep3_6_Reset();
#	  instigation inHNbDeltaBiasQtzS37 hn1.mtHNbStep3_7_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prHNbDeltaBiasQtz = NOP.Service.Premise.create_premise("NopEx.prHNbDeltaBiasQtz",
      hn1, :atHNbStep3_6, :EQ, true)

    cdHNbDeltaBiasQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdHNbDeltaBiasQtz", [prHNbDeltaBiasQtz], "NopEx.prHNbDeltaBiasQtz")

    [cdHNbDeltaBiasQtz]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inHNbDeltaBiasQtz hn1.mtHNbDeltaBiasQtz();
      {NopEx.FHiddenNeuronB, :mtHNbDeltaBiasQtz, [hn1]},
      #	  instigation inHNbDeltaBiasQtzS36 hn1.mtHNbStep3_6_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_6_Reset, [hn1]},
      #	  instigation inHNbDeltaBiasQtzS37 hn1.mtHNbStep3_7_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep3_7_Set, [hn1]},
    ]

  end


end

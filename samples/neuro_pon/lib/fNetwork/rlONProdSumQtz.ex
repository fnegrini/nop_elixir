defmodule NopEx.RlONProdSumQtz do
  use NOP.Element.Rule

#rule rlONProdSumQtz
#	condition
#	  subcondition cdONProdSumQtz
#		 premise prONProdSumQtz on0.atONStep4_2 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONProd0SumQtz on0.mtONProdSigH0Qtz();
#	  instigation inONProd1SumQtz on0.mtONProdSigH1Qtz();
#	  instigation inONProdSumQtzS42 on0.mtONStep4_2_Reset();
#	  instigation inONProdSumQtzS21 net.mtNetStep2_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONProdSumQtz = NOP.Service.Premise.create_premise("NopEx.prONProdSumQtz",
      on0, :atONStep4_2, :EQ, true)

    cdONProdSumQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdONProdSumQtz", [prONProdSumQtz], "NopEx.prONProdSumQtz")

    [cdONProdSumQtz]

  end

  defp create_instigation_list([net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONProd0SumQtz on0.mtONProdSigH0Qtz();
      {NopEx.FOutputNeuron, :mtONProdSigH0Qtz, [on0]},
      #	  instigation inONProd1SumQtz on0.mtONProdSigH1Qtz();
      {NopEx.FOutputNeuron, :mtONProdSigH1Qtz, [on0]},
      #	  instigation inONProdSumQtzS42 on0.mtONStep4_2_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_2_Reset, [on0]},
      #	  instigation inONProdSumQtzS21 net.mtNetStep2_1_Set();
      {NopEx.FNetwork, :mtNetStep2_1_Set, [net]},
    ]

  end


end

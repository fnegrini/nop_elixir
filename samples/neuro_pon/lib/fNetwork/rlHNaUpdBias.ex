defmodule NopEx.RlHNaUpdBias do
  use NOP.Element.Rule

#rule rlHNaUpdBias
#	condition
#	  subcondition cdHNaUpdBias
#		 premise prHNaUpdBias hn0.atHNaStep3_7 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inHNaUpdBias hn0.mtHNaUpdBias();
#	  instigation inHNaUpdBiasS37 hn0.mtHNaStep3_7_Reset();
#	  instigation inHNaUpdBiasS38 hn0.mtHNaStep3_8_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaUpdBias = NOP.Service.Premise.create_premise("NopEx.prHNaUpdBias",
      hn0, :atHNaStep3_7, :EQ, true)

      cdHNaUpdBias = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaUpdBias", [prHNaUpdBias], "NopEx.prHNaUpdBias")

    [cdHNaUpdBias]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaUpdBias hn0.mtHNaUpdBias();
      {NopEx.FHiddenNeuronA, :mtHNaUpdBias, [hn0]},
      #	  instigation inHNaUpdBiasS37 hn0.mtHNaStep3_7_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_7_Reset, [hn0]},
      #	  instigation inHNaUpdBiasS38 hn0.mtHNaStep3_8_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_8_Set, [hn0]},
    ]

  end


end

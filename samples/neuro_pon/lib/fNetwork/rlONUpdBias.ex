defmodule NopEx.RlONUpdBias do
  use NOP.Element.Rule

#rule rlONUpdBias
#	condition
#	  subcondition cdONUpdBias
#		 premise prONUpdBias on0.atONStep4_9 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONUpdBias on0.mtONUpdBias();
#	  instigation inONUpdBiasS49 on0.mtONStep4_9_Reset();
#	  instigation inONUpdBiasSH021 hn0.mtHNaStep2_1_Set();
#	  instigation inONUpdBiasSH121 hn1.mtHNbStep2_1_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONUpdBias = NOP.Service.Premise.create_premise("NopEx.prONUpdBias",
      on0, :atONStep4_9, :EQ, true)

    cdONUpdBias = NOP.Service.Condition.create_condition(
      "NopEx.cdONErrDif", [prONUpdBias], "NopEx.prONUpdBias")

    [cdONUpdBias]

  end

  defp create_instigation_list([_net, hn0, hn1, on0]) do

    [
      #	  instigation inONUpdBias on0.mtONUpdBias();
      {NopEx.FOutputNeuron, :mtONUpdBias, [on0]},
      #	  instigation inONUpdBiasS49 on0.mtONStep4_9_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_9_Reset, [on0]},
      #	  instigation inONUpdBiasSH021 hn0.mtHNaStep2_1_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep2_1_Set, [hn0]},
      #	  instigation inONUpdBiasSH121 hn1.mtHNbStep2_1_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep2_1_Set, [hn1]},
    ]

  end


end

defmodule NopEx.RlSumHNaBias do
  use NOP.Element.Rule

#rule rlSumHNaBias
#	condition
#	  subcondition cdSumHNaBias
#		 premise prSumHNaBias hn0.atHNaStep1_3 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inSumHNaBias hn0.mtHNaSumBias();
#	  instigation inSumHNaBiasS13 hn0.mtHNaStep1_3_Reset();
#	  instigation inSumHNaBiasS14 hn0.mtHNaStep1_4_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prSumHNaBias = NOP.Service.Premise.create_premise("NopEx.prSumHNaBias",
      hn0, :atHNaStep1_3, :EQ, true)

      cdSumHNaBias = NOP.Service.Condition.create_condition(
      "NopEx.cdSumHNaBias", [prSumHNaBias], "NopEx.prSumHNaBias")

    [cdSumHNaBias]

  end

  defp create_instigation_list([_net, hn0, _hn1, _on0]) do

    [
      #	  instigation inSumHNaBias hn0.mtHNaSumBias();
      {NopEx.FHiddenNeuronA, :mtHNaSumBias, [hn0]},
      #	  instigation inSumHNaBiasS13 hn0.mtHNaStep1_3_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_3_Reset, [hn0]},
      #	  instigation inSumHNaBiasS14 hn0.mtHNaStep1_4_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep1_4_Set, [hn0]},

    ]

  end


end

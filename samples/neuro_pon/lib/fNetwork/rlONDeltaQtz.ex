defmodule NopEx.RlONDeltaQtz do
  use NOP.Element.Rule

#rule rlONDeltaQtz
#	condition
#	  subcondition cdONDeltaQtz
#		 premise prONDeltaQtz on0.atONStep4_5 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inONDeltaQtz0 on0.mtONDeltaQtzH0();
#	  instigation inONDeltaQtz1 on0.mtONDeltaQtzH1();
#	  instigation inONDeltaQtzS45 on0.mtONStep4_5_Reset();
#	  instigation inONDeltaQtzS46 on0.mtONStep4_6_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, _hn1, on0]) do

    prONDeltaQtz = NOP.Service.Premise.create_premise("NopEx.prONDeltaQtz",
      on0, :atONStep4_5, :EQ, true)

    cdONDeltaQtz = NOP.Service.Condition.create_condition(
      "NopEx.cdONDeltaQtz", [prONDeltaQtz], "NopEx.prONDeltaQtz")

    [cdONDeltaQtz]

  end

  defp create_instigation_list([_net, _hn0, _hn1, on0]) do

    [
      #	  instigation inONDeltaQtz0 on0.mtONDeltaQtzH0();
      {NopEx.FOutputNeuron, :mtONDeltaQtzH0, [on0]},
      #	  instigation inONDeltaQtz1 on0.mtONDeltaQtzH1();
      {NopEx.FOutputNeuron, :mtONDeltaQtzH1, [on0]},
      #	  instigation inONDeltaQtzS45 on0.mtONStep4_5_Reset();
      {NopEx.FOutputNeuron, :mtONStep4_5_Reset, [on0]},
      #	  instigation inONDeltaQtzS46 on0.mtONStep4_6_Set();
      {NopEx.FOutputNeuron, :mtONStep4_6_Set, [on0]},
    ]

  end


end

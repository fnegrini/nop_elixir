defmodule NopEx.RlCalcHNbTanH do
  use NOP.Element.Rule

#rule rlCalcHNbTanH
#	condition
#	  subcondition cdCalcHNbTanH
#		 premise prCalcHNbTanHS14 hn1.atHNbStep1_4 == true
#	  end_subcondition
#	end_condition
#	action
#	  instigation inCalcHNbTanH hn1.mtHNbTanH();
#	  instigation inCalcHNbTanHS14 hn1.mtHNbStep1_4_Reset();
#	  instigation inCalcHNbTanHS15 hn1.mtHNbStep1_5_Set();
#	end_action
#end_rule

  defp create_element_list([_net, _hn0, hn1, _on0]) do

    prCalcHNbTanHS14 = NOP.Service.Premise.create_premise("NopEx.prCalcHNbTanHS14",
      hn1, :atHNbStep1_4, :EQ, true)

      cdCalcHNbTanH = NOP.Service.Condition.create_condition(
      "NopEx.cdCalcHNbTanH", [prCalcHNbTanHS14], "NopEx.prCalcHNbTanHS14")

    [cdCalcHNbTanH]

  end

  defp create_instigation_list([_net, _hn0, hn1, _on0]) do

    [
      #	  instigation inCalcHNbTanH hn1.mtHNbTanH();
      {NopEx.FHiddenNeuronB, :mtHNbTanH, [hn1]},
      #	  instigation inCalcHNbTanHS14 hn1.mtHNbStep1_4_Reset();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_4_Reset, [hn1]},
      #	  instigation inCalcHNbTanHS15 hn1.mtHNbStep1_5_Set();
      {NopEx.FHiddenNeuronB, :mtHNbStep1_5_Set, [hn1]},
    ]

  end


end

defmodule NopEx.RlHNaDeltaBias do
  use NOP.Element.Rule

#rule rlHNaDeltaBias
#	condition
#	  subcondition cdHNaDeltaBias
#		 premise prHNaDeltaBias0 hn0.atHNaStep3_5 == true
#	 end_subcondition
#	end_condition
#	action
#	  instigation inHNaDeltaBias hn0.mtHNaDeltaBias();
#	  instigation inHNaDeltaBiasS35 hn0.mtHNaStep3_5_Reset();
#	  instigation inHNaDeltaBiasS36 hn0.mtHNaStep3_6_Set();
#	end_action
#end_rule

  defp create_element_list([_net, hn0, _hn1, _on0]) do

    prHNaDeltaBias0 = NOP.Service.Premise.create_premise("NopEx.prHNaDeltaBias0",
      hn0, :atHNaStep3_5, :EQ, true)

    cdHNaDeltaBias = NOP.Service.Condition.create_condition(
      "NopEx.cdHNaDeltaBias", [prHNaDeltaBias0], "NopEx.prHNaDeltaBias0")

    [cdHNaDeltaBias]

  end

  defp create_instigation_list([net, hn0, _hn1, _on0]) do

    [
      #	  instigation inHNaDeltaBias hn0.mtHNaDeltaBias();
      {NopEx.FHiddenNeuronA, :mtHNaDeltaBias, [hn0, net]},
      #	  instigation inHNaDeltaBiasS35 hn0.mtHNaStep3_5_Reset();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_5_Reset, [hn0]},
      #	  instigation inHNaDeltaBiasS36 hn0.mtHNaStep3_6_Set();
      {NopEx.FHiddenNeuronA, :mtHNaStep3_6_Set, [hn0]},
    ]

  end


end

defmodule NeuroPon do
  @moduledoc """
  Documentation for NeuroPon.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NeuroPon.hello()
      :world

  """
  def hello do
    :world
  end

end

defmodule NopEx.FHiddenNeuronB do
  use NOP.Element.FBE

  defp int_attributes() do
    %{
      :atHNbOutput => 0,
      :atHNbSumProd => 0,
      :atIHbWeightI0 => 0,
      :atIHbWeightI1 => 0,
      :atWIH10 => 0,
      :atWIH11 => 0,
      :atIHbBias => 0,
      :atBIHb => 0,
      :atHNbProdI0 => 0,
      :atHNbProdI1 => 0,
      :atHNbDerivative => 0,
      :atHNbHSignal => 0,
      :atHNbDeltaI0 => 0,
      :atHNbDeltaQtzI0 => 0,
      :atHNbDeltaI1 => 0,
      :atHNbDeltaQtzI1 => 0,
      :atHNbDeltaB => 0,
      :atHNbDeltaQtzB => 0,
      # forward steps
      :atHNbStep1_1 => false,
      :atHNbStep1_2 => false,
      :atHNbStep1_3 => false,
      :atHNbStep1_4 => false,
      :atHNbStep1_5_ONStep => false,
      # backprop steps
      :atHNbStep2_1 => false,
      :atHNbStep2_2 => false,
      :atHNbStep2_3 => false,
      :atHNbStep2_4 => false,
      :atHNbStep3_1 => false,
      :atHNbStep3_2 => false,
      :atHNbStep3_3 => false,
      :atHNbStep3_4 => false,
      :atHNbStep3_5 => false,
      :atHNbStep3_6 => false,
      :atHNbStep3_7 => false,
      :atHNbStep3_8_NetStep => false,
    }
  end

  #method mtHNbStep1_1_Reset(atHNbStep1_1 = false)
  def mtHNbStep1_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_1, false)
  end

  #method mtHNbStep1_2_Reset(atHNbStep1_2 = false)
  def mtHNbStep1_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_2, false)
  end

  #method mtHNbStep1_3_Reset(atHNbStep1_3 = false)
  def mtHNbStep1_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_3, false)
  end

  #method mtHNbStep1_4_Reset(atHNbStep1_4 = false)
  def mtHNbStep1_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_4, false)
  end

  #method mtHNbStep1_5_Reset(atHNbStep1_5_ONStep = false)
  def mtHNbStep1_5_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_5_ONStep, false)
  end

  #method mtHNbStep2_1_Reset(atHNbStep2_1 = false)
  def mtHNbStep2_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_1, false)
  end

  #method mtHNbStep2_2_Reset(atHNbStep2_2 = false)
  def mtHNbStep2_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_2, false)
  end

  #method mtHNbStep2_3_Reset(atHNbStep2_3 = false)
  def mtHNbStep2_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_3, false)
  end

  #method mtHNbStep2_4_Reset(atHNbStep2_4 = false)
  def mtHNbStep2_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_4, false)
  end

  #method mtHNbStep3_1_Reset(atHNbStep3_1 = false)
  def mtHNbStep3_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_1, false)
  end

  #method mtHNbStep3_2_Reset(atHNbStep3_2 = false)
  def mtHNbStep3_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_2, false)
  end

  #method mtHNbStep3_3_Reset(atHNbStep3_3 = false)
  def mtHNbStep3_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_3, false)
  end

  #method mtHNbStep3_4_Reset(atHNbStep3_4 = false)
  def mtHNbStep3_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_4, false)
  end

  #method mtHNbStep3_5_Reset(atHNbStep3_5 = false)
  def mtHNbStep3_5_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_5, false)
  end

  #method mtHNbStep3_6_Reset(atHNbStep3_6 = false)
  def mtHNbStep3_6_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_6, false)
  end

  #method mtHNbStep3_7_Reset(atHNbStep3_7 = false)
  def mtHNbStep3_7_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_7, false)
  end

  #method mtHNbStep3_8_Reset(atHNbStep3_8_NetStep = false)
  def mtHNbStep3_8_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_8_NetStep, false)
  end

  #method mtHNbStep1_1_Set(atHNbStep1_1 = true)
  def mtHNbStep1_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_1, true)
  end

  #method mtHNbStep1_2_Set(atHNbStep1_2 = true)
  def mtHNbStep1_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_2, true)
  end

  #method mtHNbStep1_3_Set(atHNbStep1_3 = true)
  def mtHNbStep1_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_3, true)
  end

  #method mtHNbStep1_4_Set(atHNbStep1_4 = true)
  def mtHNbStep1_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_4, true)
  end

  #method mtHNbStep1_5_Set(atHNbStep1_5_ONStep = true)
  def mtHNbStep1_5_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep1_5_ONStep, true)
  end

  #method mtHNbStep2_1_Set(atHNbStep2_1 = true)
  def mtHNbStep2_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_1, true)
  end

  #method mtHNbStep2_2_Set(atHNbStep2_2 = true)
  def mtHNbStep2_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_2, true)
  end

  #method mtHNbStep2_3_Set(atHNbStep2_3 = true)
  def mtHNbStep2_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_3, true)
  end

  #method mtHNbStep2_4_Set(atHNbStep2_4 = true)
  def mtHNbStep2_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep2_4, true)
  end

  #method mtHNbStep3_1_Set(atHNbStep3_1 = true)
  def mtHNbStep3_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_1, true)
  end

  #method mtHNbStep3_2_Set(atHNbStep3_2 = true)
  def mtHNbStep3_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_2, true)
  end

  #method mtHNbStep3_3_Set(atHNbStep3_3 = true)
  def mtHNbStep3_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_3, true)
  end

  #method mtHNbStep3_4_Set(atHNbStep3_4 = true)
  def mtHNbStep3_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_4, true)
  end

  #method mtHNbStep3_5_Set(atHNbStep3_5 = true)
  def mtHNbStep3_5_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_5, true)
  end

  #method mtHNbStep3_6_Set(atHNbStep3_6 = true)
  def mtHNbStep3_6_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_6, true)
  end

  #method mtHNbStep3_7_Set(atHNbStep3_7 = true)
  def mtHNbStep3_7_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_7, true)
  end

  #method mtHNbStep3_8_Set(atHNbStep3_8_NetStep = true)
  def mtHNbStep3_8_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNbStep3_8_NetStep, true)
  end

  # Calc Methods
  #method mtHNbProdI0(atHNbProdI0 = net.atTrainInput0 * atIHbWeightI0)
  def mtHNbProdI0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput0)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbProdI0, to_string(value) <> " + @atIHbWeightI0")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput0", {this, :atHNbProdI0, "@result + @atIHbWeightI0"})
  end

  #method mtHNbProdI1(atHNbProdI1 = net.atTrainInput1 * atIHbWeightI1)
  def mtHNbProdI1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput1)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbProdI1, to_string(value) <> " + @atIHbWeightI1")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput1", {this, :atHNbProdI1, "@result + @atIHbWeightI1"})
  end

  #method mtHNbSumProd(atHNbSumProd = atHNbProdI0 + atHNbProdI1)
  def mtHNbSumProd(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbSumProd, "@atHNbProdI0 + @atHNbProdI1")
  end

  #method mtHNbSumBias(atHNbSumProd = atHNbSumProd + atIHbBias)
  def mtHNbSumBias(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbSumProd, "@atHNbSumProd + @atIHbBias")
  end

  #method mtHNbTanH(atHNbOutput = atHNbSumProd ~ 8)			# TanH
  def mtHNbTanH(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbOutput, "NopEx.Utils.hyper_tan( @atHNbSumProd , 8 )")
  end

  # LR
  #method mtHNbDerSquare(atHNbDerivative = atHNbOutput * atHNbOutput)
  def mtHNbDerSquare(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbDerivative, "@atHNbOutput * @atHNbOutput")
  end

  #method mtHNbDerDif(atHNbDerivative = net.atNumberOne - atHNbDerivative)
  def mtHNbDerDif(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atNumberOne)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbDerivative, to_string(value) <> " - @atHNbDerivative")

    NOP.Service.FBE.set_attribute_chain(net, "@atNumberOne", {this, :atHNbDerivative, "@result - @atHNbDerivative"})
  end

  #method mtHNbHSigMul(atHNbHSignal = atHNbDerivative * net.atProdSumON)
  def mtHNbHSigMul(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atProdSumON)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbHSignal, to_string(value) <> " * @atHNbDerivative")

    NOP.Service.FBE.set_attribute_chain(net, "@atProdSumON", {this, :atHNbHSignal, "@result * @atHNbDerivative"})
  end

  #method mtHNbHSignal(atHNbHSignal = atHNbHSignal : 8)
  def mtHNbHSignal(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbHSignal, " NopEx.Utils.quantize( @atHNbHSignal , 16, 8 )")
  end

  # UpdWeight
  #method mtHNbDeltaSigI0(atHNbDeltaI0 = atHNbHSignal * net.atTrainInput0)
  def mtHNbDeltaSigI0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput0)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaI0, to_string(value) <> " * @atHNbHSignal")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput0", {this, :atHNbDeltaI0, "@result * @atHNbHSignal"})
  end

  #method mtHNbDeltaSigI1(atHNbDeltaI1 = atHNbHSignal * net.atTrainInput1)
  def mtHNbDeltaSigI1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput1)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaI1, to_string(value) <> " * @atHNbHSignal")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput1", {this, :atHNbDeltaI1, "@result * @atHNbHSignal"})
  end

  #method mtHNbDeltaLRI0(atHNbDeltaI0 = atHNbDeltaI0 / net.atLR)
  def mtHNbDeltaLRI0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaI0, "@atHNbDeltaI0 / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atHNbDeltaI0, "@atHNbDeltaI0 / @result"})
  end

  #method mtHNbDeltaLRI1(atHNbDeltaI1 = atHNbDeltaI1 / net.atLR)
  def mtHNbDeltaLRI1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaI1, "@atHNbDeltaI1 / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atHNbDeltaI1, "@atHNbDeltaI1 / @result"})
  end

  #method mtHNbDeltaQtzI0(atHNbDeltaQtzI0 = atHNbDeltaI0 : 10)
  def mtHNbDeltaQtzI0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaQtzI0, " NopEx.Utils.quantize( @atHNbDeltaI0 , 16, 10 )")
  end

  #method mtHNbDeltaQtzI1(atHNbDeltaQtzI1 = atHNbDeltaI1 : 10)
  def mtHNbDeltaQtzI1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaQtzI1, " NopEx.Utils.quantize( @atHNbDeltaI1 , 16, 10 )")
  end

  #method mtHNbUpdWeightI0(atIHbWeightI0 = atIHbWeightI0 + atHNbDeltaQtzI0)
  def mtHNbUpdWeightI0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atIHbWeightI0, "@atIHbWeightI0 + @atHNbDeltaQtzI0")
  end

  #method mtHNbUpdWeightI1(atIHbWeightI1 = atIHbWeightI1 + atHNbDeltaQtzI1)
  def mtHNbUpdWeightI1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atIHbWeightI1, "@atIHbWeightI1 + @atHNbDeltaQtzI1")
  end

  #method mtHNbDeltaBias(atHNbDeltaB = atHNbHSignal / net.atLR)
  def mtHNbDeltaBias(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaB, "@atHNbHSignal / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atHNbDeltaB, "@atHNbHSignal / @result"})
  end

  #method mtHNbDeltaBiasQtz(atHNbDeltaQtzB = atHNbDeltaB : 10)
  def mtHNbDeltaBiasQtz(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNbDeltaQtzB, " NopEx.Utils.quantize( @atHNbDeltaB , 16 , 10)")
  end

  #method mtHNbUpdBias(atIHbBias = atIHbBias + atHNbDeltaQtzB)
  def mtHNbUpdBias(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atIHbBias, "@atIHbBias + @atHNbDeltaQtzB")
  end

  # output weights
  #method mtHNbSetWIH10(atWIH00 = atIHbWeightI0)
  def mtHNbSetWIH10(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atWIH00, "@atIHbWeightI0")
  end

  #method mtHNbSetWIH11(atWIH01 = atIHbWeightI1)
  def mtHNbSetWIH11(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atWIH01, "@atIHbWeightI1")
  end

  #method mtHNbSetBIHb(atBIHb = atIHbBias)
  def mtHNbSetBIHb(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atBIHb, "@atIHbBias")
  end

end

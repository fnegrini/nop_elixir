defmodule NopEx.FHiddenNeuronA do
  use NOP.Element.FBE


  defp int_attributes() do
    %{
      :atHNaOutput => 0,
      :atHNaSumProd => 0 ,
      :atIHaWeightI0 => 0,
      :atIHaWeightI1 => 0,
      :atWIH00 => 0,
      :atWIH01 => 0,
      :atIHaBias => 0,
      :atBIHa => 0,
      :atHNaProdI0 => 0,
      :atHNaProdI1 => 0,
      :atHNaDerivative => 0,
      :atHNaHSignal => 0,
      :atHNaDeltaI0 => 0,
      :atHNaDeltaQtzI0 => 0,
      :atHNaDeltaI1 => 0,
      :atHNaDeltaQtzI1 => 0,
      :atHNaDeltaB => 0,
      :atHNaDeltaQtzB => 0,
      # forward steps
      :atHNaStep1_1 => false,
      :atHNaStep1_2 => false,
      :atHNaStep1_3 => false,
      :atHNaStep1_4 => false,
      :atHNaStep1_5_ONStep => false,
      # backprop steps
      :atHNaStep2_1 => false,
      :atHNaStep2_2 => false,
      :atHNaStep2_3 => false,
      :atHNaStep2_4 => false,
      :atHNaStep3_1 => false,
      :atHNaStep3_2 => false,
      :atHNaStep3_3 => false,
      :atHNaStep3_4 => false,
      :atHNaStep3_5 => false,
      :atHNaStep3_6 => false,
      :atHNaStep3_7 => false,
      :atHNaStep3_8_NetStep => false,
    }
  end

  #method mtHNaStep1_1_Reset(atHNaStep1_1 = false)
  def mtHNaStep1_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_1, false)
  end

  #method mtHNaStep1_2_Reset(atHNaStep1_2 = false)
  def mtHNaStep1_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_2, false)
  end

  #method mtHNaStep1_3_Reset(atHNaStep1_3 = false)
  def mtHNaStep1_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_3, false)
  end

  #method mtHNaStep1_4_Reset(atHNaStep1_4 = false)
  def mtHNaStep1_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_4, false)
  end

  #method mtHNaStep1_5_Reset(atHNaStep1_5_ONStep = false)
  def mtHNaStep1_5_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_5_ONStep, false)
  end

  #method mtHNaStep2_1_Reset(atHNaStep2_1 = false)
  def mtHNaStep2_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_1, false)
  end

  #method mtHNaStep2_2_Reset(atHNaStep2_2 = false)
  def mtHNaStep2_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_2, false)
  end

  #method mtHNaStep2_3_Reset(atHNaStep2_3 = false)
  def mtHNaStep2_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_3, false)
  end

  #method mtHNaStep2_4_Reset(atHNaStep2_4 = false)
  def mtHNaStep2_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_4, false)
  end

  #method mtHNaStep3_1_Reset(atHNaStep3_1 = false)
  def mtHNaStep3_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_1, false)
  end

  #method mtHNaStep3_2_Reset(atHNaStep3_2 = false)
  def mtHNaStep3_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_2, false)
  end

  #method mtHNaStep3_3_Reset(atHNaStep3_3 = false)
  def mtHNaStep3_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_3, false)
  end

  #method mtHNaStep3_4_Reset(atHNaStep3_4 = false)
  def mtHNaStep3_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_4, false)
  end

  #method mtHNaStep3_5_Reset(atHNaStep3_5 = false)
  def mtHNaStep3_5_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_5, false)
  end

  #method mtHNaStep3_6_Reset(atHNaStep3_6 = false)
  def mtHNaStep3_6_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_6, false)
  end

  #method mtHNaStep3_7_Reset(atHNaStep3_7 = false)
  def mtHNaStep3_7_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_7, false)
  end

  #method mtHNaStep3_8_Reset(atHNaStep3_8_NetStep = false)
  def mtHNaStep3_8_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_8_NetStep, false)
  end

  #method mtHNaStep1_1_Set(atHNaStep1_1 = true)
  def mtHNaStep1_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_1, true)
  end

  #method mtHNaStep1_2_Set(atHNaStep1_2 = true)
  def mtHNaStep1_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_2, true)
  end

  #method mtHNaStep1_3_Set(atHNaStep1_3 = true)
  def mtHNaStep1_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_3, true)
  end

  #method mtHNaStep1_4_Set(atHNaStep1_4 = true)
  def mtHNaStep1_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_4, true)
  end

  #method mtHNaStep1_5_Set(atHNaStep1_5_ONStep = true)
  def mtHNaStep1_5_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep1_5_ONStep, true)
  end

  #method mtHNaStep2_1_Set(atHNaStep2_1 = true)
  def mtHNaStep2_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_1, true)
  end

  #method mtHNaStep2_2_Set(atHNaStep2_2 = true)
  def mtHNaStep2_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_2, true)
  end

  #method mtHNaStep2_3_Set(atHNaStep2_3 = true)
  def mtHNaStep2_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_3, true)
  end

  #method mtHNaStep2_4_Set(atHNaStep2_4 = true)
  def mtHNaStep2_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep2_4, true)
  end

  #method mtHNaStep3_1_Set(atHNaStep3_1 = true)
  def mtHNaStep3_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_1, true)
  end

  #method mtHNaStep3_2_Set(atHNaStep3_2 = true)
  def mtHNaStep3_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_2, true)
  end

  #method mtHNaStep3_3_Set(atHNaStep3_3 = true)
  def mtHNaStep3_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_3, true)
  end

  #method mtHNaStep3_4_Set(atHNaStep3_4 = true)
  def mtHNaStep3_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_4, true)
  end

  #method mtHNaStep3_5_Set(atHNaStep3_5 = true)
  def mtHNaStep3_5_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_5, true)
  end

  #method mtHNaStep3_6_Set(atHNaStep3_6 = true)
  def mtHNaStep3_6_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_6, true)
  end

  #method mtHNaStep3_7_Set(atHNaStep3_7 = true)
  def mtHNaStep3_7_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_7, true)
  end

  #method mtHNaStep3_8_Set(atHNaStep3_8_NetStep = true)
  def mtHNaStep3_8_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atHNaStep3_8_NetStep, true)
  end

  # Calc Methods
  #method mtHNaProdI0(atHNaProdI0 = net.atTrainInput0 * atIHaWeightI0)
  def mtHNaProdI0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput0)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaProdI0, to_string(value) <> " + @atIHaWeightI0")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput0", {this, :atHNaProdI0, "@result + @atIHaWeightI0"})

  end

  #method mtHNaProdI1(atHNaProdI1 = net.atTrainInput1 * atIHaWeightI1)
  def mtHNaProdI1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput1)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaProdI1, to_string(value) <> " + @atIHaWeightI1")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput1", {this, :atHNaProdI1, "@result + @atIHaWeightI1"})

  end

  #method mtHNaSumProd(atHNaSumProd = atHNaProdI0 + atHNaProdI1)
  def mtHNaSumProd(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaSumProd, "@atHNaProdI0 + @atHNaProdI1")
  end

  #method mtHNaSumBias(atHNaSumProd = atHNaSumProd + atIHaBias)
  def mtHNaSumBias(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaSumProd, "@atHNaSumProd + @atIHaBias")
  end

  #method mtHNaTanH(atHNaOutput = atHNaSumProd ~ 8)			# TanH
  def mtHNaTanH(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaOutput, "NopEx.Utils.hyper_tan( @atHNaSumProd , 8 )")
  end

  # LR
  #method mtHNaDerSquare(atHNaDerivative = atHNaOutput * atHNaOutput)
  def mtHNaDerSquare(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaDerivative, "@atHNaOutput * @atHNaOutput")
  end

  #method mtHNaDerDif(atHNaDerivative = net.atNumberOne - atHNaDerivative)
  def mtHNaDerDif(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atNumberOne)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaDerivative, to_string(value) <> " - @atHNaDerivative")

    NOP.Service.FBE.set_attribute_chain(net, "@atNumberOne", {this, :atHNaDerivative, "@result - @atHNaDerivative"})
  end

  #method mtHNaHSigMul(atHNaHSignal = atHNaDerivative * net.atProdSumON)
  def mtHNaHSigMul(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atProdSumON)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaHSignal, to_string(value) <> " * @atHNaDerivative")

    NOP.Service.FBE.set_attribute_chain(net, "@atProdSumON", {this, :atHNaHSignal, "@result * @atHNaDerivative"})
  end

  #method mtHNaHSignal(atHNaHSignal = atHNaHSignal : 8)
  def mtHNaHSignal(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaHSignal, " NopEx.Utils.quantize( @atHNaHSignal , 16 , 8 )")
  end

  # UpdWeight
  #method mtHNaDeltaSigI0(atHNaDeltaI0 = atHNaHSignal * net.atTrainInput0)
  def mtHNaDeltaSigI0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput0)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaI0, to_string(value) <> " * @atHNaHSignal")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput0", {this, :atHNaDeltaI0, "@result * @atHNaHSignal"})
  end

  #method mtHNaDeltaSigI1(atHNaDeltaI1 = atHNaHSignal * net.atTrainInput1)
  def mtHNaDeltaSigI1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainInput1)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaI1, to_string(value) <> " * @atHNaHSignal")

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainInput1", {this, :atHNaDeltaI1, "@result * @atHNaHSignal"})
  end

  #method mtHNaDeltaLRI0(atHNaDeltaI0 = atHNaDeltaI0 / net.atLR)
  def mtHNaDeltaLRI0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaI0, "@atHNaDeltaI0 / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atHNaDeltaI0, "@atHNaDeltaI0 / @result"})
  end

  #method mtHNaDeltaLRI1(atHNaDeltaI1 = atHNaDeltaI1 / net.atLR)
  def mtHNaDeltaLRI1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaI1, "@atHNaDeltaI1 / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atHNaDeltaI1, "@atHNaDeltaI1 / @result"})
  end

  #method mtHNaDeltaQtzI0(atHNaDeltaQtzI0 = atHNaDeltaI0 : 10)
  def mtHNaDeltaQtzI0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaQtzI0, " NopEx.Utils.quantize( @atHNaDeltaI0 , 16 , 10)")
  end

  #method mtHNaDeltaQtzI1(atHNaDeltaQtzI1 = atHNaDeltaI1 : 10)
  def mtHNaDeltaQtzI1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaQtzI1, " NopEx.Utils.quantize( @atHNaDeltaI1 , 16 , 10)")
  end

  #method mtHNaUpdWeightI0(atIHaWeightI0 = atIHaWeightI0 + atHNaDeltaQtzI0)
  def mtHNaUpdWeightI0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atIHaWeightI0, "@atIHaWeightI0 + @atHNaDeltaQtzI0")
  end

  #method mtHNaUpdWeightI1(atIHaWeightI1 = atIHaWeightI1 + atHNaDeltaQtzI1)
  def mtHNaUpdWeightI1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atIHaWeightI1, "@atIHaWeightI1 + @atHNaDeltaQtzI1")
  end

  #method mtHNaDeltaBias(atHNaDeltaB = atHNaHSignal / net.atLR)
  def mtHNaDeltaBias(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaB, "@atHNaHSignal / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atHNaDeltaB, "@atHNaHSignal / @result"})
  end

  #method mtHNaDeltaBiasQtz(atHNaDeltaQtzB = atHNaDeltaB : 10)
  def mtHNaDeltaBiasQtz(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHNaDeltaQtzB, " NopEx.Utils.quantize( @atHNaDeltaB , 16 , 10 )")
  end

  #method mtHNaUpdBias(atIHaBias = atIHaBias + atHNaDeltaQtzB)
  def mtHNaUpdBias(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atIHaBias, "@atIHaBias + @atHNaDeltaQtzB")
  end

  # output weights
  #method mtHNaSetWIH00(atWIH00 = atIHaWeightI0)
  def mtHNaSetWIH00(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atWIH00, "@atIHaWeightI0")
  end

  #method mtHNaSetWIH01(atWIH01 = atIHaWeightI1)
  def mtHNaSetWIH01(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atWIH01, "@atIHaWeightI1")
  end

  #method mtHNaSetBIHa(atBIHa = atIHaBias)
  def mtHNaSetBIHa(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atBIHa, "@atIHaBias")
  end
end

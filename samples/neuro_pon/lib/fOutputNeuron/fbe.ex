defmodule NopEx.FOutputNeuron do
  use NOP.Element.FBE

  defp int_attributes() do
    %{
      :atONOutput => 0,
      :atONSumProd => 0,
      :atONProdSigH0 => 0,
      :atONProdSigQtzH0 => 0,
      :atONProdSigH1 => 0,
      :atONProdSigQtzH1 => 0,
      :atHOWeightH0 => 0,
      :atHOWeightH1 => 0,
      :atWHO0 => 0,
      :atWHO1 => 0,
      :atBHO => 0,
      :atONProdH0 => 0,
      :atONProdH1 => 0,
      :atHOBias => 0,
      :atONDif => 0,
      :atONError => 0,
      :atONDerivative => 0,
      :atONOSignal => 0,
      :atONDeltaH0 => 0,
      :atONDeltaQtzH0 => 0,
      :atONDeltaH1 => 0,
      :atONDeltaQtzH1 => 0,
      :atONDeltaB => 0,
      :atONDeltaQtzB => 0,
      :atONStep1_2 => false,
      :atONStep1_4 => false,
      :atONStep1_5 => false,
      :atONStep2_1 => false,
      :atONStep2_2 => false,
      :atONStep2_3 => false,
      :atONStep3_1 => false,
      :atONStep3_2 => false,
      :atONStep3_3 => false,
      :atONStep4_1 => false,
      :atONStep4_2 => false,
      :atONStep4_3 => false,
      :atONStep4_4 => false,
      :atONStep4_5 => false,
      :atONStep4_6 => false,
      :atONStep4_7 => false,
      :atONStep4_8 => false,
      :atONStep4_9 => false,
    }
  end

  #method mtONStep1_2_Reset(atONStep1_2 = false)
  def mtONStep1_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep1_2, false)
  end

  #method mtONStep1_4_Reset(atONStep1_4 = false)
  def mtONStep1_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep1_4, false)
  end

  #method mtONStep1_5_Reset(atONStep1_5 = false)
  def mtONStep1_5_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep1_5, false)
  end

  #method mtONStep2_1_Reset(atONStep2_1 = false)
  def mtONStep2_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep2_1, false)
  end

  #method mtONStep2_2_Reset(atONStep2_2 = false)
  def mtONStep2_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep2_2, false)
  end

  #method mtONStep2_3_Reset(atONStep2_3 = false)
  def mtONStep2_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep2_3, false)
  end

  #method mtONStep3_1_Reset(atONStep3_1 = false)
  def mtONStep3_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep3_1, false)
  end

  #method mtONStep3_2_Reset(atONStep3_2 = false)
  def mtONStep3_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep3_2, false)
  end

  #method mtONStep3_3_Reset(atONStep3_3 = false)
  def mtONStep3_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep3_3, false)
  end

  #method mtONStep4_1_Reset(atONStep4_1 = false)
  def mtONStep4_1_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_1, false)
  end

  #method mtONStep4_2_Reset(atONStep4_2 = false)
  def mtONStep4_2_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_2, false)
  end

  #method mtONStep4_3_Reset(atONStep4_3 = false)
  def mtONStep4_3_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_3, false)
  end

  #method mtONStep4_4_Reset(atONStep4_4 = false)
  def mtONStep4_4_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_4, false)
  end

  #method mtONStep4_5_Reset(atONStep4_5 = false)
  def mtONStep4_5_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_5, false)
  end

  #method mtONStep4_6_Reset(atONStep4_6 = false)
  def mtONStep4_6_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_6, false)
  end

  #method mtONStep4_7_Reset(atONStep4_7 = false)
  def mtONStep4_7_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_7, false)
  end

  #method mtONStep4_8_Reset(atONStep4_8 = false)
  def mtONStep4_8_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_8, false)
  end

  #method mtONStep4_9_Reset(atONStep4_9 = false)
  def mtONStep4_9_Reset(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_9, false)
  end

  #method mtONStep1_2_Set(atONStep1_2 = true)
  def mtONStep1_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep1_2, true)
  end

  #method mtONStep1_4_Set(atONStep1_4 = true)
  def mtONStep1_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep1_4, true)
  end

  #method mtONStep1_5_Set(atONStep1_5 = true)
  def mtONStep1_5_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep1_5, true)
  end

  #method mtONStep2_1_Set(atONStep2_1 = true)
  def mtONStep2_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep2_1, true)
  end

  #method mtONStep2_2_Set(atONStep2_2 = true)
  def mtONStep2_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep2_2, true)
  end

  #method mtONStep2_3_Set(atONStep2_3 = true)
  def mtONStep2_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep2_3, true)
  end

  #method mtONStep3_1_Set(atONStep3_1 = true)
  def mtONStep3_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep3_1, true)
  end

  #method mtONStep3_2_Set(atONStep3_2 = true)
  def mtONStep3_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep3_2, true)
  end

  #method mtONStep3_3_Set(atONStep3_3 = true)
  def mtONStep3_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep3_3, true)
  end

  #method mtONStep4_1_Set(atONStep4_1 = true)
  def mtONStep4_1_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_1, true)
  end

  #method mtONStep4_2_Set(atONStep4_2 = true)
  def mtONStep4_2_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_2, true)
  end

  #method mtONStep4_3_Set(atONStep4_3 = true)
  def mtONStep4_3_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_3, true)
  end

  #method mtONStep4_4_Set(atONStep4_4 = true)
  def mtONStep4_4_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_4, true)
  end

  #method mtONStep4_5_Set(atONStep4_5 = true)
  def mtONStep4_5_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_5, true)
  end

  #method mtONStep4_6_Set(atONStep4_6 = true)
  def mtONStep4_6_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_6, true)
  end

  #method mtONStep4_7_Set(atONStep4_7 = true)
  def mtONStep4_7_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_7, true)
  end

  #method mtONStep4_8_Set(atONStep4_8 = true)
  def mtONStep4_8_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_8, true)
  end

  #method mtONStep4_9_Set(atONStep4_9 = true)
  def mtONStep4_9_Set(this) do
    NOP.Service.FBE.set_attribute(this, :atONStep4_9, true)
  end

  # Calc Methods
  #method mtONProdH0(atONProdH0 = hn0.atHNaOutput * atHOWeightH0)
  def mtONProdH0(this, hn0) do
    #value = NOP.Service.FBE.get_attribute(hn0, :atHNaOutput)
    #NOP.Service.FBE.set_attribute_expr(this, :atONProdH0, to_string(value) <> "* @atHOWeightH0")

    NOP.Service.FBE.set_attribute_chain(hn0, "@atHNaOutput", {this, :atONProdH0, "@result * @atHOWeightH0"})
  end

  #method mtONProdH1(atONProdH1 = hn1.atHNbOutput * atHOWeightH1)
  def mtONProdH1(this, hn1) do
    #value = NOP.Service.FBE.get_attribute(hn1, :atHNbOutput)
    #NOP.Service.FBE.set_attribute_expr(this, :atONProdH1, to_string(value) <> "* @atHOWeightH1")

    NOP.Service.FBE.set_attribute_chain(hn1, "@atHNbOutput", {this, :atONProdH1, "@result * @atHOWeightH1"})
  end

  #method mtONSumProd(atONSumProd = atONProdH0 + atONProdH1)
  def mtONSumProd(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONSumProd, "@atONProdH0 + @atONProdH1")
  end

  #method mtONSumBias(atONSumProd = atONSumProd + atHOBias)
  def mtONSumBias(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONSumProd, "@atONSumProd + @atHOBias")
  end

  #method mtONTanH(atONOutput = atONSumProd ~ 8)  				# TanH
  def mtONTanH(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONOutput, "NopEx.Utils.hyper_tan( @atONSumProd , 8 )")
  end

  # Error Methods
  #method mtONTrainOutQtz(atONDif = net.atTrainOutput : 8)
  def mtONTrainOutQtz(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainOutput)
    #value =  NopEx.Utils.quantize(value, 0, 8)
    #NOP.Service.FBE.set_attribute(this, :atONDif, value)

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainOutput", {this, :atONDif, "NopEx.Utils.quantize( @result , 0, 8)"})
  end

  #method mtONErrDif(atONDif = atONDif - atONOutput)
  def mtONErrDif(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONDif, "@atONDif - @atONOutput")
  end

  #method mtONErrSquare(atONError = atONDif * atONDif)
  def mtONErrSquare(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONError, "@atONDif * @atONDif")
  end

  # LR
  #method mtONDerSquare(atONDerivative = atONOutput * atONOutput)
  def mtONDerSquare(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONDerivative, "@atONOutput * @atONOutput")
  end

  #method mtONDerDif(atONDerivative = net.atNumberOne - atONDerivative)
  def mtONDerDif(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atNumberOne)
    #NOP.Service.FBE.set_attribute_expr(this, :atONDerivative, to_string(value) <> " - @atONDerivative")

    NOP.Service.FBE.set_attribute_chain(net, "@atNumberOne", {this, :atONDerivative, "@result - @atONDerivative"})
  end

  #method mtONOSigQtz(atONOSignal = net.atTrainOutput : 8)
  def mtONOSigQtz(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atTrainOutput)
    #value =  NopEx.Utils.hyper_tan(value, 8)
    #NOP.Service.FBE.set_attribute(this, :atONOSignal, value)

    NOP.Service.FBE.set_attribute_chain(net, "@atTrainOutput", {this, :atONOSignal, "NopEx.Utils.hyper_tan( @result , 8)"})
  end

  #method mtONOSigDif(atONOSignal = atONOSignal - atONOutput)
  def mtONOSigDif(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONOSignal, "@atONOSignal - @atONOutput")
  end

  #method mtONOSignal(atONOSignal = atONDif * atONDerivative)
  def mtONOSignal(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONOSignal, "@atONDif * @atONDerivative")
  end

  # UpdWeight
  #method mtONProdSigH0(atONProdSigH0 = atONOSignal * atHOWeightH0)
  def mtONProdSigH0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONProdSigH0, "@atONOSignal * @atHOWeightH0")
  end

  #method mtONProdSigH1(atONProdSigH1 = atONOSignal * atHOWeightH1)
  def mtONProdSigH1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONProdSigH1, "@atONOSignal * @atHOWeightH1")
  end

  #method mtONProdSigH0Qtz(atONProdSigQtzH0 = atONProdSigH0 : 16)
  def mtONProdSigH0Qtz(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONProdSigQtzH0, " NopEx.Utils.quantize( @atONProdSigH0 , 8 , 16 )")
  end

  #method mtONProdSigH1Qtz(atONProdSigQtzH1 = atONProdSigH1 : 16)
  def mtONProdSigH1Qtz(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONProdSigQtzH1, " NopEx.Utils.quantize( @atONProdSigH1 , 8 , 16 )")
  end

  #method mtONDeltaSigH0(atONDeltaH0 = atONOSignal * hn0.atHNaOutput)
  def mtONDeltaSigH0(this, hn0) do
    #value = NOP.Service.FBE.get_attribute(hn0, :atHNaOutput)
    #NOP.Service.FBE.set_attribute_expr(this, :atONDeltaH0, "@atONOSignal * " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(hn0, "@atHNaOutput", {this, :atONDeltaH0, "@atONOSignal * @result"})
  end

  #method mtONDeltaSigH1(atONDeltaH1 = atONOSignal * hn1.atHNbOutput)
  def mtONDeltaSigH1(this, hn1) do
    #value = NOP.Service.FBE.get_attribute(hn1, :atHNbOutput)
    #NOP.Service.FBE.set_attribute_expr(this, :atONDeltaH1, "@atONOSignal * " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(hn1, "@atHNbOutput", {this, :atONDeltaH1, "@atONOSignal * @result"})
  end

  #method mtONDeltaLRH0(atONDeltaH0 = atONDeltaH0 / net.atLR)
  def mtONDeltaLRH0(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atONDeltaH0, "@atONDeltaH0 / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atONDeltaH0, "@atONDeltaH0 / @result"})
  end

  #method mtONDeltaLRH1(atONDeltaH1 = atONDeltaH1 / net.atLR)
  def mtONDeltaLRH1(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atONDeltaH1, "@atONDeltaH1 / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atONDeltaH1, "@atONDeltaH1 / @result"})
  end

  #method mtONDeltaQtzH0(atONDeltaQtzH0 = atONDeltaH0 : 10)
  def mtONDeltaQtzH0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONDeltaQtzH0, " NopEx.Utils.quantize( @atONDeltaH0 , 16 , 10 )")
  end

  #method mtONDeltaQtzH1(atONDeltaQtzH1 = atONDeltaH1 : 10)
  def mtONDeltaQtzH1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONDeltaQtzH1, " NopEx.Utils.quantize( @atONDeltaH1 , 16 , 10)")
  end

  #method mtONUpdWeightH0(atHOWeightH0 = atHOWeightH0 + atONDeltaQtzH0)
  def mtONUpdWeightH0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHOWeightH0, "@atHOWeightH0 + @atONDeltaQtzH0")
  end

  #method mtONUpdWeightH1(atHOWeightH1 = atHOWeightH1 + atONDeltaQtzH1)
  def mtONUpdWeightH1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHOWeightH1, "@atHOWeightH1 + @atONDeltaQtzH1")
  end

  #UpdBias
  #method mtONDeltaBias(atONDeltaB = atONOSignal / net.atLR)
  def mtONDeltaBias(this, net) do
    #value = NOP.Service.FBE.get_attribute(net, :atLR)
    #NOP.Service.FBE.set_attribute_expr(this, :atONDeltaB, "@atONOSignal / " <> to_string(value))

    NOP.Service.FBE.set_attribute_chain(net, "@atLR", {this, :atONDeltaB, "@atONOSignal / @result"})
  end

  #method mtONDeltaBiasQtz(atONDeltaQtzB = atONDeltaB : 10)
  def mtONDeltaBiasQtz(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atONDeltaQtzB, " NopEx.Utils.quantize( @atONDeltaB , 8 , 10)")
  end

  #method mtONUpdBias(atHOBias = atHOBias + atONDeltaQtzB)
  def mtONUpdBias(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atHOBias, "@atHOBias + @atONDeltaQtzB")
  end

  # output weights
  #method mtONSetWHO0(atWHO0 = atHOWeightH0)
  def mtONSetWHO0(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atWHO0, "@atHOWeightH0")
  end

  #method mtONSetWHO1(atWHO1 = atHOWeightH1)
  def mtONSetWHO1(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atWHO1, "@atHOWeightH1")
  end

  #method mtONSetBHO(atBHO = atHOBias)
  def mtONSetBHO(this) do
    NOP.Service.FBE.set_attribute_expr(this, :atBHO, "@atHOBias")
  end
end

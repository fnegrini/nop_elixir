defmodule NopEx.Utils do

  def quantize(number, from, to) do

      #int quantize(int number, int from, int to){
      #  int divident = pow(2,to-1)-1;
      #  int divisor = pow(2,from-1)-1;
      #  float merda = float(divident) / float(divisor);
      #  int ret = trunc(number * merda);

      dividend = :math.pow(2, to-1) -1
      divisor = :math.pow(2,from-1) -1
      factor = dividend / divisor
      trunc( number * factor)

  end

  def hyper_tan(x, inBits) do

    ret = abs(x)

    ret =
    cond do
      (inBits == 16) ->
        cond do
          (ret >= 9088) -> 127
          (ret >= 8000) -> 126
          (ret >= 7296) -> 125
          (ret >= 6848) -> 124
          (ret >= 6464) -> 123
          (ret >= 6144) -> 122
          (ret >= 5888) -> 121
          (ret >= 5632) -> 120
          (ret >= 5440) -> 119
          (ret >= 5248) -> 118
          (ret >= 5120) -> 117
          (ret >= 4992) -> 116
          (ret >= 4800) -> 115
          (ret >= 4672) -> 114
          (ret >= 4608) -> 113
          (ret >= 4480) -> 112
          (ret >= 4352) -> 111
          (ret >= 4288) -> 110
          (ret >= 4160) -> 109
          (ret >= 4096) -> 108
          (ret >= 3968) -> 107
          (ret >= 3904) -> 106
          (ret >= 3840) -> 105
          (ret >= 3776) -> 104
          (ret >= 3648) -> 103
          (ret >= 3584) -> 102
          (ret >= 3520) -> 101
          (ret >= 3456) -> 100
          (ret >= 3392) -> 99
          (ret >= 3328) -> 98
          (ret >= 3264) -> 97
          (ret >= 3200) -> 96
          (ret >= 3136) -> 95
          (ret >= 3072) -> 93
          (ret >= 3008) -> 92
          (ret >= 2944) -> 91
          (ret >= 2880) -> 90
          (ret >= 2816) -> 89
          (ret >= 2752) -> 87
          (ret >= 2688) -> 86
          (ret >= 2624) -> 85
          (ret >= 2560) -> 83
          (ret >= 2496) -> 82
          (ret >= 2432) -> 80
          (ret >= 2368) -> 79
          (ret >= 2304) -> 77
          (ret >= 2240) -> 76
          (ret >= 2176) -> 74
          (ret >= 2112) -> 72
          (ret >= 2048) -> 70
          (ret >= 1984) -> 69
          (ret >= 1920) -> 67
          (ret >= 1856) -> 65
          (ret >= 1792) -> 63
          (ret >= 1728) -> 61
          (ret >= 1664) -> 59
          (ret >= 1600) -> 57
          (ret >= 1536) -> 55
          (ret >= 1472) -> 53
          (ret >= 1408) -> 51
          (ret >= 1344) -> 49
          (ret >= 1280) -> 47
          (ret >= 1216) -> 45
          (ret >= 1152) -> 43
          (ret >= 1088) -> 41
          (ret >= 1024) -> 38
          (ret >= 960)  -> 36
          (ret >= 896)  -> 34
          (ret >= 832)  -> 31
          (ret >= 768)  -> 29
          (ret >= 704)  -> 27
          (ret >= 640)  -> 24
          (ret >= 576)  -> 22
          (ret >= 512)  -> 19
          (ret >= 448)  -> 17
          (ret >= 384)  -> 14
          (ret >= 320)  -> 12
          (ret >= 256)  -> 9
          (ret >= 192)  -> 7
          (ret >= 128)  -> 4
          (ret >= 64)   -> 2
          true ->          0
        end
      (inBits == 18 ) ->
        cond do
          (ret >= 36352) ->  127
          (ret >= 32000) ->  126
          (ret >= 29184) ->  125
          (ret >= 27392) ->  124
          (ret >= 25856) ->  123
          (ret >= 24576) ->  122
          (ret >= 23552) ->  121
          (ret >= 22528) ->  120
          (ret >= 21760) ->  119
          (ret >= 20992) ->  118
          (ret >= 20480) ->  117
          (ret >= 19968) ->  116
          (ret >= 19200) ->  115
          (ret >= 18688) ->  114
          (ret >= 18432) ->  113
          (ret >= 18176) ->  112
          (ret >= 17408) ->  111
          (ret >= 17152) ->  110
          (ret >= 16640) ->  109
          (ret >= 16384) ->  108
          (ret >= 15872) ->  107
          (ret >= 15616) ->  106
          (ret >= 15360) ->  105
          (ret >= 15104) ->  104
          (ret >= 14592) ->  103
          (ret >= 14336) ->  102
          (ret >= 14080) ->  101
          (ret >= 13824) ->  100
          (ret >= 13568) ->  99
          (ret >= 13312) ->  98
          (ret >= 13056) ->  97
          (ret >= 12800) ->  96
          (ret >= 12544) ->  95
          (ret >= 12288) ->  93
          (ret >= 12032) ->  92
          (ret >= 11776) ->  91
          (ret >= 11520) ->  90
          (ret >= 11264) ->  89
          (ret >= 11008) ->  87
          (ret >= 10752) ->  86
          (ret >= 10496) ->  85
          (ret >= 10240) ->  83
          (ret >= 9984)  ->  82
          (ret >= 9728)  ->  80
          (ret >= 9472)  ->  79
          (ret >= 9216)  ->  77
          (ret >= 8960)  ->  76
          (ret >= 8704)  ->  74
          (ret >= 8448)  ->  72
          (ret >= 8192)  ->  70
          (ret >= 7936)  ->  69
          (ret >= 7680)  ->  67
          (ret >= 7424)  ->  65
          (ret >= 7168)  ->  63
          (ret >= 6912)  ->  61
          (ret >= 6656)  ->  59
          (ret >= 6400)  ->  57
          (ret >= 6144)  ->  55
          (ret >= 5888)  ->  53
          (ret >= 5632)  ->  51
          (ret >= 5376)  ->  49
          (ret >= 5120)  ->  47
          (ret >= 4864)  ->  45
          (ret >= 4608)  ->  43
          (ret >= 4352)  ->  41
          (ret >= 4096)  ->  38
          (ret >= 3840)  ->  36
          (ret >= 3584)  ->  34
          (ret >= 3328)  ->  31
          (ret >= 3072)  ->  29
          (ret >= 2816)  ->  27
          (ret >= 2560)  ->  24
          (ret >= 2304)  ->  22
          (ret >= 2048)  ->  19
          (ret >= 1792)  ->  17
          (ret >= 1536)  ->  14
          (ret >= 1280)  ->  12
          (ret >= 1024)  ->  9
          (ret >= 768)   ->  7
          (ret >= 512)   ->  4
          (ret >= 256)   ->  2
          true ->            0
        end
      true ->  0
	  end

    ret = if (x < 0) do -1 * ret else ret end

    ret

  end

  def set_initial_values(fbes, num_epochs) do

    NOP.Service.FBE.set_attribute(fbes[:network], :atNumEpochs,  num_epochs)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTotTrainInputs, 4)

  end

  def train_network(fbes) do
    run_step(fbes, 1)
    run_step(fbes, 2)
    run_step(fbes, 3)
    run_step(fbes, 4)
  end

  def run_step(fbes, 1) do

    # STEP 1 -----------------------------------------------------------
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput0,  0)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput1,  0)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainOutput,  0)
    #Launch
    NOP.Service.FBE.set_attribute(fbes[:network], :atNetStep1_1,  true)
  end

  def run_step(fbes, 2) do

    # STEP 2 -----------------------------------------------------------
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput0,  0)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput1,  1)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainOutput,  1)
    #Launch
    NOP.Service.FBE.set_attribute(fbes[:network], :atNetStep1_1,  true)
  end

  def run_step(fbes, 3) do

    # STEP 3 -----------------------------------------------------------
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput0,  0)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput1,  1)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainOutput,  1)
    #Launch
    NOP.Service.FBE.set_attribute(fbes[:network], :atNetStep1_1,  true)

  end

  def run_step(fbes, 4) do

    # STEP 4 -----------------------------------------------------------
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput0,  1)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainInput1,  1)
    NOP.Service.FBE.set_attribute(fbes[:network], :atTrainOutput,  0)
    #Launch
    NOP.Service.FBE.set_attribute(fbes[:network], :atNetStep1_1,  true)

  end

  def init_network(sufix) do

    #FBES

    net = NOP.Service.FBE.create_fbe(NopEx.FNetwork, "network_" <> sufix)
    hn0 = NOP.Service.FBE.create_fbe(NopEx.FHiddenNeuronA, "hiddenA_" <> sufix)
    hn1 = NOP.Service.FBE.create_fbe(NopEx.FHiddenNeuronB, "hiddenB_" <> sufix)
    on0 =  NOP.Service.FBE.create_fbe(NopEx.FOutputNeuron, "output_" <> sufix)

    #Rules

    NOP.Service.Rule.create_rule(NopEx.RNetStart, "NopEx.RNetStart_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlNetStartCalc, "NopEx.RlNetStartCalc_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlNetProdSumON, "NopEx.RlNetProdSumON_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlNetNextInput, "NopEx.RlNetNextInput_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlNetRaiseEpoch, "NopEx.RlNetRaiseEpoch_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcHNaProd, "NopEx.RlCalcHNaProd_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcHNaSum, "NopEx.RlCalcHNaSum_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlSumHNaBias, "NopEx.RlSumHNaBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcHNaTanH, "NopEx.RlCalcHNaTanH_" <> sufix, [net, hn0, hn1, on0])

    NOP.Service.Rule.create_rule(NopEx.RlHNaDerSquare, "NopEx.RlHNaDerSquare_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaDerDif, "NopEx.RlHNaDerDif_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaHSigMul, "NopEx.RlHNaHSigMul_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaHSignal, "NopEx.RlHNaHSignal_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaDeltaSig, "NopEx.RlHNaDeltaSig_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaDeltaLR, "NopEx.RlHNaDeltaLR_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaDeltaQtz, "NopEx.RlHNaDeltaQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaUpdWeight, "NopEx.RlHNaUpdWeight_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaDeltaBias, "NopEx.RlHNaDeltaBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaDeltaBiasQtz, "NopEx.RlHNaDeltaBiasQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNaUpdBias, "NopEx.RlHNaUpdBias_" <> sufix, [net, hn0, hn1, on0])

    NOP.Service.Rule.create_rule(NopEx.RlCalcHNbProd, "NopEx.RlCalcHNbProd_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcHNbSum, "NopEx.RlCalcHNbSum_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlSumHNbBias, "NopEx.RlSumHNbBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcHNbTanH, "NopEx.RlCalcHNbTanH_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDerSquare, "NopEx.RlHNbDerSquare_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDerDif, "NopEx.RlHNbDerDif_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbHSigMul, "NopEx.RlHNbHSigMul_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbHSignal, "NopEx.RlHNbHSignal_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDeltaSig, "NopEx.RlHNbDeltaSig_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDeltaLR, "NopEx.RlHNbDeltaLR_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDeltaQtz, "NopEx.RlHNbDeltaQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbUpdWeight, "NopEx.RlHNbUpdWeight_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDeltaBias, "NopEx.RlHNbDeltaBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlSumHNbBias, "NopEx.RlSumHNbBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbDeltaBiasQtz, "NopEx.RlHNbDeltaBiasQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlHNbUpdBias, "NopEx.RlHNbUpdBias_" <> sufix, [net, hn0, hn1, on0])

    NOP.Service.Rule.create_rule(NopEx.RlCalcONProd, "NopEx.RlCalcONProd_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcONSum, "NopEx.RlCalcONSum_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlSumONBias, "NopEx.RlSumONBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlCalcONTanH, "NopEx.RlCalcONTanH_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONTrainOutQtz, "NopEx.RlONTrainOutQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlSumONBias, "NopEx.RlSumONBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONErrSquare, "NopEx.RlONErrSquare_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDerSquare, "NopEx.RlONDerSquare_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDerDif, "NopEx.RlONDerDif_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONOSignal, "NopEx.RlONOSignal_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONProdSum, "NopEx.RlONProdSum_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONProdSumQtz, "NopEx.RlONProdSumQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDeltaSig, "NopEx.RlONDeltaSig_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDeltaLR, "NopEx.RlONDeltaLR_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDeltaQtz, "NopEx.RlONDeltaQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONUpdWeight, "NopEx.RlONUpdWeight_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDeltaBias, "NopEx.RlONDeltaBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONDeltaBiasQtz, "NopEx.RlONDeltaBiasQtz_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONUpdBias, "NopEx.RlONUpdBias_" <> sufix, [net, hn0, hn1, on0])
    NOP.Service.Rule.create_rule(NopEx.RlONErrDif, "NopEx.RlONErrDif_" <> sufix, [net, hn0, hn1, on0])


    %{
      :network => net,
      :hiddenA => hn0,
      :hiddenB => hn1,
      :output => on0,
    }

  end

  defp simulate_int(networks, _epoch_count, 0) do
    networks
  end

  defp simulate_int(networks, epoch_count, count) do
    fbes = init_network(to_string(count))
    set_initial_values(fbes, epoch_count)
    networks = networks ++ [fbes]
    Task.start(NopEx.Utils, :train_network, [fbes])
    simulate_int(networks, epoch_count, count-1)
  end

  def simulate(count, epoch_count) do

    networks = simulate_int([], epoch_count, count)

    networks
  end

end

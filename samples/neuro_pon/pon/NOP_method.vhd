-------------------- METHOD -------------------------
-- version 3.1 -- schutz
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.data_type_pkg.all;

ENTITY NOP_method IS
	GENERIC (bits_in_a	: bits := 32;
				bits_in_b	: bits := 32;
				bits_res 	: bits := 32;
				dataType		: bits := 1;
				operation	: bits := 1);
	PORT (in_a 		: IN STD_LOGIC_VECTOR(bits_in_a-1 downto 0); -- the value of input a
		   in_b 		: IN STD_LOGIC_VECTOR(bits_in_b-1 downto 0); -- the value of input b
		   execute 	: IN STD_LOGIC; 										-- signals to execute the method
		   result 	: OUT STD_LOGIC_VECTOR(bits_res-1 downto 0); -- the value of output
	      notify 	: OUT STD_LOGIC); 									-- notify the attribute		
END NOP_method;

ARCHITECTURE NOP_method_arq OF NOP_method IS

SUBTYPE stIn_a_value IS STD_LOGIC_VECTOR(bits_in_a-1 downto 0);
SUBTYPE stIn_b_value IS STD_LOGIC_VECTOR(bits_in_b-1 downto 0);
SUBTYPE stOut_value IS STD_LOGIC_VECTOR(bits_res-1 downto 0);

function quantize(number : stIn_a_value) return stOut_value is
	variable dif	   : integer range -63 to 63;
	variable ret	   : stOut_value;
	variable vect_in	: signed (bits_in_a-1 downto 0) := (others => '0');
	variable vect_out	: signed (bits_in_b-1 downto 0) := (others => '0');
begin
	dif := bits_in_a - bits_in_b;
	vect_in := signed(number); 
	if dif < 0 then
		dif := abs(dif);
		vect_out(bits_in_a - 1 downto 0) := vect_in;
		vect_out := (vect_out sll dif) + (2**dif) - 1;
	else
		vect_in := vect_in srl dif;
		vect_out:= vect_in(bits_res - 1 downto 0);
	end if;
	ret := std_logic_vector(vect_out);
	return ret;
end function;


function hyTan(x : stIn_a_value) return stOut_value is
	variable ret : signed(bits_res-1 downto 0);
	variable y 	 : natural range 0 to 2**(bits_in_a-1)-1;
begin
	y:=to_integer(abs(signed(x)));
	if (bits_in_a = 15) then	
	  if (y < 1344) then
		  if (y >= 1280) then ret:=to_signed(47, bits_res);
		  elsif (y >= 1216) then ret:=to_signed(45, bits_res);
		  elsif (y >= 1152) then ret:=to_signed(43, bits_res);
		  elsif (y >= 1088) then ret:=to_signed(41, bits_res);
		  elsif (y >= 1024) then ret:=to_signed(38, bits_res);
		  elsif (y >= 960) then ret:=to_signed(36, bits_res);
		  elsif (y >= 896) then ret:=to_signed(34, bits_res);
		  elsif (y >= 832) then ret:=to_signed(31, bits_res);
		  elsif (y >= 768) then ret:=to_signed(29, bits_res);
		  elsif (y >= 704) then ret:=to_signed(27, bits_res);
		  elsif (y >= 640) then ret:=to_signed(24, bits_res);
		  elsif (y >= 576) then ret:=to_signed(22, bits_res);
		  elsif (y >= 512) then ret:=to_signed(19, bits_res);
		  elsif (y >= 448) then ret:=to_signed(17, bits_res);
		  elsif (y >= 384) then ret:=to_signed(14, bits_res);
		  elsif (y >= 320) then ret:=to_signed(12, bits_res);
		  elsif (y >= 256) then ret:=to_signed(9, bits_res);
		  elsif (y >= 192) then ret:=to_signed(7, bits_res);
		  elsif (y >= 128) then ret:=to_signed(4, bits_res);
		  elsif (y >= 64) then ret:=to_signed(2, bits_res);
		  else ret:=(others => '0');
		  end if;
	  elsif (y < 2624) then
		  if (y >= 2560) then ret:=to_signed(83, bits_res);
		  elsif (y >= 2496) then ret:=to_signed(82, bits_res);
		  elsif (y >= 2432) then ret:=to_signed(80, bits_res);
		  elsif (y >= 2368) then ret:=to_signed(79, bits_res);
		  elsif (y >= 2304) then ret:=to_signed(77, bits_res);
		  elsif (y >= 2240) then ret:=to_signed(76, bits_res);
		  elsif (y >= 2176) then ret:=to_signed(74, bits_res);
		  elsif (y >= 2112) then ret:=to_signed(72, bits_res);
		  elsif (y >= 2048) then ret:=to_signed(70, bits_res);
		  elsif (y >= 1984) then ret:=to_signed(69, bits_res);
		  elsif (y >= 1920) then ret:=to_signed(67, bits_res);
		  elsif (y >= 1856) then ret:=to_signed(65, bits_res);
		  elsif (y >= 1792) then ret:=to_signed(63, bits_res);
		  elsif (y >= 1728) then ret:=to_signed(61, bits_res);
		  elsif (y >= 1664) then ret:=to_signed(59, bits_res);
		  elsif (y >= 1600) then ret:=to_signed(57, bits_res);
		  elsif (y >= 1536) then ret:=to_signed(55, bits_res);
		  elsif (y >= 1472) then ret:=to_signed(53, bits_res);
		  elsif (y >= 1408) then ret:=to_signed(51, bits_res);
		  else ret:=to_signed(49, bits_res);
		  end if;
	  elsif (y < 3986) then
		  if (y >= 3904) then ret:=to_signed(106, bits_res);
		  elsif (y >= 3840) then ret:=to_signed(105, bits_res);
		  elsif (y >= 3776) then ret:=to_signed(104, bits_res);
		  elsif (y >= 3648) then ret:=to_signed(103, bits_res);
		  elsif (y >= 3584) then ret:=to_signed(102, bits_res);
		  elsif (y >= 3520) then ret:=to_signed(101, bits_res);
		  elsif (y >= 3456) then ret:=to_signed(100, bits_res);
		  elsif (y >= 3392) then ret:=to_signed(99, bits_res);
		  elsif (y >= 3328) then ret:=to_signed(98, bits_res);
		  elsif (y >= 3264) then ret:=to_signed(97, bits_res);
		  elsif (y >= 3200) then ret:=to_signed(96, bits_res);
		  elsif (y >= 3136) then ret:=to_signed(95, bits_res);
		  elsif (y >= 3072) then ret:=to_signed(93, bits_res);
		  elsif (y >= 3008) then ret:=to_signed(92, bits_res);
		  elsif (y >= 2944) then ret:=to_signed(91, bits_res);
		  elsif (y >= 2880) then ret:=to_signed(90, bits_res);
		  elsif (y >= 2816) then ret:=to_signed(89, bits_res);
		  elsif (y >= 2752) then ret:=to_signed(87, bits_res);
		  elsif (y >= 2688) then ret:=to_signed(86, bits_res);
		  else ret:=to_signed(85, bits_res);
		  end if;
	  else
		  if (y >= 9088) then ret:=to_signed(127, bits_res);
		  elsif (y >= 8000) then ret:=to_signed(126, bits_res);
		  elsif (y >= 7296) then ret:=to_signed(125, bits_res);
		  elsif (y >= 6848) then ret:=to_signed(124, bits_res);
		  elsif (y >= 6464) then ret:=to_signed(123, bits_res);
		  elsif (y >= 6144) then ret:=to_signed(122, bits_res);
		  elsif (y >= 5888) then ret:=to_signed(121, bits_res);
		  elsif (y >= 5632) then ret:=to_signed(120, bits_res);
		  elsif (y >= 5440) then ret:=to_signed(119, bits_res);
		  elsif (y >= 5248) then ret:=to_signed(118, bits_res);
		  elsif (y >= 5120) then ret:=to_signed(117, bits_res);
		  elsif (y >= 4992) then ret:=to_signed(116, bits_res);
		  elsif (y >= 4800) then ret:=to_signed(115, bits_res);
		  elsif (y >= 4672) then ret:=to_signed(114, bits_res);
		  elsif (y >= 4608) then ret:=to_signed(113, bits_res);
		  elsif (y >= 4480) then ret:=to_signed(112, bits_res);
		  elsif (y >= 4352) then ret:=to_signed(111, bits_res);
		  elsif (y >= 4288) then ret:=to_signed(110, bits_res);
		  elsif (y >= 4160) then ret:=to_signed(109, bits_res);
		  elsif (y >= 4096) then ret:=to_signed(108, bits_res);
		  else ret:=to_signed(107, bits_res);
		  end if;
	  end if;
	elsif (bits_in_a = 17) then
	  if (y < 4865) then
		  if (y >= 4864) then ret:=to_signed(45, bits_res);
		  elsif (y >= 4608) then ret:=to_signed(43, bits_res);
		  elsif (y >= 4352) then ret:=to_signed(41, bits_res);
		  elsif (y >= 4096) then ret:=to_signed(38, bits_res);
		  elsif (y >= 3840) then ret:=to_signed(36, bits_res);
		  elsif (y >= 3584) then ret:=to_signed(34, bits_res);
		  elsif (y >= 3328) then ret:=to_signed(31, bits_res);
		  elsif (y >= 3072) then ret:=to_signed(29, bits_res);
		  elsif (y >= 2816) then ret:=to_signed(27, bits_res);
		  elsif (y >= 2560) then ret:=to_signed(24, bits_res);
		  elsif (y >= 2304) then ret:=to_signed(22, bits_res);
		  elsif (y >= 2048) then ret:=to_signed(19, bits_res);
		  elsif (y >= 1792) then ret:=to_signed(17, bits_res);
		  elsif (y >= 1536) then ret:=to_signed(14, bits_res);
		  elsif (y >= 1280) then ret:=to_signed(12, bits_res);
		  elsif (y >= 1024) then ret:=to_signed(9, bits_res);
		  elsif (y >= 768) then ret:=to_signed(7, bits_res);
		  elsif (y >= 512) then ret:=to_signed(4, bits_res);
		  elsif (y >= 256) then ret:=to_signed(2, bits_res);
		  else ret:=(others => '0');
		  end if;
	  elsif (y < 9985) then
		  if (y >= 9984) then ret:=to_signed(82, bits_res);
		  elsif (y >= 9728) then ret:=to_signed(80, bits_res);
		  elsif (y >= 9472) then ret:=to_signed(79, bits_res);
		  elsif (y >= 9216) then ret:=to_signed(77, bits_res);
		  elsif (y >= 8960) then ret:=to_signed(76, bits_res);
		  elsif (y >= 8704) then ret:=to_signed(74, bits_res);
		  elsif (y >= 8448) then ret:=to_signed(72, bits_res);
		  elsif (y >= 8192) then ret:=to_signed(70, bits_res);
		  elsif (y >= 7936) then ret:=to_signed(69, bits_res);
		  elsif (y >= 7680) then ret:=to_signed(67, bits_res);
		  elsif (y >= 7424) then ret:=to_signed(65, bits_res);
		  elsif (y >= 7168) then ret:=to_signed(63, bits_res);
		  elsif (y >= 6912) then ret:=to_signed(61, bits_res);
		  elsif (y >= 6656) then ret:=to_signed(59, bits_res);
		  elsif (y >= 6400) then ret:=to_signed(57, bits_res);
		  elsif (y >= 6144) then ret:=to_signed(55, bits_res);
		  elsif (y >= 5888) then ret:=to_signed(53, bits_res);
		  elsif (y >= 5632) then ret:=to_signed(51, bits_res);
		  elsif (y >= 5376) then ret:=to_signed(49, bits_res);
		  elsif (y >= 5120) then ret:=to_signed(47, bits_res);
		  end if;
	  elsif (y < 15361) then
		  if (y >= 15360) then ret:=to_signed(105, bits_res);
		  elsif (y >= 15104) then ret:=to_signed(104, bits_res);
		  elsif (y >= 14592) then ret:=to_signed(103, bits_res);
		  elsif (y >= 14336) then ret:=to_signed(102, bits_res);
		  elsif (y >= 14080) then ret:=to_signed(101, bits_res);
		  elsif (y >= 13824) then ret:=to_signed(100, bits_res);
		  elsif (y >= 13568) then ret:=to_signed(99, bits_res);
		  elsif (y >= 13312) then ret:=to_signed(98, bits_res);
		  elsif (y >= 13056) then ret:=to_signed(97, bits_res);
		  elsif (y >= 12800) then ret:=to_signed(96, bits_res);
		  elsif (y >= 12544) then ret:=to_signed(95, bits_res);
		  elsif (y >= 12288) then ret:=to_signed(93, bits_res);
		  elsif (y >= 12032) then ret:=to_signed(92, bits_res);
		  elsif (y >= 11776) then ret:=to_signed(91, bits_res);
		  elsif (y >= 11520) then ret:=to_signed(90, bits_res);
		  elsif (y >= 11264) then ret:=to_signed(89, bits_res);
		  elsif (y >= 11008) then ret:=to_signed(87, bits_res);
		  elsif (y >= 10752) then ret:=to_signed(86, bits_res);
		  elsif (y >= 10496) then ret:=to_signed(85, bits_res);
		  elsif (y >= 10240) then ret:=to_signed(83, bits_res);
		  end if;
	  else 	
		  if (y >= 36352) then ret:=to_signed(127, bits_res);
		  elsif (y >= 32000) then ret:=to_signed(126, bits_res);
		  elsif (y >= 29184) then ret:=to_signed(125, bits_res);
		  elsif (y >= 27392) then ret:=to_signed(124, bits_res);
		  elsif (y >= 25856) then ret:=to_signed(123, bits_res);
		  elsif (y >= 24576) then ret:=to_signed(122, bits_res);
		  elsif (y >= 23552) then ret:=to_signed(121, bits_res);
		  elsif (y >= 22528) then ret:=to_signed(120, bits_res);
		  elsif (y >= 21760) then ret:=to_signed(119, bits_res);
		  elsif (y >= 20992) then ret:=to_signed(118, bits_res);
		  elsif (y >= 20480) then ret:=to_signed(117, bits_res);
		  elsif (y >= 19968) then ret:=to_signed(116, bits_res);
		  elsif (y >= 19200) then ret:=to_signed(115, bits_res);
		  elsif (y >= 18688) then ret:=to_signed(114, bits_res);
		  elsif (y >= 18432) then ret:=to_signed(113, bits_res);
		  elsif (y >= 18176) then ret:=to_signed(112, bits_res);
		  elsif (y >= 17408) then ret:=to_signed(111, bits_res);
		  elsif (y >= 17152) then ret:=to_signed(110, bits_res);
		  elsif (y >= 16640) then ret:=to_signed(109, bits_res);
		  elsif (y >= 16384) then ret:=to_signed(108, bits_res);
		  elsif (y >= 15872) then ret:=to_signed(107, bits_res);
		  else ret:=to_signed(106, bits_res);
		  end if;
	  end if;
	end if;	  
	if (signed(x) < 0) then
		ret := resize((-1 * ret),bits_res);
	end if;
	return std_logic_vector(ret);
end function;

BEGIN
	PROCESS(execute)
	variable res   	: stOut_value;
	BEGIN
		-- Attribution
		if (operation = 0)THEN
			res := in_a;	
		END IF;
		-- Arithmetic operations
		if (operation = 1)THEN -- +
			res := std_logic_vector(signed(in_a)+signed(in_b));
		END IF;
		if (operation = 2)THEN -- -
			res := std_logic_vector(signed(in_a)-signed(in_b));	
		END IF;
		if (operation = 3)THEN -- *                                                 
--			res := std_logic_vector(signed(in_a)*signed(in_b))(bits_res-1 downto 0);	    -- schutz
			res := std_logic_vector(resize(signed(in_a)*signed(in_b),result'length));	 -- schutz
		END IF;                                                                   
		if (operation = 4)THEN -- /
			res := std_logic_vector(signed(in_a)/signed(in_b));	
		END IF;	
		if (operation = 5)THEN -- ABS (absolute value)
			res := std_logic_vector(ABS(signed(in_a)));	
		END IF;
		if (operation = 6)THEN -- REM (remainder of in_a / in_b with the signal of in_a)
			res := std_logic_vector(signed(in_a) REM signed(in_b));	
		END IF;
		if (operation = 7)THEN -- MOD (remainder of in_a / in_b with the signal of in_b)
			res := std_logic_vector(signed(in_a) MOD signed(in_b));	
		END IF;
		-- Logical operations
		if (operation = 8)THEN -- NOT
			res :=  NOT in_a;
		END IF;
		if (operation = 9)THEN -- AND
			res :=  in_a AND in_b;	
		END IF;
		if (operation = 10)THEN -- NAND
			res :=  in_a NAND in_b;
		END IF;
		if (operation = 11)THEN -- OR
			res :=  in_a OR in_b;	
		END IF;	
		if (operation = 12)THEN -- NOR
			res :=  in_a NOR in_b;	
		END IF;	
		if (operation = 13)THEN -- XOR
			res :=  in_a XOR in_b;
		END IF;	
		if (operation = 14)THEN -- XNOR
			res :=  in_a XNOR in_b;	
		END IF;	
		if (operation = 15)THEN -- SLL (shift left logical)
			res :=  to_stdlogicvector(to_bitvector(in_a) SLL  to_integer(unsigned(in_b)));
		END IF;
		if (operation = 16)THEN -- SRL (shift right logical)
			res :=  to_stdlogicvector(to_bitvector(in_a) SRL  to_integer(unsigned(in_b)));
		END IF;
		if (operation = 17)THEN -- SLA (shift left arithmetic)
			res :=  to_stdlogicvector(to_bitvector(in_a) SLA to_integer(unsigned(in_b)));
		END IF;
		if (operation = 18)THEN -- SRA (shift right arithmetic)
			res :=  to_stdlogicvector(to_bitvector(in_a) SRA to_integer(unsigned(in_b)));
		END IF;
		if (operation = 19)THEN -- ROL (rotate left)
			res :=  to_stdlogicvector(to_bitvector(in_a) ROL  to_integer(unsigned(in_b)));
		END IF;
		if (operation = 20)THEN -- ROR (rotate right)
			res :=  to_stdlogicvector(to_bitvector(in_a) ROR  to_integer(unsigned(in_b)));
		END IF;
		if (operation = 21) then
			res := hyTan(in_a);
		end if;
		if (operation = 22) then
			res := quantize(in_a);
		end if;
		--------------------------
		result <= res;
		notify<=execute;
	END PROCESS;	
END NOP_method_arq;
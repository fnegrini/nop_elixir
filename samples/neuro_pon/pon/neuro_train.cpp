/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   neuro_train.cpp
 * Author: FernandoSchutz
 *
 * Created on December 23, 2018, 10:00 PM
 */

#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <sys/time.h>
#include <fstream> 

using namespace std;

#define numInputs 2
#define numHidden 2
#define numOutputs 1
#define numPatterns 4

#define fTANH 1
#define fSoftMaxS 2      // softMax sum process
#define fSoftMax 3
#define fReLU 4

#define fHid fTANH
#define fOut fTANH

const int numEpochs = 15000;
const int minError = 25;  // (150 - 0.009)

const int LR = 10;     // ideia é dividir agora
const int alpha = 5; // momentum - exemplo: 2*0.8 = 2/1/8/10; pensar em valores grandes!
const float smallWT = 0.1;

//// Good values for XOR with TANH INTEGER version
//const int LR = 10;     // ideia é dividir agora
//const int alpha = 5; // momentum - exemplo: 2*0.8 = 2/1/8/10; pensar em valores grandes!
//const float smallWT = 0.1;

// Good values for XOR with ReLU
//const float LR = 0.01;
//const float alpha = 0.1; // momentum 
//const float smallWT = 2;

int randPatterns[numPatterns];
int weightsIH[numInputs][numHidden];
int weightsHO[numHidden][numOutputs];
int hiddenVal[numHidden];
int outputVal[numOutputs];

int hBiases[numHidden];
int oBiases[numOutputs];
int hSignals[numHidden];
int oSignals[numOutputs];
int ihPrevWeightsDelta[numInputs][numHidden];
int hoPrevWeightsDelta[numHidden][numOutputs];
int hPrevBiasesDelta[numHidden];
int oPrevBiasesDelta[numOutputs];

int trainInputs[numPatterns][numInputs];
int trainOutputs[numPatterns][numOutputs];

int  msePattern = 0.0;

FILE *value_file;

int hyperTan(int x, int inBits);
int actFunc(int func, int x, int z = 1);
int deractFunc(int func, int x, int outBits = 8);
void calcHidNeuron(int patNum);
void calcOutNeuron(int patNum);
void updateValues(int patNum);
int quantize(int number, int from, int to);

int actFunc(int func, int x, int z) {
    // temp to-do: o segundo parâmetro:
    // qtde de bits na Tanh
    // divisor no softmax
    int ret = 0;
    switch(func) {
      case fTANH : ret = hyperTan(x,z);
                   break;
      case fSoftMaxS : ret = trunc(exp(x));
                       break;
      case fSoftMax : ret = trunc(exp(x) / z);
                      break;
      case fReLU : ret = (x < 0 ? 0 : x);
                   break;
      default : ret = 0;
    }
    return ret;
}

int deractFunc(int func, int x, int outBits) {
    int ret = 0.0;
    int maxBits = pow(2,outBits-1);
    switch(func) {
      case fTANH : ret = (maxBits - (x*x));
                   break;
      case fSoftMax : ret = ((1 - x) * x);
                      break;
        case fReLU :  ret = (x < 0 ? 0 : 1);
                      break;
      default : ret = 0;
    }
    return ret;
}

int hyperTan(int x, int inBits){
    // máximo de quatro entradas
    int ret = abs(x);
    
    if (inBits == 16) {
        if (ret >= 9088) ret = 127;
        else if (ret >= 8000) ret = 126;
        else if (ret >= 7296) ret = 125;
        else if (ret >= 6848) ret = 124;
        else if (ret >= 6464) ret = 123;
        else if (ret >= 6144) ret = 122;
        else if (ret >= 5888) ret = 121;
        else if (ret >= 5632) ret = 120;
        else if (ret >= 5440) ret = 119;
        else if (ret >= 5248) ret = 118;
        else if (ret >= 5120) ret = 117;
        else if (ret >= 4992) ret = 116;
        else if (ret >= 4800) ret = 115;
        else if (ret >= 4672) ret = 114;
        else if (ret >= 4608) ret = 113;
        else if (ret >= 4480) ret = 112;
        else if (ret >= 4352) ret = 111;
        else if (ret >= 4288) ret = 110;
        else if (ret >= 4160) ret = 109;
        else if (ret >= 4096) ret = 108;
        else if (ret >= 3968) ret = 107;
        else if (ret >= 3904) ret = 106;
        else if (ret >= 3840) ret = 105;
        else if (ret >= 3776) ret = 104;
        else if (ret >= 3648) ret = 103;
        else if (ret >= 3584) ret = 102;
        else if (ret >= 3520) ret = 101;
        else if (ret >= 3456) ret = 100;
        else if (ret >= 3392) ret = 99;
        else if (ret >= 3328) ret = 98;
        else if (ret >= 3264) ret = 97;
        else if (ret >= 3200) ret = 96;
        else if (ret >= 3136) ret = 95;
        else if (ret >= 3072) ret = 93;
        else if (ret >= 3008) ret = 92;
        else if (ret >= 2944) ret = 91;
        else if (ret >= 2880) ret = 90;
        else if (ret >= 2816) ret = 89;
        else if (ret >= 2752) ret = 87;
        else if (ret >= 2688) ret = 86;
        else if (ret >= 2624) ret = 85;
        else if (ret >= 2560) ret = 83;
        else if (ret >= 2496) ret = 82;
        else if (ret >= 2432) ret = 80;
        else if (ret >= 2368) ret = 79;
        else if (ret >= 2304) ret = 77;
        else if (ret >= 2240) ret = 76;
        else if (ret >= 2176) ret = 74;
        else if (ret >= 2112) ret = 72;
        else if (ret >= 2048) ret = 70;
        else if (ret >= 1984) ret = 69;
        else if (ret >= 1920) ret = 67;
        else if (ret >= 1856) ret = 65;
        else if (ret >= 1792) ret = 63;
        else if (ret >= 1728) ret = 61;
        else if (ret >= 1664) ret = 59;
        else if (ret >= 1600) ret = 57;
        else if (ret >= 1536) ret = 55;
        else if (ret >= 1472) ret = 53;
        else if (ret >= 1408) ret = 51;
        else if (ret >= 1344) ret = 49;
        else if (ret >= 1280) ret = 47;
        else if (ret >= 1216) ret = 45;
        else if (ret >= 1152) ret = 43;
        else if (ret >= 1088) ret = 41;
        else if (ret >= 1024) ret = 38;
        else if (ret >= 960) ret = 36;
        else if (ret >= 896) ret = 34;
        else if (ret >= 832) ret = 31;
        else if (ret >= 768) ret = 29;
        else if (ret >= 704) ret = 27;
        else if (ret >= 640) ret = 24;
        else if (ret >= 576) ret = 22;
        else if (ret >= 512) ret = 19;
        else if (ret >= 448) ret = 17;
        else if (ret >= 384) ret = 14;
        else if (ret >= 320) ret = 12;
        else if (ret >= 256) ret = 9;
        else if (ret >= 192) ret = 7;
        else if (ret >= 128) ret = 4;
        else if (ret >= 64) ret = 2;
        else ret = 0;
    }
    if (inBits == 18) {
        if (ret >= 36352) ret = 127;
        else if (ret >= 32000) ret = 126;
        else if (ret >= 29184) ret = 125;
        else if (ret >= 27392) ret = 124;
        else if (ret >= 25856) ret = 123;
        else if (ret >= 24576) ret = 122;
        else if (ret >= 23552) ret = 121;
        else if (ret >= 22528) ret = 120;
        else if (ret >= 21760) ret = 119;
        else if (ret >= 20992) ret = 118;
        else if (ret >= 20480) ret = 117;
        else if (ret >= 19968) ret = 116;
        else if (ret >= 19200) ret = 115;
        else if (ret >= 18688) ret = 114;
        else if (ret >= 18432) ret = 113;
        else if (ret >= 18176) ret = 112;
        else if (ret >= 17408) ret = 111;
        else if (ret >= 17152) ret = 110;
        else if (ret >= 16640) ret = 109;
        else if (ret >= 16384) ret = 108;
        else if (ret >= 15872) ret = 107;
        else if (ret >= 15616) ret = 106;
        else if (ret >= 15360) ret = 105;
        else if (ret >= 15104) ret = 104;
        else if (ret >= 14592) ret = 103;
        else if (ret >= 14336) ret = 102;
        else if (ret >= 14080) ret = 101;
        else if (ret >= 13824) ret = 100;
        else if (ret >= 13568) ret = 99;
        else if (ret >= 13312) ret = 98;
        else if (ret >= 13056) ret = 97;
        else if (ret >= 12800) ret = 96;
        else if (ret >= 12544) ret = 95;
        else if (ret >= 12288) ret = 93;
        else if (ret >= 12032) ret = 92;
        else if (ret >= 11776) ret = 91;
        else if (ret >= 11520) ret = 90;
        else if (ret >= 11264) ret = 89;
        else if (ret >= 11008) ret = 87;
        else if (ret >= 10752) ret = 86;
        else if (ret >= 10496) ret = 85;
        else if (ret >= 10240) ret = 83;
        else if (ret >= 9984) ret = 82;
        else if (ret >= 9728) ret = 80;
        else if (ret >= 9472) ret = 79;
        else if (ret >= 9216) ret = 77;
        else if (ret >= 8960) ret = 76;
        else if (ret >= 8704) ret = 74;
        else if (ret >= 8448) ret = 72;
        else if (ret >= 8192) ret = 70;
        else if (ret >= 7936) ret = 69;
        else if (ret >= 7680) ret = 67;
        else if (ret >= 7424) ret = 65;
        else if (ret >= 7168) ret = 63;
        else if (ret >= 6912) ret = 61;
        else if (ret >= 6656) ret = 59;
        else if (ret >= 6400) ret = 57;
        else if (ret >= 6144) ret = 55;
        else if (ret >= 5888) ret = 53;
        else if (ret >= 5632) ret = 51;
        else if (ret >= 5376) ret = 49;
        else if (ret >= 5120) ret = 47;
        else if (ret >= 4864) ret = 45;
        else if (ret >= 4608) ret = 43;
        else if (ret >= 4352) ret = 41;
        else if (ret >= 4096) ret = 38;
        else if (ret >= 3840) ret = 36;
        else if (ret >= 3584) ret = 34;
        else if (ret >= 3328) ret = 31;
        else if (ret >= 3072) ret = 29;
        else if (ret >= 2816) ret = 27;
        else if (ret >= 2560) ret = 24;
        else if (ret >= 2304) ret = 22;
        else if (ret >= 2048) ret = 19;
        else if (ret >= 1792) ret = 17;
        else if (ret >= 1536) ret = 14;
        else if (ret >= 1280) ret = 12;
        else if (ret >= 1024) ret = 9;
        else if (ret >= 768) ret = 7;
        else if (ret >= 512) ret = 4;
        else if (ret >= 256) ret = 2;
        else ret = 0;
    }
    /*
    if (ret > 1696) ret = 31;
    else if (ret > 1376) ret = 30;
    else if (ret > 1248) ret = 29;
    else if (ret > 1120) ret = 28;
    else if (ret > 992) ret = 27;
    else if (ret > 928) ret = 26;
    else if (ret > 864) ret = 25;
    else if (ret > 800) ret = 24;
    else if (ret > 736) ret = 23;
    else if (ret > 672) ret = 22;
    else if (ret > 608) ret = 20;
    else if (ret > 544) ret = 19;
    else if (ret > 480) ret = 17;
    else if (ret > 416) ret = 15;
    else if (ret > 352) ret = 13;
    else if (ret > 288) ret = 11;
    else if (ret > 224) ret = 9;
    else if (ret > 160) ret = 7;
    else if (ret > 96) ret = 4;
    else if (ret > 32) ret = 2;
*/
    if (x < 0) ret = -1 * ret;
    return ret;
}

void calcHidNeuron(int patNum){
    int hSums[numHidden];
    for (int i = 0;i < numHidden;i++) hSums[i] = 0;
    for (int j = 0;j < numHidden; j++){
        for (int i = 0;i < numInputs;i++){
            hSums[j] += trainInputs[patNum][i] * weightsIH[i][j];
        }
        hSums[j] += hBiases[j]; 
        hiddenVal[j] = actFunc(fHid,hSums[j],16);
        fprintf(value_file,"MAC%d: %d; hiddenVal%d: %d;\n",j,hSums[j],j,hiddenVal[j]);
    }
}

void calcOutNeuron(int patNum){               // compute outputs
    int oSums[numOutputs];
    int errSum = 0.0;
    for (int i = 0;i < numOutputs;i++) oSums[i] = 0;
    for (int k = 0;k < numOutputs; k++){
        for (int j = 0;j < numHidden;j++){
            oSums[k] += hiddenVal[j] * weightsHO[j][k];
        }
        oSums[k] += oBiases[k]; 
    }
    for (int k = 0;k < numOutputs;k++){
        outputVal[k] = actFunc(fOut,oSums[k],18);
        fprintf(value_file,"MAC%d: %d; outputVal%d: %d;pred: %d;\n",k,oSums[k],k,outputVal[k],quantize(trainOutputs[patNum][k],6,8));
        errSum += (quantize(trainOutputs[patNum][k],6,8) - outputVal[k]) * (quantize(trainOutputs[patNum][k],6,8) - outputVal[k]);
        printf("%3d;%3d;",quantize(trainOutputs[patNum][k],6,8),outputVal[k]);
    }
    msePattern += trunc(errSum/2);
//    printf("%5d ",msePattern);
    fprintf(value_file,"error%d: %d;\n",patNum,msePattern);    
}

int quantize(int number, int from, int to){
    int divident = pow(2,to-1)-1;
    int divisor = pow(2,from-1)-1;
    float merda = float(divident) / float(divisor);
    int ret = trunc(number * merda);
       
//    int high = pow(2,power);
//    int ret = number/high;
//    if ((number % high) >= (high / 2)) ret = ret + 1;
    return (ret);
}

void updateValues(int patNum){
    int oProdSum = 0;
    int delta = 0;
    int aux = 0;
    int aux2 = 0;
    int dif = 0;
    int dif2 = 0;
    for (int k = 0;k < numOutputs;k++){
        aux = deractFunc(fOut,outputVal[k],15);
        dif = quantize(trainOutputs[patNum][k],6,8);
        dif = dif - outputVal[k];
        oSignals[k] = dif * aux;  
        fprintf(value_file,"dif%d: %d;Derivative: %d;oSignals%d: %d;\n",patNum,dif,aux,k,oSignals[k]);
        for (int j = 0;j < numHidden;j++){
            oProdSum += quantize(oSignals[k] * weightsHO[j][k],31,16);
            fprintf(value_file,"oProdSum%d: %d;",j,oProdSum);
            delta = oSignals[k] * hiddenVal[j];
            fprintf(value_file,"delta%d: %d;",j,delta);
            delta = trunc(delta / LR);
            fprintf(value_file,"delta%d: %d;",j,delta);
            weightsHO[j][k] += quantize(delta,30,10);
            fprintf(value_file,"weightHO%d%d: %d;\n",j,k,weightsHO[j][k]);
//            dif2 = weightsHO[j][k] - hoPrevWeightsDelta[j][k];
//            dif2 = ceil(dif2 / alpha);
//            aux2 = quantize(delta,30,10);
//            printf("[d:%d;%d;]",aux2,dif2);
//            weightsHO[j][k] -= dif2;
//            hoPrevWeightsDelta[j][k] = weightsHO[j][k];
        }
        delta = quantize(oSignals[k] / LR,22,10);
        oBiases[k] += delta;
    }    

    for (int j = 0;j < numHidden;j++){
        aux = deractFunc(fHid,hiddenVal[j],15);
        hSignals[j] = quantize(aux * oProdSum,30,16);
        fprintf(value_file,"Derivative%d: %d;oSignals%d: %d;\n",j,aux,j,hSignals[j]);
        for (int i = 0;i < numInputs;i++){
            delta = hSignals[j] * trainInputs[patNum][i];
            fprintf(value_file,"trainInput%d: %d;delta%d: %d;",i,trainInputs[patNum][i],j,delta);
            delta = trunc(delta / LR); 
            fprintf(value_file,"delta%d: %d;",j,delta);            
            weightsIH[i][j] += quantize(delta,22,10);
            fprintf(value_file,"weightIH%d%d: %d;\n",i,j,weightsIH[i][j]);
//            dif2 = weightsIH[i][j] - ihPrevWeightsDelta[i][j];
//            dif2 = ceil(dif2 / alpha);
//            weightsIH[i][j] -= dif2;
//            ihPrevWeightsDelta[i][j] = weightsIH[i][j];
        }
        delta = quantize(hSignals[j] / LR,30,10);
        hBiases[j] += delta;
    }
}

int rnd(int limit){
//    return trunc(rand() % limit);  // somente positivos
    return ceil(smallWT * ((rand() % (limit*2) ) - limit) );
}

void initializeWeights(int bits){
    int power;
    power = pow(2,bits-1);
    printf("Weights input-hidden: \n");    
    for (int j = 0;j < numHidden;j++){
        for (int i = 0;i < numInputs;i++){ 
            weightsIH[i][j] = rnd(power);
            ihPrevWeightsDelta[i][j] = weightsIH[i][j]+1;
//            weightsIH[i][j] = 52;
            printf("%4d;",weightsIH[i][j]);
        }
        hBiases[j] = rnd(power);
//        hBiases[j] = 52;
    }
    printf("\nWeights hidden-output: \n");
    for (int k = 0;k < numOutputs;k++){
        for (int j = 0;j < numHidden;j++){
            weightsHO[j][k] = rnd(power);
            hoPrevWeightsDelta[j][k] = weightsHO[j][k]+1;
//            weightsHO[j][k] = 52;
            printf("%4d;",weightsHO[j][k]);
        }
//        oBiases[k] = 52;
        oBiases[k] = rnd(power);
    }
    printf("\n");
}

void randomPatterns(void){
    int aux = 0;
    for (int p = 0;p < numPatterns;p++){
        randPatterns[p] = p;
    }
//    for (int p = numPatterns - 1;p > 0;p--){
//        int np = (unsigned int)(rand() % numPatterns);
//        aux = randPatterns[np];
//        randPatterns[np] = randPatterns[p];
//        randPatterns[p] = aux;
//    }
}

int getPattern(string archive, int totPat){
    printf("Training patterns: \n");
    ifstream file;
    string slice,text;
    size_t *sz = 0;
    int comma = 0;
    file.open(archive,ifstream::in);
    if (!file.is_open()){
        return 1;
    } else {
        for (int np = 0;np < totPat;np++){
//            fscanf(file,"%s\n",&file_line);
            getline(file,text);
            for (int pos = 0;pos < (numInputs+numOutputs);pos++){
                comma = text.find_first_of(",");
                slice = text.substr(0,comma);
                if (pos < numInputs) { 
                    trainInputs[np][pos] = stof(slice);
                    printf("%4d; ",trainInputs[np][pos]);
                } else {
                    trainOutputs[np][pos-numInputs] = stoi(slice);
                    printf("%d; ",trainOutputs[np][pos-numInputs]);
                }
                text.erase(0,comma+1);
            }
            printf("\n");
        }
        file.close();
    }
    return 0;
}

int saveDataFT(string archive) {     // ne - numEpochs; oe - OverallError
    FILE *file;
    file = fopen(archive.c_str(),"w+");
    if (file == NULL){
        return 1;
    } else {
        fprintf(file,"WIH(%d,%d);",numInputs,numHidden);
        for (int j = 0;j < numHidden;j++)
            for (int i = 0;i < numInputs;i++)
                fprintf(file,"%d;",weightsIH[i][j]);

        fprintf(file,"\nWHB(%d);",numHidden);
        for (int j = 0;j < numHidden;j++)
            fprintf(file,"%d;",hBiases[j]);

        fprintf(file,"\nWHO(%d,%d);",numHidden,numOutputs);
        for (int k = 0;k < numOutputs;k++)
            for (int j = 0;j < numHidden;j++)
                fprintf(file,"%d;",weightsHO[j][k]);

        fprintf(file,"\nWOB(%d);",numOutputs);
        for (int k = 0;k < numOutputs;k++)
            fprintf(file,"%d;",oBiases[k]);

        fclose(file);
    }
    return 0;    
}
int saveData(string archive, int ne, int oe) {     // ne - numEpochs; oe - OverallError
    FILE *file;
    file = fopen(archive.c_str(),"w+");
    if (file == NULL){
        return 1;
    } else {
        fprintf(file,"Params - LR: %d; ME: %d; A: %d; SW: %d; \n",LR,minError,alpha,smallWT);
        fprintf(file,"Epochs: %d; Overall Error: %d;\n",ne,oe);
        fprintf(file,"Weights Input-Hidden (i,h) - (%d,%d): \n",numInputs,numHidden);
        for (int j = 0;j < numHidden;j++)
            for (int i = 0;i < numInputs;i++)
                fprintf(file,"%d;",weightsIH[i][j]);

        fprintf(file,"\nWeights Hidden Biases (%d): \n",numHidden);
        for (int j = 0;j < numHidden;j++)
            fprintf(file,"%d;",hBiases[j]);

        fprintf(file,"\nWeights Hidden-Output (h,o) - (%d,%d): \n",numHidden,numOutputs);
        for (int k = 0;k < numOutputs;k++)
            for (int j = 0;j < numHidden;j++)
                fprintf(file,"%d;",weightsHO[j][k]);

        fprintf(file,"\nWeights Output Biases (%d): \n",numOutputs);
        for (int k = 0;k < numOutputs;k++)
            fprintf(file,"%d;",oBiases[k]);

        fclose(file);
    }
    return 0;    
}

/*
 * 
 */
int main(int argc, char** argv) {
    int contEpochs = 1;
    int overallError = 0.0;
    FILE *archive;
    archive = fopen("error.txt","w+");
    value_file = fopen("values.txt","w+");
    srand(time(NULL));
    initializeWeights(10);
    randomPatterns();
    if (getPattern("XORDataIN.txt",numPatterns) == 1) {
        printf("probrema");
    } else {
        do {
            randomPatterns();
//                calcHidNeuron(3);
//                calcOutNeuron(3);
//                updateValues(3);
            for (int contPattern = 0;contPattern < numPatterns;contPattern++){
                calcHidNeuron(randPatterns[contPattern]);
                calcOutNeuron(randPatterns[contPattern]);
                updateValues(randPatterns[contPattern]);
            }
            overallError = trunc(msePattern / numPatterns);
            msePattern = 0;
            contEpochs++;
            printf("Overall error in epoch %5d: %d;\n",contEpochs,overallError);
            fprintf(archive,"%d;%d;\n",contEpochs,overallError);
        } while ((contEpochs <= numEpochs) && (overallError > minError));
        saveData("XORDataOUT.txt",contEpochs,overallError);
        saveDataFT("XORDataFT.txt");
    }
    fclose(value_file);
    fclose(archive);
    return (EXIT_SUCCESS);
}


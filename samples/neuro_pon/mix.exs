defmodule NeuroPon.MixProject do
  use Mix.Project

  def project do
    [
      app: :neuro_pon,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :nop_elixir]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nop_elixir, "~> 0.0.24"}
    ]
  end

end

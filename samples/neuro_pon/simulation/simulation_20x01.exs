# Run in iex:
# c "./simulation/simulation_20x01.exs"

NOP.Application.reset_element_list_total()
{time_in_microseconds, _ret_val} =  :timer.tc(
  fn ->
    NopEx.Utils.simulate(20,1)
    NOP.Application.wait_up_to_end_all_process()
  end)

IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
IO.puts("The gross time is #{NOP.Application.get_statistics_from_total()}")

defmodule CarlTest do
  use ExUnit.Case
  doctest Carl

  test "greets the world" do
    assert Carl.hello() == :world
  end

  test "Creating FBE carl" do

    carl =  NOP.Service.FBE.create_fbe(NOP.Carl.FBE, "FBE.Carl")

    assert NOP.Service.FBE.get_name(carl) == "FBE.Carl"

  end

  test "FBE carl linking to premise" do

    carl =  NOP.Service.FBE.create_fbe(NOP.Carl.FBE, "FBE.Carl")

    premise = NOP.Service.Premise.create_premise("Premise.Is_no", carl, :answer, :EQ, :no)

    NOP.Service.FBE.set_attribute(carl, :answer, :no)

    :timer.sleep(100)

    assert NOP.Service.Premise.evaluate_premise(premise) == true

  end

  test "FBE carl linking to premise and condition" do

    carl =  NOP.Service.FBE.create_fbe(NOP.Carl.FBE, "FBE.Carl")

    premise = NOP.Service.Premise.create_premise(
      "Premise.Is_no", carl, :answer, :EQ, :no)

    condition1 = NOP.Service.Condition.create_condition(
      "Condition.Always_true", [], "true")

    condition2 = NOP.Service.Condition.create_condition(
      "Condition.Expression",
      [premise, condition1],
      "( Premise.Is_no and Condition.Always_true)")

    NOP.Service.FBE.set_attribute(carl, :answer, :no)

    :timer.sleep(100)

    assert NOP.Service.Condition.evaluate_condition(condition2) == true

  end

  test "Create FBE rule and testing Rule" do

    carl  = NOP.Service.FBE.create_fbe(NOP.Carl.FBE, "FBE.Carl")

    NOP.Service.Rule.create_rule(NOP.Carl.RuleNo,
      "Rule.Carl.No",  [carl])

    NOP.Service.FBE.set_attribute(carl, :answer, :no)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(carl, :answer) == :yes

  end

  test "Create total envirment" do

    carl = Carl.init("Carl")

    NOP.Service.FBE.set_attribute(carl, :answer, :no)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(carl, :answer) == :yes

  end

  test "Change my mind test" do

    carl = Carl.init("Carl")

    NOP.Carl.FBE.change_my_mind(carl)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(carl, :answer) == :yes

  end
end

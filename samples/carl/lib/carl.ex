defmodule Carl do
  @moduledoc """
  Carl's Yes Man! simulation test.
  """

  @doc """
  Carl's Yes Man! simulation test.

  ## Examples

  #    iex> Resposta para o exercício é:
    NOP.Application.reset_element_list_total()
    {time_in_microseconds, _ret_val} =  :timer.tc(
      fn ->
        Carl.run_stress_multi("Carl", 1000, 5000)
        NOP.Application.wait_up_to_end_all_process()
      end)

    #The time spent is div(time_in_microseconds,1000)

  """

  def init(name) do
    carl  = NOP.Service.FBE.create_fbe(NOP.Carl.FBE, "FBE." <> name)

    NOP.Service.Rule.create_rule(NOP.Carl.RuleNo,
      "Rule." <> name,  [carl])

    carl
  end

  defp init_multi_int(_name, 0, list) do
    list
  end

  defp init_multi_int(name, count, list) do
    carl = init( name <> to_string(count))
    init_multi_int(name, count-1, list ++ [carl])
  end

  def init_multi(name, count) do
    init_multi_int(name, count, [])
  end

  def set_answer_to_no(carl, count) do
    Enum.map(1..count,
      fn(_x) ->
        NOP.Service.FBE.set_attribute(carl, :answer, :no)
        #:timer.sleep(100)
       end)
  end

  def run_stress(carl, count) do
    #Start in separated task
    {:ok, pid} = Task.start(Carl, :set_answer_to_no, [carl, count])
    pid
  end

  def run_stress_list([], _count) do
    #stop
  end

  def run_stress_list([carl | carl_list], count) do
    run_stress(carl, count)
    run_stress_list(carl_list, count)
  end

  def run_stress_multi(name, count_fbe, count_set) do
    carl_list = init_multi(name, count_fbe)
    run_stress_list(carl_list, count_set)
  end

  def hello do
    :world
  end
end

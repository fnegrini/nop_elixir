defmodule NOP.Carl.FBE do
  use NOP.Element.FBE

  defp int_attributes() do
    %{:answer => :yes}
  end

  def change_to_yes(carl) do
    NOP.Service.FBE.set_attribute(carl, :answer, :yes)
  end

  def change_my_mind(carl) do
    NOP.Service.FBE.set_attribute_expr(carl, :answer, "if @answer == :yes do :no else :yes end")
  end
end

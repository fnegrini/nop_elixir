defmodule NOP.Carl.RuleNo do
  use NOP.Element.Rule

  defp create_element_list([carl]) do

    name = NOP.Service.FBE.get_name(carl)

    premise_no = NOP.Service.Premise.create_premise(
      "Premise." <> name , carl, :answer, :EQ, :no)

    [premise_no]

  end

  defp create_instigation_list([carl]) do

    [{NOP.Carl.FBE, :change_to_yes, [carl]}]

  end

end

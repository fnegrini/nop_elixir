# Run in iex interface:
# c "./statistics/stat_2000.exs"

NOP.Application.reset_element_list_total()
{time_in_microseconds, _ret_val} =  :timer.tc(
  fn ->
    Carl.run_stress_multi("Carl", 1000, 2000)
    NOP.Application.wait_up_to_end_all_process()
  end)

IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
IO.puts("The notification count is #{NOP.Application.get_statistics_from_total()}")

defmodule NopExTest do
  use ExUnit.Case
  doctest NopEx

  test "Run 2000 simulation" do

    NOP.Application.reset_element_list_total()
    {time_in_microseconds, _ret_val} =  :timer.tc(
      fn ->
        NopEx.mtSIMULATE(nil, 2000)
        NOP.Application.wait_up_to_end_all_process()
      end)

    IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
    IO.puts("The notification count is #{NOP.Application.get_statistics_from_total()}")

    assert True
  end
end

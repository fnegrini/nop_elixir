defmodule NopEx.Semaphore_NOP.RlCBCL10 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSecondsCBCL6 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL10.PrSecondsCBCL6", this, :atSeconds, :EQ, 6)

    prSemaphoreStateCBCL6 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL10.PrSemaphoreStateCBCL6", this, :atSemaphoreState, :EQ, 9)

    sbCBCL10 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL10.SbCBCL10",  [prSecondsCBCL6, prSemaphoreStateCBCL6, ],"( NopEx.Semaphore_NOP.RlCBCL10.PrSecondsCBCL6 and NopEx.Semaphore_NOP.RlCBCL10.PrSemaphoreStateCBCL6 )")

    [sbCBCL10,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLR, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

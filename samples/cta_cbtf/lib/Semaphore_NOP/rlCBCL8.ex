defmodule NopEx.Semaphore_NOP.RlCBCL8 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSecondsCBCL5 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL8.PrSecondsCBCL5", this, :atSeconds, :EQ, 30)

    prSemaphoreStateCBCL5 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL8.PrSemaphoreStateCBCL5", this, :atSemaphoreState, :EQ, 8)

    sbCBCL8 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL8.SbCBCL8",  [prSecondsCBCL5, prSemaphoreStateCBCL5, ],"( NopEx.Semaphore_NOP.RlCBCL8.PrSecondsCBCL5 and NopEx.Semaphore_NOP.RlCBCL8.PrSemaphoreStateCBCL5 )")

    [sbCBCL8,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLY, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

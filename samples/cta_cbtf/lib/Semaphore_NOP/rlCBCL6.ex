defmodule NopEx.Semaphore_NOP.RlCBCL6 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSemaphoreState4 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL6.PrSemaphoreState4", this, :atSemaphoreState, :EQ, 2)

    pratSeconds4 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL6.PratSeconds4", this, :atSeconds, :EQ, 2)

    sbCBCL6 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL6.SbCBCL6",  [prSemaphoreState4, pratSeconds4, ],"( NopEx.Semaphore_NOP.RlCBCL6.PrSemaphoreState4 and NopEx.Semaphore_NOP.RlCBCL6.PratSeconds4 )")

    [sbCBCL6,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLG, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

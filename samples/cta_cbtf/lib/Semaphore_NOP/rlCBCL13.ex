defmodule NopEx.Semaphore_NOP.RlCBCL13 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds8 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL13.PrSeconds8", this, :atSeconds, :GE, 18)

    prSecondsSup8 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL13.PrSecondsSup8", this, :atSeconds, :LT, 32)

    prSemaphoreState8 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL13.PrSemaphoreState8", this, :atSemaphoreState, :EQ, 0)

    prVehicleSensorState8 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL13.PrVehicleSensorState8", this, :atVVSS, :EQ, 1)

    sbCBCL13 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL13.SbCBCL13",  [prSeconds8, prSecondsSup8, prSemaphoreState8, prVehicleSensorState8, ],"( NopEx.Semaphore_NOP.RlCBCL13.PrSeconds8 and NopEx.Semaphore_NOP.RlCBCL13.PrSecondsSup8 and NopEx.Semaphore_NOP.RlCBCL13.PrSemaphoreState8 and NopEx.Semaphore_NOP.RlCBCL13.PrVehicleSensorState8 )")

    [sbCBCL13,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLYCBCL, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

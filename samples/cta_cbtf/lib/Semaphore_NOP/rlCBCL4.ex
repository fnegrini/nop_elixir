defmodule NopEx.Semaphore_NOP.RlCBCL4 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSemaphoreState3 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL4.PrSemaphoreState3", this, :atSemaphoreState, :EQ, 1)

    pratSeconds3 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL4.PratSeconds3", this, :atSeconds, :EQ, 5)

    sbCBCL4 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL4.SbCBCL4",  [prSemaphoreState3, pratSeconds3, ],"( NopEx.Semaphore_NOP.RlCBCL4.PrSemaphoreState3 and NopEx.Semaphore_NOP.RlCBCL4.PratSeconds3 )")

    [sbCBCL4,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLR, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

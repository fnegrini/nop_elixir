defmodule NopEx.Semaphore_NOP.RlCBCL14 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds8Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL14.PrSeconds8Full", this, :atSeconds, :GE, 18)

    prSecondsSup8Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL14.PrSecondsSup8Full", this, :atSeconds, :LT, 32)

    prSemaphoreState8Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL14.PrSemaphoreState8Full", this, :atSemaphoreState, :EQ, 0)

    prVehicleSensorState8Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL14.PrVehicleSensorState8Full", this, :atVVSS, :EQ, 2)

    sbCBCL14 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL14.SbCBCL14",  [prSeconds8Full, prSecondsSup8Full, prSemaphoreState8Full, prVehicleSensorState8Full, ],"( NopEx.Semaphore_NOP.RlCBCL14.PrSeconds8Full and NopEx.Semaphore_NOP.RlCBCL14.PrSecondsSup8Full and NopEx.Semaphore_NOP.RlCBCL14.PrSemaphoreState8Full and NopEx.Semaphore_NOP.RlCBCL14.PrVehicleSensorState8Full )")

    [sbCBCL14,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLYCBCL, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

defmodule NopEx.Semaphore_NOP.RlCBCL17 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds10 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL17.PrSeconds10", this, :atSeconds, :GE, 18)

    prSeconds210 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL17.PrSeconds210", this, :atSeconds, :LT, 32)

    prSemaphoreState10 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL17.PrSemaphoreState10", this, :atSemaphoreState, :EQ, 3)

    prVehicleSensorState10 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL17.PrVehicleSensorState10", this, :atHVSS, :EQ, 1)

    sbCBCL17 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL17.SbCBCL17",  [prSeconds10, prSeconds210, prSemaphoreState10, prVehicleSensorState10, ],"( NopEx.Semaphore_NOP.RlCBCL17.PrSeconds10 and NopEx.Semaphore_NOP.RlCBCL17.PrSeconds210 and NopEx.Semaphore_NOP.RlCBCL17.PrSemaphoreState10 and NopEx.Semaphore_NOP.RlCBCL17.PrVehicleSensorState10 )")

    [sbCBCL17,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLYCBCL, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

defmodule NopEx.Semaphore_NOP.RlCBCL7 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSemaphoreState5 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL7.PrSemaphoreState5", this, :atSemaphoreState, :EQ, 3)

    pratSeconds5 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL7.PratSeconds5", this, :atSeconds, :EQ, 38)

    sbCBCL7 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL7.SbCBCL7",  [prSemaphoreState5, pratSeconds5, ],"( NopEx.Semaphore_NOP.RlCBCL7.PrSemaphoreState5 and NopEx.Semaphore_NOP.RlCBCL7.PratSeconds5 )")

    [sbCBCL7,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLY, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

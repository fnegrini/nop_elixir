defmodule NopEx.Semaphore_NOP.RlCBCL15 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds9 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL15.PrSeconds9", this, :atSeconds, :LE, 17)

    prSemaphoreState9 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL15.PrSemaphoreState9", this, :atSemaphoreState, :EQ, 3)

    prVehicleSensorState9 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL15.PrVehicleSensorState9", this, :atVVSS, :EQ, 1)

    sbCBCL15 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL15.SbCBCL15",  [prSeconds9, prSemaphoreState9, prVehicleSensorState9, ],"( NopEx.Semaphore_NOP.RlCBCL15.PrSeconds9 and NopEx.Semaphore_NOP.RlCBCL15.PrSemaphoreState9 and NopEx.Semaphore_NOP.RlCBCL15.PrVehicleSensorState9 )")

    [sbCBCL15,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLGCBCL, [this]},
    ]

  end

end

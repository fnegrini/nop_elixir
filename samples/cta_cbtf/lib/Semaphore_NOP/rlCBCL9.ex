defmodule NopEx.Semaphore_NOP.RlCBCL9 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSemaphoreState6 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL9.PrSemaphoreState6", this, :atSemaphoreState, :EQ, 4)

    pratSeconds6 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL9.PratSeconds6", this, :atSeconds, :EQ, 5)

    sbCBCL9 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL9.SbCBCL9",  [prSemaphoreState6, pratSeconds6, ],"( NopEx.Semaphore_NOP.RlCBCL9.PrSemaphoreState6 and NopEx.Semaphore_NOP.RlCBCL9.PratSeconds6 )")

    [sbCBCL9,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLR, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

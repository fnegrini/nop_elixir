defmodule NopEx.Semaphore_NOP.RlCBCL2 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds2 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL2.PrSeconds2", this, :atSeconds, :EQ, 38)

    prSemaphoreState2 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL2.PrSemaphoreState2", this, :atSemaphoreState, :EQ, 0)

    sbCBCL2 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL2.SbCBCL2",  [prSeconds2, prSemaphoreState2, ],"( NopEx.Semaphore_NOP.RlCBCL2.PrSeconds2 and NopEx.Semaphore_NOP.RlCBCL2.PrSemaphoreState2 )")

    [sbCBCL2,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLY, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

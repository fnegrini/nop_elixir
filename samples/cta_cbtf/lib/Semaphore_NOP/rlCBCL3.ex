defmodule NopEx.Semaphore_NOP.RlCBCL3 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSecondsCBCL2 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL3.PrSecondsCBCL2", this, :atSeconds, :EQ, 30)

    prSemaphoreStateCBCL2 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL3.PrSemaphoreStateCBCL2", this, :atSemaphoreState, :EQ, 6)

    sbCBCL3 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL3.SbCBCL3",  [prSecondsCBCL2, prSemaphoreStateCBCL2, ],"( NopEx.Semaphore_NOP.RlCBCL3.PrSecondsCBCL2 and NopEx.Semaphore_NOP.RlCBCL3.PrSemaphoreStateCBCL2 )")

    [sbCBCL3,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLY, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

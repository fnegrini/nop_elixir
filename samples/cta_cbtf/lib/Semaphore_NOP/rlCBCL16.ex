defmodule NopEx.Semaphore_NOP.RlCBCL16 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds9Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL16.PrSeconds9Full", this, :atSeconds, :LE, 17)

    prSemaphoreState9Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL16.PrSemaphoreState9Full", this, :atSemaphoreState, :EQ, 3)

    prVehicleSensorState9Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL16.PrVehicleSensorState9Full", this, :atVVSS, :EQ, 2)

    sbCBCL16 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL16.SbCBCL16",  [prSeconds9Full, prSemaphoreState9Full, prVehicleSensorState9Full, ],"( NopEx.Semaphore_NOP.RlCBCL16.PrSeconds9Full and NopEx.Semaphore_NOP.RlCBCL16.PrSemaphoreState9Full and NopEx.Semaphore_NOP.RlCBCL16.PrVehicleSensorState9Full )")

    [sbCBCL16,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLGCBCL, [this]},
    ]

  end

end

defmodule NopEx.Semaphore_NOP.RlCBCL5 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSecondsCBCL3 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL5.PrSecondsCBCL3", this, :atSeconds, :EQ, 6)

    prSemaphoreStateCBCL3 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL5.PrSemaphoreStateCBCL3", this, :atSemaphoreState, :EQ, 7)

    sbCBCL5 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL5.SbCBCL5",  [prSecondsCBCL3, prSemaphoreStateCBCL3, ],"( NopEx.Semaphore_NOP.RlCBCL5.PrSecondsCBCL3 and NopEx.Semaphore_NOP.RlCBCL5.PrSemaphoreStateCBCL3 )")

    [sbCBCL5,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLR, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

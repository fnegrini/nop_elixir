defmodule NopEx.Semaphore_NOP.RlCBCL18 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds10Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL18.PrSeconds10Full", this, :atSeconds, :GE, 18)

    prSeconds210Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL18.PrSeconds210Full", this, :atSeconds, :LT, 32)

    prSemaphoreState10Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL18.PrSemaphoreState10Full", this, :atSemaphoreState, :EQ, 3)

    prVehicleSensorState10Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL18.PrVehicleSensorState10Full", this, :atHVSS, :EQ, 2)

    sbCBCL18 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL18.SbCBCL18",  [prSeconds10Full, prSeconds210Full, prSemaphoreState10Full, prVehicleSensorState10Full, ],"( NopEx.Semaphore_NOP.RlCBCL18.PrSeconds10Full and NopEx.Semaphore_NOP.RlCBCL18.PrSeconds210Full and NopEx.Semaphore_NOP.RlCBCL18.PrSemaphoreState10Full and NopEx.Semaphore_NOP.RlCBCL18.PrVehicleSensorState10Full )")

    [sbCBCL18,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtVTLYCBCL, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

defmodule NopEx.Semaphore_NOP do
  use NOP.Element.FBE

  defp int_attributes() do
    %{
        :atHVSS => 0,
        :atSeconds => 0,
        :atSemaphoreState => 5,
        :atVVSS => 0,
    }
  end

  def mtHTLG(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 0)
  end
  def mtHTLGCBCL(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 6)
  end
  def mtHTLR(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 2)
  end
  def mtHTLY(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 1)
  end
  def mtHTLYCBCL(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 7)
  end
  def mtINC(this, count) do


			NOP.Service.FBE.set_attribute_expr(this, :atSeconds, "@atSeconds + " <> to_string(count))


  end
  def mtRT(this) do
    NOP.Service.FBE.set_attribute(this, :atSeconds, 0)
  end
  def mtRUN(this, iterations) do


			Enum.map(1..iterations,
				fn(_x) ->
					NopEx.Semaphore_NOP.mtINC(this, 1)
				end)


  end
  def mtVTLG(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 3)
  end
  def mtVTLGCBCL(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 8)
  end
  def mtVTLR(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 5)
  end
  def mtVTLY(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 4)
  end
  def mtVTLYCBCL(this) do
    NOP.Service.FBE.set_attribute(this, :atSemaphoreState, 9)
  end

  def initialize(name) do

    this = NOP.Service.FBE.create_fbe(NopEx.Semaphore_NOP, name)

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL1, "NopEx.Semaphore_NOP.RlCBCL1", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL10, "NopEx.Semaphore_NOP.RlCBCL10", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL11, "NopEx.Semaphore_NOP.RlCBCL11", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL12, "NopEx.Semaphore_NOP.RlCBCL12", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL13, "NopEx.Semaphore_NOP.RlCBCL13", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL14, "NopEx.Semaphore_NOP.RlCBCL14", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL15, "NopEx.Semaphore_NOP.RlCBCL15", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL16, "NopEx.Semaphore_NOP.RlCBCL16", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL17, "NopEx.Semaphore_NOP.RlCBCL17", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL18, "NopEx.Semaphore_NOP.RlCBCL18", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL2, "NopEx.Semaphore_NOP.RlCBCL2", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL3, "NopEx.Semaphore_NOP.RlCBCL3", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL4, "NopEx.Semaphore_NOP.RlCBCL4", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL5, "NopEx.Semaphore_NOP.RlCBCL5", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL6, "NopEx.Semaphore_NOP.RlCBCL6", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL7, "NopEx.Semaphore_NOP.RlCBCL7", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL8, "NopEx.Semaphore_NOP.RlCBCL8", [this])

    NOP.Service.Rule.create_rule(NopEx.Semaphore_NOP.RlCBCL9, "NopEx.Semaphore_NOP.RlCBCL9", [this])

    this
  end

end

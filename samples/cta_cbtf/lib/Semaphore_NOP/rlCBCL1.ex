defmodule NopEx.Semaphore_NOP.RlCBCL1 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL1.PrSeconds", this, :atSeconds, :EQ, 2)

    prSemaphoreState = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL1.PrSemaphoreState", this, :atSemaphoreState, :EQ, 5)

    sbCBCL1 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL1.SbCBCL1",  [prSeconds, prSemaphoreState, ],"( NopEx.Semaphore_NOP.RlCBCL1.PrSeconds and NopEx.Semaphore_NOP.RlCBCL1.PrSemaphoreState )")

    [sbCBCL1,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLG, [this]},
      {NopEx.Semaphore_NOP, :mtRT, [this]},
    ]

  end

end

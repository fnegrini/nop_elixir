defmodule NopEx.Semaphore_NOP.RlCBCL11 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds7 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL11.PrSeconds7", this, :atSeconds, :LE, 17)

    prSemaphoreState7 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL11.PrSemaphoreState7", this, :atSemaphoreState, :EQ, 0)

    prVehicleSensorState7 = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL11.PrVehicleSensorState7", this, :atHVSS, :EQ, 1)

    sbCBCL11 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL11.SbCBCL11",  [prSeconds7, prSemaphoreState7, prVehicleSensorState7, ],"( NopEx.Semaphore_NOP.RlCBCL11.PrSeconds7 and NopEx.Semaphore_NOP.RlCBCL11.PrSemaphoreState7 and NopEx.Semaphore_NOP.RlCBCL11.PrVehicleSensorState7 )")

    [sbCBCL11,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLGCBCL, [this]},
    ]

  end

end

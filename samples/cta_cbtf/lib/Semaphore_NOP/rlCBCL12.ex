defmodule NopEx.Semaphore_NOP.RlCBCL12 do
  use NOP.Element.Rule

  defp create_element_list([this]) do

    prSeconds7Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL12.PrSeconds7Full", this, :atSeconds, :LE, 17)

    prSemaphoreState7Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL12.PrSemaphoreState7Full", this, :atSemaphoreState, :EQ, 0)

    prVehicleSensorState7Full = NOP.Service.Premise.create_premise("NopEx.Semaphore_NOP.RlCBCL12.PrVehicleSensorState7Full", this, :atHVSS, :EQ, 2)

    sbCBCL12 = NOP.Service.Condition.create_condition("NopEx.Semaphore_NOP.RlCBCL12.SbCBCL12",  [prSeconds7Full, prSemaphoreState7Full, prVehicleSensorState7Full, ],"( NopEx.Semaphore_NOP.RlCBCL12.PrSeconds7Full and NopEx.Semaphore_NOP.RlCBCL12.PrSemaphoreState7Full and NopEx.Semaphore_NOP.RlCBCL12.PrVehicleSensorState7Full )")

    [sbCBCL12,]
  end

  defp create_instigation_list([this]) do

    [
      {NopEx.Semaphore_NOP, :mtHTLGCBCL, [this]},
    ]

  end

end

defmodule NopEx do
  @moduledoc """
  Documentation for NopEx.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NopEx.hello()
      :world

  """
  def hello do
    :world
  end


  defp mtRUN_ALL(_this, [], _clock_count) do
    #End of iteration
  end

  defp mtRUN_ALL(this, [{_name, semaphore} | semaphore_list], clock_count) do

    Task.start(NopEx.Semaphore_NOP, :mtRUN, [semaphore, clock_count])

    mtRUN_ALL(this, semaphore_list, clock_count)

  end
  def mtSIMULATE(this, iterations) do


      instances = initialize()

      list = Map.to_list(instances)

      mtRUN_ALL(this, list, iterations)



  end

  def initialize() do
    instances = %{}
    instances = Map.put(instances, :Semaphore_01_01, NopEx.Semaphore_NOP.initialize("Semaphore_01_01"))

    instances = Map.put(instances, :Semaphore_01_02, NopEx.Semaphore_NOP.initialize("Semaphore_01_02"))

    instances = Map.put(instances, :Semaphore_01_03, NopEx.Semaphore_NOP.initialize("Semaphore_01_03"))

    instances = Map.put(instances, :Semaphore_01_04, NopEx.Semaphore_NOP.initialize("Semaphore_01_04"))

    instances = Map.put(instances, :Semaphore_01_05, NopEx.Semaphore_NOP.initialize("Semaphore_01_05"))

    instances = Map.put(instances, :Semaphore_01_06, NopEx.Semaphore_NOP.initialize("Semaphore_01_06"))

    instances = Map.put(instances, :Semaphore_01_07, NopEx.Semaphore_NOP.initialize("Semaphore_01_07"))

    instances = Map.put(instances, :Semaphore_01_08, NopEx.Semaphore_NOP.initialize("Semaphore_01_08"))

    instances = Map.put(instances, :Semaphore_01_09, NopEx.Semaphore_NOP.initialize("Semaphore_01_09"))

    instances = Map.put(instances, :Semaphore_01_10, NopEx.Semaphore_NOP.initialize("Semaphore_01_10"))

    instances = Map.put(instances, :Semaphore_02_01, NopEx.Semaphore_NOP.initialize("Semaphore_02_01"))

    instances = Map.put(instances, :Semaphore_02_02, NopEx.Semaphore_NOP.initialize("Semaphore_02_02"))

    instances = Map.put(instances, :Semaphore_02_03, NopEx.Semaphore_NOP.initialize("Semaphore_02_03"))

    instances = Map.put(instances, :Semaphore_02_04, NopEx.Semaphore_NOP.initialize("Semaphore_02_04"))

    instances = Map.put(instances, :Semaphore_02_05, NopEx.Semaphore_NOP.initialize("Semaphore_02_05"))

    instances = Map.put(instances, :Semaphore_02_06, NopEx.Semaphore_NOP.initialize("Semaphore_02_06"))

    instances = Map.put(instances, :Semaphore_02_07, NopEx.Semaphore_NOP.initialize("Semaphore_02_07"))

    instances = Map.put(instances, :Semaphore_02_08, NopEx.Semaphore_NOP.initialize("Semaphore_02_08"))

    instances = Map.put(instances, :Semaphore_02_09, NopEx.Semaphore_NOP.initialize("Semaphore_02_09"))

    instances = Map.put(instances, :Semaphore_02_10, NopEx.Semaphore_NOP.initialize("Semaphore_02_10"))

    instances = Map.put(instances, :Semaphore_03_01, NopEx.Semaphore_NOP.initialize("Semaphore_03_01"))

    instances = Map.put(instances, :Semaphore_03_02, NopEx.Semaphore_NOP.initialize("Semaphore_03_02"))

    instances = Map.put(instances, :Semaphore_03_03, NopEx.Semaphore_NOP.initialize("Semaphore_03_03"))

    instances = Map.put(instances, :Semaphore_03_04, NopEx.Semaphore_NOP.initialize("Semaphore_03_04"))

    instances = Map.put(instances, :Semaphore_03_05, NopEx.Semaphore_NOP.initialize("Semaphore_03_05"))

    instances = Map.put(instances, :Semaphore_03_06, NopEx.Semaphore_NOP.initialize("Semaphore_03_06"))

    instances = Map.put(instances, :Semaphore_03_07, NopEx.Semaphore_NOP.initialize("Semaphore_03_07"))

    instances = Map.put(instances, :Semaphore_03_08, NopEx.Semaphore_NOP.initialize("Semaphore_03_08"))

    instances = Map.put(instances, :Semaphore_03_09, NopEx.Semaphore_NOP.initialize("Semaphore_03_09"))

    instances = Map.put(instances, :Semaphore_03_10, NopEx.Semaphore_NOP.initialize("Semaphore_03_10"))

    instances = Map.put(instances, :Semaphore_04_01, NopEx.Semaphore_NOP.initialize("Semaphore_04_01"))

    instances = Map.put(instances, :Semaphore_04_02, NopEx.Semaphore_NOP.initialize("Semaphore_04_02"))

    instances = Map.put(instances, :Semaphore_04_03, NopEx.Semaphore_NOP.initialize("Semaphore_04_03"))

    instances = Map.put(instances, :Semaphore_04_04, NopEx.Semaphore_NOP.initialize("Semaphore_04_04"))

    instances = Map.put(instances, :Semaphore_04_05, NopEx.Semaphore_NOP.initialize("Semaphore_04_05"))

    instances = Map.put(instances, :Semaphore_04_06, NopEx.Semaphore_NOP.initialize("Semaphore_04_06"))

    instances = Map.put(instances, :Semaphore_04_07, NopEx.Semaphore_NOP.initialize("Semaphore_04_07"))

    instances = Map.put(instances, :Semaphore_04_08, NopEx.Semaphore_NOP.initialize("Semaphore_04_08"))

    instances = Map.put(instances, :Semaphore_04_09, NopEx.Semaphore_NOP.initialize("Semaphore_04_09"))

    instances = Map.put(instances, :Semaphore_04_10, NopEx.Semaphore_NOP.initialize("Semaphore_04_10"))

    instances = Map.put(instances, :Semaphore_05_01, NopEx.Semaphore_NOP.initialize("Semaphore_05_01"))

    instances = Map.put(instances, :Semaphore_05_02, NopEx.Semaphore_NOP.initialize("Semaphore_05_02"))

    instances = Map.put(instances, :Semaphore_05_03, NopEx.Semaphore_NOP.initialize("Semaphore_05_03"))

    instances = Map.put(instances, :Semaphore_05_04, NopEx.Semaphore_NOP.initialize("Semaphore_05_04"))

    instances = Map.put(instances, :Semaphore_05_05, NopEx.Semaphore_NOP.initialize("Semaphore_05_05"))

    instances = Map.put(instances, :Semaphore_05_06, NopEx.Semaphore_NOP.initialize("Semaphore_05_06"))

    instances = Map.put(instances, :Semaphore_05_07, NopEx.Semaphore_NOP.initialize("Semaphore_05_07"))

    instances = Map.put(instances, :Semaphore_05_08, NopEx.Semaphore_NOP.initialize("Semaphore_05_08"))

    instances = Map.put(instances, :Semaphore_05_09, NopEx.Semaphore_NOP.initialize("Semaphore_05_09"))

    instances = Map.put(instances, :Semaphore_05_10, NopEx.Semaphore_NOP.initialize("Semaphore_05_10"))

    instances = Map.put(instances, :Semaphore_06_01, NopEx.Semaphore_NOP.initialize("Semaphore_06_01"))

    instances = Map.put(instances, :Semaphore_06_02, NopEx.Semaphore_NOP.initialize("Semaphore_06_02"))

    instances = Map.put(instances, :Semaphore_06_03, NopEx.Semaphore_NOP.initialize("Semaphore_06_03"))

    instances = Map.put(instances, :Semaphore_06_04, NopEx.Semaphore_NOP.initialize("Semaphore_06_04"))

    instances = Map.put(instances, :Semaphore_06_05, NopEx.Semaphore_NOP.initialize("Semaphore_06_05"))

    instances = Map.put(instances, :Semaphore_06_06, NopEx.Semaphore_NOP.initialize("Semaphore_06_06"))

    instances = Map.put(instances, :Semaphore_06_07, NopEx.Semaphore_NOP.initialize("Semaphore_06_07"))

    instances = Map.put(instances, :Semaphore_06_08, NopEx.Semaphore_NOP.initialize("Semaphore_06_08"))

    instances = Map.put(instances, :Semaphore_06_09, NopEx.Semaphore_NOP.initialize("Semaphore_06_09"))

    instances = Map.put(instances, :Semaphore_06_10, NopEx.Semaphore_NOP.initialize("Semaphore_06_10"))

    instances = Map.put(instances, :Semaphore_07_01, NopEx.Semaphore_NOP.initialize("Semaphore_07_01"))

    instances = Map.put(instances, :Semaphore_07_02, NopEx.Semaphore_NOP.initialize("Semaphore_07_02"))

    instances = Map.put(instances, :Semaphore_07_03, NopEx.Semaphore_NOP.initialize("Semaphore_07_03"))

    instances = Map.put(instances, :Semaphore_07_04, NopEx.Semaphore_NOP.initialize("Semaphore_07_04"))

    instances = Map.put(instances, :Semaphore_07_05, NopEx.Semaphore_NOP.initialize("Semaphore_07_05"))

    instances = Map.put(instances, :Semaphore_07_06, NopEx.Semaphore_NOP.initialize("Semaphore_07_06"))

    instances = Map.put(instances, :Semaphore_07_07, NopEx.Semaphore_NOP.initialize("Semaphore_07_07"))

    instances = Map.put(instances, :Semaphore_07_08, NopEx.Semaphore_NOP.initialize("Semaphore_07_08"))

    instances = Map.put(instances, :Semaphore_07_09, NopEx.Semaphore_NOP.initialize("Semaphore_07_09"))

    instances = Map.put(instances, :Semaphore_07_10, NopEx.Semaphore_NOP.initialize("Semaphore_07_10"))

    instances = Map.put(instances, :Semaphore_08_01, NopEx.Semaphore_NOP.initialize("Semaphore_08_01"))

    instances = Map.put(instances, :Semaphore_08_02, NopEx.Semaphore_NOP.initialize("Semaphore_08_02"))

    instances = Map.put(instances, :Semaphore_08_03, NopEx.Semaphore_NOP.initialize("Semaphore_08_03"))

    instances = Map.put(instances, :Semaphore_08_04, NopEx.Semaphore_NOP.initialize("Semaphore_08_04"))

    instances = Map.put(instances, :Semaphore_08_05, NopEx.Semaphore_NOP.initialize("Semaphore_08_05"))

    instances = Map.put(instances, :Semaphore_08_06, NopEx.Semaphore_NOP.initialize("Semaphore_08_06"))

    instances = Map.put(instances, :Semaphore_08_07, NopEx.Semaphore_NOP.initialize("Semaphore_08_07"))

    instances = Map.put(instances, :Semaphore_08_08, NopEx.Semaphore_NOP.initialize("Semaphore_08_08"))

    instances = Map.put(instances, :Semaphore_08_09, NopEx.Semaphore_NOP.initialize("Semaphore_08_09"))

    instances = Map.put(instances, :Semaphore_08_10, NopEx.Semaphore_NOP.initialize("Semaphore_08_10"))

    instances = Map.put(instances, :Semaphore_09_01, NopEx.Semaphore_NOP.initialize("Semaphore_09_01"))

    instances = Map.put(instances, :Semaphore_09_02, NopEx.Semaphore_NOP.initialize("Semaphore_09_02"))

    instances = Map.put(instances, :Semaphore_09_03, NopEx.Semaphore_NOP.initialize("Semaphore_09_03"))

    instances = Map.put(instances, :Semaphore_09_04, NopEx.Semaphore_NOP.initialize("Semaphore_09_04"))

    instances = Map.put(instances, :Semaphore_09_05, NopEx.Semaphore_NOP.initialize("Semaphore_09_05"))

    instances = Map.put(instances, :Semaphore_09_06, NopEx.Semaphore_NOP.initialize("Semaphore_09_06"))

    instances = Map.put(instances, :Semaphore_09_07, NopEx.Semaphore_NOP.initialize("Semaphore_09_07"))

    instances = Map.put(instances, :Semaphore_09_08, NopEx.Semaphore_NOP.initialize("Semaphore_09_08"))

    instances = Map.put(instances, :Semaphore_09_09, NopEx.Semaphore_NOP.initialize("Semaphore_09_09"))

    instances = Map.put(instances, :Semaphore_09_10, NopEx.Semaphore_NOP.initialize("Semaphore_09_10"))

    instances = Map.put(instances, :Semaphore_10_01, NopEx.Semaphore_NOP.initialize("Semaphore_10_01"))

    instances = Map.put(instances, :Semaphore_10_02, NopEx.Semaphore_NOP.initialize("Semaphore_10_02"))

    instances = Map.put(instances, :Semaphore_10_03, NopEx.Semaphore_NOP.initialize("Semaphore_10_03"))

    instances = Map.put(instances, :Semaphore_10_04, NopEx.Semaphore_NOP.initialize("Semaphore_10_04"))

    instances = Map.put(instances, :Semaphore_10_05, NopEx.Semaphore_NOP.initialize("Semaphore_10_05"))

    instances = Map.put(instances, :Semaphore_10_06, NopEx.Semaphore_NOP.initialize("Semaphore_10_06"))

    instances = Map.put(instances, :Semaphore_10_07, NopEx.Semaphore_NOP.initialize("Semaphore_10_07"))

    instances = Map.put(instances, :Semaphore_10_08, NopEx.Semaphore_NOP.initialize("Semaphore_10_08"))

    instances = Map.put(instances, :Semaphore_10_09, NopEx.Semaphore_NOP.initialize("Semaphore_10_09"))

    instances = Map.put(instances, :Semaphore_10_10, NopEx.Semaphore_NOP.initialize("Semaphore_10_10"))

    instances
  end
end

# Run in iex:
# c "./statistics/stat_1500.exs"

NOP.Application.reset_element_list_total()
{time_in_microseconds, _ret_val} =  :timer.tc(
  fn ->
    NopEx.mtSIMULATE(nil, 1500)
    NOP.Application.wait_up_to_end_all_process()
  end)

IO.puts("The liquid time is #{div(time_in_microseconds,1000)}")
IO.puts("The notification count is #{NOP.Application.get_statistics_from_total()}")

defmodule CTA.RuleVerticallGreen do
  use NOP.Element.Rule


  defp create_element_list([semaphore_seconds, _semaphore_state]) do
    #prSeconds semaphore_NOP.atSeconds == 47  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore_seconds, :value, :EQ, 47)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 2 {HorizontalRED}
    #prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore_state, :value, :EQ, :HorizontalRED)

    #[prSeconds, prSemaphoreState]
    [prSeconds]
  end

  defp create_instigation_list([_semaphore_seconds, semaphore_state]) do
    [{CTA.Semaphore_NOP.State, :mtVerticalTrafficLightGREEN, [semaphore_state]}]
  end

end

defmodule CTA.RuleVerticalYellow do
  use NOP.Element.Rule


  defp create_element_list([semaphore_seconds, _semaphore_state]) do
    #prSeconds semaphore_NOP.atSeconds == 85  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore_seconds, :value, :EQ, 85)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 3 {VerticalGREEN}
    #prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore_state, :value, :EQ, :VerticalGREEN)

    #[prSeconds, prSemaphoreState]
    [prSeconds]
  end

  defp create_instigation_list([_semaphore_seconds, semaphore_state]) do
    [{CTA.Semaphore_NOP.State, :mtVerticalTrafficLightYELLOW, [semaphore_state]}]
  end

end

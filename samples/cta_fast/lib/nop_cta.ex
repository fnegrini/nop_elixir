defmodule NopCta do
  @moduledoc """
  NOP simulation for Traffic Control .
  """

  @doc """
  NOP simulation for Traffic Control

  """
  def hello do
    :world
  end
end

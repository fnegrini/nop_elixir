defmodule CTA.RuleVerticalRed do
  use NOP.Element.Rule


  defp create_element_list([semaphore_seconds, _semaphore_state]) do
    #prSeconds semaphore_NOP.atSeconds == 90  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore_seconds, :value, :EQ, 0)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 4 {VerticalYELLOW}
    #prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore_state, :value, :EQ, :VerticalYELLOW)

    #[prSeconds, prSemaphoreState]
    [prSeconds]
  end

  defp create_instigation_list([_semaphore_seconds, semaphore_state]) do
    [{CTA.Semaphore_NOP.State, :mtVerticalTrafficLightRED, [semaphore_state]}]
  end

end

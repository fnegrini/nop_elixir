defmodule CTA.RuleHorizontalGreen do
  use NOP.Element.Rule


  defp create_element_list([semaphore_seconds, _semaphore_state]) do
    #prSeconds semaphore_NOP.atSeconds == 2  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore_seconds, :value, :EQ, 2)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 5 {VerticalRED}
    #prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore_state, :value, :EQ, :VerticalRED)

    #[prSeconds, prSemaphoreState]
    [prSeconds]
  end

  defp create_instigation_list([_semaphore_seconds, semaphore_state]) do
    [{CTA.Semaphore_NOP.State, :mtHorizontalTrafficLightGREEN, [semaphore_state]}]
  end

end

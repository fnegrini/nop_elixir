defmodule CTA.RuleHorizontalYellow do
  use NOP.Element.Rule


  defp create_element_list([semaphore_seconds, _semaphore_state]) do
    #prSeconds semaphore_NOP.atSeconds == 40  and
    prSeconds = NOP.Service.Premise.create_premise("CTA.premise.prSeconds", semaphore_seconds, :value, :EQ, 40)

    #premise prSemaphoreState semaphore_NOP.atSemaphoreState == 0 {HorizontalGREEN}
    #prSemaphoreState = NOP.Service.Premise.create_premise("CTA.premise.prSemaphoreState", semaphore_state, :value, :EQ, :HorizontalGREEN)

    #[prSeconds, prSemaphoreState]
    [prSeconds]
  end

  defp create_instigation_list([_semaphore_seconds, semaphore_state]) do
    [{CTA.Semaphore_NOP.State, :mtHorizontalTrafficLightYELLOW, [semaphore_state]}]
  end

end

defmodule NopCtaTest do
  use ExUnit.Case
  doctest NopCta

  test "greets the world" do
    assert NopCta.hello() == :world
  end

  test "Create first FBE" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    assert NOP.Service.FBE.get_name(fbe) == "NOP.element.MyFBE"
  end

  test "Create first Semaphore" do
    semaphore = NOP.Service.FBE.create_fbe(CTA.Semaphore_NOP.Seconds, "NOP.FBE.Semaphore")

    assert NOP.Service.FBE.get_name(semaphore) == "NOP.FBE.Semaphore"
  end

  test "Init two semaphores" do
    {sem_seconds1, sem_state1} = CTA.Init.init_semaphore("test1")

    {sem_seconds2, sem_state2} = CTA.Init.init_semaphore("test2")

    NOP.Service.FBE.set_attribute(sem_seconds1, :value, 2)

    NOP.Service.FBE.set_attribute(sem_seconds2, :value, 2)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state1, :value) == :HorizontalGREEN
  end

  test "Init Semaphore and test Rule RuleHorizontalGreen" do
    {sem_seconds, sem_state} = CTA.Init.init_semaphore("test")

    NOP.Service.FBE.set_attribute(sem_seconds, :value, 2)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state, :value) == :HorizontalGREEN
  end

  test "Init Semaphore and test Rule RuleHorizontalYellow" do
    {sem_seconds, sem_state} = CTA.Init.init_semaphore("test")

    #NOP.Service.FBE.set_attribute(sem_state, :value, :HorizontalGREEN)

    NOP.Service.FBE.set_attribute(sem_seconds, :value, 40)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state, :value) == :HorizontalYELLOW
  end

  test "Init Semaphore and test Rule RuleHorizontalRed" do
    {sem_seconds, sem_state} = CTA.Init.init_semaphore("test")

    #NOP.Service.FBE.set_attribute(sem_state, :value, :HorizontalYELLOW)

    NOP.Service.FBE.set_attribute(sem_seconds, :value, 45)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state, :value) == :HorizontalRED
  end

  test "Init Semaphore and test Rule RuleVerticalGreen" do
    {sem_seconds, sem_state} = CTA.Init.init_semaphore("test")

    #NOP.Service.FBE.set_attribute(sem_state, :value, :HorizontalRED)

    NOP.Service.FBE.set_attribute(sem_seconds, :value, 47)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state, :value) == :VerticalGREEN
  end

  test "Init Semaphore and test Rule RuleVerticalYellow" do
    {sem_seconds, sem_state} = CTA.Init.init_semaphore("test")

    #NOP.Service.FBE.set_attribute(sem_state, :value, :VerticalGREEN)

    NOP.Service.FBE.set_attribute(sem_seconds, :value, 85)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state, :value) == :VerticalYELLOW
  end

  test "Init Semaphore and test Rule RuleVerticalRed" do

    {sem_seconds, sem_state} = CTA.Init.init_semaphore("test")

    #NOP.Service.FBE.set_attribute(sem_state, :value, :VerticalYELLOW)

    NOP.Service.FBE.set_attribute(sem_seconds, :value, 0)

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(sem_state, :value) == :VerticalRED
  end

end


# Vetores

## Declaração

__private boolean[10] bla__

* Cada elemento do vetor vira um atributo/instância: bla_0, bla_1... bla_9
* Defini-se um novo mapa no NOPGraph (this->vectors = new std::map<std::string, vector*>()) onde cada elemento do vetor aponta para os atributos gerados.
* O objetivo de criar os atributos e vetor é que utilizando os atributos fica transparente para os gbla__eradores de código, porém novas versões deles podem utilizar o this->vectors para melhorias na compilação de vetores.
* Seria interessante se o valor 10 não estivesse na declaração, mas que a quantidade de itens fosse levantada dinamicamente

## Acesso direto

Acessos diretos aos atributos/instâncias deve ser convertida na hora de adicionar ao grapho: bla[1] vira bla_1

## Formation rules

Permite a criação de vários blocos de regras dinamicamente a partir da quantidade de itens nos índices especificados.

Ex:
``` code
rule rlFireAlarm
    index i from 0 to 1
    index s from 0 to 1
    condition
        subcondition
            premise
                sensor[i].state == true and alarm[s].ringing == false
            end_premise
        end_subcondition
    end_condition
    ...
```

Gera X blocos de código (no caso acima 2 * 2) , acessand as variáveis bla_0, bla_1, etc.

## Acesso dinâmico

TBD - ideal seria utilizar o novo mapa __vectors__ para o acesso. Caso contrário seria necessário gerar um _switch_ entre todas as variáveis do vetor.

Ex de uso:
``` code
   private method mtSendSms
        params
            Integer index
        end_params
         condition
            premise prSectorAInvaded
                bla[index] == true
            end_premise
        end_condition
    end_method
```

No caso acima, por se tratar de variáveis seria necessário um _switch_ entre as variáveis caso o mapa vector não seja utilizado.


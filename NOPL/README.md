# NOPL - NOP Language

NOPL is the new language/compiler to the Notification-Oriented Paradigm.


### Prerequisites

In order to use the NOPL compiler you must first install Flex and Bison applications.


In macOS:

```
brew install flex
```
```
brew install bison
```


### Compiling

In order to compile the NOPL compiler, you must run, in the root folder, the following command:

```
sh Compiler/compile.sh
```

The output should look like with something as follows:

```
Cleaning output...
Deleting old NOPL compiler...
Compiling Flex file...
Compiling Bison file...
Compiling NOPL compiler...
NOPL compiler has compiled with success
```

The NOPL compiler binary is now located at the Compiler folder.

In order to compile your specific NOP code, you must inform the folder(s) containing the .nop files.

```
Compiler/NOPL 1 Applications/Example/src
```

The second argument [e.g. 1] is used to choose the class responsible for generating code.

Bellow a preliminar table of versions (to be updated):

```
1 - Framework C++ 2.0
2 - Namespaces C++ 2.0
...
```

Moreover, it is possible to inform more than one folder to the compiler at the same time.

```
Compiler/NOPL 1 Applications/Example/src/folder1 Applications/Example/src/folder2
```
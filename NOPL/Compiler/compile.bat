@echo off

echo "Compiling NOPL compiler..."
g++ -std=c++11 -Wno-deprecated -Wno-deprecated-register -Iback/include/ front/output/lex.yy.c front/output/bison.tab.c back/src/*.cpp back/src/elements/*.cpp back/src/generation/example/*.cpp back/src/generation/namespaces/*.cpp back/src/generation/elixir/*.cpp -ggdb -o NOPL > log.txt 2>&1


if exist NOPL.exe (
   echo "NOPL compiler has compiled with success"
) else (
    echo "NOPL has not been compiled =("
)

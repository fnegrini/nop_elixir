%{
    
    #include "Compiler.h"
    #include "NOPGraph.h"

    #include "elements/Target.h"
    #include "elements/Strategy.h"
    #include "elements/Type.h"
    #include "elements/Visibility.h"
    #include "elements/Param.h"
    #include "elements/Symbol.h"
    #include "elements/Conjunction.h"
    #include "elements/Factor.h"
    #include "elements/Expression.h"
    #include "elements/Argument.h"
    #include "elements/Instigation.h"
    #include "elements/Execution.h"
    #include "elements/VectorDto.h"

    #include "generation/example/CodeGenerationExample.h"
    #include "generation/namespaces/NamespacesCompiler.h"
    //#include "generation/framework_cpp_2_0/FrameworkCPP20Compiler.h"
    //#include "generation/framework_cpp_3_0/FrameworkCPP30Compiler.h"
    #include "generation/elixir/CodeGenerationElixir.h"

    #include <stdio.h>
    #include <dirent.h>
    #include <sys/stat.h>

    #include <string.h>
    #include <iostream>

    using namespace std;

    Compiler *compiler = 0;
    NOPGraph *graph = 0;
    
    extern FILE *yyin;
    
    int yylex(void);
    void yyerror(const char *s);
    void validate(const std::string errorMessage);
    
    std::string current_file;
    extern int line_num;

%}

%union
{
    int ival;
    float fval;
    char *sval;
    void *pval;
}

%start PROGRAM

%token INCLUDES
%token <sval> END_INCLUDES

%token CODE_GENERATION_EXAMPLE
%token NAMESPACES
%token FRAMEWORK_CPP_2_0
%token FRAMEWORK_CPP_3_0
%token ELIXIR

%token NO_ONE
%token BREADTH
%token DEPTH
%token PRIORITY

%token FBE
%token END_FBE

%token PROPERTIES
%token END_PROPERTIES

%token TARGET
%token STRATEGY

%token KEEPER

%token PRIVATE
%token PUBLIC

%token INSTANCE
%token ATTRIBUTE

%token BOOLEAN
%token INTEGER
%token DOUBLE
%token STRING
%token CHAR

%token METHOD
%token END_METHOD

%token PARAMS
%token END_PARAMS

%token CODE
%token <sval> END_CODE

%token ATTRIBUTION
%token END_ATTRIBUTION

%token RULE
%token END_RULE

%token FORMATION_RULE
%token END_FORMATION_RULE
%token INDEX
%token FROM
%token TO

%token CONDITION
%token END_CONDITION

%token SUBCONDITION
%token END_SUBCONDITION

%token PREMISE
%token END_PREMISE

%token IMPERTINENT

%token ACTION
%token END_ACTION

%token INSTIGATION
%token END_INSTIGATION

%token SEQUENTIAL
%token PARALLEL

%token CALL

%token MAIN
%token END_MAIN

%token ASSIGN

%token EQ
%token NE
%token LT
%token GT
%token LE
%token GE

%token LB
%token RB

%token LC
%token RC

%token PLUS
%token MINUS
%token MULTIPLY
%token DIVIDE

%token SQRT

%token LP
%token RP

%token COMMA

%token POINT

%token <sval> AND 
%token <sval> OR

%token <sval> TRUE
%token <sval> FALSE

%token <sval> THIS

%token <sval> INTEGER_VALUE
%token <sval> DOUBLE_VALUE
%token <sval> CHAR_VALUE
%token <sval> STRING_VALUE

%token <sval> ID

%token <sval> COMMENT

%type <pval> strategy
%type <pval> target
%type <pval> code_block
%type <pval> symbol
%type <pval> factor
%type <pval> expression
%type <pval> boolean
%type <pval> type
%type <pval> supertype
%type <ival> basictype
%type <pval> visibility
%type <pval> conjunction
%type <sval> id
%type <sval> ID_or_VectElem
%type <pval> VectElem
%type <pval> param
%type <pval> element
%type <pval> attribution
%type <pval> elementcall
%type <pval> execution

%type <pval> array_factor
%type <pval> vector_operation
%type <pval> operation

%%

PROGRAM                     : FBE id fbe_body END_FBE {
                                validate(
                                    graph->addFbe(
                                        (Fbe*)graph->createFbe($2)
                                    )
                                );
                            }
                            ;

fbe_body                    : include_block
                            | include_block fbe_body
                            | properties_block
                            | properties_block fbe_body
                            | instance
                            | instance fbe_body
                            | attribute
                            | attribute fbe_body
                            | vector_instance
                            | vector_instance fbe_body
                            | vector_attribute
                            | vector_attribute fbe_body
                            | method
                            | method fbe_body
                            | rule
                            | rule fbe_body
                            | formation_rule
                            | formation_rule fbe_body
							| main_block
							| main_block fbe_body
                            ;

include_block               : INCLUDES target END_INCLUDES {
                                validate(
                                    graph->addIncludeBlock(
                                        (IncludeBlock*)graph->createIncludeBlock((Target*)$2, $3)
                                    )
                                );
                            }
                            ;

properties_block            : PROPERTIES properties END_PROPERTIES
                            ;

properties                  : property
                            | property properties

property                    : TARGET target {
                                validate(
                                    graph->addProperty(
                                        (Property*)$2
                                    )
                                );
                            }
                            | STRATEGY strategy {
                                validate(
                                    graph->addProperty(
                                        (Property*)$2
                                    )
                                );
                            }
                            ;

target                      : CODE_GENERATION_EXAMPLE {
                                $$ = graph->createTarget(Target::CODE_GENERATION_EXAMPLE_TARGET);
                            }
                            | NAMESPACES {
                                $$ = graph->createTarget(Target::NAMESPACES_TARGET);
                            }
							| FRAMEWORK_CPP_2_0 {
                                //$$ = graph->createTarget(Target::FRAMEWORK_CPP_2_0_TARGET);
								$$ = graph->createTarget(Target::CODE_GENERATION_EXAMPLE_TARGET);
                            }
							| FRAMEWORK_CPP_3_0 {
                                //$$ = graph->createTarget(Target::FRAMEWORK_CPP_3_0_TARGET);
								$$ = graph->createTarget(Target::CODE_GENERATION_EXAMPLE_TARGET);
                            }
							| ELIXIR {
								$$ = graph->createTarget(Target::ELIXIR_TARGET);
                            }
                            ;

strategy                    : NO_ONE {
                                $$ = graph->createStrategy(Strategy::NO_ONE_STRATEGY);
                            }
                            | BREADTH {
                                $$ = graph->createStrategy(Strategy::BREADTH_STRATEGY);
                            }
                            | DEPTH {
                                $$ = graph->createStrategy(Strategy::DEPTH_STRATEGY);
                            }
                            | PRIORITY {
                                $$ = graph->createStrategy(Strategy::PRIORITY_STRATEGY);
                            }
                            ;

instance                    : visibility id id {
                                validate(
                                    graph->addInstance(
                                        (Instance*)graph->createInstance((Visibility*)$1, $2, $3)
                                    )
                                );
                            }
                            ;

attribute                   : visibility type id ASSIGN factor {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAttribute((Visibility*)$1, (Type*)$2, $3, (Factor*)$5)
                                    )
                                );
                            }
                            ;

vector_instance            : visibility id LB INTEGER_VALUE RB id {
                                std::list<Instance*> *instances = (std::list<Instance*> *)
                                        graph->createInstances((Visibility*)$1, $2, $6, atoi($4));
                                for (Instance* instance : *instances) {
                                    validate(
                                        graph->addInstance(instance)
                                    );
                                }
                            }
                            ;


vector_attribute            : visibility type LB INTEGER_VALUE RB id {
                                std::list<Attribute*> *attributes = (std::list<Attribute*> *)
                                        graph->createAttributes((Visibility*)$1, (Type*)$2, $6, NULL, atoi($4));
                                for (Attribute* attribute : *attributes) {
                                    validate(
                                        graph->addEntity(
                                            (Entity*)attribute)
                                    );
                                }
                            }
                            ;

vector_attribute            : visibility type LB INTEGER_VALUE RB id ASSIGN LC array_factor RC {
                                std::list<Attribute*> *attributes = (std::list<Attribute*> *)
                                        graph->createAttributes((Visibility*)$1, (Type*)$2, $6, (std::vector<Factor*>*)$9, atoi($4));
                                for (Attribute* attribute : *attributes) {
                                    validate(
                                        graph->addEntity(
                                            (Entity*)attribute)
                                    );
                                }
                            }
                            ;

method                      : visibility METHOD id method_body END_METHOD {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createMethod((Visibility*)$1, $3)
                                    )
                                );
                            }
                            ;

method_body                 : code_blocks
                            | params_body attribution
                            | params_body code_blocks
                            | attribution
                            ;

params_body                 : PARAMS params END_PARAMS
                            ;

params                      : param
                            | param params
                            ;

param                       : type id {
                                validate(
                                    graph->addParam(
                                        (Param*)graph->createParam((Type*)$1, $2)
                                    )
                                );
                            }
                            ;

code_blocks                 : code_block
                            | code_block code_blocks
                            ;

code_block                  : CODE target END_CODE {
                                validate(
                                    graph->addCodeBlock(
                                        (CodeBlock*)graph->createCodeBlock((Target*)$2, $3)
                                    )
                                );
                            }
                            ;

attribution                 : ATTRIBUTION element ASSIGN factor END_ATTRIBUTION {
                                validate(
                                    graph->addAttribution(
                                        (Attribution*)graph->createAttribution((ElementFactor*)$2, (Factor*)$4)
                                    )
                                );
                            }
                            ;

rule                        : RULE id rule_properties_block condition action END_RULE {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createRule($2)
                                    )
                                );
                            }
                            | RULE id condition action END_RULE {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createRule($2)
                                    )
                                );
                            }
                            ;

formation_rule              : FORMATION_RULE id rule_indexes rule_properties_block condition action END_FORMATION_RULE {
                                //printf("\n\n\n[Formation Rule 1st rule]: %s", $2);
                                validate(
                                    graph->createFormationRule($2)
                                );
                            }
                            | FORMATION_RULE id rule_indexes condition action END_FORMATION_RULE {
                                //printf("\n\n\n[Formation Rule 2nd rule]: %s\n", $2);
                                validate(
                                    graph->createFormationRule($2)
                                );
                            }
                            ;

rule_indexes                : rule_index
                            | rule_index rule_indexes
                            ;

rule_index                  : INDEX id FROM INTEGER_VALUE TO INTEGER_VALUE {
                                //printf("\n\n\n[Rule Index]: %s\n", $2);
                                validate(graph->addFormationRuleIndex($2, atoi($4), atoi($6)));
                            }

rule_properties_block       : PROPERTIES rule_properties END_PROPERTIES
                            ;

rule_properties             : rule_property
                            | rule_property rule_properties
                            ;

rule_property               : PRIORITY INTEGER_VALUE {
                                //printf("\n\n\n[Priority]: %s", $2);
                            }
                            | KEEPER boolean {
                                //printf("\n\n\n[Keeper]: %s", $2);
                            }
                            ;

condition                   : CONDITION subconditions END_CONDITION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition("")
                                    )
                                );
                            }
                            | CONDITION id subconditions END_CONDITION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition($2)
                                    )
                                );
                            }
                            | CONDITION premises END_CONDITION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition("")
                                    )
                                );
                            }
                            | CONDITION id premises END_CONDITION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition($2)
                                    )
                                );
                            }
                            ;

subconditions               : subcondition
                            | subcondition conjunction subconditions {
                                validate(
                                    graph->checkSubconditionsConjunction(
                                        (Conjunction*)$2
                                    )
                                );
                            }
                            ;

subcondition                : SUBCONDITION premises END_SUBCONDITION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createSubcondition("")
                                    )
                                );
                            }
                            | SUBCONDITION id premises END_SUBCONDITION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createSubcondition($2)
                                    )
                                );
                            }
                            ;

premises                    : premise
                            | premise conjunction premises {
                                validate(
                                    graph->checkPremisesConjunction(
                                        (Conjunction*)$2
                                    )
                                );
                            }
                            ;

premise                     : PREMISE expression END_PREMISE {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise("", (Expression*) $2, false)
                                    )
                                );
                            }
                            | PREMISE id expression END_PREMISE {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise($2, (Expression*) $3, false)
                                    )
                                );
                            }
                            | PREMISE IMPERTINENT expression END_PREMISE {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise("", (Expression*) $3, true)
                                    )
                                );
                            }
                            | PREMISE IMPERTINENT id expression END_PREMISE {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise($3, (Expression*) $4, true)
                                    )
                                );
                            }
                            ;

expression                  : factor symbol factor {
                                $$ = graph->createExpression((Factor*)$1, (Symbol*)$2, (Factor*)$3);
                            }
                            ;

symbol                      : EQ {
                                $$ = graph->createSymbol(Symbol::EQUAL_SYMBOL);
                            }
                            | NE {
                                $$ = graph->createSymbol(Symbol::NOT_EQUAL_SYMBOL);
                            }
                            | LT {
                                $$ = graph->createSymbol(Symbol::LESSER_THAN_SYMBOL);
                            }
                            | GT {
                                $$ = graph->createSymbol(Symbol::GREATER_THAN_SYMBOL);
                            }
                            | LE {
                                $$ = graph->createSymbol(Symbol::LESS_OR_EQUAL_SYMBOL);
                            }
                            | GE {
                                $$ = graph->createSymbol(Symbol::GREATER_OR_EQUAL_SYMBOL);
                            }
                            ;

action                      : ACTION execution instigations END_ACTION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction("", (Execution*)$2)
                                    )
                                );
                            }
                            | ACTION id execution instigations END_ACTION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction($2, (Execution*)$3)
                                    )
                                );
                            }
                            | ACTION instigations END_ACTION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction("", (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            }
                            | ACTION id instigations END_ACTION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction($2, (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            }
                            ;

instigations                : instigation
                            | instigation instigations
                            ;

instigation                 : INSTIGATION execution calls END_INSTIGATION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation("", (Execution*)$2)
                                    )
                                );
                            }
                            | INSTIGATION id execution calls END_INSTIGATION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation($2, (Execution*)$3)
                                    )
                                );
                            }
                            | INSTIGATION calls END_INSTIGATION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation("", (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            }
                            | INSTIGATION id calls END_INSTIGATION {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation($2, (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            }
                            ;

execution                   : SEQUENTIAL {
                                $$ = (Execution*)graph->createExecution(Execution::SEQUENTIAL_EXECUTION);
                            }
                            | PARALLEL {
                                $$ = (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION);
                            }
                            ;

calls                       : call
                            | call calls
                            ;

call                        : CALL elementcall {
                                validate(
                                    graph->addCall(
                                        (Call*)$2
                                    )
                                );
                            }
                            ;

elementcall                 : ID POINT ID LP RP {
                                $$ = (Call*)graph->createCall($1, $3);
                            }
                            | THIS POINT ID LP RP {
                                $$ = (Call*)graph->createCall($1, $3);
                            }
                            | ID POINT ID LP arguments RP {
                                $$ = (Call*)graph->createCall($1, $3);
                            }
                            | THIS POINT ID LP arguments RP {
                                $$ = (Call*)graph->createCall($1, $3);
                            }
                            ;
 
arguments                   : argument
                            | argument COMMA arguments
                            ;

argument                    : factor {
                                validate(
                                    graph->addArgument(
                                        (Argument*)graph->createArgument((Factor*)$1)
                                    )
                                );
                            }
                            ;

type                        : basictype {
                                $$ = graph->createType($1);
                            }
                            | supertype {
                                $$ = $1;
                            }
                            ;

supertype                   : basictype LT INTEGER_VALUE GT {
                                $$ = graph->createType($1, atoi($3));
                            }
                            ;

basictype                   : BOOLEAN {
                                $$ = Type::BOOLEAN_TYPE;
                            }
                            | INTEGER {
                                $$ = Type::INTEGER_TYPE;
                            }
                            | DOUBLE {
                                $$ = Type::DOUBLE_TYPE;
                            }
                            | STRING {
                                $$ = Type::STRING_TYPE;
                            }
                            | CHAR {
                                $$ = Type::CHAR_TYPE;
                            }
                            ;

visibility                  : PRIVATE {
                                $$ = graph->createVisibility(Visibility::PRIVATE_VISIBILITY);
                            }
                            | PUBLIC {
                                $$ = graph->createVisibility(Visibility::PUBLIC_VISIBILITY);
                            }
                            ;

conjunction                 : AND {
                                $$ = (Conjunction*)graph->createConjunction(Conjunction::AND_CONJUNCTION);
                            }
                            | OR {
                                $$ = (Conjunction*)graph->createConjunction(Conjunction::OR_CONJUNCTION);
                            }
                            ;

array_factor                : array_factor COMMA factor {
                                $$ = graph->addToFactorArray((Factor*)$3);
                            }
                            | factor {
                                $$ = graph->addToFactorArray((Factor*)$1);
                            };

factor                      : element {
                                $$ = $1;
                            }
                            | boolean {
                                $$ = $1;
                            }
                            | INTEGER_VALUE {
                                $$ = graph->createIntegerFactor(atoi($1));
                            }
                            | DOUBLE_VALUE {
                                $$ = graph->createDoubleFactor(atof($1));
                            }
                            | CHAR_VALUE {
                                $$ = graph->createCharFactor(((char*)$1)[0]);
                            }
                            | STRING_VALUE {
                                $$ = graph->createStringFactor($1);
                            }
                            ;

boolean                     : TRUE {
                                $$ = (Factor*)graph->createBooleanFactor(true);
                            }
                            | FALSE {
                                $$ = (Factor*)graph->createBooleanFactor(false);
                            }
                            ;

element                     : ID_or_VectElem POINT ID_or_VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor($1, $3);
                            }
                            | THIS POINT ID_or_VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor($1, $3);
                            }
                            | ID_or_VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor("", $1);
                            }
                            | ID_or_VectElem POINT VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor($1, (VectorDto*)$3);
                            }
                            | VectElem POINT ID_or_VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor((VectorDto*)$1, $3);
                            }
                            | VectElem POINT VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor((VectorDto*)$1, (VectorDto*)$3);
                            }
                            | THIS POINT VectElem {
                                $$ = (ElementFactor*)graph->createElementFactor($1, (VectorDto*)$3);
                            }
                            ;

ID_or_VectElem              : ID LB INTEGER_VALUE RB {
                                //printf("Evaluating static vector value %s[%s]", $1, $3);
                                $$ = graph->convertVectorToId($1, atoi($3));
                            }
                            | id {
                                $$ = $1;
                            }
                            ;

VectElem                    : ID LB vector_operation RB {
                                //printf("Evaluating static vector expression value %s ", $1);
                                $$ = (VectorDto*)graph->convertVectorToVectorElement($1, (Expression*)($3));
                            }
                            | ID LB id RB {
                                //printf("Evaluating dinamic vector %s[%s]\n", $1, $3);
                                $$ = (VectorDto*)graph->convertVectorToVectorElement($1, $3);
                            }
                            ;

id                          : ID {
                                $$ = $1;
                            }
                            ;

vector_operation           : id operation INTEGER_VALUE {
                                //printf("Evaluating vector operation 1st factor %s\n", $1);
                                $$ = graph->createExpression((Factor*) graph->createStringFactor($1), (Symbol*)$2, (Factor*) graph->createIntegerFactor(atoi($3)));
                            }
                            |
                            INTEGER_VALUE operation id {
                                //printf("Evaluating vector operation 2nd factor %s\n", $3);
                                $$ = graph->createExpression((Factor*) graph->createIntegerFactor(atoi($1)), (Symbol*)$2, (Factor*) graph->createStringFactor($3));
                            }
                            ;

operation                   : PLUS {
                                $$ = graph->createSymbol(Symbol::PLUS_SYMBOL);
                            }
                            | MINUS {
                                $$ = graph->createSymbol(Symbol::MINUS_SYMBOL);
                            }
                            | MULTIPLY {
                                $$ = graph->createSymbol(Symbol::MULTIPLY_SYMBOL);
                            }
                            | DIVIDE {
                                $$ = graph->createSymbol(Symbol::DIVIDE_SYMBOL);
                            }
                            | SQRT {
                                $$ = graph->createSymbol(Symbol::SQRT_SYMBOL);
                            }
                            ;

main_block            		: MAIN main_attributions END_MAIN {
                                validate(
                                    graph->saveMainBlock()
                                );
                            }
							|  MAIN END_MAIN {
                                validate(
                                    graph->saveMainBlock()
                                );
                            }
                            ;

main_attributions			: main_attribution
                            | main_attribution main_attributions

main_attribution			: element ASSIGN factor {
                                validate(
                                    graph->addAttribution(
                                        (Attribution*)graph->createAttribution((ElementFactor*)$1, (Factor*)$3)
                                    )
                                );
                            }
                            ;

%%

extern char *yytext;

int isDirectory(const char *path) {
    
    struct stat statbuf;
    
    if (stat(path, &statbuf) != 0)
        return 0;
        
    return S_ISDIR(statbuf.st_mode);
    
}

int main (int argc, char * argv[]) {

    if (argc < 3) {
        cout << "Missing arguments NOPL => folder/*.nop" << endl;
        return EXIT_FAILURE;
    }

    int target = atoi(argv[1]);

    switch (target) {

        case Target::CODE_GENERATION_EXAMPLE_TARGET: {

            compiler = new CodeGenerationExample();
            graph = compiler->graph;

        } break;

        case Target::NAMESPACES_TARGET: {

            compiler = new NamespacesCompiler();
            graph = compiler->graph;

        } break;

		/*
        case Target::FRAMEWORK_CPP_2_0_TARGET: {

            compiler = new FrameworkCPP20Compiler();
			graph = compiler->graph;

        } break;

        case Target::FRAMEWORK_CPP_3_0_TARGET: {

            compiler = new FrameworkCPP30Compiler();
			graph = compiler->graph;

        } break;
		*/

        case Target::ELIXIR_TARGET: {

            compiler = new CodeGenerationElixir();
			graph = compiler->graph;

        } break;

        default: {

            cout << "Target is undefined" << endl;
            return EXIT_FAILURE;

        } break;

    }

    extern int yydebug;
    yydebug = 0;
    
    int len;
    
    struct dirent *pDirent;

    for (int i = 2; i < argc; i++) {

        if (isDirectory(argv[i])) {

            cout << "Openning directory '" << argv[i] << "'" << endl;

            DIR *pDir = opendir(argv[i]);

            if (pDir == NULL) {
                cout << "Directory not found '" << argv[i] << "'" << endl;
                return EXIT_FAILURE;
            }
            
            while ((pDirent = readdir(pDir)) != NULL) {

                if ((strcmp(pDirent->d_name,".") != 0) && (strcmp(pDirent->d_name,"..") != 0) && (strstr(pDirent->d_name,".nop") != NULL)) {
                    
                    cout << "\n\n[" << pDirent->d_name << "]\n";

                    string fullpath = argv[i];

                    if (fullpath.at(fullpath.size()-1) != '/') {
                        fullpath += "/";
                    }

                    fullpath += pDirent->d_name;

                    cout << "[" << fullpath << "]\n";
                    
                    graph->clear();

                    line_num = 1;
                    
                    yyin = fopen(fullpath.c_str(),"r");
                    
                    current_file = fullpath;
                    
                    yyparse();

                }

            }

            cout << "\n\nClosing directory '" << argv[i] << "'" << endl;

            closedir(pDir);

        } else {

            cout << "'" << argv[i] << "' is not a valid directory." << endl;
            return EXIT_FAILURE;

        }
        
    }

    cout << "\n\n> Checking consistency of NOP graph...\n" << endl;

    if (graph->checkConsistency()) {
        cout << "+ NOP Graph has been successfully checked and has no errors." << endl;
    } else {
        return EXIT_FAILURE;
    }

    cout << "\n\n> Printing NOP Graph" << endl;

	graph->iterateOverNOPGraph();

    cout << "\n\n> Initiating compilation process..." << endl;

    compiler->generateCode();

    cout << "\n\n> Compilation process has finished..." << endl;

}

void yyerror(const char *s) {

    cout << "\n\nNOPL compilation error:\n" << current_file << ":" << line_num << "\t" << s << "\n\n" << std::endl;
    exit(-1);
}

void validate(std::string errorMessage) {

    if (errorMessage == "") {
        return;
    }

    cout << "\n\nNOPL compilation error:\n" << current_file << ":" << line_num << "\t" << errorMessage << "\n\n" << std::endl;
    exit(-1);

}

%{
	
	#include <stdio.h>
	#include <string>

	#include "bison.tab.h"
	
	int line_num = 1;

	int code_block = 0;
	
	std::string code = "";
	
%}
       
%%

includes						{

									if (code_block == 0) {
										
										code_block = 1;

										code = "";

										return INCLUDES;

									} else {
										code += strdup(yytext);
									}

								}

end_includes						{
	
									code_block = 0;

									yylval.sval = strdup(code.c_str());
									
									return END_INCLUDES;

								}

CODE_GENERATION_EXAMPLE						{

									if (code_block == 0) {
										return CODE_GENERATION_EXAMPLE;
									} else if (code_block == 1) {
										code_block = 2;
										return CODE_GENERATION_EXAMPLE;
									} else {
										code += strdup(yytext);
									}

								}

NAMESPACES						{

									if (code_block == 0) {
										return NAMESPACES;
									} else if (code_block == 1) {
										code_block = 2;
										return NAMESPACES;
									} else {
										code += strdup(yytext);
									}
								}

ELIXIR							{

									if (code_block == 0) {
										return ELIXIR;
									} else if (code_block == 1) {
										code_block = 2;
										return ELIXIR;
									} else {
										code += strdup(yytext);
									}

								}

NO_ONE							{

									if (code_block == 0) {
										return NO_ONE;
									} else {
										code += strdup(yytext);
									}

								}

BREADTH							{

									if (code_block == 0) {
										return BREADTH;
									} else {
										code += strdup(yytext);
									}

								}

DEPTH							{

									if (code_block == 0) {
										return DEPTH;
									} else {
										code += strdup(yytext);
									}

								}

PRIORITY|priority				{

									if (code_block == 0) {
										return PRIORITY;
									} else {
										code += strdup(yytext);
									}

								}

properties						{

									if (code_block == 0) {
										return PROPERTIES;
									} else {
										code += strdup(yytext);
									}

								};

end_properties					{

									if (code_block == 0) {
										return END_PROPERTIES;
									} else {
										code += strdup(yytext);
									}

								};

target							{

									if (code_block == 0) {
										return TARGET;
									} else {
										code += strdup(yytext);
									}

								};

strategy						{

									if (code_block == 0) {
										return STRATEGY;
									} else {
										code += strdup(yytext);
									}

								};

keeper							{

									if (code_block == 0) {
										return KEEPER;
									} else {
										code += strdup(yytext);
									}

								}

fbe								{

									if (code_block == 0) {
										return FBE;
									} else {
										code += strdup(yytext);
									}

								};

end_fbe							{

									if (code_block == 0) {
										return END_FBE;
									} else {
										code += strdup(yytext);
									}

								}

private							{

									if (code_block == 0) {
										return PRIVATE;
									} else {
										code += strdup(yytext);
									}

								}

public							{

									if (code_block == 0) {
										return PUBLIC;
									} else {
										code += strdup(yytext);
									}

								}

instance						{

									if (code_block == 0) {
										return INSTANCE;
									} else {
										code += strdup(yytext);
									}

								}

attribute						{

									if (code_block == 0) {
										return ATTRIBUTE;
									} else {
										code += strdup(yytext);
									}

								}

boolean|Boolean							{

									if (code_block == 0) {
										return BOOLEAN;
									} else {
										code += strdup(yytext);
									}

								}

integer|Integer							{

									if (code_block == 0) {
										return INTEGER;
									} else {
										code += strdup(yytext);
									}

								}

double|Double							{

									if (code_block == 0) {
										return DOUBLE;
									} else {
										code += strdup(yytext);
									}

								}

string|String							{

									if (code_block == 0) {
										return STRING;
									} else {
										code += strdup(yytext);
									}

								}

char|Char							{

									if (code_block == 0) {
										return CHAR;
									} else {
										code += strdup(yytext);
									}

								}

index							{

									if (code_block == 0) {
										return INDEX;
									} else {
										code += strdup(yytext);
									}

								}

from							{

									if (code_block == 0) {
										return FROM;
									} else {
										code += strdup(yytext);
									}

								}

to							{

									if (code_block == 0) {
										return TO;
									} else {
										code += strdup(yytext);
									}

								}


method							{

									if (code_block == 0) {
										return METHOD;
									} else {
										code += strdup(yytext);
									}

								}

end_method						{

									if (code_block == 0) {
										return END_METHOD;
									} else {
										code += strdup(yytext);
									}

								}

params							{

									if (code_block == 0) {
										return PARAMS;
									} else {
										code += strdup(yytext);
									}

								}

end_params						{

									if (code_block == 0) {
										return END_PARAMS;
									} else {
										code += strdup(yytext);
									}

								}

code							{

									if (code_block == 0) {
										
										code_block = 1;

										code = "";

										return CODE;

									} else {
										code += strdup(yytext);
									}

								}

end_code						{
	
									code_block = 0;

									yylval.sval = strdup(code.c_str());
									
									return END_CODE;

								}

attribution							{

									if (code_block == 0) {
										return ATTRIBUTION;
									} else {
										code += strdup(yytext);
									}

								}

end_attribution							{

									if (code_block == 0) {
										return END_ATTRIBUTION;
									} else {
										code += strdup(yytext);
									}

								}

rule							{

									if (code_block == 0) {
										return RULE;
									} else {
										code += strdup(yytext);
									}

								}

end_rule						{

									if (code_block == 0) {
										return END_RULE;
									} else {
										code += strdup(yytext);
									}

								}

formation_rule					{

									if (code_block == 0) {
										return FORMATION_RULE;
									} else {
										code += strdup(yytext);
									}

								}

end_formation_rule				{

									if (code_block == 0) {
										return END_FORMATION_RULE;
									} else {
										code += strdup(yytext);
									}

								}

condition						{

									if (code_block == 0) {
										return CONDITION;
									} else {
										code += strdup(yytext);
									}

								}

end_condition					{

									if (code_block == 0) {
										return END_CONDITION;
									} else {
										code += strdup(yytext);
									}

								}

subcondition					{

									if (code_block == 0) {
										return SUBCONDITION;
									} else {
										code += strdup(yytext);
									}

								}

end_subcondition				{

									if (code_block == 0) {
										return END_SUBCONDITION;
									} else {
										code += strdup(yytext);
									}

								}

premise							{

									if (code_block == 0) {
										return PREMISE;
									} else {
										code += strdup(yytext);
									}

								}

end_premise						{

									if (code_block == 0) {
										return END_PREMISE;
									} else {
										code += strdup(yytext);
									}

								}

impertinent						{

									if (code_block == 0) {
										return IMPERTINENT;
									} else {
										code += strdup(yytext);
									}

								}

action							{

									if (code_block == 0) {
										return ACTION;
									} else {
										code += strdup(yytext);
									}

								}

end_action						{

									if (code_block == 0) {
										return END_ACTION;
									} else {
										code += strdup(yytext);
									}

								}

instigation						{

									if (code_block == 0) {
										return INSTIGATION;
									} else {
										code += strdup(yytext);
									}

								}

end_instigation					{

									if (code_block == 0) {
										return END_INSTIGATION;
									} else {
										code += strdup(yytext);
									}

								}

sequential						{

									if (code_block == 0) {
										return SEQUENTIAL;
									} else {
										code += strdup(yytext);
									}

								}

parallel						{

									if (code_block == 0) {
										return PARALLEL;
									} else {
										code += strdup(yytext);
									}

								}

call							{

									if (code_block == 0) {
										return CALL;
									} else {
										code += strdup(yytext);
									}

								}


main							{

									if (code_block == 0) {
										return MAIN;
									} else {
										code += strdup(yytext);
									}

								}

end_main						{

									if (code_block == 0) {
										return END_MAIN;
									} else {
										code += strdup(yytext);
									}

								}

"="								{

									if (code_block == 0) {
										return ASSIGN;
									} else {
										code += strdup(yytext);
									}

								}

"=="							{

									if (code_block == 0) {
										return EQ;
									} else {
										code += strdup(yytext);
									}

								}

"!="							{

									if (code_block == 0) {
										return NE;
									} else {
										code += strdup(yytext);
									}

								}

"<"								{

									if (code_block == 0) {
										return LT;
									} else {
										code += strdup(yytext);
									}

								}

">"								{

									if (code_block == 0) {
										return GT;
									} else {
										code += strdup(yytext);
									}

								}

"<="							{

									if (code_block == 0) {
										return LE;
									} else {
										code += strdup(yytext);
									}

								}

">="								{

									if (code_block == 0) {
										return GE;
									} else {
										code += strdup(yytext);
									}

								}

"+"								{
									if (code_block == 0) {
										return PLUS;
									} else {
										code += strdup(yytext);
									}
								}

"-"								{
									if (code_block == 0) {
										return MINUS;
									} else {
										code += strdup(yytext);
									}
								}

"*"								{
									if (code_block == 0) {
										return MULTIPLY;
									} else {
										code += strdup(yytext);
									}
								}

"/"								{
									if (code_block == 0) {
										return DIVIDE;
									} else {
										code += strdup(yytext);
									}
								}

"sqrt"							{
									if (code_block == 0) {
										return SQRT;
									} else {
										code += strdup(yytext);
									}
								}

"("								{

									if (code_block == 0) {
										return LP;
									} else {
										code += strdup(yytext);
									}

								}

")"								{

									if (code_block == 0) {
										return RP;
									} else {
										code += strdup(yytext);
									}

								}

"["								{

									if (code_block == 0) {
										return LB;
									} else {
										code += strdup(yytext);
									}

								}

"]"								{

									if (code_block == 0) {
										return RB;
									} else {
										code += strdup(yytext);
									}

								}

"{"								{

									if (code_block == 0) {
										return LC;
									} else {
										code += strdup(yytext);
									}

								}

"}"								{

									if (code_block == 0) {
										return RC;
									} else {
										code += strdup(yytext);
									}

								}

","								{

									if (code_block == 0) {
										return COMMA;
									} else {
										code += strdup(yytext);
									}

								}

"."								{

									if (code_block == 0) {
										
										return POINT;

									} else {
										code += strdup(yytext);
									}

								}

and								{

									if (code_block == 0) {
										return AND;
									} else {
										code += strdup(yytext);
									}

								}

or								{

									if (code_block == 0) {
										return OR;
									} else {
										code += strdup(yytext);
									}

								}

true							{

									if (code_block == 0) {
										return TRUE;
									} else {
										code += strdup(yytext);
									}

								}

false							{

									if (code_block == 0) {
										return FALSE;
									} else {
										code += strdup(yytext);
									}

								}

this								{

									if (code_block == 0) {

										yylval.sval=strdup(yytext);
										return THIS;

									} else {
										code += strdup(yytext);
									}

								}

[-+]?[0-9]+						{

									if (code_block == 0) {
										
										yylval.sval=strdup(yytext);
										return INTEGER_VALUE;

									} else {
										code += strdup(yytext);
									}

								}


[-+]?[0-9]*\.?[0-9]+			{

									if (code_block == 0) {
										yylval.sval=strdup(yytext);
										return DOUBLE_VALUE;
									} else {
										code += strdup(yytext);
									}

								}

\'.\'							{

									if (code_block == 0) {
										yylval.sval=strdup(yytext);
										return CHAR_VALUE;
									} else {
										code += strdup(yytext);
									}

								}

\"(\\.|[^"\\])*\"				{

									if (code_block == 0) {
										
										yylval.sval = strdup(yytext);
										return STRING_VALUE;

									} else {
										code += strdup(yytext);
									}

								}

[a-zA-Z\_]([a-zA-Z0-9\_])*		{

									if (code_block == 0) {
										
										yylval.sval = strdup(yytext);
										return ID;

									} else {
										code += strdup(yytext);
									}

								}

\n								{

									line_num++;

									if (code_block == 2) {
										code += strdup(yytext);
									}

								}

"//".*							{

									if (code_block == 2) {
										code += strdup(yytext);
									}

								}

"/*"(.|\n)*"*/"					{

									if (code_block == 2) {
										code += strdup(yytext);
									}

								}


[\t\f " "]						{

									if (code_block == 2) {
										code += strdup(yytext);
									}

								}

.								{

									if (code_block == 0) {
										fprintf (stderr, "'%c' (0%o): Caractere ilegal na linha %d\n",
										yytext[0], yytext[0], line_num);
									} else {
										code += strdup(yytext);
									}

								}

<<EOF>>							{
									return 0;
								}

%%

int yywrap() {
	return 1;
}

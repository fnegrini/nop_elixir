
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 1 "front\\src\\bison.y"

    
    #include "Compiler.h"
    #include "NOPGraph.h"

    #include "elements/Target.h"
    #include "elements/Strategy.h"
    #include "elements/Type.h"
    #include "elements/Visibility.h"
    #include "elements/Param.h"
    #include "elements/Symbol.h"
    #include "elements/Conjunction.h"
    #include "elements/Factor.h"
    #include "elements/Expression.h"
    #include "elements/Argument.h"
    #include "elements/Instigation.h"
    #include "elements/Execution.h"
    #include "elements/VectorDto.h"

    #include "generation/example/CodeGenerationExample.h"
    #include "generation/namespaces/NamespacesCompiler.h"
    //#include "generation/framework_cpp_2_0/FrameworkCPP20Compiler.h"
    //#include "generation/framework_cpp_3_0/FrameworkCPP30Compiler.h"
    #include "generation/elixir/CodeGenerationElixir.h"

    #include <stdio.h>
    #include <dirent.h>
    #include <sys/stat.h>

    #include <string.h>
    #include <iostream>

    using namespace std;

    Compiler *compiler = 0;
    NOPGraph *graph = 0;
    
    extern FILE *yyin;
    
    int yylex(void);
    void yyerror(const char *s);
    void validate(const std::string errorMessage);
    
    std::string current_file;
    extern int line_num;



/* Line 189 of yacc.c  */
#line 122 "front\\output\\bison.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INCLUDES = 258,
     END_INCLUDES = 259,
     CODE_GENERATION_EXAMPLE = 260,
     NAMESPACES = 261,
     FRAMEWORK_CPP_2_0 = 262,
     FRAMEWORK_CPP_3_0 = 263,
     ELIXIR = 264,
     NO_ONE = 265,
     BREADTH = 266,
     DEPTH = 267,
     PRIORITY = 268,
     FBE = 269,
     END_FBE = 270,
     PROPERTIES = 271,
     END_PROPERTIES = 272,
     TARGET = 273,
     STRATEGY = 274,
     KEEPER = 275,
     PRIVATE = 276,
     PUBLIC = 277,
     INSTANCE = 278,
     ATTRIBUTE = 279,
     BOOLEAN = 280,
     INTEGER = 281,
     DOUBLE = 282,
     STRING = 283,
     CHAR = 284,
     METHOD = 285,
     END_METHOD = 286,
     PARAMS = 287,
     END_PARAMS = 288,
     CODE = 289,
     END_CODE = 290,
     ATTRIBUTION = 291,
     END_ATTRIBUTION = 292,
     RULE = 293,
     END_RULE = 294,
     FORMATION_RULE = 295,
     END_FORMATION_RULE = 296,
     INDEX = 297,
     FROM = 298,
     TO = 299,
     CONDITION = 300,
     END_CONDITION = 301,
     SUBCONDITION = 302,
     END_SUBCONDITION = 303,
     PREMISE = 304,
     END_PREMISE = 305,
     IMPERTINENT = 306,
     ACTION = 307,
     END_ACTION = 308,
     INSTIGATION = 309,
     END_INSTIGATION = 310,
     SEQUENTIAL = 311,
     PARALLEL = 312,
     CALL = 313,
     MAIN = 314,
     END_MAIN = 315,
     ASSIGN = 316,
     EQ = 317,
     NE = 318,
     LT = 319,
     GT = 320,
     LE = 321,
     GE = 322,
     LB = 323,
     RB = 324,
     LC = 325,
     RC = 326,
     PLUS = 327,
     MINUS = 328,
     MULTIPLY = 329,
     DIVIDE = 330,
     SQRT = 331,
     LP = 332,
     RP = 333,
     COMMA = 334,
     POINT = 335,
     AND = 336,
     OR = 337,
     TRUE = 338,
     FALSE = 339,
     THIS = 340,
     INTEGER_VALUE = 341,
     DOUBLE_VALUE = 342,
     CHAR_VALUE = 343,
     STRING_VALUE = 344,
     ID = 345,
     COMMENT = 346
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 50 "front\\src\\bison.y"

    int ival;
    float fval;
    char *sval;
    void *pval;



/* Line 214 of yacc.c  */
#line 258 "front\\output\\bison.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 270 "front\\output\\bison.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   333

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  92
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  61
/* YYNRULES -- Number of rules.  */
#define YYNRULES  156
/* YYNRULES -- Number of states.  */
#define YYNSTATES  287

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   346

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     8,    10,    13,    15,    18,    20,    23,
      25,    28,    30,    33,    35,    38,    40,    43,    45,    48,
      50,    53,    55,    58,    62,    66,    68,    71,    74,    77,
      79,    81,    83,    85,    87,    89,    91,    93,    95,    99,
     105,   112,   119,   130,   136,   138,   141,   144,   146,   150,
     152,   155,   158,   160,   163,   167,   173,   180,   186,   194,
     201,   203,   206,   213,   217,   219,   222,   225,   228,   232,
     237,   241,   246,   248,   252,   256,   261,   263,   267,   271,
     276,   281,   287,   291,   293,   295,   297,   299,   301,   303,
     308,   314,   318,   323,   325,   328,   333,   339,   343,   348,
     350,   352,   354,   357,   360,   366,   372,   379,   386,   388,
     392,   394,   396,   398,   403,   405,   407,   409,   411,   413,
     415,   417,   419,   421,   425,   427,   429,   431,   433,   435,
     437,   439,   441,   443,   447,   451,   453,   457,   461,   465,
     469,   474,   476,   481,   486,   488,   492,   496,   498,   500,
     502,   504,   506,   510,   513,   515,   518
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
      93,     0,    -1,    14,   147,    94,    15,    -1,    95,    -1,
      95,    94,    -1,    96,    -1,    96,    94,    -1,   101,    -1,
     101,    94,    -1,   102,    -1,   102,    94,    -1,   103,    -1,
     103,    94,    -1,   104,    -1,   104,    94,    -1,   105,    -1,
     105,    94,    -1,   113,    -1,   113,    94,    -1,   114,    -1,
     114,    94,    -1,   150,    -1,   150,    94,    -1,     3,    99,
       4,    -1,    16,    97,    17,    -1,    98,    -1,    98,    97,
      -1,    18,    99,    -1,    19,   100,    -1,     5,    -1,     6,
      -1,     7,    -1,     8,    -1,     9,    -1,    10,    -1,    11,
      -1,    12,    -1,    13,    -1,   139,   147,   147,    -1,   139,
     136,   147,    61,   142,    -1,   139,   147,    68,    86,    69,
     147,    -1,   139,   136,    68,    86,    69,   147,    -1,   139,
     136,    68,    86,    69,   147,    61,    70,   141,    71,    -1,
     139,    30,   147,   106,    31,    -1,   110,    -1,   107,   112,
      -1,   107,   110,    -1,   112,    -1,    32,   108,    33,    -1,
     109,    -1,   109,   108,    -1,   136,   147,    -1,   111,    -1,
     111,   110,    -1,    34,    99,    35,    -1,    36,   144,    61,
     142,    37,    -1,    38,   147,   117,   120,   127,    39,    -1,
      38,   147,   120,   127,    39,    -1,    40,   147,   115,   117,
     120,   127,    41,    -1,    40,   147,   115,   120,   127,    41,
      -1,   116,    -1,   116,   115,    -1,    42,   147,    43,    86,
      44,    86,    -1,    16,   118,    17,    -1,   119,    -1,   119,
     118,    -1,    13,    86,    -1,    20,   143,    -1,    45,   121,
      46,    -1,    45,   147,   121,    46,    -1,    45,   123,    46,
      -1,    45,   147,   123,    46,    -1,   122,    -1,   122,   140,
     121,    -1,    47,   123,    48,    -1,    47,   147,   123,    48,
      -1,   124,    -1,   124,   140,   123,    -1,    49,   125,    50,
      -1,    49,   147,   125,    50,    -1,    49,    51,   125,    50,
      -1,    49,    51,   147,   125,    50,    -1,   142,   126,   142,
      -1,    62,    -1,    63,    -1,    64,    -1,    65,    -1,    66,
      -1,    67,    -1,    52,   130,   128,    53,    -1,    52,   147,
     130,   128,    53,    -1,    52,   128,    53,    -1,    52,   147,
     128,    53,    -1,   129,    -1,   129,   128,    -1,    54,   130,
     131,    55,    -1,    54,   147,   130,   131,    55,    -1,    54,
     131,    55,    -1,    54,   147,   131,    55,    -1,    56,    -1,
      57,    -1,   132,    -1,   132,   131,    -1,    58,   133,    -1,
      90,    80,    90,    77,    78,    -1,    85,    80,    90,    77,
      78,    -1,    90,    80,    90,    77,   134,    78,    -1,    85,
      80,    90,    77,   134,    78,    -1,   135,    -1,   135,    79,
     134,    -1,   142,    -1,   138,    -1,   137,    -1,   138,    64,
      86,    65,    -1,    25,    -1,    26,    -1,    27,    -1,    28,
      -1,    29,    -1,    21,    -1,    22,    -1,    81,    -1,    82,
      -1,   141,    79,   142,    -1,   142,    -1,   144,    -1,   143,
      -1,    86,    -1,    87,    -1,    88,    -1,    89,    -1,    83,
      -1,    84,    -1,   145,    80,   145,    -1,    85,    80,   145,
      -1,   145,    -1,   145,    80,   146,    -1,   146,    80,   145,
      -1,   146,    80,   146,    -1,    85,    80,   146,    -1,    90,
      68,    86,    69,    -1,   147,    -1,    90,    68,   148,    69,
      -1,    90,    68,   147,    69,    -1,    90,    -1,   147,   149,
      86,    -1,    86,   149,   147,    -1,    72,    -1,    73,    -1,
      74,    -1,    75,    -1,    76,    -1,    59,   151,    60,    -1,
      59,    60,    -1,   152,    -1,   152,   151,    -1,   144,    61,
     142,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   215,   215,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   233,   234,   235,   236,   237,   238,   239,   240,
     241,   242,   243,   246,   255,   258,   259,   261,   268,   277,
     280,   283,   287,   291,   296,   299,   302,   305,   310,   319,
     328,   340,   352,   364,   373,   374,   375,   376,   379,   382,
     383,   386,   395,   396,   399,   408,   417,   424,   433,   439,
     447,   448,   451,   456,   459,   460,   463,   466,   471,   478,
     485,   492,   501,   502,   511,   518,   527,   528,   537,   544,
     551,   558,   567,   572,   575,   578,   581,   584,   587,   592,
     599,   606,   613,   622,   623,   626,   633,   640,   647,   656,
     659,   664,   665,   668,   677,   680,   683,   686,   691,   692,
     695,   704,   707,   712,   717,   720,   723,   726,   729,   734,
     737,   742,   745,   750,   753,   757,   760,   763,   766,   769,
     772,   777,   780,   785,   788,   791,   794,   797,   800,   803,
     808,   812,   817,   821,   827,   832,   837,   843,   846,   849,
     852,   855,   860,   865,   872,   873,   875
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "INCLUDES", "END_INCLUDES",
  "CODE_GENERATION_EXAMPLE", "NAMESPACES", "FRAMEWORK_CPP_2_0",
  "FRAMEWORK_CPP_3_0", "ELIXIR", "NO_ONE", "BREADTH", "DEPTH", "PRIORITY",
  "FBE", "END_FBE", "PROPERTIES", "END_PROPERTIES", "TARGET", "STRATEGY",
  "KEEPER", "PRIVATE", "PUBLIC", "INSTANCE", "ATTRIBUTE", "BOOLEAN",
  "INTEGER", "DOUBLE", "STRING", "CHAR", "METHOD", "END_METHOD", "PARAMS",
  "END_PARAMS", "CODE", "END_CODE", "ATTRIBUTION", "END_ATTRIBUTION",
  "RULE", "END_RULE", "FORMATION_RULE", "END_FORMATION_RULE", "INDEX",
  "FROM", "TO", "CONDITION", "END_CONDITION", "SUBCONDITION",
  "END_SUBCONDITION", "PREMISE", "END_PREMISE", "IMPERTINENT", "ACTION",
  "END_ACTION", "INSTIGATION", "END_INSTIGATION", "SEQUENTIAL", "PARALLEL",
  "CALL", "MAIN", "END_MAIN", "ASSIGN", "EQ", "NE", "LT", "GT", "LE", "GE",
  "LB", "RB", "LC", "RC", "PLUS", "MINUS", "MULTIPLY", "DIVIDE", "SQRT",
  "LP", "RP", "COMMA", "POINT", "AND", "OR", "TRUE", "FALSE", "THIS",
  "INTEGER_VALUE", "DOUBLE_VALUE", "CHAR_VALUE", "STRING_VALUE", "ID",
  "COMMENT", "$accept", "PROGRAM", "fbe_body", "include_block",
  "properties_block", "properties", "property", "target", "strategy",
  "instance", "attribute", "vector_instance", "vector_attribute", "method",
  "method_body", "params_body", "params", "param", "code_blocks",
  "code_block", "attribution", "rule", "formation_rule", "rule_indexes",
  "rule_index", "rule_properties_block", "rule_properties",
  "rule_property", "condition", "subconditions", "subcondition",
  "premises", "premise", "expression", "symbol", "action", "instigations",
  "instigation", "execution", "calls", "call", "elementcall", "arguments",
  "argument", "type", "supertype", "basictype", "visibility",
  "conjunction", "array_factor", "factor", "boolean", "element",
  "ID_or_VectElem", "VectElem", "id", "vector_operation", "operation",
  "main_block", "main_attributions", "main_attribution", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    92,    93,    94,    94,    94,    94,    94,    94,    94,
      94,    94,    94,    94,    94,    94,    94,    94,    94,    94,
      94,    94,    94,    95,    96,    97,    97,    98,    98,    99,
      99,    99,    99,    99,   100,   100,   100,   100,   101,   102,
     103,   104,   104,   105,   106,   106,   106,   106,   107,   108,
     108,   109,   110,   110,   111,   112,   113,   113,   114,   114,
     115,   115,   116,   117,   118,   118,   119,   119,   120,   120,
     120,   120,   121,   121,   122,   122,   123,   123,   124,   124,
     124,   124,   125,   126,   126,   126,   126,   126,   126,   127,
     127,   127,   127,   128,   128,   129,   129,   129,   129,   130,
     130,   131,   131,   132,   133,   133,   133,   133,   134,   134,
     135,   136,   136,   137,   138,   138,   138,   138,   138,   139,
     139,   140,   140,   141,   141,   142,   142,   142,   142,   142,
     142,   143,   143,   144,   144,   144,   144,   144,   144,   144,
     145,   145,   146,   146,   147,   148,   148,   149,   149,   149,
     149,   149,   150,   150,   151,   151,   152
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     4,     1,     2,     1,     2,     1,     2,     1,
       2,     1,     2,     1,     2,     1,     2,     1,     2,     1,
       2,     1,     2,     3,     3,     1,     2,     2,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     5,
       6,     6,    10,     5,     1,     2,     2,     1,     3,     1,
       2,     2,     1,     2,     3,     5,     6,     5,     7,     6,
       1,     2,     6,     3,     1,     2,     2,     2,     3,     4,
       3,     4,     1,     3,     3,     4,     1,     3,     3,     4,
       4,     5,     3,     1,     1,     1,     1,     1,     1,     4,
       5,     3,     4,     1,     2,     4,     5,     3,     4,     1,
       1,     1,     2,     2,     5,     5,     6,     6,     1,     3,
       1,     1,     1,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     3,     1,     3,     3,     3,     3,
       4,     1,     4,     4,     1,     3,     3,     1,     1,     1,
       1,     1,     3,     2,     1,     2,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,   144,     0,     1,     0,     0,   119,   120,
       0,     0,     0,     0,     3,     5,     7,     9,    11,    13,
      15,    17,    19,     0,    21,    29,    30,    31,    32,    33,
       0,     0,     0,     0,    25,     0,     0,   153,     0,   144,
       0,   135,     0,   141,     0,   154,     2,     4,     6,     8,
      10,    12,    14,    16,    18,    20,   114,   115,   116,   117,
     118,     0,     0,   112,   111,     0,    22,    23,    27,    34,
      35,    36,    37,    28,    24,    26,     0,     0,     0,     0,
       0,     0,    60,     0,     0,     0,     0,     0,   152,   155,
       0,     0,     0,     0,     0,    38,     0,     0,     0,    64,
       0,     0,     0,    72,     0,    76,     0,     0,     0,     0,
       0,     0,     0,    61,   134,   139,     0,     0,     0,   131,
     132,   127,   128,   129,   130,   156,   126,   125,   133,   136,
     137,   138,     0,     0,     0,     0,     0,    44,    52,    47,
       0,     0,     0,     0,    66,    67,    63,    65,     0,     0,
       0,     0,     0,   141,    68,   121,   122,     0,    70,     0,
       0,     0,     0,     0,    99,   100,     0,    93,     0,     0,
      57,     0,     0,     0,   140,   147,   148,   149,   150,   151,
       0,   143,     0,   142,     0,    49,     0,     0,     0,    43,
      46,    45,    53,     0,    39,   113,     0,    74,     0,     0,
     141,    78,    83,    84,    85,    86,    87,    88,     0,     0,
      73,    77,    69,    71,    56,     0,     0,     0,   101,     0,
      91,    94,     0,     0,     0,     0,     0,    59,   146,   145,
      48,    50,    51,    54,     0,    41,    40,    75,    80,     0,
      82,    79,     0,     0,   103,     0,    97,   102,     0,     0,
      89,    92,     0,     0,    58,     0,     0,    81,     0,     0,
      95,     0,    98,    90,    62,    55,     0,     0,     0,    96,
       0,   124,     0,     0,    42,     0,   105,     0,   108,   110,
     104,     0,   123,   107,     0,   106,   109
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,    13,    14,    15,    33,    34,    30,    73,    16,
      17,    18,    19,    20,   135,   136,   184,   185,   137,   138,
     139,    21,    22,    81,    82,    78,    98,    99,    79,   102,
     103,   104,   105,   151,   208,   109,   166,   167,   168,   217,
     218,   244,   277,   278,   186,    63,    64,    23,   157,   270,
     152,   126,   127,    41,    42,    43,   118,   180,    24,    44,
      45
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -245
static const yytype_int16 yypact[] =
{
      69,     7,   102,  -245,     9,  -245,   195,   141,  -245,  -245,
       7,     7,   -33,   111,     9,     9,     9,     9,     9,     9,
       9,     9,     9,   -21,     9,  -245,  -245,  -245,  -245,  -245,
     125,   195,   205,   130,   141,     6,   120,  -245,    97,   110,
     123,   142,   143,  -245,   131,   -26,  -245,  -245,  -245,  -245,
    -245,  -245,  -245,  -245,  -245,  -245,  -245,  -245,  -245,  -245,
    -245,     7,   -40,  -245,   160,   -24,  -245,  -245,  -245,  -245,
    -245,  -245,  -245,  -245,  -245,  -245,    35,   -11,   181,   173,
       7,     6,   120,   137,    99,    68,   137,   137,  -245,  -245,
     178,   144,   167,   145,   146,  -245,   147,    87,   212,    35,
     -16,     2,   188,   116,   189,   116,    -4,   173,   -15,   197,
     194,   181,   173,  -245,  -245,  -245,    92,   100,   169,  -245,
    -245,  -245,  -245,  -245,  -245,  -245,  -245,  -245,  -245,  -245,
    -245,  -245,   180,   195,   -26,   208,    24,  -245,   206,  -245,
     172,    68,   177,   174,  -245,  -245,  -245,  -245,   196,   198,
      68,   199,    55,    68,  -245,  -245,  -245,   201,  -245,   198,
     200,   204,   213,    37,  -245,  -245,   192,   202,   202,   138,
    -245,   165,   173,   214,  -245,  -245,  -245,  -245,  -245,  -245,
       7,  -245,   168,  -245,   220,   180,     7,   222,   203,  -245,
    -245,  -245,  -245,     7,  -245,  -245,     7,  -245,   210,   209,
      68,  -245,  -245,  -245,  -245,  -245,  -245,  -245,    68,   211,
    -245,  -245,  -245,  -245,  -245,    -9,   207,   215,   207,   163,
    -245,  -245,   216,   218,   202,   219,   221,  -245,  -245,  -245,
    -245,  -245,  -245,  -245,    68,   217,  -245,  -245,  -245,   223,
    -245,  -245,   186,   187,  -245,   224,  -245,  -245,   207,   225,
    -245,  -245,   228,   182,  -245,   235,   190,  -245,   184,   185,
    -245,   227,  -245,  -245,  -245,  -245,    68,   226,   229,  -245,
     -25,  -245,    26,    47,  -245,    68,  -245,   230,   231,  -245,
    -245,   233,  -245,  -245,    68,  -245,  -245
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -245,  -245,   124,  -245,  -245,   242,  -245,   -28,  -245,  -245,
    -245,  -245,  -245,  -245,  -245,  -245,    98,  -245,    75,  -245,
     148,  -245,  -245,   232,  -245,   234,   191,  -245,   -44,   -87,
    -245,   -86,  -245,  -129,  -245,   -94,  -152,  -245,  -137,  -120,
    -245,  -245,  -244,  -245,   254,  -245,  -245,  -245,   183,  -245,
     -85,   236,   -10,    20,    96,     0,  -245,   170,  -245,   240,
    -245
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint16 yytable[] =
{
     125,     4,    40,    68,    56,    57,    58,    59,    60,    61,
      35,    36,     6,   162,   148,   221,   222,   223,   173,   160,
     161,   199,    76,    65,   209,     7,   216,    37,    91,   281,
       8,     9,   224,   101,   107,    40,   100,   112,   101,   163,
     286,   164,   165,   100,    94,   101,   274,    10,    96,    11,
       3,    77,    38,   150,   275,    97,   194,    39,   133,    38,
     134,    90,    92,   198,    39,    95,     3,   172,    12,     3,
     210,   239,   252,   211,     3,     3,   242,   106,   226,     3,
     110,   243,   248,     1,   117,   119,   120,    38,   121,   122,
     123,   124,    39,   164,   165,   215,   245,     3,   247,   249,
     149,   153,     5,   114,   276,   187,   128,   130,   169,   119,
     120,    38,   121,   122,   123,   124,    39,   202,   203,   204,
     205,   206,   207,   240,   188,   280,    46,     3,   261,    67,
     119,   120,    38,   121,   122,   123,   124,    39,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    74,    66,   255,
     200,   119,   120,    38,   121,   122,   123,   124,    39,    31,
      32,   174,    80,   219,   175,   176,   177,   178,   179,   181,
     119,   120,   175,   176,   177,   178,   179,    83,    84,   115,
     228,   271,   129,   131,    85,   116,   232,   279,   279,     3,
     282,    88,   163,   235,   164,   165,   236,   155,   156,   279,
      25,    26,    27,    28,    29,    56,    57,    58,    59,    60,
     132,   190,   133,   192,   134,    69,    70,    71,    72,   164,
     165,   215,    86,    87,    93,   108,    77,    39,   141,   146,
     140,   142,   143,   144,   154,   158,   170,   171,   183,   189,
     133,   193,   195,   196,   197,   220,   212,   101,   100,   201,
     213,   225,   214,   230,   229,   227,   163,   233,   237,   238,
     266,   241,   254,   253,   234,   215,   258,   259,   264,   250,
     246,   251,   265,   257,   267,   268,    75,    62,   256,   260,
     262,   263,   269,   231,   191,    89,     0,   182,   159,     0,
     147,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   272,     0,     0,   273,     0,   283,     0,
     284,   285,     0,     0,   113,   111,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   145
};

static const yytype_int16 yycheck[] =
{
      85,     1,    12,    31,    25,    26,    27,    28,    29,    30,
      10,    11,     3,   107,   100,   167,   168,   169,   112,   106,
     106,   150,    16,    23,   153,    16,   163,    60,    68,   273,
      21,    22,   169,    49,    78,    45,    47,    81,    49,    54,
     284,    56,    57,    47,    68,    49,    71,    38,    13,    40,
      90,    45,    85,    51,    79,    20,   141,    90,    34,    85,
      36,    61,    62,   149,    90,    65,    90,   111,    59,    90,
     157,   200,   224,   159,    90,    90,    85,    77,   172,    90,
      80,    90,   219,    14,    84,    83,    84,    85,    86,    87,
      88,    89,    90,    56,    57,    58,   216,    90,   218,   219,
     100,   101,     0,    83,    78,   133,    86,    87,   108,    83,
      84,    85,    86,    87,    88,    89,    90,    62,    63,    64,
      65,    66,    67,   208,   134,    78,    15,    90,   248,     4,
      83,    84,    85,    86,    87,    88,    89,    90,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    17,    24,   234,
     150,    83,    84,    85,    86,    87,    88,    89,    90,    18,
      19,    69,    42,   163,    72,    73,    74,    75,    76,    69,
      83,    84,    72,    73,    74,    75,    76,    80,    68,    83,
     180,   266,    86,    87,    61,    86,   186,   272,   273,    90,
     275,    60,    54,   193,    56,    57,   196,    81,    82,   284,
       5,     6,     7,     8,     9,    25,    26,    27,    28,    29,
      32,   136,    34,   138,    36,    10,    11,    12,    13,    56,
      57,    58,    80,    80,    64,    52,    45,    90,    61,    17,
      86,    86,    86,    86,    46,    46,    39,    43,    69,    31,
      34,    69,    65,    69,    48,    53,    46,    49,    47,    50,
      46,    86,    39,    33,    86,    41,    54,    35,    48,    50,
      70,    50,    41,    44,    61,    58,    80,    80,    86,    53,
      55,    53,    37,    50,    90,    90,    34,    23,    61,    55,
      55,    53,    55,   185,   136,    45,    -1,   117,   105,    -1,
      99,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    77,    -1,    -1,    77,    -1,    78,    -1,
      79,    78,    -1,    -1,    82,    81,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    97
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    14,    93,    90,   147,     0,     3,    16,    21,    22,
      38,    40,    59,    94,    95,    96,   101,   102,   103,   104,
     105,   113,   114,   139,   150,     5,     6,     7,     8,     9,
      99,    18,    19,    97,    98,   147,   147,    60,    85,    90,
     144,   145,   146,   147,   151,   152,    15,    94,    94,    94,
      94,    94,    94,    94,    94,    94,    25,    26,    27,    28,
      29,    30,   136,   137,   138,   147,    94,     4,    99,    10,
      11,    12,    13,   100,    17,    97,    16,    45,   117,   120,
      42,   115,   116,    80,    68,    61,    80,    80,    60,   151,
     147,    68,   147,    64,    68,   147,    13,    20,   118,   119,
      47,    49,   121,   122,   123,   124,   147,   120,    52,   127,
     147,   117,   120,   115,   145,   146,    86,   147,   148,    83,
      84,    86,    87,    88,    89,   142,   143,   144,   145,   146,
     145,   146,    32,    34,    36,   106,   107,   110,   111,   112,
      86,    61,    86,    86,    86,   143,    17,   118,   123,   147,
      51,   125,   142,   147,    46,    81,    82,   140,    46,   140,
     121,   123,   127,    54,    56,    57,   128,   129,   130,   147,
      39,    43,   120,   127,    69,    72,    73,    74,    75,    76,
     149,    69,   149,    69,   108,   109,   136,    99,   144,    31,
     110,   112,   110,    69,   142,    65,    69,    48,   123,   125,
     147,    50,    62,    63,    64,    65,    66,    67,   126,   125,
     121,   123,    46,    46,    39,    58,   130,   131,   132,   147,
      53,   128,   128,   128,   130,    86,   127,    41,   147,    86,
      33,   108,   147,    35,    61,   147,   147,    48,    50,   125,
     142,    50,    85,    90,   133,   131,    55,   131,   130,   131,
      53,    53,   128,    44,    41,   142,    61,    50,    80,    80,
      55,   131,    55,    53,    86,    37,    70,    90,    90,    55,
     141,   142,    77,    77,    71,    79,    78,   134,   135,   142,
      78,   134,   142,    78,    79,    78,   134
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 215 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addFbe(
                                        (Fbe*)graph->createFbe((yyvsp[(2) - (4)].sval))
                                    )
                                );
                            ;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 246 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addIncludeBlock(
                                        (IncludeBlock*)graph->createIncludeBlock((Target*)(yyvsp[(2) - (3)].pval), (yyvsp[(3) - (3)].sval))
                                    )
                                );
                            ;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 261 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addProperty(
                                        (Property*)(yyvsp[(2) - (2)].pval)
                                    )
                                );
                            ;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 268 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addProperty(
                                        (Property*)(yyvsp[(2) - (2)].pval)
                                    )
                                );
                            ;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 277 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createTarget(Target::CODE_GENERATION_EXAMPLE_TARGET);
                            ;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 280 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createTarget(Target::NAMESPACES_TARGET);
                            ;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 283 "front\\src\\bison.y"
    {
                                //$$ = graph->createTarget(Target::FRAMEWORK_CPP_2_0_TARGET);
								(yyval.pval) = graph->createTarget(Target::CODE_GENERATION_EXAMPLE_TARGET);
                            ;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 287 "front\\src\\bison.y"
    {
                                //$$ = graph->createTarget(Target::FRAMEWORK_CPP_3_0_TARGET);
								(yyval.pval) = graph->createTarget(Target::CODE_GENERATION_EXAMPLE_TARGET);
                            ;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 291 "front\\src\\bison.y"
    {
								(yyval.pval) = graph->createTarget(Target::ELIXIR_TARGET);
                            ;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 296 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createStrategy(Strategy::NO_ONE_STRATEGY);
                            ;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 299 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createStrategy(Strategy::BREADTH_STRATEGY);
                            ;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 302 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createStrategy(Strategy::DEPTH_STRATEGY);
                            ;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 305 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createStrategy(Strategy::PRIORITY_STRATEGY);
                            ;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 310 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addInstance(
                                        (Instance*)graph->createInstance((Visibility*)(yyvsp[(1) - (3)].pval), (yyvsp[(2) - (3)].sval), (yyvsp[(3) - (3)].sval))
                                    )
                                );
                            ;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 319 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAttribute((Visibility*)(yyvsp[(1) - (5)].pval), (Type*)(yyvsp[(2) - (5)].pval), (yyvsp[(3) - (5)].sval), (Factor*)(yyvsp[(5) - (5)].pval))
                                    )
                                );
                            ;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 328 "front\\src\\bison.y"
    {
                                std::list<Instance*> *instances = (std::list<Instance*> *)
                                        graph->createInstances((Visibility*)(yyvsp[(1) - (6)].pval), (yyvsp[(2) - (6)].sval), (yyvsp[(6) - (6)].sval), atoi((yyvsp[(4) - (6)].sval)));
                                for (Instance* instance : *instances) {
                                    validate(
                                        graph->addInstance(instance)
                                    );
                                }
                            ;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 340 "front\\src\\bison.y"
    {
                                std::list<Attribute*> *attributes = (std::list<Attribute*> *)
                                        graph->createAttributes((Visibility*)(yyvsp[(1) - (6)].pval), (Type*)(yyvsp[(2) - (6)].pval), (yyvsp[(6) - (6)].sval), NULL, atoi((yyvsp[(4) - (6)].sval)));
                                for (Attribute* attribute : *attributes) {
                                    validate(
                                        graph->addEntity(
                                            (Entity*)attribute)
                                    );
                                }
                            ;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 352 "front\\src\\bison.y"
    {
                                std::list<Attribute*> *attributes = (std::list<Attribute*> *)
                                        graph->createAttributes((Visibility*)(yyvsp[(1) - (10)].pval), (Type*)(yyvsp[(2) - (10)].pval), (yyvsp[(6) - (10)].sval), (std::vector<Factor*>*)(yyvsp[(9) - (10)].pval), atoi((yyvsp[(4) - (10)].sval)));
                                for (Attribute* attribute : *attributes) {
                                    validate(
                                        graph->addEntity(
                                            (Entity*)attribute)
                                    );
                                }
                            ;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 364 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createMethod((Visibility*)(yyvsp[(1) - (5)].pval), (yyvsp[(3) - (5)].sval))
                                    )
                                );
                            ;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 386 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addParam(
                                        (Param*)graph->createParam((Type*)(yyvsp[(1) - (2)].pval), (yyvsp[(2) - (2)].sval))
                                    )
                                );
                            ;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 399 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addCodeBlock(
                                        (CodeBlock*)graph->createCodeBlock((Target*)(yyvsp[(2) - (3)].pval), (yyvsp[(3) - (3)].sval))
                                    )
                                );
                            ;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 408 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addAttribution(
                                        (Attribution*)graph->createAttribution((ElementFactor*)(yyvsp[(2) - (5)].pval), (Factor*)(yyvsp[(4) - (5)].pval))
                                    )
                                );
                            ;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 417 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createRule((yyvsp[(2) - (6)].sval))
                                    )
                                );
                            ;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 424 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createRule((yyvsp[(2) - (5)].sval))
                                    )
                                );
                            ;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 433 "front\\src\\bison.y"
    {
                                //printf("\n\n\n[Formation Rule 1st rule]: %s", $2);
                                validate(
                                    graph->createFormationRule((yyvsp[(2) - (7)].sval))
                                );
                            ;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 439 "front\\src\\bison.y"
    {
                                //printf("\n\n\n[Formation Rule 2nd rule]: %s\n", $2);
                                validate(
                                    graph->createFormationRule((yyvsp[(2) - (6)].sval))
                                );
                            ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 451 "front\\src\\bison.y"
    {
                                //printf("\n\n\n[Rule Index]: %s\n", $2);
                                validate(graph->addFormationRuleIndex((yyvsp[(2) - (6)].sval), atoi((yyvsp[(4) - (6)].sval)), atoi((yyvsp[(6) - (6)].sval))));
                            ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 463 "front\\src\\bison.y"
    {
                                //printf("\n\n\n[Priority]: %s", $2);
                            ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 466 "front\\src\\bison.y"
    {
                                //printf("\n\n\n[Keeper]: %s", $2);
                            ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 471 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition("")
                                    )
                                );
                            ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 478 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition((yyvsp[(2) - (4)].sval))
                                    )
                                );
                            ;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 485 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition("")
                                    )
                                );
                            ;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 492 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createCondition((yyvsp[(2) - (4)].sval))
                                    )
                                );
                            ;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 502 "front\\src\\bison.y"
    {
                                validate(
                                    graph->checkSubconditionsConjunction(
                                        (Conjunction*)(yyvsp[(2) - (3)].pval)
                                    )
                                );
                            ;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 511 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createSubcondition("")
                                    )
                                );
                            ;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 518 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createSubcondition((yyvsp[(2) - (4)].sval))
                                    )
                                );
                            ;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 528 "front\\src\\bison.y"
    {
                                validate(
                                    graph->checkPremisesConjunction(
                                        (Conjunction*)(yyvsp[(2) - (3)].pval)
                                    )
                                );
                            ;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 537 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise("", (Expression*) (yyvsp[(2) - (3)].pval), false)
                                    )
                                );
                            ;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 544 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise((yyvsp[(2) - (4)].sval), (Expression*) (yyvsp[(3) - (4)].pval), false)
                                    )
                                );
                            ;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 551 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise("", (Expression*) (yyvsp[(3) - (4)].pval), true)
                                    )
                                );
                            ;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 558 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createPremise((yyvsp[(3) - (5)].sval), (Expression*) (yyvsp[(4) - (5)].pval), true)
                                    )
                                );
                            ;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 567 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createExpression((Factor*)(yyvsp[(1) - (3)].pval), (Symbol*)(yyvsp[(2) - (3)].pval), (Factor*)(yyvsp[(3) - (3)].pval));
                            ;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 572 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::EQUAL_SYMBOL);
                            ;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 575 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::NOT_EQUAL_SYMBOL);
                            ;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 578 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::LESSER_THAN_SYMBOL);
                            ;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 581 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::GREATER_THAN_SYMBOL);
                            ;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 584 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::LESS_OR_EQUAL_SYMBOL);
                            ;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 587 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::GREATER_OR_EQUAL_SYMBOL);
                            ;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 592 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction("", (Execution*)(yyvsp[(2) - (4)].pval))
                                    )
                                );
                            ;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 599 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction((yyvsp[(2) - (5)].sval), (Execution*)(yyvsp[(3) - (5)].pval))
                                    )
                                );
                            ;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 606 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction("", (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            ;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 613 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createAction((yyvsp[(2) - (4)].sval), (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            ;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 626 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation("", (Execution*)(yyvsp[(2) - (4)].pval))
                                    )
                                );
                            ;}
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 633 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation((yyvsp[(2) - (5)].sval), (Execution*)(yyvsp[(3) - (5)].pval))
                                    )
                                );
                            ;}
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 640 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation("", (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            ;}
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 647 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addEntity(
                                        (Entity*)graph->createInstigation((yyvsp[(2) - (4)].sval), (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION))
                                    )
                                );
                            ;}
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 656 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Execution*)graph->createExecution(Execution::SEQUENTIAL_EXECUTION);
                            ;}
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 659 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Execution*)graph->createExecution(Execution::PARALLEL_EXECUTION);
                            ;}
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 668 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addCall(
                                        (Call*)(yyvsp[(2) - (2)].pval)
                                    )
                                );
                            ;}
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 677 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Call*)graph->createCall((yyvsp[(1) - (5)].sval), (yyvsp[(3) - (5)].sval));
                            ;}
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 680 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Call*)graph->createCall((yyvsp[(1) - (5)].sval), (yyvsp[(3) - (5)].sval));
                            ;}
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 683 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Call*)graph->createCall((yyvsp[(1) - (6)].sval), (yyvsp[(3) - (6)].sval));
                            ;}
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 686 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Call*)graph->createCall((yyvsp[(1) - (6)].sval), (yyvsp[(3) - (6)].sval));
                            ;}
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 695 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addArgument(
                                        (Argument*)graph->createArgument((Factor*)(yyvsp[(1) - (1)].pval))
                                    )
                                );
                            ;}
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 704 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createType((yyvsp[(1) - (1)].ival));
                            ;}
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 707 "front\\src\\bison.y"
    {
                                (yyval.pval) = (yyvsp[(1) - (1)].pval);
                            ;}
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 712 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createType((yyvsp[(1) - (4)].ival), atoi((yyvsp[(3) - (4)].sval)));
                            ;}
    break;

  case 114:

/* Line 1455 of yacc.c  */
#line 717 "front\\src\\bison.y"
    {
                                (yyval.ival) = Type::BOOLEAN_TYPE;
                            ;}
    break;

  case 115:

/* Line 1455 of yacc.c  */
#line 720 "front\\src\\bison.y"
    {
                                (yyval.ival) = Type::INTEGER_TYPE;
                            ;}
    break;

  case 116:

/* Line 1455 of yacc.c  */
#line 723 "front\\src\\bison.y"
    {
                                (yyval.ival) = Type::DOUBLE_TYPE;
                            ;}
    break;

  case 117:

/* Line 1455 of yacc.c  */
#line 726 "front\\src\\bison.y"
    {
                                (yyval.ival) = Type::STRING_TYPE;
                            ;}
    break;

  case 118:

/* Line 1455 of yacc.c  */
#line 729 "front\\src\\bison.y"
    {
                                (yyval.ival) = Type::CHAR_TYPE;
                            ;}
    break;

  case 119:

/* Line 1455 of yacc.c  */
#line 734 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createVisibility(Visibility::PRIVATE_VISIBILITY);
                            ;}
    break;

  case 120:

/* Line 1455 of yacc.c  */
#line 737 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createVisibility(Visibility::PUBLIC_VISIBILITY);
                            ;}
    break;

  case 121:

/* Line 1455 of yacc.c  */
#line 742 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Conjunction*)graph->createConjunction(Conjunction::AND_CONJUNCTION);
                            ;}
    break;

  case 122:

/* Line 1455 of yacc.c  */
#line 745 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Conjunction*)graph->createConjunction(Conjunction::OR_CONJUNCTION);
                            ;}
    break;

  case 123:

/* Line 1455 of yacc.c  */
#line 750 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->addToFactorArray((Factor*)(yyvsp[(3) - (3)].pval));
                            ;}
    break;

  case 124:

/* Line 1455 of yacc.c  */
#line 753 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->addToFactorArray((Factor*)(yyvsp[(1) - (1)].pval));
                            ;}
    break;

  case 125:

/* Line 1455 of yacc.c  */
#line 757 "front\\src\\bison.y"
    {
                                (yyval.pval) = (yyvsp[(1) - (1)].pval);
                            ;}
    break;

  case 126:

/* Line 1455 of yacc.c  */
#line 760 "front\\src\\bison.y"
    {
                                (yyval.pval) = (yyvsp[(1) - (1)].pval);
                            ;}
    break;

  case 127:

/* Line 1455 of yacc.c  */
#line 763 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createIntegerFactor(atoi((yyvsp[(1) - (1)].sval)));
                            ;}
    break;

  case 128:

/* Line 1455 of yacc.c  */
#line 766 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createDoubleFactor(atof((yyvsp[(1) - (1)].sval)));
                            ;}
    break;

  case 129:

/* Line 1455 of yacc.c  */
#line 769 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createCharFactor(((char*)(yyvsp[(1) - (1)].sval))[0]);
                            ;}
    break;

  case 130:

/* Line 1455 of yacc.c  */
#line 772 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createStringFactor((yyvsp[(1) - (1)].sval));
                            ;}
    break;

  case 131:

/* Line 1455 of yacc.c  */
#line 777 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Factor*)graph->createBooleanFactor(true);
                            ;}
    break;

  case 132:

/* Line 1455 of yacc.c  */
#line 780 "front\\src\\bison.y"
    {
                                (yyval.pval) = (Factor*)graph->createBooleanFactor(false);
                            ;}
    break;

  case 133:

/* Line 1455 of yacc.c  */
#line 785 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor((yyvsp[(1) - (3)].sval), (yyvsp[(3) - (3)].sval));
                            ;}
    break;

  case 134:

/* Line 1455 of yacc.c  */
#line 788 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor((yyvsp[(1) - (3)].sval), (yyvsp[(3) - (3)].sval));
                            ;}
    break;

  case 135:

/* Line 1455 of yacc.c  */
#line 791 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor("", (yyvsp[(1) - (1)].sval));
                            ;}
    break;

  case 136:

/* Line 1455 of yacc.c  */
#line 794 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor((yyvsp[(1) - (3)].sval), (VectorDto*)(yyvsp[(3) - (3)].pval));
                            ;}
    break;

  case 137:

/* Line 1455 of yacc.c  */
#line 797 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor((VectorDto*)(yyvsp[(1) - (3)].pval), (yyvsp[(3) - (3)].sval));
                            ;}
    break;

  case 138:

/* Line 1455 of yacc.c  */
#line 800 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor((VectorDto*)(yyvsp[(1) - (3)].pval), (VectorDto*)(yyvsp[(3) - (3)].pval));
                            ;}
    break;

  case 139:

/* Line 1455 of yacc.c  */
#line 803 "front\\src\\bison.y"
    {
                                (yyval.pval) = (ElementFactor*)graph->createElementFactor((yyvsp[(1) - (3)].sval), (VectorDto*)(yyvsp[(3) - (3)].pval));
                            ;}
    break;

  case 140:

/* Line 1455 of yacc.c  */
#line 808 "front\\src\\bison.y"
    {
                                //printf("Evaluating static vector value %s[%s]", $1, $3);
                                (yyval.sval) = graph->convertVectorToId((yyvsp[(1) - (4)].sval), atoi((yyvsp[(3) - (4)].sval)));
                            ;}
    break;

  case 141:

/* Line 1455 of yacc.c  */
#line 812 "front\\src\\bison.y"
    {
                                (yyval.sval) = (yyvsp[(1) - (1)].sval);
                            ;}
    break;

  case 142:

/* Line 1455 of yacc.c  */
#line 817 "front\\src\\bison.y"
    {
                                //printf("Evaluating static vector expression value %s ", $1);
                                (yyval.pval) = (VectorDto*)graph->convertVectorToVectorElement((yyvsp[(1) - (4)].sval), (Expression*)((yyvsp[(3) - (4)].pval)));
                            ;}
    break;

  case 143:

/* Line 1455 of yacc.c  */
#line 821 "front\\src\\bison.y"
    {
                                //printf("Evaluating dinamic vector %s[%s]\n", $1, $3);
                                (yyval.pval) = (VectorDto*)graph->convertVectorToVectorElement((yyvsp[(1) - (4)].sval), (yyvsp[(3) - (4)].sval));
                            ;}
    break;

  case 144:

/* Line 1455 of yacc.c  */
#line 827 "front\\src\\bison.y"
    {
                                (yyval.sval) = (yyvsp[(1) - (1)].sval);
                            ;}
    break;

  case 145:

/* Line 1455 of yacc.c  */
#line 832 "front\\src\\bison.y"
    {
                                //printf("Evaluating vector operation 1st factor %s\n", $1);
                                (yyval.pval) = graph->createExpression((Factor*) graph->createStringFactor((yyvsp[(1) - (3)].sval)), (Symbol*)(yyvsp[(2) - (3)].pval), (Factor*) graph->createIntegerFactor(atoi((yyvsp[(3) - (3)].sval))));
                            ;}
    break;

  case 146:

/* Line 1455 of yacc.c  */
#line 837 "front\\src\\bison.y"
    {
                                //printf("Evaluating vector operation 2nd factor %s\n", $3);
                                (yyval.pval) = graph->createExpression((Factor*) graph->createIntegerFactor(atoi((yyvsp[(1) - (3)].sval))), (Symbol*)(yyvsp[(2) - (3)].pval), (Factor*) graph->createStringFactor((yyvsp[(3) - (3)].sval)));
                            ;}
    break;

  case 147:

/* Line 1455 of yacc.c  */
#line 843 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::PLUS_SYMBOL);
                            ;}
    break;

  case 148:

/* Line 1455 of yacc.c  */
#line 846 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::MINUS_SYMBOL);
                            ;}
    break;

  case 149:

/* Line 1455 of yacc.c  */
#line 849 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::MULTIPLY_SYMBOL);
                            ;}
    break;

  case 150:

/* Line 1455 of yacc.c  */
#line 852 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::DIVIDE_SYMBOL);
                            ;}
    break;

  case 151:

/* Line 1455 of yacc.c  */
#line 855 "front\\src\\bison.y"
    {
                                (yyval.pval) = graph->createSymbol(Symbol::SQRT_SYMBOL);
                            ;}
    break;

  case 152:

/* Line 1455 of yacc.c  */
#line 860 "front\\src\\bison.y"
    {
                                validate(
                                    graph->saveMainBlock()
                                );
                            ;}
    break;

  case 153:

/* Line 1455 of yacc.c  */
#line 865 "front\\src\\bison.y"
    {
                                validate(
                                    graph->saveMainBlock()
                                );
                            ;}
    break;

  case 156:

/* Line 1455 of yacc.c  */
#line 875 "front\\src\\bison.y"
    {
                                validate(
                                    graph->addAttribution(
                                        (Attribution*)graph->createAttribution((ElementFactor*)(yyvsp[(1) - (3)].pval), (Factor*)(yyvsp[(3) - (3)].pval))
                                    )
                                );
                            ;}
    break;



/* Line 1455 of yacc.c  */
#line 2915 "front\\output\\bison.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 884 "front\\src\\bison.y"


extern char *yytext;

int isDirectory(const char *path) {
    
    struct stat statbuf;
    
    if (stat(path, &statbuf) != 0)
        return 0;
        
    return S_ISDIR(statbuf.st_mode);
    
}

int main (int argc, char * argv[]) {

    if (argc < 3) {
        cout << "Missing arguments NOPL => folder/*.nop" << endl;
        return EXIT_FAILURE;
    }

    int target = atoi(argv[1]);

    switch (target) {

        case Target::CODE_GENERATION_EXAMPLE_TARGET: {

            compiler = new CodeGenerationExample();
            graph = compiler->graph;

        } break;

        case Target::NAMESPACES_TARGET: {

            compiler = new NamespacesCompiler();
            graph = compiler->graph;

        } break;

		/*
        case Target::FRAMEWORK_CPP_2_0_TARGET: {

            compiler = new FrameworkCPP20Compiler();
			graph = compiler->graph;

        } break;

        case Target::FRAMEWORK_CPP_3_0_TARGET: {

            compiler = new FrameworkCPP30Compiler();
			graph = compiler->graph;

        } break;
		*/

        case Target::ELIXIR_TARGET: {

            compiler = new CodeGenerationElixir();
			graph = compiler->graph;

        } break;

        default: {

            cout << "Target is undefined" << endl;
            return EXIT_FAILURE;

        } break;

    }

    extern int yydebug;
    yydebug = 0;
    
    int len;
    
    struct dirent *pDirent;

    for (int i = 2; i < argc; i++) {

        if (isDirectory(argv[i])) {

            cout << "Openning directory '" << argv[i] << "'" << endl;

            DIR *pDir = opendir(argv[i]);

            if (pDir == NULL) {
                cout << "Directory not found '" << argv[i] << "'" << endl;
                return EXIT_FAILURE;
            }
            
            while ((pDirent = readdir(pDir)) != NULL) {

                if ((strcmp(pDirent->d_name,".") != 0) && (strcmp(pDirent->d_name,"..") != 0) && (strstr(pDirent->d_name,".nop") != NULL)) {
                    
                    cout << "\n\n[" << pDirent->d_name << "]\n";

                    string fullpath = argv[i];

                    if (fullpath.at(fullpath.size()-1) != '/') {
                        fullpath += "/";
                    }

                    fullpath += pDirent->d_name;

                    cout << "[" << fullpath << "]\n";
                    
                    graph->clear();

                    line_num = 1;
                    
                    yyin = fopen(fullpath.c_str(),"r");
                    
                    current_file = fullpath;
                    
                    yyparse();

                }

            }

            cout << "\n\nClosing directory '" << argv[i] << "'" << endl;

            closedir(pDir);

        } else {

            cout << "'" << argv[i] << "' is not a valid directory." << endl;
            return EXIT_FAILURE;

        }
        
    }

    cout << "\n\n> Checking consistency of NOP graph...\n" << endl;

    if (graph->checkConsistency()) {
        cout << "+ NOP Graph has been successfully checked and has no errors." << endl;
    } else {
        return EXIT_FAILURE;
    }

    cout << "\n\n> Printing NOP Graph" << endl;

	graph->iterateOverNOPGraph();

    cout << "\n\n> Initiating compilation process..." << endl;

    compiler->generateCode();

    cout << "\n\n> Compilation process has finished..." << endl;

}

void yyerror(const char *s) {

    cout << "\n\nNOPL compilation error:\n" << current_file << ":" << line_num << "\t" << s << "\n\n" << std::endl;
    exit(-1);
}

void validate(std::string errorMessage) {

    if (errorMessage == "") {
        return;
    }

    cout << "\n\nNOPL compilation error:\n" << current_file << ":" << line_num << "\t" << errorMessage << "\n\n" << std::endl;
    exit(-1);

}


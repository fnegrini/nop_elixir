
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INCLUDES = 258,
     END_INCLUDES = 259,
     CODE_GENERATION_EXAMPLE = 260,
     NAMESPACES = 261,
     FRAMEWORK_CPP_2_0 = 262,
     FRAMEWORK_CPP_3_0 = 263,
     ELIXIR = 264,
     NO_ONE = 265,
     BREADTH = 266,
     DEPTH = 267,
     PRIORITY = 268,
     FBE = 269,
     END_FBE = 270,
     PROPERTIES = 271,
     END_PROPERTIES = 272,
     TARGET = 273,
     STRATEGY = 274,
     KEEPER = 275,
     PRIVATE = 276,
     PUBLIC = 277,
     INSTANCE = 278,
     ATTRIBUTE = 279,
     BOOLEAN = 280,
     INTEGER = 281,
     DOUBLE = 282,
     STRING = 283,
     CHAR = 284,
     METHOD = 285,
     END_METHOD = 286,
     PARAMS = 287,
     END_PARAMS = 288,
     CODE = 289,
     END_CODE = 290,
     ATTRIBUTION = 291,
     END_ATTRIBUTION = 292,
     RULE = 293,
     END_RULE = 294,
     FORMATION_RULE = 295,
     END_FORMATION_RULE = 296,
     INDEX = 297,
     FROM = 298,
     TO = 299,
     CONDITION = 300,
     END_CONDITION = 301,
     SUBCONDITION = 302,
     END_SUBCONDITION = 303,
     PREMISE = 304,
     END_PREMISE = 305,
     IMPERTINENT = 306,
     ACTION = 307,
     END_ACTION = 308,
     INSTIGATION = 309,
     END_INSTIGATION = 310,
     SEQUENTIAL = 311,
     PARALLEL = 312,
     CALL = 313,
     MAIN = 314,
     END_MAIN = 315,
     ASSIGN = 316,
     EQ = 317,
     NE = 318,
     LT = 319,
     GT = 320,
     LE = 321,
     GE = 322,
     LB = 323,
     RB = 324,
     LC = 325,
     RC = 326,
     PLUS = 327,
     MINUS = 328,
     MULTIPLY = 329,
     DIVIDE = 330,
     SQRT = 331,
     LP = 332,
     RP = 333,
     COMMA = 334,
     POINT = 335,
     AND = 336,
     OR = 337,
     TRUE = 338,
     FALSE = 339,
     THIS = 340,
     INTEGER_VALUE = 341,
     DOUBLE_VALUE = 342,
     CHAR_VALUE = 343,
     STRING_VALUE = 344,
     ID = 345,
     COMMENT = 346
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 50 "front\\src\\bison.y"

    int ival;
    float fval;
    char *sval;
    void *pval;



/* Line 1676 of yacc.c  */
#line 152 "front\\output\\bison.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;



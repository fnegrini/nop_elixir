echo "Cleaning output..."
rm Compiler/front/output/*

echo "Deleting old NOPL compiler..."
rm Compiler/NOPL

echo "Compiling Flex file..."
#use flex -d to enable debugging
flex --outfile="Compiler/front/output/lex.yy.c" Compiler/front/src/lex.l

echo "Compiling Bison file..."
bison -t -d --output="Compiler/front/output/bison.tab.c" Compiler/front/src/bison.y

echo "Compiling NOPL compiler..."
g++ -std=c++11 -Wno-deprecated -Wno-deprecated-register -ICompiler/back/include/ Compiler/front/output/lex.yy.c Compiler/front/output/bison.tab.c Compiler/back/src/*.cpp Compiler/back/src/*/*.cpp Compiler/back/src/*/*/*.cpp -o Compiler/NOPL

if [ -f "Compiler/NOPL" ]
then
    echo "NOPL compiler has compiled with success"
else
    echo "NOPL has not been compiled =("
fi

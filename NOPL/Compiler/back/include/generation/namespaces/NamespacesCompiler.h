#ifndef _NAMESPACES_COMPILER_H_
#define _NAMESPACES_COMPILER_H_

class Instance;
class Premise;
class Attribute;
class Condition;
class Rule;

#include "Compiler.h"

#include <string>
#include <iostream>
#include <fstream>
#include <list>

using namespace std;

class NamespacesCompiler : public Compiler {

public:
	std::list<std::string> init_premises;

	std::string getInstanceCompleteName(Instance* instance);

	void generateCode();
	void generateCodeInstance(Instance *mainInstance, std::string includeBlock);
	void generateAllPremises(Instance *mainInstance, std::string includeBlock);
	void generateAllAttributes(Instance *mainInstance, std::string includeBlock);
	void generateAllMethods(Instance *mainInstance, std::string includeBlock);
	void generateAllRules(Instance *mainInstance, std::string includeBlock);
	void generateBody();
	void generateBatch();

	void generateCodeAttribute(ofstream& fileH, ofstream& fileCPP, Instance *instance, int level);
	void generateCodeMethod(ofstream& fileH, ofstream& fileCPP, Instance *instance, int level);
	void generateCodeRule(ofstream& fileH, ofstream& fileCPP, Instance *instance, int level);
	void generateCodePremise(ofstream& fileH, ofstream& fileCPP, Instance *instance, int level);

	void generatePremise(ofstream& fileCPP, ofstream& fileH, Premise *premise, std::string instName, std::string ruleName, int subConds, int level);
	void generateInstigation(ofstream& fileCPP, ofstream& fileH, Instance *instance, Rule *rule, int level);
	
	void assemble();
};
#endif	/* NAMESPACES_2_0_COMPILER_H */

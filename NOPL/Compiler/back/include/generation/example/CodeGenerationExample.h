#ifndef _CODE_GENERATION_EXAMPLE_H_
#define _CODE_GENERATION_EXAMPLE_H_

#include "Compiler.h"

class Instance;
class Premise;
class Attribute;
class Method;

#include <fstream>
#include <sstream>

using namespace std;

class CodeGenerationExample : public Compiler {

    public:

        CodeGenerationExample();
        virtual ~CodeGenerationExample();

    public:

        void generateCode();

    public:

        void generateCodeInstance(Instance *instance, int level);

    public:

        void iterateOverInstances(Instance *instance, int level);

        void iterateOverAttributes(Instance *instance, int level);

        void iterateOverMethods(Instance *instance, int level);

        void iterateOverRules(Instance *instance, int level);

    public:

        void generateCodeAttribute(Attribute *attribute, int level);

        void generateCodeMethod(Method *method, int level);

        void generateCodePremise(Premise *premise, int level);

    private:

        std::fstream filestream;

	std::stringstream sstream;

};

#endif
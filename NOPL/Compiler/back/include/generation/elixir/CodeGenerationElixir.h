#ifndef _CODE_GENERATION_ELIXIR_H_
#define _CODE_GENERATION_ELIXIR_H_

#define MAIN_FOLDER "nop_ex"
#define MAIN_MODULE "NopEx"

#include "Compiler.h"

class Instance;
class Premise;
class Attribute;
class Condition;
class Rule;
class Method;
class Fbe;
class Subcondition;
class Instigation;

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <map>

using namespace std;

class CodeGenerationElixir : public Compiler {

    public:

        CodeGenerationElixir();
        virtual ~CodeGenerationElixir();
    
    private:

        std::map<std::string, Fbe*> fbeList;

        void clearFbeList();

        void addFbeToList(std::string name,  Fbe *fbe);

        std::map<std::string, Fbe*>* getFbeList();

        void generateCodeInstance(Instance *instance, int level);

        void iterateOverInstances(Instance *instance, int level);

        void createFileStructure();

        void setConfigurationFiles();

        void checkFBE(Fbe *fbe);

        void createFBE(Fbe *fbe, std::string path);

        bool directoryExists(std::string path);

        void checkAgregatedAttributes(std::fstream *filestream, Fbe *fbe);

        void iterateOverAttributes(std::fstream *filestream, Fbe *fbe);

        void generateCodeAttribute(std::fstream *filestream, Attribute *attribute);

        std::string elixirNameofFBE(Fbe *fbe);

        void iterateOverMethods(std::fstream *filestream, Fbe *fbe);

        void generateCodeMethod(std::fstream *filestream, Fbe *fbe, Method *method);

        void iterateOverRules(Fbe *fbe, std::string path);

        void generateCodeRule(Fbe *fbe, std::string path, Rule *rule);

        std::string elixirNameofRule(Fbe *fbe, Rule *rule);

        void iterateOverCondition(std::fstream *filestream, Fbe *fbe, Rule *rule, Condition *condition);

        void generateCodeSubcondition(std::fstream *filestream, Fbe *fbe, Rule *rule, Subcondition *subcondition);

        void generateCodePremise(std::fstream *filestream, Fbe *fbe, Rule *rule, Premise *premise);

        std::string elixirNameofPremise(Fbe *fbe, Rule *rule, Premise *premise);

        std::string elixirNameofSubcondition(Fbe *fbe, Rule *rule, Subcondition *subcondition);

        void iterateOverInstigations(std::fstream *filestream, Fbe *fbe, Rule *rule);

        void generateCodeInstigation(std::fstream *filestream, Fbe *fbe, Rule *rule, Instigation *instigation);

        void generateMainRules(std::fstream *filestream, Fbe *fbeMain);

        void iterateOverMainRules(Fbe *fbe, std::string path, std::fstream *filestreamMain);

        void generateCodeMainRule(Fbe *fbe, std::string path, Rule *rule, std::fstream *filestreamMain);

        void determineMainRuleFbeList(Fbe *fbe, Rule *rule);

        std::string capitalize(std::string word);

    public:

        void generateCode();

    public:


    private:

    std::fstream fileElixir;

};

#endif
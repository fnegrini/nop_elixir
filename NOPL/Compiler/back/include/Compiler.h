#ifndef _COMPILER_H_
#define _COMPILER_H_

class NOPGraph;

#include <string>

class Compiler {

    public:

        Compiler();
        virtual ~Compiler();

    public:

        virtual void generateCode() = 0;

    public:

        std::string getLevel(int level);

    public:

        NOPGraph *graph;

};

#endif
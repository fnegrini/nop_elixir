#ifndef _NOP_GRAPH_H_
#define _NOP_GRAPH_H_

#include <string>
#include <map>
#include <list>
#include <vector>

class Entity;

class Fbe;
class Instance;
class Attribute;
class Method;
class Target;
class IncludeBlock;
class CodeBlock;
class Property;
class Strategy;
class Type;
class Visibility;
class Param;
class Symbol;
class Conjunction;
class Factor;
class ElementFactor;
class BooleanFactor;
class IntegerFactor;
class DoubleFactor;
class CharFactor;
class StringFactor;
class Attribution;
class Expression;
class Premise;
class Subcondition;
class Condition;
class Rule;
class Argument;
class Call;
class Execution;
class Instigation;
class Action;
class FormationRule;
class FormationRuleIndex;
class VectorDto;
class VectorElementFactor;
class MainBlock;

class NOPGraph {

    public:

        NOPGraph();
        virtual ~NOPGraph();

    public:

        void clear();

        bool checkConsistency();

        bool checkFbeMain();
        bool checkFbeInstances();
        bool instantiateElements(Instance *instance);
        bool connectEntities(Instance *instance);

	private:

		bool connectPremises(Premise *premise);
        
    public:

        Fbe* createFbe(std::string name);
        Instance* createInstance(Visibility *visibility, std::string fbeName, std::string name);

        Attribute* createAttribute(Visibility *visibility, Type *type, std::string name, Factor *value);
        Method* createMethod(Visibility *visibility, std::string name);

        Premise* createPremise(std::string name, Expression *expression, bool impertinent);

        Subcondition* createSubcondition(std::string name);
        Condition* createCondition(std::string name);
        Rule* createRule(std::string name);

        Instigation* createInstigation(std::string name, Execution *execution);
        Action* createAction(std::string name, Execution *execution);

    public:

        std::string addIncludeBlock(IncludeBlock *includeBlock);

        std::string addCodeBlock(CodeBlock *codeBlock);
        std::string addParam(Param *param);
        std::string addAttribution(Attribution *attribution);
        
        std::string addProperty(Property *property);

        std::string addArgument(Argument *argument);
        std::string addCall(Call *call);

        std::string addFbe(Fbe *fbe);
        std::string addInstance(Instance *instance);
        std::string addEntity(Entity *entity);

		std::string saveMainBlock();

    public: // vector stuff

        std::vector<Factor*> *factorArray;

        std::vector<Factor*>* addToFactorArray(Factor* factor);

        std::string createVectorName(std::string name, int position);
        std::list<Instance*>* createInstances(Visibility *visibility, std::string fbeName, std::string name, int quantity);
        std::list<Attribute*>* createAttributes(Visibility *visibility, Type *type, std::string name, std::vector<Factor*> *values, int quantity);
        char *convertVectorToId(std::string id, int position);
        VectorDto *convertVectorToVectorElement(std::string id, Expression *expression);
        VectorDto *convertVectorToVectorElement(std::string id, std::string position);

        std::string addFormationRuleIndex(std::string id, int from, int to);
        std::string createFormationRule(std::string name);

        Rule* createVectorRule(std::string name);
        Condition* createVectorCondition(std::string name);

        std::map<std::string, Premise*> * clonePremisseArray(std::map<std::string, Premise*> *original, std::string name);
        Factor *createElementFactorForClonedPromise(Factor *factor);
        std::string evaluateVectorOrVariableName(std::string name, std::string index, Expression *expression);

        Action* createVectorAction(std::string name, Execution *execution, std::map<std::string, Instigation*> *instigations);
        
        std::map<std::string, Instigation*>* cloneInstigationArray(std::map<std::string, Instigation*> *original, std::string name, Execution *execution);
        Instigation* createVectorInstigation(std::string name, Execution *execution, std::list<Call*> *calls);
        

    private:

        std::string createRuleFromFormationRule(FormationRuleIndex *fri, std::string name);

    public:

        Instance* getFbeMainInstance();

		MainBlock *getMainBlock();

	private:

		MainBlock *mainBlock;
        
    public:

        Target* createTarget(int target);

        IncludeBlock* createIncludeBlock(Target *target, std::string code);
        CodeBlock* createCodeBlock(Target *target, std::string code);

        Strategy* createStrategy(int strategy);

        Type* createType(int basicType, int numBytes = 0);
        Visibility* createVisibility(int visibility);

        Param* createParam(Type* type, std::string name);
        Symbol* createSymbol(int symbol);
        Conjunction* createConjunction(int conjunction);

        ElementFactor* createElementFactor(std::string fbeName, std::string attributeName);
        VectorElementFactor* createElementFactor(std::string fbeName, VectorDto *attributeName);
        VectorElementFactor* createElementFactor(VectorDto *fbeName, VectorDto *attributeName);
        VectorElementFactor* createElementFactor(VectorDto *fbeName, std::string attributeName);

        BooleanFactor* createBooleanFactor(bool value);
        IntegerFactor* createIntegerFactor(int value);
        DoubleFactor* createDoubleFactor(double value);
        CharFactor* createCharFactor(char value);
        StringFactor* createStringFactor(std::string value);

        Attribution* createAttribution(ElementFactor *element, Factor *factor);
        
        Expression* createExpression(Factor *leftFactor, Symbol *symbol, Factor *rightFactor);

        Argument* createArgument(Factor* factor);
        Call* createCall(std::string fbeName, std::string methodName);
        Execution* createExecution(int execution);

    public:

        Fbe* getFbe(std::string name);

	std::map<std::string, Fbe*> *getFbes();

    private:

        std::map<std::string, Fbe*> *fbes;

    private:

        std::map<std::string, IncludeBlock*> *includes;
    
        std::map<std::string, CodeBlock*> *codes;
        
        std::map<std::string, Param*> *params;

        std::map<std::string, Attribution*> *attributions;

        std::map<std::string, Property*> *properties;

    private:

        std::list<Argument*> *arguments;

        std::list<Call*> *calls;

    private:
        
        std::map<std::string, Instance*> *instances;

        std::map<std::string, Attribute*> *attributes;
        
        std::map<std::string, Method*> *methods;

        std::map<std::string, Premise*> *premises;

        std::map<std::string, Subcondition*> *subconditions;

        std::map<std::string, Rule*> *rules;

        std::map<std::string, Instigation*> *instigations;

    private:

        int unnamedEntityCount;

        std::string generateEntityName(std::string prefix);

    public:

        std::string checkSubconditionsConjunction(Conjunction *conjunction);
        std::string checkPremisesConjunction(Conjunction *conjunction);

    private:

        Conjunction *currentSubconditionsConjunction;
        Conjunction *currentPremisesConjunction;

    private:

        Condition *currentCondition;
        Action *currentAction;

    private:

        FormationRule* formationRule = NULL;

    public:

        void iterateOverNOPGraph();

        void iterateOverNOPGraphInstances(Instance *instance);
        
        void iterateOverNOPGraphInstance(Instance *instance);
        void iterateOverNOPGraphInstanceAttributes(Instance *instance);

        void iterateOverNOPGraphAttribute(Attribute *attribute);
        
};

#endif
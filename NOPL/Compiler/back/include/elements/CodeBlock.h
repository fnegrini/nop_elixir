#ifndef _CODE_BLOCK_H_
#define _CODE_BLOCK_H_

#include <string>

class Target;

class CodeBlock {

    public:

        CodeBlock(Target *target, std::string code);
        virtual ~CodeBlock();

    public:

        void print();

        Target* getTarget();

        std::string getCode();

    private:

        Target *target;

        std::string code;

};

#endif

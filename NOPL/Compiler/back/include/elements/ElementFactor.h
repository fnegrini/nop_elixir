#ifndef _ELEMENT_FACTOR_H_
#define _ELEMENT_FACTOR_H_

#include "elements/Factor.h"

class Instance;
class Attribute;

class ElementFactor : public Factor {

    public:

        ElementFactor(std::string instanceName, std::string attributeName);
        ElementFactor(std::string instanceName, std::string attributeName, int factor);
        virtual ~ElementFactor();

    public:

        void setInstance(Instance *instance);
        Instance* getInstance();

        void setAttribute(Attribute *attribute);
        Attribute *getAttribute();

        std::string getInstanceName();

        std::string getAttributeName();

        virtual std::string getStringValue();

		ElementFactor* clone();

    private:

        Instance *instance;

        Attribute *attribute;

        std::string instanceName;

        std::string attributeName;

};

#endif

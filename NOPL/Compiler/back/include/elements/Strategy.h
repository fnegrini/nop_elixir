#ifndef _STRATEGY_H_
#define _STRATEGY_H_

#include "Property.h"

#include <string>

class Strategy : public Property {

    public:

        static const int NO_ONE_STRATEGY = 1;
        static const int BREADTH_STRATEGY = 2;
        static const int DEPTH_STRATEGY = 3;
        static const int PRIORITY_STRATEGY = 4;

    public:

        Strategy(int strategy);
        virtual ~Strategy();

    public:

        void print();

        int getStrategyId();

        std::string getStrategyName();

    private:

        int strategy;

};

#endif

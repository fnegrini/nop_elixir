#ifndef _CHAR_FACTOR_H_
#define _CHAR_FACTOR_H_

#include "elements/Factor.h"

class CharFactor : public Factor {

    public:

        CharFactor(char value);
        virtual ~CharFactor();

    public:

        char getValue();

        std::string getStringValue();
	
	Factor* clone();

    private:

        char value;

};

#endif

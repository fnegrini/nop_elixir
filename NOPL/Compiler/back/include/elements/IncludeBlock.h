#ifndef _INCLUDE_BLOCK_H_
#define _INCLUDE_BLOCK_H_

#include <string>

class Target;

class IncludeBlock {

    public:

        IncludeBlock(Target *target, std::string code);
        virtual ~IncludeBlock();

    public:

        void print();

        Target* getTarget();

        std::string getCode();

    private:

        Target *target;

        std::string code;

};

#endif

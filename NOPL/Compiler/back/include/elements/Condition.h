#ifndef _CONDITION_H_
#define _CONDITION_H_

#include "Entity.h"

#include <string>
#include <map>

class Conjunction;
class Premise;
class Subcondition;
class Rule;

class Condition : public Entity {

    public:

        Condition(Instance *instance, std::string name, Conjunction *conjunction, std::map<std::string, Premise*> *premises, std::map<std::string, Subcondition*> *subconditions);
        virtual ~Condition();

    public:

        void print();

        Conjunction* getConjunction();

		void setNumberApprovedPremises(int numberApprovedPremises);
		int getNumberApprovedPremises();

	public:

        std::map<std::string, Premise*>* getPremises();

        std::map<std::string, Subcondition*>* getSubconditions();
        
    private:

        Conjunction *conjunction;

		int numberApprovedPremises;

	private:

        std::map<std::string, Premise*> *premises;

        std::map<std::string, Subcondition*> *subconditions;
		
    public:

        void setRule(Rule *rule);
        Rule* getRule();

    private:

        Rule *rule;

    public:

        Condition *clone(Instance *instance);

};

#endif

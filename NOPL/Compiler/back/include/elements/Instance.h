#ifndef _INSTANCE_H_
#define _INSTANCE_H_

#include "Entity.h"
class Visibility;
class Fbe;
class Attribute;
class Method;
class Rule;

#include <string>
#include <map>

class Instance : public Entity {

    public:

        Instance(Instance *instance, Visibility *visibility, std::string fbeName, std::string name);
        virtual ~Instance();

    public:

        void print();

        Visibility* getVisibility();

        void setFbe(Fbe *fbe);
        Fbe* getFbe();

	std::string getFbeName();

        void instantiateElements();

    private:

        Visibility *visibility;

        Fbe *fbe;

	std::string fbeName;

        Instance *instanceThis;

    public:

        Instance* getInstance(std::string name);
        
        Attribute* getAttribute(std::string name);
        
        Method* getMethod(std::string name);

        Rule* getRule(std::string name);

		Instance* clone(Instance *instance);

    public:

        std::map<std::string, Instance*>* getInstances();
        std::map<std::string, Attribute*>* getAttributes();
        std::map<std::string, Method*>* getMethods();
        std::map<std::string, Rule*>* getRules();

    private:

        std::map<std::string, Instance*> *instances;
        std::map<std::string, Attribute*> *attributes;
        std::map<std::string, Method*> *methods;
        std::map<std::string, Rule*> *rules;

};

#endif

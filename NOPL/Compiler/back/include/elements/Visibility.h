#ifndef _VISIBILITY_H_
#define _VISIBILITY_H_

#include <string>

class Visibility {

    public:

        static const int PRIVATE_VISIBILITY = 1;
        static const int PUBLIC_VISIBILITY = 2;

    public:

        Visibility(int visibility);
        virtual ~Visibility();

    public:

        void print();

        int getVisibilityId();

        std::string getVisibilityName();

    private:

        int visibility;

};

#endif

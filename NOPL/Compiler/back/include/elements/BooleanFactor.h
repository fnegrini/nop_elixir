#ifndef _BOOLEAN_FACTOR_H_
#define _BOOLEAN_FACTOR_H_

#include "elements/Factor.h"

class BooleanFactor : public Factor {

    public:

        BooleanFactor(bool value);
        virtual ~BooleanFactor();

    public:

        bool getValue();

        std::string getStringValue();

	Factor* clone();

    private:

        bool value;

};

#endif

#ifndef _PARAM_H_
#define _PARAM_H_

#include <string>

class Type;

class Param {

    public:

        Param(Type *type, std::string id);
        virtual ~Param();

    public:

        void print();

        Type* getType();

        std::string getName();

    private:

        Type *type;

        std::string name;

};

#endif

#ifndef _ATTRIBUTION_H_
#define _ATTRIBUTION_H_

#include <string>

class ElementFactor;
class Factor;
class Instance;

class Attribution {

    public:

        Attribution(ElementFactor *element, Factor *factor);
        virtual ~Attribution();

    public:

        void print();

        ElementFactor* getElement();

        Factor* getFactor();

		Attribution *clone(Instance *instance);

    private:

        ElementFactor *element;

        Factor *factor;

};

#endif

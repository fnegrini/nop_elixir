#ifndef _ACTION_H_
#define _ACTION_H_

#include "Entity.h"

#include <string>
#include <map>

class Execution;

class Instigation;

class Rule;

class Action : public Entity {

    public:

        Action(Instance *instance, std::string name, Execution *execution, std::map<std::string, Instigation*> *instigations);
        virtual ~Action();

    public:

        void print();

        Execution* getExecution();

        std::map<std::string, Instigation*>* getInstigations();
        
    private:

        Execution *execution;

        std::map<std::string, Instigation*> *instigations;

    public:

        void setRule(Rule *rule);
        Rule* getRule();

    private:

        Rule *rule;

    public:

        Action *clone(Instance *instance);

};

#endif

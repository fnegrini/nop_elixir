#ifndef _SYMBOL_H_
#define _SYMBOL_H_

#include <string>

class Symbol {

    public:

        static const int EQUAL_SYMBOL = 1;
        static const int NOT_EQUAL_SYMBOL = 2;
        static const int LESSER_THAN_SYMBOL = 3;
        static const int GREATER_THAN_SYMBOL = 4;
        static const int LESS_OR_EQUAL_SYMBOL = 5;
        static const int GREATER_OR_EQUAL_SYMBOL = 6;

        static const int PLUS_SYMBOL = 7;
        static const int MINUS_SYMBOL = 8;
		static const int MULTIPLY_SYMBOL = 9;
		static const int DIVIDE_SYMBOL = 10;

		static const int SQRT_SYMBOL = 11;

    public:

        Symbol(int symbol);
        virtual ~Symbol();

    public:

        void print();

        int getSymbolId();

        std::string getSymbolName();

    private:

        int symbol;

};

#endif

#ifndef _FormationRule_H_
#define _FormationRule_H_

#include <string>
#include <map>
#include <list>

class FormationRuleIndex;

class FormationRule {

    public:

        FormationRule();
        virtual ~FormationRule();

    public:

        void print();
        void setName(std::string name);
        std::string addFormationRuleIndex(FormationRuleIndex *indexes);
        FormationRuleIndex* getFormationRuleIndex();
        void updateIndex(std::string id, int position);
        int getCurrentPositionForIndex(std::string id);

        
    private:

        std::string name;
        std::map<std::string, int> valueMaps;
        FormationRuleIndex* formationRuleIndex;
        

};

#endif

#ifndef _VECTOR_ELEMENT_FACTOR_H_
#define _VECTOR_ELEMENT_FACTOR_H_

#include "ElementFactor.h"
#include "Expression.h"
#include "VectorDto.h"

class VectorElementFactor : public ElementFactor {

    public:

        VectorElementFactor(std::string instanceName, VectorDto *instanceDto, std::string attributeName, VectorDto *attributeDto);
        virtual ~VectorElementFactor();

    public:

        std::string getInstanceIndex();

        std::string getAttributeIndex();

        std::string getStringValue();

        Expression *getInstanceExpression();

        Expression *getAttributeExpression();

    private:

        std::string instanceIndex;

        std::string attributeIndex;

        Expression *instanceExpression;

        Expression *attributeExpression;

};

#endif

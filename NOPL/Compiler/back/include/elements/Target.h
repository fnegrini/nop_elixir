#ifndef _TARGET_H_
#define _TARGET_H_

#include "Property.h"

#include <string>

class Target : public Property {

    public:

        static const int CODE_GENERATION_EXAMPLE_TARGET = 0;
        static const int NAMESPACES_TARGET = 1;
        static const int ELIXIR_TARGET = 4;


    public:

        Target(int target);
        virtual ~Target();

    public:

        void print();

        int getTargetId();

        std::string getTargetName();

    private:

        int target;

};

#endif

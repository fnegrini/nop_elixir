#ifndef _FACTOR_H_
#define _FACTOR_H_

#include <string>

class Factor {

    public:

        static const int ELEMENT_FACTOR = 1;
        static const int BOOLEAN_FACTOR = 2;
        static const int INTEGER_FACTOR = 3;
        static const int DOUBLE_FACTOR = 4;
        static const int CHAR_FACTOR = 5;
        static const int STRING_FACTOR = 6;
        static const int VECTOR_ELEMENT_FACTOR = 7;

    public:

        Factor(int factor);
        virtual ~Factor();

    public:

        void print();

        int getFactorId();

        std::string getFactorName();

        virtual std::string getStringValue() = 0;

	virtual Factor* clone() = 0;

    private:

        int factor;

};

#endif

#ifndef _RULE_H_
#define _RULE_H_

#include "Entity.h"

#include <string>
#include <map>

class Condition;
class Action;

class Rule : public Entity {

    public:

        Rule(Instance *instance, std::string name, Condition *condition, Action *action);
        virtual ~Rule();

    public:

        void print();

        Condition* getCondition();

        Action* getAction();
        
    private:

        Condition *condition;

        Action *action;
        
    public:

        Rule *clone(Instance *instance);

};

#endif

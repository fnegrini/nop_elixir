#ifndef _MAIN_BLOCK_H_
#define _MAIN_BLOCK_H_

#include <string>
#include <map>

class Attribution;

class MainBlock {

    public:

        MainBlock(std::map<std::string, Attribution*> *attributions);
        virtual ~MainBlock();

    public:

        void print();

		std::map<std::string, Attribution*>* getAttributions();

	private:

		std::map<std::string, Attribution*> *attributions;

};

#endif

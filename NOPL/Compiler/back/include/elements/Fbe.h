#ifndef _FBE_H_
#define _FBE_H_

#include <string>
#include <map>

class IncludeBlock;
class Property;
class Instance;
class Attribute;
class Method;
class Rule;

class Fbe {

    public:

        Fbe(std::string name,
            std::map<std::string, IncludeBlock*> *includes,
            std::map<std::string, Property*> *properties,
            std::map<std::string, Instance*> *instances,
            std::map<std::string, Attribute*> *attributes,
            std::map<std::string, Method*> *methods,
            std::map<std::string, Rule*> *rules);
        
        virtual ~Fbe();

    public:

        void print();

        std::string getName();

    public:

        Instance* getInstance(std::string name);
        
        Attribute* getAttribute(std::string name);
        
        Method* getMethod(std::string name);

        Rule* getRule(std::string name);

    public:

        std::map<std::string, IncludeBlock*>* getIncludes();
        std::map<std::string, Property*>* getProperties();
        std::map<std::string, Instance*>* getInstances();
        std::map<std::string, Attribute*>* getAttributes();
        std::map<std::string, Method*>* getMethods();
        std::map<std::string, Rule*>* getRules();

    private:

        std::string name;

        std::map<std::string, IncludeBlock*> *includes;
        std::map<std::string, Property*> *properties;
        std::map<std::string, Instance*> *instances;
        std::map<std::string, Attribute*> *attributes;
        std::map<std::string, Method*> *methods;
        std::map<std::string, Rule*> *rules;

};

#endif

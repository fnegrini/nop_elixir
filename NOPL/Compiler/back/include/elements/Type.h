#ifndef _TYPE_H_
#define _TYPE_H_

#include <string>

class Type {

    public:

        static const int BOOLEAN_TYPE = 1;
        static const int INTEGER_TYPE = 2;
        static const int DOUBLE_TYPE = 3;
        static const int STRING_TYPE = 4;
        static const int CHAR_TYPE = 5;

    public:

        Type(int basicType, int numBytes);
        virtual ~Type();

    public:

        void print();

        int getTypeId();

        std::string getTypeName();

        int getNumBytes();

    private:

        int basicType;
        int numBytes;

};

#endif

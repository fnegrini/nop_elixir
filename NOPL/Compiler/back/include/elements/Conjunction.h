#ifndef _CONJUNCTION_H_
#define _CONJUNCTION_H_

#include <string>

class Conjunction {

    public:

        static const int NO_CONJUNCTION = 0;
        static const int AND_CONJUNCTION = 1;
        static const int OR_CONJUNCTION = 2;

    public:

        Conjunction(int conjunction);
        virtual ~Conjunction();

    public:

        void print();

        int getConjunctionId();

        std::string getConjunctionName();

    private:

        int conjunction;

};

#endif

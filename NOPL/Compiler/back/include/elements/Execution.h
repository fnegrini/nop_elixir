#ifndef _EXECUTION_H_
#define _EXECUTION_H_

#include <string>

class Execution {

    public:

        static const int SEQUENTIAL_EXECUTION = 1;
        static const int PARALLEL_EXECUTION = 2;

    public:

        Execution(int execution);
        virtual ~Execution();

    public:

        int getExecution();
        void print();

    private:

        int execution;

};

#endif

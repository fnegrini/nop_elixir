#ifndef _EXPRESSION_H_
#define _EXPRESSION_H_

#include <string>

class Factor;
class Symbol;
class Instance;

class Expression {

    public:

        Expression(Factor *leftFactor, Symbol *symbol, Factor *rightFactor);
        virtual ~Expression();

    public:

        void print();

        Factor* getLeftFactor();

        Symbol* getSymbol();
        
        Factor* getRightFactor();

        std::string getVectorIndex();

        int evaluateVectorExpression(int indexValue);

	Expression* clone();

    private:

        Factor *leftFactor;

        Symbol *symbol;

        Factor *rightFactor;

};

#endif

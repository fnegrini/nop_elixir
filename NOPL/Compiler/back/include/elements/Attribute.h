#ifndef _ATTRIBUTE_H_
#define _ATTRIBUTE_H_

#include "Entity.h"

#include <string>
#include <map>

class Type;
class Visibility;
class Factor;
class Instance;
class Premise;

class Attribute : public Entity {

    public:

        Attribute(Instance *instance, Visibility *visibility, Type *type, std::string name, Factor *factor);
        virtual ~Attribute();

    public:

        void print();

        Visibility* getVisibility();
        
        Type* getType();

        Factor* getFactor();

    private:

        Visibility *visibility;
        
        Type *type;
        
        Factor *factor;

    public:

        void addPremise(Premise *premise);
        Premise* getPremise(std::string name);

        std::map<std::string, Premise*>* getPremises();

    private:

	Instance *parentInstance;

        std::map<std::string, Premise*> *premises;

    public:

        Attribute *clone(Instance *instance);

};

#endif

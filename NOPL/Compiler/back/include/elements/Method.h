#ifndef _METHOD_H_
#define _METHOD_H_

#include "Entity.h"

#include <string>
#include <map>

class Visibility;

class Param;
class CodeBlock;
class Attribution;

class Method : public Entity {

    public:

        Method(Instance *instance, Visibility *visibility, std::string name, std::map<std::string, Param*> *params, std::map<std::string, CodeBlock*> *codeBlocks, std::map<std::string, Attribution*> *attributions);
        virtual ~Method();

    public:

        void print();

        Visibility* getVisibility();

        std::map<std::string, Param*>* getParams();

        std::map<std::string, CodeBlock*>* getCodeBlocks();

        std::map<std::string, Attribution*>* getAttributions();
        
    private:

        Visibility *visibility;

        std::map<std::string, Param*> *params;

        std::map<std::string, CodeBlock*> *codeBlocks;

        std::map<std::string, Attribution*> *attributions;

    public:

        Method *clone(Instance *instance);

};

#endif

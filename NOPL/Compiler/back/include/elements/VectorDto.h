#ifndef _VECTOR_DTO_H_
#define _VECTOR_DTO_H_

#include <string>

#include "Expression.h"

class VectorDto {

    public:

        VectorDto(std::string name, std::string index);
        VectorDto(std::string name, Expression *expression);
        virtual ~VectorDto();

    public:

        std::string getIndex();
        std::string getName();
        Expression *getExpression();
    private:

        std::string name;
        std::string index;
        Expression *expression;

};

#endif

#ifndef _CALL_H_
#define _CALL_H_

class Instance;
class Method;
class Argument;

#include <string>
#include <list>

class Call {

    public:

        Call(std::string instanceName, std::string attributeName, std::list<Argument*> *arguments);
        virtual ~Call();

    public:

        void print();

        void setInstance(Instance *instance);
        Instance* getInstance();

        void setMethod(Method *method);
        Method *getMethod();

        std::string getInstanceName();

        std::string getMethodName();

        std::string getStringValue();

        std::list<Argument*>* getArguments();

    private:

        Instance *instance;

        Method *method;

        std::string instanceName;

        std::string methodName;

        std::list<Argument*> *arguments;

};

#endif

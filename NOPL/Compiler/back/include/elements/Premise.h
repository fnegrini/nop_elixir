#ifndef _PREMISE_H_
#define _PREMISE_H_

#include "Entity.h"

#include <string>
#include <map>

class Expression;
class Attribute;
class Instance;
class Condition;
class Subcondition;

class Premise : public Entity {

    public:

        Premise(Instance *instance, std::string name, Expression *expression, bool impertinent);
        virtual ~Premise();

    public:

        void print();

        Expression* getExpression();
        
        bool getImpertinent();

		void setInitialLogicalValue(bool initialLogicalValue);
		bool getInitialLogicalValue();

    private:

        Expression *expression;

        bool impertinent;

		bool initialLogicalValue;

    public:

        void addAttribute(Attribute *attribute);
        Attribute* getAttribute(std::string name);

        std::map<std::string, Attribute*>* getAttributes();

    private:

        std::map<std::string, Attribute*> *attributes;

    public:

        void setCondition(Condition *condition);
        Condition* getCondition();

        void setSubcondition(Subcondition *subcondition);
        Subcondition* getSubcondition();

    private:

        Condition *condition;
        Subcondition *subcondition;

    public:

        Premise *clone(Instance *instance);

};

#endif

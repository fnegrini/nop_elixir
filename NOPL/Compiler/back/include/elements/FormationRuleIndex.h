#ifndef _FormationRuleIndex_H_
#define _FormationRuleIndex_H_

#include <string>

class FormationRuleIndex {

    public:

        FormationRuleIndex(std::string id, int from, int to);
        virtual ~FormationRuleIndex();

    public:

        std::string getName();
        void print();
        int getFrom();
        int getTo();
        std::string getId();
        void addFormationRuleIndex(FormationRuleIndex *nextFormationRuleIndex);
        FormationRuleIndex *getNextFormationRuleIndex();

    private:

        std::string id;
        int from;
        int to;
        FormationRuleIndex *nextFormationRuleIndex = NULL;

};

#endif

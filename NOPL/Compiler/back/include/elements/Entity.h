#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <string>

class Instance;

class Entity {

    public:

        Entity(Instance *instance, std::string name);
        virtual ~Entity();

    public:
	
        Instance* getParentInstance();

        std::string getName();

    private:

	Instance *parentInstance;

        std::string name;

};

#endif

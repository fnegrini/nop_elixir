#ifndef _STRING_FACTOR_H_
#define _STRING_FACTOR_H_

#include "elements/Factor.h"

#include <string>

class StringFactor : public Factor {

    public:

        StringFactor(std::string value);
        virtual ~StringFactor();

    public:

        std::string getValue();

        std::string getStringValue();

	Factor* clone();

    private:

        std::string value;

};

#endif

#ifndef _DOUBLE_FACTOR_H_
#define _DOUBLE_FACTOR_H_

#include "elements/Factor.h"

class DoubleFactor : public Factor {

    public:

        DoubleFactor(double value);
        virtual ~DoubleFactor();

    public:

        double getValue();

        std::string getStringValue();

	Factor* clone();

    private:

        double value;

};

#endif

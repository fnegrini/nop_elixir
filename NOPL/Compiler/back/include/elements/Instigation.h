#ifndef _INSTIGATION_H_
#define _INSTIGATION_H_

#include "Entity.h"

#include <string>
#include <list>

class Execution;
class Call;
class Action;

class Instigation : public Entity {

    public:

        Instigation(Instance *instance, std::string name, Execution *execution, std::list<Call*> *calls);
        virtual ~Instigation();

    public:

        void print();

        Execution* getExecution();

        std::list<Call*>* getCalls();
        
    private:

        Execution *execution;

        std::list<Call*> *calls;

    public:

        void setAction(Action *action);
        Action* getAction();

    private:

        Action *action;

    public:

        Instigation *clone(Instance *instance);

};

#endif

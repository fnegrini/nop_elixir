#ifndef _PROPERTY_H_
#define _PROPERTY_H_

#include <string>

class Property {

    public:

        static const int TARGET_PROPERTY = 1;
        static const int STRATEGY_PROPERTY = 2;

    public:

        Property(int type);
        virtual ~Property();

    public:

        void print();

        int getType();
        std::string getTypeName();

    private:

        int type;

};

#endif

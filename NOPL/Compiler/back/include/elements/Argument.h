#ifndef _ARGUMENT_H_
#define _ARGUMENT_H_

#include <string>

class Factor;

class Argument {

    public:

        Argument(Factor *factor);
        virtual ~Argument();

    public:

        void print();

        Factor* getFactor();

    private:

        Factor *factor;

};

#endif

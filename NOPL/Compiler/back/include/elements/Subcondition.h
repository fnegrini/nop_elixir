#ifndef _SUBCONDITION_H_
#define _SUBCONDITION_H_

#include "Entity.h"

#include <string>
#include <map>

class Conjunction;
class Premise;
class Condition;

class Subcondition : public Entity {

    public:

        Subcondition(Instance *instance, std::string name, Conjunction *conjunction, std::map<std::string, Premise*> *premises);
        virtual ~Subcondition();

    public:

        void print();

        Conjunction* getConjunction();

		void setNumberApprovedPremises(int numberApprovedPremises);
		int getNumberApprovedPremises();

	public:

        std::map<std::string, Premise*>* getPremises();
        
    private:

        Conjunction *conjunction;

		int numberApprovedPremises;

	private:

        std::map<std::string, Premise*> *premises;

    public:

        void setCondition(Condition *condition);
        Condition* getCondition();

    private:

        Condition *condition;

    public:

        Subcondition *clone(Instance *instance);

};

#endif

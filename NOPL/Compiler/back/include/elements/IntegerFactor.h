#ifndef _INTEGER_FACTOR_H_
#define _INTEGER_FACTOR_H_

#include "elements/Factor.h"

class IntegerFactor : public Factor {

    public:

        IntegerFactor(int value);
        virtual ~IntegerFactor();

    public:

        int getValue();

        std::string getStringValue();

	Factor* clone();

    private:

        int value;

};

#endif

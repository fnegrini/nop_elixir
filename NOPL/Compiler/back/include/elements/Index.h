#ifndef _CONDITION_H_
#define _CONDITION_H_

#include "Entity.h"

#include <string>
#include <map>

class Index : public Entity {

    public:

        Index(Instance *instance, std::string name, int from, int to);
        virtual ~Index();

    public:

        void print();
        
    private:

        int from, to;

    public:

        int getFrom();
        int getTo();
        std::string getName();

};

#endif

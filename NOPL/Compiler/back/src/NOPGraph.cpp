#include "NOPGraph.h"
#include "NOPGraph.h"

#include "elements/Target.h"
#include "elements/IncludeBlock.h"
#include "elements/CodeBlock.h"
#include "elements/Strategy.h"
#include "elements/Type.h"
#include "elements/Visibility.h"
#include "elements/Param.h"
#include "elements/Symbol.h"
#include "elements/Conjunction.h"
#include "elements/Factor.h"
#include "elements/ElementFactor.h"
#include "elements/BooleanFactor.h"
#include "elements/IntegerFactor.h"
#include "elements/DoubleFactor.h"
#include "elements/CharFactor.h"
#include "elements/StringFactor.h"
#include "elements/Argument.h"
#include "elements/Call.h"
#include "elements/Execution.h"

#include "elements/Attribution.h"
#include "elements/Expression.h"
#include "elements/Premise.h"
#include "elements/Subcondition.h"
#include "elements/Condition.h"
#include "elements/Rule.h"
#include "elements/Instigation.h"
#include "elements/Action.h"

#include "elements/Entity.h"
#include "elements/Attribute.h"
#include "elements/Method.h"

#include "elements/Fbe.h"
#include "elements/Instance.h"
#include "elements/FormationRule.h"
#include "elements/FormationRuleIndex.h"
#include "elements/VectorDto.h"
#include "elements/VectorElementFactor.h"
#include "elements/MainBlock.h"

#include <cstring>
#include <string>
#include <iostream>

NOPGraph::NOPGraph() {

    this->unnamedEntityCount = 0;

    this->fbes = new std::map<std::string, Fbe*>();

}

NOPGraph::~NOPGraph() {

}

void NOPGraph::clear() {

    this->includes = new std::map<std::string, IncludeBlock*>();
    
    this->codes = new std::map<std::string, CodeBlock*>();
        
    this->params = new std::map<std::string, Param*>();

    this->attributions = new std::map<std::string, Attribution*>();

    this->properties = new std::map<std::string, Property*>();

    this->instances = new std::map<std::string, Instance*>();

    this->attributes = new std::map<std::string, Attribute*>();
    
    this->methods = new std::map<std::string, Method*>();

    this->premises = new std::map<std::string, Premise*>();

    this->subconditions = new std::map<std::string, Subcondition*>();

    this->rules = new std::map<std::string, Rule*>();
    
    this->arguments = new std::list<Argument*>();

    this->calls = new std::list<Call*>();

    this->factorArray = new std::vector<Factor*>();

    this->instigations = new std::map<std::string, Instigation*>();

    this->currentSubconditionsConjunction = 0;
    this->currentPremisesConjunction = 0;

    this->currentCondition = 0;
    this->currentAction = 0;

	this->mainBlock = 0;

}

Fbe* NOPGraph::createFbe(std::string name) {

    Fbe *fbe = new Fbe(name, this->includes, this->properties, this->instances, this->attributes, this->methods, this->rules);

	std::cout << "FBE: " << name << std::endl;

	for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

	Instance *instance = it->second;

	std::cout << instance << " " << instance->getName() << std::endl;

	}

	for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {

		Rule *rule = it->second;

		Condition *condition = rule->getCondition();

		std::map<std::string, Subcondition*> *subconditions = condition->getSubconditions();

		for (std::map<std::string, Subcondition*>::iterator it = subconditions->begin(); it != subconditions->end(); ++it) {

			Subcondition *subcondition = it->second;

			for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

				Premise *premise = it->second;

				std::cout << premise << " " << premise->getName() << std::endl;

			}

		}

		std::map<std::string, Premise*> *premises = condition->getPremises();

		for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

			Premise *premise = it->second;

			std::cout << premise << " " << premise->getName() << std::endl;

		}

	}

    this->includes = new std::map<std::string, IncludeBlock*>();

    this->properties = new std::map<std::string, Property*>();

    this->instances = new std::map<std::string, Instance*>();

    this->attributes = new std::map<std::string, Attribute*>();
    
    this->methods = new std::map<std::string, Method*>();

    this->rules = new std::map<std::string, Rule*>();

    return fbe;

}

Instance* NOPGraph::createInstance(Visibility *visibility, std::string fbeName, std::string name) {

    return new Instance(0, visibility, fbeName, name);

}

std::list<Instance*> *NOPGraph::createInstances(Visibility *visibility, std::string fbeName, std::string name, int quantity) {

    std::list<Instance*> *instances = new std::list<Instance*>();
    for (int i = 0; i < quantity; i++) {
        instances->push_back(this->createInstance(visibility, fbeName, this->createVectorName(name, i)));
    }
    return instances;
}

std::string NOPGraph::createVectorName(std::string name, int position) {
    return name + "__vector_" +  std::to_string(position);
}

Attribute* NOPGraph::createAttribute(Visibility *visibility, Type *type, std::string name, Factor *value) {

    return new Attribute(0, visibility, type, name, value);

}

std::list<Attribute*>* NOPGraph::createAttributes(Visibility *visibility, Type *type, std::string name, std::vector<Factor*> *values, int quantity) {

    if (values != NULL &&
        values->size() != quantity) {
            std::string error = "Declared size for array " + name + "[" + std::to_string(quantity) + "] does not match number of declared factors (" + std::to_string(values->size()) + ").";
        std::cerr << std::endl << error << std::endl;
        throw new std::invalid_argument(error);
    }

    std::list<Attribute*> *attributes = new std::list<Attribute*>();
    for (int i = 0; i < quantity; i++) {
        attributes->push_back(this->createAttribute(visibility, type, this->createVectorName(name, i), values == NULL ? NULL : (*values)[i]));
    }

    this->factorArray = new std::vector<Factor*>();

    return attributes;

}

Method* NOPGraph::createMethod(Visibility *visibility, std::string name) {

    Method *method = new Method(0, visibility, name, this->params, this->codes, this->attributions);

    this->params = new std::map<std::string, Param*>();

    this->codes = new std::map<std::string, CodeBlock*>();

    this->attributions = new std::map<std::string, Attribution*>();

    return method;

}

Premise* NOPGraph::createPremise(std::string name, Expression *expression, bool impertinent) {

    if (name == "") {
        name = this->generateEntityName("pr");
    }

    return new Premise(0, name, expression, impertinent);

}

Subcondition* NOPGraph::createSubcondition(std::string name) {

    if (name == "") {
        name = this->generateEntityName("sc");
    }
    
    Subcondition *subcondition = new Subcondition(0, name, this->currentPremisesConjunction, this->premises);

    this->currentPremisesConjunction = 0;

    this->premises = new std::map<std::string, Premise*>();

    return subcondition;

}

Condition* NOPGraph::createCondition(std::string name) {

    if (name == "") {
        name = this->generateEntityName("cd");
    }

    Conjunction *conjunction = (this->currentSubconditionsConjunction != 0 ? this->currentSubconditionsConjunction : this->currentPremisesConjunction);

    this->currentCondition = new Condition(0, name, conjunction, this->premises, this->subconditions);

    this->currentSubconditionsConjunction = 0;
    this->currentPremisesConjunction = 0;

    this->premises = new std::map<std::string, Premise*>();

    this->subconditions = new std::map<std::string, Subcondition*>();

    return this->currentCondition;

}

Rule* NOPGraph::createRule(std::string name) {

    if (name == "") {
        name = this->generateEntityName("rl");
    }

    Rule *rule = new Rule(0, name, this->currentCondition, this->currentAction);

    return rule;

}

Instigation* NOPGraph::createInstigation(std::string name, Execution *execution) {

    if (name == "") {
        name = this->generateEntityName("in");
    }

    Instigation *instigation = new Instigation(0, name, execution, this->calls);
    
    this->calls = new std::list<Call*>();

    return instigation;

}

Action* NOPGraph::createAction(std::string name, Execution *execution) {

    if (name == "") {
        name = this->generateEntityName("ac");
    }

    this->currentAction = new Action(0, name, execution, this->instigations);
    
    this->instigations = new std::map<std::string, Instigation*>();

    return this->currentAction;

}

std::string NOPGraph::generateEntityName(std::string prefix) {

    std::string name = prefix;

    name += "Unnamed";
    name += std::to_string(++this->unnamedEntityCount);

    return name;

}

std::string NOPGraph::addFbe(Fbe *fbe) {

    std::string errorMessage = "";

    if (fbe->getName() == "") {
        return "Invalid name";
    }

    if (this->fbes->count(fbe->getName()) == 0) {

        this->fbes->insert(std::pair<std::string, Fbe*>(fbe->getName(), fbe));

    } else {
        
        errorMessage += "Fbe redefinition [";
        errorMessage += fbe->getName();
        errorMessage += "]";
        return errorMessage;
        
    }
    
    return "";

}

std::string NOPGraph::addIncludeBlock(IncludeBlock *includeBlock) {

    std::string errorMessage = "";

    if (this->includes->count(includeBlock->getTarget()->getTargetName()) == 0) {

        this->includes->insert(std::pair<std::string, IncludeBlock*>(includeBlock->getTarget()->getTargetName(), includeBlock));

    } else {
        
        errorMessage += "Include block redefinition [";
        errorMessage += includeBlock->getTarget()->getTargetName();
        errorMessage += "]";
        return errorMessage;
        
    }
    
    return "";

}

std::string NOPGraph::addCodeBlock(CodeBlock *codeBlock) {

    std::string errorMessage = "";

    if (this->codes->count(codeBlock->getTarget()->getTargetName()) == 0) {

        this->codes->insert(std::pair<std::string, CodeBlock*>(codeBlock->getTarget()->getTargetName(), codeBlock));

    } else {
        
        errorMessage += "Code block redefinition [";
        errorMessage += codeBlock->getTarget()->getTargetName();
        errorMessage += "]";
        return errorMessage;
        
    }
    
    return "";

}

std::string NOPGraph::addParam(Param *param) {

    std::string errorMessage = "";

    if (this->params->count(param->getName()) == 0) {

        this->params->insert(std::pair<std::string, Param*>(param->getName(), param));

    } else {
        
        errorMessage += "Param redefinition [";
        errorMessage += param->getName();
        errorMessage += "]";
        return errorMessage;
        
    }
    
    return "";

}

std::string NOPGraph::addAttribution(Attribution *attribution) {

    std::string errorMessage = "";

    if (this->attributions->count(attribution->getElement()->getStringValue()) == 0) {

        this->attributions->insert(std::pair<std::string, Attribution*>(attribution->getElement()->getStringValue(), attribution));

    } else {
        
        errorMessage += "Attribution redefinition [";
        errorMessage += attribution->getElement()->getStringValue();
        errorMessage += "]. Consider using parameters instead.";
        return errorMessage;
        
    }
    
    return "";

}

std::string NOPGraph::addProperty(Property *property) {

    std::string errorMessage = "";

    if (this->properties->count(property->getTypeName()) == 0) {

        this->properties->insert(std::pair<std::string, Property*>(property->getTypeName(), property));

    } else {
        
        errorMessage += "Property redefinition [";
        errorMessage += property->getTypeName();
        errorMessage += "]";
        return errorMessage;
        
    }
    
    return "";

}

std::string NOPGraph::addArgument(Argument *argument) {

    std::string errorMessage = "";

    this->arguments->push_back(argument);
    
    return "";

}

std::string NOPGraph::addCall(Call *call) {

    std::string errorMessage = "";

    this->calls->push_back(call);
    
    return "";

}

std::string NOPGraph::addInstance(Instance *instance) {

    std::string errorMessage = "";

    if (instance->getName() == "") {
        return "Invalid name";
    }

    // TODO
    // if (instance->getFbe() == 0) {
    //     return "Instance of an undefined FBE";
    // }

    if (this->instances->count(instance->getName()) == 0) {

        this->instances->insert(std::pair<std::string, Instance*>(instance->getName(), instance));

    } else {
        
        errorMessage += "Instance redefinition [";
        errorMessage += instance->getName();
        errorMessage += "]";
        return errorMessage;
            
    }
    
    return "";

}

std::string NOPGraph::addEntity(Entity *entity) {

    std::string errorMessage = "";

    if (entity->getName() == "") {
        return "Invalid Name";
    }

    if (Attribute* attribute = dynamic_cast<Attribute*>(entity)) {

        if (this->attributes->count(attribute->getName()) == 0) {

            this->attributes->insert(std::pair<std::string, Attribute*>(attribute->getName(), attribute));

        } else {
            
            errorMessage += "Attribute redefinition [";
            errorMessage += attribute->getName();
            errorMessage += "]";
            return errorMessage;
             
        }

    } else if (Method* method = dynamic_cast<Method*>(entity)) {
        
        if (this->methods->count(method->getName()) == 0) {

            this->methods->insert(std::pair<std::string, Method*>(method->getName(), method));

        } else {
            
            errorMessage += "Method redefinition [";
            errorMessage += method->getName();
            errorMessage += "]";
            return errorMessage;
            
        }

    } else if (Premise* premise = dynamic_cast<Premise*>(entity)) {
        
        if (this->premises->count(premise->getName()) == 0) {

            this->premises->insert(std::pair<std::string, Premise*>(premise->getName(), premise));

        } else {
            
            errorMessage += "Premise redefinition [";
            errorMessage += premise->getName();
            errorMessage += "]";
            return errorMessage;
            
        }

    } else if (Subcondition* subcondition = dynamic_cast<Subcondition*>(entity)) {
        
        if (this->subconditions->count(subcondition->getName()) == 0) {

            this->subconditions->insert(std::pair<std::string, Subcondition*>(subcondition->getName(), subcondition));
            
        } else {
            
            errorMessage += "Subcondition redefinition [";
            errorMessage += subcondition->getName();
            errorMessage += "]";
            return errorMessage;
            
        }

    } else if (Rule* rule = dynamic_cast<Rule*>(entity)) {
        
        if (this->rules->count(rule->getName()) == 0) {

            this->rules->insert(std::pair<std::string, Rule*>(rule->getName(), rule));
            
        } else {
            
            errorMessage += "Rule redefinition [";
            errorMessage += rule->getName();
            errorMessage += "]";
            return errorMessage;
            
        }

    } else if (Instigation* instigation = dynamic_cast<Instigation*>(entity)) {
        
        if (this->instigations->count(instigation->getName()) == 0) {

            this->instigations->insert(std::pair<std::string, Instigation*>(instigation->getName(), instigation));
            
        } else {
            
            errorMessage += "Instigation redefinition [";
            errorMessage += instigation->getName();
            errorMessage += "]";
            return errorMessage;
            
        }

    }

    return errorMessage;

}

std::string NOPGraph::saveMainBlock() {

	std::string errorMessage = "";
	
	if (this->attributions != 0 && this->attributions->size() > 0) {

		this->mainBlock = new MainBlock(this->attributions);

		this->attributions = new std::map<std::string, Attribution*>();

	} else {

		errorMessage = "Main block without attributions";
		return errorMessage;

	}
	
	return errorMessage;

}

Fbe* NOPGraph::getFbe(std::string name) {

    if (this->fbes->count(name) > 0) {

        std::map<std::string, Fbe*>::iterator it = this->fbes->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

std::map<std::string, Fbe*>* NOPGraph::getFbes() {

	return this->fbes;

}

Instance* NOPGraph::getFbeMainInstance() {

    return this->getFbe("Main")->getInstance("this");

}

MainBlock* NOPGraph::getMainBlock() {

	return this->mainBlock;

}

Target* NOPGraph::createTarget(int target) {

    return new Target(target);

}

IncludeBlock* NOPGraph::createIncludeBlock(Target *target, std::string code) {

    return new IncludeBlock(target, code);

}

CodeBlock* NOPGraph::createCodeBlock(Target *target, std::string code) {

    return new CodeBlock(target, code);

}

Strategy* NOPGraph::createStrategy(int strategy) {

    return new Strategy(strategy);

}

Type* NOPGraph::createType(int basicType, int numBytes) {

    return new Type(basicType, numBytes);

}

Visibility* NOPGraph::createVisibility(int visibility) {

    return new Visibility(visibility);

}

Param* NOPGraph::createParam(Type* type, std::string name) {

    return new Param(type, name);

}

Symbol* NOPGraph::createSymbol(int symbol) {

    return new Symbol(symbol);

}

Conjunction* NOPGraph::createConjunction(int conjunction) {

    return new Conjunction(conjunction);

}

ElementFactor* NOPGraph::createElementFactor(std::string fbeName, std::string attributeName) {

    return new ElementFactor(fbeName, attributeName);

}

VectorElementFactor* NOPGraph::createElementFactor(std::string fbeName, VectorDto *attributeName) {
    return new VectorElementFactor(fbeName, NULL, attributeName->getName(), attributeName);

}

VectorElementFactor* NOPGraph::createElementFactor(VectorDto *fbeName, VectorDto *attributeName) {
    return new VectorElementFactor(fbeName->getName(), fbeName, attributeName->getName(), attributeName);

}

VectorElementFactor* NOPGraph::createElementFactor(VectorDto *fbeName, std::string attributeName) {
    return new VectorElementFactor(fbeName->getName(), fbeName, attributeName, NULL);
}

BooleanFactor* NOPGraph::createBooleanFactor(bool value) {

    return new BooleanFactor(value);

}

IntegerFactor* NOPGraph::createIntegerFactor(int value) {

    return new IntegerFactor(value);

}

DoubleFactor* NOPGraph::createDoubleFactor(double value) {

    return new DoubleFactor(value);

}

CharFactor* NOPGraph::createCharFactor(char value) {

    return new CharFactor(value);

}

StringFactor* NOPGraph::createStringFactor(std::string value) {

    return new StringFactor(value);

}

std::vector<Factor*>* NOPGraph::addToFactorArray(Factor* factor) {
    this->factorArray->push_back(factor);
    return this->factorArray;
}

Attribution* NOPGraph::createAttribution(ElementFactor *element, Factor *factor) {

    return new Attribution(element, factor);

}

Expression* NOPGraph::createExpression(Factor *leftFactor, Symbol *symbol, Factor *rightFactor) {

    return new Expression(leftFactor, symbol, rightFactor);

}

Argument* NOPGraph::createArgument(Factor* factor) {

    return new Argument(factor);

}

Call* NOPGraph::createCall(std::string fbeName, std::string methodName) {

    Call *call = new Call(fbeName, methodName, this->arguments);

    this->arguments = new std::list<Argument*>();

    return call;

}

Execution* NOPGraph::createExecution(int execution) {

    return new Execution(execution);

}

std::string NOPGraph::checkPremisesConjunction(Conjunction *conjunction) {

    if (this->currentPremisesConjunction == 0) {

        this->currentPremisesConjunction = conjunction;

    } else {

        if (this->currentPremisesConjunction != 0 && this->currentPremisesConjunction->getConjunctionId() != conjunction->getConjunctionId()) {

            return "Conjunction mismatch in premises";

        }

    }

    return "";

}

std::string NOPGraph::checkSubconditionsConjunction(Conjunction *conjunction) {

    if (this->currentSubconditionsConjunction == 0) {

        this->currentSubconditionsConjunction = conjunction;

    } else {

        if (this->currentSubconditionsConjunction != 0 && this->currentSubconditionsConjunction->getConjunctionId() != conjunction->getConjunctionId()) {

            return "Conjunction mismatch in subconditions";

        }

    }

    return "";

}

bool NOPGraph::checkConsistency() {

    if (!this->checkFbeMain()) {
        return false;
    }

    if (!this->checkFbeInstances()) {
        return false;
    }

    if (!this->instantiateElements(getFbeMainInstance())) {
        return false;
    }

    if (!this->connectEntities(getFbeMainInstance())) {
        return false;
    }

    return true;

}

bool NOPGraph::checkFbeMain() {

    std::cout << "> Looking for FBE Main" << std::endl;

    Fbe *fbe = this->getFbe("Main");

    if (fbe == 0) {
        
        std::cout << "> Error: Fbe Main was not found in the project" << std::endl;
        
        return false;

    }

    return true;

}

bool NOPGraph::checkFbeInstances() {

    std::cout << "> Checking FBE instances consistency..." << std::endl;

    for (std::map<std::string, Fbe*>::iterator it = fbes->begin(); it != fbes->end(); ++it) {

        Fbe *fbe = it->second;

        std::map<std::string, Instance*> *instances = fbe->getInstances();

        for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

            Instance *instance = it->second;

            instance->setFbe(this->getFbe(instance->getFbeName()));

            if (instance->getFbe() == 0) {

                std::cout << "> Error: Fbe [" << instance->getFbeName() << "] was not defined for instance [" << instance->getName() << "]" << std::endl;

                return false;

            }

        }

    }

    return true;

}

bool NOPGraph::instantiateElements(Instance *instance) {

    std::cout << "> Instantiate instance " << instance->getName() << " elements..." << std::endl;

	instance->setFbe(this->getFbe(instance->getFbeName()));

	instance->instantiateElements();

	std::map<std::string, Instance*> *instances = instance->getInstances();

	for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

		Instance *i = it->second;

		if (i->getName() != "this") {

			instantiateElements(i);

		}

	}

    return true;

}

bool NOPGraph::connectPremises(Premise *premise) {

	Expression *expression = premise->getExpression();

	Factor *leftFactor = expression->getLeftFactor();

	std::string leftValue = "";
	std::string rightValue = "";
	
	if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

		ElementFactor *element = (ElementFactor*)leftFactor;

		std::string instanceName = element->getInstanceName();

		// std::cout << premise->getParentInstance()->getName() << "." << premise->getName() << std::endl;
		
		if (instanceName == "this") {

			element->setInstance(premise->getParentInstance());

		} else {

			Instance *elementInstance = premise->getParentInstance()->getInstance(instanceName);

			element->setInstance(elementInstance);

		}

		if (element->getInstance() == 0) {

			std::cout << "> Error: Instance [" << element->getInstanceName() << "] was not defined for premise [" << premise->getName() << "]" << std::endl;

			return false;

		}

		std::string attributeName = element->getAttributeName();

		// std::cout << instanceName << "." << attributeName << std::endl;

		// std::cout << instanceName << " != " << element->getInstance()->getName() << std::endl;
		// if (instanceName != element->getInstance()->getName()) {
		// 	return false;
		// }

		Attribute *attribute = element->getInstance()->getAttribute(attributeName);

		element->setAttribute(attribute);

		if (element->getAttribute() == 0) {

			std::cout << "> Error: Attribute [" << element->getAttributeName() << "] was not defined for premise [" << premise->getName() << "]" << std::endl;

			return false;

		}

		attribute->addPremise(premise);

		premise->addAttribute(attribute);

		leftValue = attribute->getFactor()->getStringValue();

	} else {

		leftValue = leftFactor->getStringValue();

	}

	Factor *rightFactor = expression->getRightFactor();
	
	if (rightFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

		ElementFactor *element = (ElementFactor*)rightFactor;

		std::string instanceName = element->getInstanceName();

		// std::cout << premise->getParentInstance()->getName() << "." << premise->getName() << std::endl;
		
		if (instanceName == "this") {

			element->setInstance(premise->getParentInstance());

		} else {

			Instance *elementInstance = premise->getParentInstance()->getInstance(instanceName);

			element->setInstance(elementInstance);

		}

		if (element->getInstance() == 0) {

			std::cout << "> Error: Instance [" << element->getInstanceName() << "] was not defined for premise [" << premise->getName() << "]" << std::endl;

			return false;

		}

		std::string attributeName = element->getAttributeName();
		
		// std::cout << instanceName << "." << attributeName << std::endl;

		// std::cout << instanceName << "." << element->getInstance()->getName() << std::endl;
		// if (instanceName != element->getInstance()->getName()) {
		// 	return false;
		// }

		Attribute *attribute = element->getInstance()->getAttribute(attributeName);

		element->setAttribute(attribute);

		if (element->getAttribute() == 0) {

			std::cout << "> Error: Attribute [" << element->getAttributeName() << "] was not defined for premise [" << premise->getName() << "]" << std::endl;

			return false;

		}

		attribute->addPremise(premise);

		premise->addAttribute(attribute);

		rightValue = attribute->getFactor()->getStringValue();

	} else {

		rightValue = rightFactor->getStringValue();

	}

	if (leftValue == rightValue) {
		premise->setInitialLogicalValue(true);
	}

	return true;

}

bool NOPGraph::connectEntities(Instance *instance) {

    std::cout << "> Connecting entities with one another..." << std::endl;

	std::cout << instance << " " << instance->getName() << std::endl;

	std::cout << "rules:" << std::endl;

	std::map<std::string, Rule*> *rules = instance->getRules();

	for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {

		Rule *rule = it->second;

		Condition *condition = rule->getCondition();

		std::map<std::string, Premise*> *premises = condition->getPremises();

		for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

			Premise *premise = it->second;

			if (!connectPremises(premise)) {
				return false;
			}

			if (premise->getInitialLogicalValue()) {
				condition->setNumberApprovedPremises(condition->getNumberApprovedPremises() + 1);
			}

		}

		std::map<std::string, Subcondition*> *subconditions = condition->getSubconditions();

		for (std::map<std::string, Subcondition*>::iterator it = subconditions->begin(); it != subconditions->end(); ++it) {

			Subcondition *subcondition = it->second;

			std::map<std::string, Premise*> *premises = subcondition->getPremises();

			for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

				Premise *premise = it->second;

				if (!connectPremises(premise)) {
					return false;
				}

				if (premise->getInitialLogicalValue()) {
					subcondition->setNumberApprovedPremises(subcondition->getNumberApprovedPremises() + 1);
				}

			}

		}

		Action *action = rule->getAction();

		std::map<std::string, Instigation*> *instigations = action->getInstigations();

		for (std::map<std::string, Instigation*>::iterator it = instigations->begin(); it != instigations->end(); ++it) {

			Instigation *instigation = it->second;

			std::cout << instigation << std::endl;

			instigation->print();

			std::list<Call*> *calls = instigation->getCalls();
			for (std::list<Call*>::iterator it = calls->begin(); it != calls->end(); ++it) {

				Call *call = (*it);
				
				std::string instanceName = call->getInstanceName();

				if (instanceName == "this") {

					call->setInstance(instigation->getParentInstance());

				} else {

					Instance *elementInstance = instigation->getParentInstance()->getInstance(instanceName);

					call->setInstance(elementInstance);

				}

				if (call->getInstance() == 0) {

					std::cout << "> Error: Instance [" << call->getInstanceName() << "] was not defined for call [" << call->getInstanceName() << "." << call->getMethodName() << "]" << std::endl;

					return false;

				}

				std::string methodName = call->getMethodName();

				Method *method = call->getInstance()->getMethod(methodName);

				call->setMethod(method);

				if (call->getMethod() == 0) {

					std::cout << "> Error: Method [" << call->getInstanceName() << "] was not defined for call [" << call->getInstanceName() << "." << call->getMethodName() << "]" << std::endl;

					return false;

				}

				std::map<std::string, Attribution*> *attributions = method->getAttributions();
				for (std::map<std::string, Attribution*>::iterator it = attributions->begin(); it != attributions->end(); ++it) {

					Attribution *attribution = it->second;
					Factor *factor = attribution->getFactor();
					
					ElementFactor *element =  attribution->getElement();

					std::string instanceName = element->getInstanceName();
					
					if (instanceName == "this") {

						element->setInstance(method->getParentInstance());

					} else {

						Instance *elementInstance = method->getParentInstance()->getInstance(instanceName);

						element->setInstance(elementInstance);

					}

					if (element->getInstance() == 0) {

						std::cout << "> Error: Instance [" << element->getInstanceName() << "] was not defined for method [" << method->getName() << "]" << std::endl;

						return false;

					}

					std::string attributeName = element->getAttributeName();
					
					Attribute *attribute = element->getInstance()->getAttribute(attributeName);

					element->setAttribute(attribute);

					if (element->getAttribute() == 0) {

						std::cout << "> Error: Attribute [" << element->getAttributeName() << "] was not defined for method [" << method->getName() << "]" << std::endl;

						return false;

					}
					
				}

			}

		}

	}

	std::map<std::string, Instance*> *instances = instance->getInstances();

	for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

		Instance *i = it->second;

		if (i->getName() != "this") {

			connectEntities(i);

		}

	}
	
	if (this->getMainBlock() != 0) {

		std::map<std::string, Attribution*> *attributions = this->getMainBlock()->getAttributions();

		for (std::map<std::string, Attribution*>::iterator it = attributions->begin(); it != attributions->end(); ++it) {

			Attribution *attribution = it->second;
			
			ElementFactor *element =  attribution->getElement();
			
			std::string instanceName = element->getInstanceName();
			
			if (instanceName == "this") {

				element->setInstance(getFbeMainInstance());

			} else {

				Instance *elementInstance = getFbeMainInstance()->getInstance(instanceName);

				element->setInstance(elementInstance);

			}

			std::string attributeName = element->getAttributeName();
			
			Attribute *attribute = element->getInstance()->getAttribute(attributeName);

			element->setAttribute(attribute);

		}

	}

	return true;

}

char *NOPGraph::convertVectorToId(std::string name, int position) {
    std::string fullName = this->createVectorName(name, position);
    char *toBeReturned = new char[fullName.length() + 1];
    std::strcpy(toBeReturned, (char *)fullName.c_str());
    return toBeReturned;
}

VectorDto *NOPGraph::convertVectorToVectorElement(std::string name, Expression *expression) {
    if (this->formationRule == NULL) {
        std::string error = "You can't use indexes outside of a formationRule";
        std::cerr << std::endl << error << std::endl;
        throw new std::invalid_argument(error);
    }
    return new VectorDto(name, expression);
}

VectorDto *NOPGraph::convertVectorToVectorElement(std::string name, std::string index) {
    if (this->formationRule == NULL) {
        std::string error = "You can't use index [" + index + "] outside of a formationRule";
        std::cerr << std::endl << error << std::endl;
        throw new std::invalid_argument(error);
    }
    return new VectorDto(name, index);
}

std::string NOPGraph::addFormationRuleIndex(std::string id, int from, int to) {

    std::string errorMessage = "";

    if (this->formationRule == NULL) {
        this->formationRule = new FormationRule();
    }

    errorMessage = this->formationRule->addFormationRuleIndex(new FormationRuleIndex(id, from, to));

    return errorMessage;

}

std::string NOPGraph::createFormationRule(std::string name) {

    if (this->formationRule == NULL) {
        return "You can't create a FORMATION_RULE without indexes";
    }
    if (this->formationRule->getFormationRuleIndex() == NULL) {
        return "You need to have at least 1 index in your FORMATION_RULE";
    }

    std::string errorMessage = this->createRuleFromFormationRule(this->formationRule->getFormationRuleIndex(), name);

    this->formationRule = NULL;

    return errorMessage;
}


std::string NOPGraph::createRuleFromFormationRule(FormationRuleIndex *fri, std::string name) {
    std::string errorMessage = "";    
    std::string internalName = name + "_" + fri->getId();

    for (int i = fri->getFrom(); i<= fri->getTo(); i++) {
        this->formationRule->updateIndex(fri->getId(), i);

        std::string internalNameFor = internalName + "_" + std::to_string(i);
        if (fri->getNextFormationRuleIndex()) {
            errorMessage += this->createRuleFromFormationRule(fri->getNextFormationRuleIndex(), internalNameFor);
        } else {
            errorMessage += this->addEntity((Entity*)this->createVectorRule(internalNameFor));
        }
    }

    return errorMessage;
}

Rule* NOPGraph::createVectorRule(std::string name) {

    if (name == "") {
        name = this->generateEntityName("rl");
    }

    Action *oldAction = this->currentAction;
    Execution *execution = this->createExecution(this->currentAction->getExecution()->getExecution());
    Rule *rule = new Rule(0, name, this->createVectorCondition(name + "_" + this->currentCondition->getName()), 
                    this->createVectorAction(name + "_"  + this->currentAction->getName(), 
                    execution,
                    this->cloneInstigationArray(this->currentAction->getInstigations(), name, execution)));
    
    this->currentAction = oldAction;

    return rule;

}

Action* NOPGraph::createVectorAction(std::string name, Execution *execution, std::map<std::string, Instigation*> *instigations) {

    if (name == "") {
        name = this->generateEntityName("ac");
    }

    Action* action = new Action(0, name, execution, instigations);
    
    return action;

}

std::map<std::string, Instigation*>* NOPGraph::cloneInstigationArray(std::map<std::string, Instigation*> *original, std::string name, Execution *execution) {
    std::map<std::string, Instigation*>* instigations = new std::map<std::string, Instigation*>();

    if (original == NULL) {
        return instigations;
    }

    for (std::map<std::string, Instigation*>::iterator it=original->begin(); it!=original->end(); ++it) {
        Instigation *instigation = this->createVectorInstigation(name, execution, it->second->getCalls());
        instigations->insert((std::pair<std::string, Instigation*>(it->first, instigation)));
    }


    return instigations;
}

Instigation* NOPGraph::createVectorInstigation(std::string name, Execution *execution, std::list<Call*> *calls) {

    if (name == "") {
        name = this->generateEntityName("in");
    }

    Instigation *instigation = new Instigation(0, name, execution, calls);

    return instigation;

}

Condition* NOPGraph::createVectorCondition(std::string name) {

    if (name == "") {
        name = this->generateEntityName("cd");
    }

    std::map<std::string, Premise*> *premises = this->clonePremisseArray(this->currentCondition->getPremises(), name);

    std::map<std::string, Subcondition*> *subConditions = new std::map<std::string, Subcondition*>();

    for (std::map<std::string, Subcondition*>::iterator itSub=this->currentCondition->getSubconditions()->begin(); 
        itSub!=this->currentCondition->getSubconditions()->end(); ++itSub) {

            int conjunctionId = itSub->second->getConjunction() != NULL ? itSub->second->getConjunction()->getConjunctionId() : -1;
            Subcondition *subCondition = new Subcondition(0, name + "_" + itSub->second->getName(), 
                conjunctionId != -1 ? this->createConjunction(conjunctionId) : NULL,
                this->clonePremisseArray(itSub->second->getPremises(), name + "_" + itSub->second->getName()));

            subConditions->insert((std::pair<std::string, Subcondition*>(itSub->first, subCondition)));
    }

    int conjunctionId = this->currentCondition->getConjunction() != NULL ? this->currentCondition->getConjunction()->getConjunctionId() : -1;
    Condition *condition = new Condition(0, name, 
        conjunctionId != -1 ? this->createConjunction(conjunctionId) : NULL,
            premises, subConditions);

    return condition;

}


std::map<std::string, Premise*> * NOPGraph::clonePremisseArray(std::map<std::string, Premise*> *original, std::string name) {
    std::map<std::string, Premise*> *premises = new std::map<std::string, Premise*>();

    if (original == NULL) {
        return premises;
    }

    for (std::map<std::string, Premise*>::iterator it=original->begin(); it!=original->end(); ++it) {
        Factor *leftFactor = this->createElementFactorForClonedPromise(it->second->getExpression()->getLeftFactor());
        Factor *rightFactor = this->createElementFactorForClonedPromise(it->second->getExpression()->getRightFactor());

        Expression *expression = new Expression(leftFactor, it->second->getExpression()->getSymbol(), rightFactor);

        Premise *premise = new Premise(0, name + "_" + it->second->getName(), expression, it->second->getImpertinent());
        premises->insert((std::pair<std::string, Premise*>(it->first, premise)));
    }
    return premises;
}

Factor *NOPGraph::createElementFactorForClonedPromise(Factor *factor) {
    std::string instance;
    std::string attribute;

    if (factor->getFactorId() == Factor::VECTOR_ELEMENT_FACTOR) {
        VectorElementFactor *vef = (VectorElementFactor*) factor;
        instance = this->evaluateVectorOrVariableName(vef->getInstanceName(), vef->getInstanceIndex(), vef->getInstanceExpression());
        attribute = this->evaluateVectorOrVariableName(vef->getAttributeName(), vef->getAttributeIndex(), vef->getAttributeExpression());
        return new ElementFactor(instance, attribute);
    } else if (factor->getFactorId() == Factor::ELEMENT_FACTOR) {
        ElementFactor *ef = (ElementFactor*) factor;
        return new ElementFactor(ef->getInstanceName(), ef->getAttributeName());
    } else {
        return factor;
    }
}

std::string NOPGraph::evaluateVectorOrVariableName(std::string name, std::string index, Expression *expression) {
    if (index.empty() && expression == NULL) {
        return name;
    } 

    if (!index.empty()) {
        try {
            int position = this->formationRule->getCurrentPositionForIndex(index);
            return this->createVectorName(name, position);
        } catch (std::out_of_range e) {
            return "vector index '" + index + "'";
        }
    }

    if (expression != NULL) {
        try {
            int position = this->formationRule->getCurrentPositionForIndex(expression->getVectorIndex());
            return this->createVectorName(name, expression->evaluateVectorExpression(position));
        } catch (const std::out_of_range e) {
            return "vector index '" + index + "'";
        } catch (const std::invalid_argument e) {
            return e.what();
        }
    }
    
    return NULL;
}

void NOPGraph::iterateOverNOPGraph() {

	{ // fbes

        std::cout << "\n" << "Fbes: [";
        
        std::map<std::string, Fbe*> *fbes = getFbes();
        for (std::map<std::string, Fbe*>::iterator it = fbes->begin(); it != fbes->end(); ++it) {

            Fbe *fbe = it->second;

            std::cout << " " << fbe->getName();

        }

        std::cout << " ]" << std::endl;

    }

    { // instances

        std::cout << "\n\n" << "Instances:\n\n";

        iterateOverNOPGraphInstances(getFbeMainInstance());

    }

}

void NOPGraph::iterateOverNOPGraphInstances(Instance *instance) {

    { // instances

        iterateOverNOPGraphInstance(instance);

	std::map<std::string, Instance*> *instances = instance->getInstances();

        for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

            Instance *i = it->second;

            if (i->getName() != "this") {

                iterateOverNOPGraphInstances(i);

            }

        }

    }

}

void NOPGraph::iterateOverNOPGraphInstance(Instance *instance) {

    std::cout << " " << instance << " " << instance->getFbeName() << " - " << instance->getName() << ":" << std::endl;

    std::cout << "\tInstances: [";
    
    std::map<std::string, Instance*> *instances = instance->getInstances();
    for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

        Instance *instance = it->second;

        if (instance->getName() != "this") {

            std::cout << " " << instance << " " << instance->getName();

        }

    }

    std::cout << " ]" << std::endl;

    iterateOverNOPGraphInstanceAttributes(instance);

}

void NOPGraph::iterateOverNOPGraphInstanceAttributes(Instance *instance) {

    { // attributes
        
        std::cout << "\tAttributes: [";

        std::map<std::string, Attribute*> *attributes = instance->getAttributes();

        for (std::map<std::string, Attribute*>::iterator it = attributes->begin(); it != attributes->end(); ++it) {

            Attribute *attribute = it->second;

            std::cout << " " << attribute->getParentInstance() << " " << attribute->getParentInstance()->getName() << "." << attribute->getName();

        }

        std::cout << " ]" << std::endl;

	for (std::map<std::string, Attribute*>::iterator it = attributes->begin(); it != attributes->end(); ++it) {

            Attribute *attribute = it->second;

            iterateOverNOPGraphAttribute(attribute);

        }

    }

}

void NOPGraph::iterateOverNOPGraphAttribute(Attribute *attribute) {

    std::cout << "\tPremises: [";
    
    std::map<std::string, Premise*> *premises = attribute->getPremises();
    for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

        Premise *premise = it->second;

        std::cout << " " << premise->getParentInstance() << " " << premise->getParentInstance()->getName() << "." << premise->getName();

    }

    std::cout << " ]" << std::endl;

}


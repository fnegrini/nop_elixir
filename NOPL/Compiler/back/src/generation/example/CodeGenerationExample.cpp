#include "generation/example/CodeGenerationExample.h"

#include "NOPGraph.h"

#include "elements/Fbe.h"
#include "elements/Instance.h"
#include "elements/Attribute.h"
#include "elements/Method.h"
#include "elements/Premise.h"
#include "elements/Subcondition.h"
#include "elements/Condition.h"
#include "elements/Rule.h"
#include "elements/Action.h"
#include "elements/Instigation.h"
#include "elements/Expression.h"
#include "elements/Factor.h"
#include "elements/ElementFactor.h"
#include "elements/Symbol.h"
#include "elements/Type.h"
#include "elements/Visibility.h"

#include <map>
#include <iostream>

CodeGenerationExample::CodeGenerationExample() : Compiler() {

    //

}

CodeGenerationExample::~CodeGenerationExample() {

    //

}

void CodeGenerationExample::generateCode() {

    std::cout << "\nGenerating code..." << std::endl;

    Instance *mainInstance = graph->getFbeMainInstance();

    generateCodeInstance(mainInstance, 0);

    std::string filename = "Generated/example.nop";
    
    filestream.open(filename, std::fstream::out);
    
    std:string s = sstream.str();

    filestream << s;

    filestream.flush();
    filestream.close();

}

void CodeGenerationExample::generateCodeInstance(Instance *instance, int level) {

    level++;

    sstream << getLevel(level) << instance->getFbe()->getName() << " " << instance->getName() << ";\n" << std::endl;

    iterateOverInstances(instance, level);

    iterateOverAttributes(instance, level);

    iterateOverMethods(instance, level);

    iterateOverRules(instance, level);

}

void CodeGenerationExample::iterateOverInstances(Instance *instance, int level) {

    std::map<std::string, Instance*> *instances = instance->getInstances();

    for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

        Instance *instance = it->second;

        // TODO - GAMBI
        if (instance->getName() != "this") {

            generateCodeInstance(instance, level);

        }

    }

}

void CodeGenerationExample::iterateOverAttributes(Instance *instance, int level) {

    std::map<std::string, Attribute*> *attributes = instance->getAttributes();

    for (std::map<std::string, Attribute*>::iterator it = attributes->begin(); it != attributes->end(); ++it) {

        Attribute *attribute = it->second;

        generateCodeAttribute(attribute, level);

    }

}

void CodeGenerationExample::iterateOverMethods(Instance *instance, int level) {

    std::map<std::string, Method*> *methods = instance->getMethods();

    for (std::map<std::string, Method*>::iterator it = methods->begin(); it != methods->end(); ++it) {

        Method *method = it->second;

        generateCodeMethod(method, level);

    }

}

void CodeGenerationExample::iterateOverRules(Instance *instance, int level) {

    std::map<std::string, Rule*> *rules = instance->getRules();

    for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {

        Rule *rule = it->second;

        Condition *condition = rule->getCondition();

        std::map<std::string, Subcondition*> *subconditions = condition->getSubconditions();

        for (std::map<std::string, Subcondition*>::iterator it = subconditions->begin(); it != subconditions->end(); ++it) {

            Subcondition *subcondition = it->second;

            std::map<std::string, Premise*> *premises = subcondition->getPremises();

            for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

                Premise *premise = it->second;

                generateCodePremise(premise, level);

            }

        }

        std::map<std::string, Premise*> *premises = condition->getPremises();

        for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

            Premise *premise = it->second;

            generateCodePremise(premise, level);

        }

    }

    

}

void CodeGenerationExample::generateCodeAttribute(Attribute *attribute, int level) {

    level++;

    sstream << getLevel(level) << attribute->getVisibility()->getVisibilityName() << " " << attribute->getType()->getTypeName() << "* " << attribute->getName() << ";\n\n";

}

void CodeGenerationExample::generateCodeMethod(Method *method, int level) {

    level++;

    sstream << getLevel(level) << method->getVisibility()->getVisibilityName() << " " << method->getName() << ";\n\n";

}

void CodeGenerationExample::generateCodePremise(Premise *premise, int level) {

    level++;

    sstream << getLevel(level) << "Premise* " << premise->getName() << "(";

    Expression *expression = premise->getExpression();

    Factor *leftFactor = expression->getLeftFactor();

    if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

        ElementFactor *element = (ElementFactor*)leftFactor;

        sstream << element->getInstance()->getName();

        sstream << "->";

        sstream << element->getAttribute()->getName();

    } else {

        sstream << leftFactor->getStringValue();

    }

    sstream << ", " << expression->getSymbol()->getSymbolName() << ", ";

    Factor *rightFactor = expression->getRightFactor();

    if (rightFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

            ElementFactor *element = (ElementFactor*)rightFactor;

            sstream << element->getInstance()->getName();

            sstream << "->";

            sstream << element->getAttribute()->getName();

    } else {

            sstream << rightFactor->getStringValue();

    }

    sstream << ");\n" << std::endl;

}

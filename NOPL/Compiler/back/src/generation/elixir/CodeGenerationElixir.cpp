#include "generation/elixir/CodeGenerationElixir.h"

#include "NOPGraph.h"

#include "elements/Fbe.h"
#include "elements/Instance.h"
#include "elements/Attribute.h"
#include "elements/Attribution.h"
#include "elements/Method.h"
#include "elements/Premise.h"
#include "elements/Subcondition.h"
#include "elements/Conjunction.h"
#include "elements/Condition.h"
#include "elements/Rule.h"
#include "elements/Action.h"
#include "elements/Instigation.h"
#include "elements/Call.h"
#include "elements/Expression.h"
#include "elements/Factor.h"
#include "elements/ElementFactor.h"
#include "elements/Symbol.h"
#include "elements/Target.h"
#include "elements/Type.h"
#include "elements/Visibility.h"
#include "elements/Param.h"
#include "elements/CodeBlock.h"

#include <map>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

CodeGenerationElixir::CodeGenerationElixir() : Compiler() {

    //

}

CodeGenerationElixir::~CodeGenerationElixir(){

    //

}

void CodeGenerationElixir::createFileStructure(){

    std::cout << "\nGenerating file structure..." << std::endl;

    std::stringstream commandStream;
    commandStream << "rmdir /Q /S " << MAIN_FOLDER;
    // Eliminate old structure
    system(commandStream.str().c_str());

    // Create project system
    std::stringstream commandStream2;
    commandStream2 << "mix new  " << MAIN_FOLDER;
    system(commandStream2.str().c_str());

}

void CodeGenerationElixir::setConfigurationFiles(){

    std::cout << "\nGenerating configuration file \"mix.exs\"..." << std::endl;

    std::stringstream filenameStream;
    filenameStream << MAIN_FOLDER << "/mix.exs";
    std::fstream filestream;

    filestream.open(filenameStream.str(), std::fstream::out | std::fstream::trunc);

    // Set dependencies and initialize elixir app on mix.exs file
    filestream << "defmodule "<< MAIN_MODULE<< ".MixProject do\n";
    filestream << "  use Mix.Project\n";
    filestream << "\n";
    filestream << "  def project do\n";
    filestream << "    [\n";
    filestream << "      app: :" << MAIN_FOLDER << ",\n";
    filestream << "      version: \"0.1.0\",\n";
    filestream << "      elixir: \"~> 1.7\",\n";
    filestream << "      start_permanent: Mix.env() == :prod,\n";
    filestream << "      deps: deps()\n";
    filestream << "    ]\n";
    filestream << "  end\n";
    filestream << "\n";
    filestream << "  # Run \"mix help compile.app\" to learn about applications.\n";
    filestream << "  def application do\n";
    filestream << "    [\n";
    filestream << "      extra_applications: [:logger, :nop_elixir]\n";
    filestream << "    ]\n";
    filestream << "  end\n";
    filestream << "\n";
    filestream << "  # Run \"mix help deps\" to learn about dependencies.\n";
    filestream << "  defp deps do\n";
    filestream << "    [\n";
    filestream << "      {:nop_elixir, \"~> 0.0.22\"}\n",
    filestream << "    ]\n";
    filestream << "  end\n";
    filestream << "end\n";

    filestream.flush();
    filestream.close();

    // Get dependencies
    std::stringstream commandStream;
    commandStream << "cd " MAIN_FOLDER << " && mix deps.get";
    system(commandStream.str().c_str());

}


void CodeGenerationElixir::generateCode() {

    std::cout << "\nGenerating elixir project..." << std::endl;

    Instance *mainInstance = graph->getFbeMainInstance();
    createFileStructure();

    setConfigurationFiles();
    // Create main FBe
    Fbe *fbeMain = graph->getFbe("Main");


    std::stringstream filenameStream;
    filenameStream << MAIN_FOLDER << "/lib/" << MAIN_FOLDER << ".ex";
    fileElixir.open(filenameStream.str(), std::fstream::out | std::fstream::trunc);

    fileElixir << "defmodule " << MAIN_MODULE << " do\n";
    fileElixir << "  @moduledoc \"\"\"\n";
    fileElixir << "  Documentation for " << MAIN_MODULE <<".\n";
    fileElixir << "  \"\"\"\n";
    fileElixir << "\n";
    fileElixir << "  @doc \"\"\"\n";
    fileElixir << "  Hello world.\n";
    fileElixir << "\n";
    fileElixir << "  ## Examples\n";
    fileElixir << "\n";
    fileElixir << "      iex> "<< MAIN_MODULE << ".hello()\n";
    fileElixir << "      :world\n";
    fileElixir << "\n";
    fileElixir << "  \"\"\"\n";
    fileElixir << "  def hello do\n";
    fileElixir << "    :world\n";
    fileElixir << "  end\n";
    fileElixir << "\n";
    iterateOverMethods(&fileElixir, fbeMain);
    fileElixir << "\n";
    fileElixir << "  def initialize() do\n";
    fileElixir << "    instances = %{}\n";
    generateCodeInstance(mainInstance, 0);
    fileElixir << "\n";
    fileElixir << "    create_main_rules(instances)\n";
    fileElixir << "\n";
    fileElixir << "    instances\n";    
    fileElixir << "  end\n";
    fileElixir << "  def create_main_rules(instances) do\n";
    fileElixir << "\n";
    generateMainRules(&fileElixir, fbeMain);
    fileElixir << "\n";
    fileElixir << "  end\n";
    fileElixir << "end\n";
    fileElixir << "\n";

    fileElixir.flush();
    fileElixir.close();

    // Compile project
    std::stringstream commandStream;
    commandStream << "cd " MAIN_FOLDER << " && mix compile";
    system(commandStream.str().c_str());    

    // Run self test
    std::stringstream commandStream2;
    commandStream2 << "cd " MAIN_FOLDER << " && mix test";
    system(commandStream2.str().c_str());    

    std::cout << "\n Run command " << MAIN_MODULE << ".initialize() to start NOP environment..." << std::endl;
}

void CodeGenerationElixir::generateMainRules(std::fstream *filestream, Fbe *fbeMain){
    
    std::stringstream ss;
    ss << MAIN_FOLDER << "/lib/" /*<< fbe->getName()*/;
    std::string path = ss.str();

    iterateOverMainRules(fbeMain, path, filestream);

}

void CodeGenerationElixir::iterateOverMainRules(Fbe *fbe, std::string path, std::fstream *filestreamMain){

    std::map<std::string, Rule*> *rules = fbe->getRules();

    for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {

        Rule *rule = it->second;

        generateCodeMainRule(fbe, path, rule, filestreamMain);

    }

}

void CodeGenerationElixir::generateCodeMainRule(Fbe *fbe, std::string path, Rule *rule, std::fstream *filestreamMain){

    std::cout << "\nInterpreting Rule " << rule->getName() << " of FBE " << fbe->getName() << " as an actor" << std::endl;
    // Create Rule File
    std::stringstream filenameStream;
    filenameStream << path << "/" << rule->getName() << ".ex";
    std:string filename = filenameStream.str();
    std::fstream filestream;
    std::string rule_param;
    std::string rule_param_call;

    clearFbeList();
    determineMainRuleFbeList(fbe, rule);
    rule_param = "[";
    rule_param_call = "[";

    for (std::map<std::string, Fbe*>::iterator it = fbeList.begin(); it != fbeList.end(); ++it) {

        rule_param += it->first + ", ";

        rule_param_call += "instances[:" + it->first + "], ";

    }

    rule_param += "]";
    rule_param_call += "]";

    // Initialization call
    *filestreamMain << "    NOP.Service.Rule.create_rule(" << elixirNameofRule(fbe, rule);
    *filestreamMain << ", \"" << elixirNameofRule(fbe, rule) << "\", [" << rule_param_call << "])\n\n";

    filestream.open(filename, std::fstream::out | std::fstream::trunc);
    filestream << "defmodule "<< elixirNameofRule(fbe, rule) << " do\n";
    filestream << "  use NOP.Element.Rule\n";
    filestream << "\n";
    filestream << "  defp create_element_list([" << rule_param << "]) do\n";
    filestream << "\n";

    Condition *condition = rule->getCondition();
    
    iterateOverCondition(&filestream, fbe, rule, condition);

    filestream << "  end\n";
    filestream << "\n";
    filestream << "  defp create_instigation_list([" << rule_param << "]) do\n";
    filestream << "\n";
    filestream << "    [\n";

    iterateOverInstigations(&filestream, fbe, rule);

    filestream << "    ]\n";
    filestream << "\n";
    filestream << "  end\n";
    filestream << "\n";
    filestream << "end\n";
    filestream.flush();
    filestream.close();

}

bool CodeGenerationElixir::directoryExists(std::string path){
    struct stat info;

    if( stat( path.c_str(), &info ) != 0 )
        return false;
    else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows 
        return true;
    else
        return false;
}

void CodeGenerationElixir::checkAgregatedAttributes(std::fstream *filestream, Fbe *fbe) {

    std::map<std::string, Attribute*> *attributes = fbe->getAttributes();

    for (std::map<std::string, Attribute*>::iterator it = attributes->begin(); it != attributes->end(); ++it) {

        Attribute *attribute = it->second;

        //TODO: Generate a creation process like:
        // <attribute_name> = NOP.Service.FBE.create_fbe(Elixir.<FBE_TYPE>.FBE, "atribute_name")

    }

}

void CodeGenerationElixir::iterateOverAttributes(std::fstream *filestream, Fbe *fbe) {

    std::map<std::string, Attribute*> *attributes = fbe->getAttributes();

    for (std::map<std::string, Attribute*>::iterator it = attributes->begin(); it != attributes->end(); ++it) {

        Attribute *attribute = it->second;

        generateCodeAttribute(filestream, attribute);

    }

}

void CodeGenerationElixir::generateCodeAttribute(std::fstream *filestream, Attribute *attribute) {
    
    int typeID = attribute->getType()->getTypeId();

    if((typeID==Type::STRING_TYPE)||(typeID==Type::CHAR_TYPE)){
        *filestream << "        :" << attribute->getName() << " => \"" << attribute->getFactor()->getStringValue() << "\",\n";
    }else{
        *filestream << "        :" << attribute->getName() << " => " << attribute->getFactor()->getStringValue() << ",\n";
    }


}

std::string CodeGenerationElixir::capitalize(std::string word){
    std::string capitalized = word;
    capitalized[0] = toupper(capitalized[0]);
    return capitalized;
}

std::string CodeGenerationElixir::elixirNameofFBE(Fbe *fbe){
    std::stringstream s;
    s << MAIN_MODULE << "." << capitalize(fbe->getName());
    return s.str();
}

void CodeGenerationElixir::iterateOverMethods(std::fstream *filestream, Fbe *fbe){

    std::map<std::string, Method*> *methods = fbe->getMethods();

    for (std::map<std::string, Method*>::iterator it = methods->begin(); it != methods->end(); ++it) {

        Method *method = it->second;

        generateCodeMethod(filestream, fbe, method);

    }

}

void CodeGenerationElixir::generateCodeMethod(std::fstream *filestream, Fbe *fbe, Method *method) {

    std::map<std::string, Attribution*> *attributions = method->getAttributions();

    std::string paramsCode = "this";

    // Params
    std::map<std::string, Param*> *params = method->getParams();
    for (std::map<std::string, Param*>::iterator it = params->begin(); it != params->end(); ++it) {

        Param *param = it->second;
        paramsCode = paramsCode +  ", " + param->getName();
                    
    } 

    *filestream << "  def " << method->getName() << "(" << paramsCode << ") do \n";

    // Attribution
    for (std::map<std::string, Attribution*>::iterator it = attributions->begin(); it != attributions->end(); ++it) {

        Attribution *attribution = it->second;

        *filestream << "    NOP.Service.FBE.set_attribute(" << attribution->getElement()->getInstanceName() <<", :" <<  attribution->getElement()->getAttributeName() << ", " << attribution->getFactor()->getStringValue() << ")\n";

    }

    // Code
    std::map<std::string, CodeBlock*> *codeBlocks = method->getCodeBlocks();
    for (std::map<std::string, CodeBlock*>::iterator it = codeBlocks->begin(); it != codeBlocks->end(); ++it) {
        
        CodeBlock *codeBlock = it->second;

        if(codeBlock->getTarget()->getTargetId() == Target::ELIXIR_TARGET){
            *filestream << std::endl;
            *filestream << codeBlock->getCode() << std::endl;
            *filestream << std::endl;
        }
    }    

    *filestream << "  end\n";

}

void CodeGenerationElixir::createFBE(Fbe *fbe, std::string path){

    std::cout << "\nInterpreting FBE " << fbe->getName() << " as an actor" << std::endl;

    // Create folder
    std::stringstream comandStream;
    comandStream << "cd "<< MAIN_FOLDER << "/lib && mkdir " << fbe->getName();
    std::string command = comandStream.str();
    system(command.c_str());

    // Create FBE Definition
    std::stringstream filenameStream;
    filenameStream << path << "/fbe.ex";
    std:string filename = filenameStream.str();
    std::fstream filestream;

    filestream.open(filename, std::fstream::out | std::fstream::trunc);

    // Set dependencies and initialize elixir app on mix.exs file
    filestream << "defmodule " << elixirNameofFBE(fbe) << " do\n";
    filestream << "  use NOP.Element.FBE\n";
    filestream << "\n";
    filestream << "  defp int_attributes() do\n";

    checkAgregatedAttributes(&filestream, fbe);

    filestream << "    %{\n";

    iterateOverAttributes(&filestream, fbe);

    filestream << "    }\n";
    filestream << "  end\n";
    filestream << "\n";

    iterateOverMethods(&filestream, fbe);

    filestream << "\n";
    filestream << "  def initialize(name) do\n";
    filestream << "\n";
    filestream << "    this = NOP.Service.FBE.create_fbe(" << elixirNameofFBE(fbe) << ", name)\n";
    filestream << "\n";

    std::map<std::string, Rule*> *rules = fbe->getRules();
    for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {
        Rule *rule = it->second;
        filestream << "    NOP.Service.Rule.create_rule(" << elixirNameofRule(fbe, rule);
        filestream << ", \"" << elixirNameofRule(fbe, rule) << "\", [this])\n\n";
    }
	
    filestream << "    this\n";
    filestream << "  end\n";
    filestream << "\n";
    filestream << "end\n";
    filestream.flush();
    filestream.close();

    // Crete Rule Definition - one file per Rule
    iterateOverRules(fbe, path);
}

void CodeGenerationElixir::checkFBE(Fbe *fbe){
    
    std::stringstream ss;
    ss << MAIN_FOLDER << "/lib/" << fbe->getName();
    std::string path = ss.str();

    //Skip Main FBE Creation
    if(!strcmp(fbe->getName().c_str(),"Main"))
        return;

    //Check if FBE implemented: check if respective folder exists
    if(!directoryExists(path)){
        createFBE(fbe, path);
    }

}

void CodeGenerationElixir::generateCodeInstance(Instance *instance, int level) {

    Fbe *fbe = instance->getFbe();
    
    checkFBE(fbe);

    //Skip Main FBE Creation
    if(strcmp(instance->getName().c_str(),"this")){
        fileElixir << "    instances = Map.put(instances, :" <<  instance->getName() << ", " << elixirNameofFBE(fbe) << ".initialize(\"" << instance->getName() << "\"))\n\n";
    }

    level++;

    iterateOverInstances(instance, level);

}

void CodeGenerationElixir::iterateOverInstances(Instance *instance, int level) {

    std::map<std::string, Instance*> *instances = instance->getInstances();

    for (std::map<std::string, Instance*>::iterator it = instances->begin(); it != instances->end(); ++it) {

        Instance *instance = it->second;

        // TODO - GAMBI
        if (instance->getName() != "this") {

            generateCodeInstance(instance, level);

        }

    }

}

std::string CodeGenerationElixir::elixirNameofRule(Fbe *fbe, Rule *rule){
    std::stringstream s;
    s << MAIN_MODULE << "." <<  capitalize(fbe->getName()) << "." << capitalize(rule->getName()) ;
    return s.str();
}

std::string CodeGenerationElixir::elixirNameofPremise(Fbe *fbe, Rule *rule, Premise *premise){
    std::stringstream s;
    s << MAIN_MODULE << "." <<  capitalize(fbe->getName()) << "." << capitalize(rule->getName()) << "." << capitalize(premise->getName()) ;
    return s.str();    
}

void CodeGenerationElixir::generateCodePremise(std::fstream *filestream, Fbe *fbe, Rule *rule, Premise *premise){

    std::cout << "\nInterpreting Premise " << premise->getName() << " of rule " << fbe->getName() << "." << rule->getName() << " as an actor" << std::endl;

    *filestream << "    " << premise->getName() << " = NOP.Service.Premise.create_premise(";
    *filestream << "\"" << elixirNameofPremise(fbe, rule, premise) << "\"";

    Expression *expression = premise->getExpression();
    Factor *leftFactor = expression->getLeftFactor();

    if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

        ElementFactor *elementLeft = (ElementFactor*)leftFactor;

        *filestream << ", " << elementLeft->getInstanceName() << ", :" << elementLeft->getAttributeName() << ", ";

    }

    Symbol *symbol = expression->getSymbol();

    switch (symbol->getSymbolId())
    {
        case Symbol::EQUAL_SYMBOL:
            *filestream << ":EQ, ";
            break;

        case Symbol::NOT_EQUAL_SYMBOL:
            *filestream << ":NE, ";
            break;
    
        case Symbol::LESSER_THAN_SYMBOL:
            *filestream << ":LT, ";
            break;

        case Symbol::GREATER_THAN_SYMBOL:
            *filestream << ":GT, ";
            break;

        case Symbol::LESS_OR_EQUAL_SYMBOL:
            *filestream << ":LE, ";
            break;

        case Symbol::GREATER_OR_EQUAL_SYMBOL:
            *filestream << ":GE, ";
            break;

        default:
            *filestream << ":EQ, ";
            break;
    }

 
    Factor *rightFactor = expression->getRightFactor();

    if (rightFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

        ElementFactor *elementRight = (ElementFactor*)leftFactor;

        *filestream << elementRight->getInstance()->getName() << ", :" << elementRight->getAttribute()->getName() << ")\n";

    }else{
        *filestream << rightFactor->getStringValue() << ")\n";
    }

    //New line
    *filestream << "\n";

}

std::string CodeGenerationElixir::elixirNameofSubcondition(Fbe *fbe, Rule *rule, Subcondition *subcondition){
    std::stringstream s;
    s << MAIN_MODULE << "." <<  capitalize(fbe->getName()) << "." << capitalize(rule->getName()) << "." << capitalize(subcondition->getName()) ;
    return s.str(); 
}


void CodeGenerationElixir::generateCodeSubcondition(std::fstream *filestream, Fbe *fbe, Rule *rule, Subcondition *subcondition) {

    std::cout << "\nInterpreting Subcondition " << subcondition->getName() << " of rule " << fbe->getName() << "." << rule->getName() << " as an actor" << std::endl;

    std::map<std::string, Premise*> *premises = subcondition->getPremises();

    std::stringstream conditionStream;
    std::stringstream expressionStream;
    std::string conjuctionStr;
    bool first = true;

    Conjunction *conjunction = subcondition->getConjunction();

    switch (conjunction->getConjunctionId())
    {
        case Conjunction::NO_CONJUNCTION:
            conjuctionStr = " and not ";
            break;
    
        case Conjunction::AND_CONJUNCTION:
            conjuctionStr = " and ";
            break;
    
        case Conjunction::OR_CONJUNCTION:
            conjuctionStr = " or ";
            break;
    
        default:
            break;
    }

    for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

        Premise *premise = it->second;

        generateCodePremise(filestream, fbe, rule, premise);

        conditionStream << premise->getName() << ", ";

        if (first){
            expressionStream << elixirNameofPremise(fbe, rule, premise);
            first = false;
        }else{
            expressionStream << conjuctionStr << elixirNameofPremise(fbe, rule, premise);
        }

    }

    *filestream << "    " << subcondition->getName() << " = NOP.Service.Condition.create_condition(";
    *filestream << "\"" << elixirNameofSubcondition(fbe, rule, subcondition) << "\"";
    *filestream << ",  [" << conditionStream.str() << "],"; 
    *filestream << "\"( " << expressionStream.str() << " )\"";
    *filestream << ")\n";
    *filestream << "\n";

}

void CodeGenerationElixir::iterateOverCondition(std::fstream *filestream, Fbe *fbe, Rule *rule, Condition *condition){
    
    std::stringstream ruleStream;
    std::map<std::string, Subcondition*> *subconditions = condition->getSubconditions();

    for (std::map<std::string, Subcondition*>::iterator it = subconditions->begin(); it != subconditions->end(); ++it) {

        Subcondition *subcondition = it->second;

        generateCodeSubcondition(filestream, fbe, rule, subcondition);

        ruleStream << subcondition->getName() << ",";
    }

    std::map<std::string, Premise*> *premises = condition->getPremises();

    for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

        Premise *premise = it->second;

        generateCodePremise(filestream, fbe, rule, premise);

        ruleStream << premise->getName() << ",";
    }

    *filestream << "    [" << ruleStream.str() << "]\n";
}

void CodeGenerationElixir::generateCodeInstigation(std::fstream *filestream, Fbe *fbe, Rule *rule, Instigation *instigation){

    std::list<Call*> *calls = instigation->getCalls();

    for (std::list<Call*>::iterator it = calls->begin(); it != calls->end(); ++it) {

        Call *call = ((Call*)*it);

        // raising exception
        *filestream << "      {" << elixirNameofFBE(call->getInstance()->getFbe()) << ", :" << call->getMethodName() << ", [" << call->getInstanceName() << "]},\n";

        //*filestream << "      {" << elixirNameofFBE(fbe) << ", :" << call->getMethodName() << ", [" << "this" << "]},\n"; 

    }
}

void CodeGenerationElixir::iterateOverInstigations(std::fstream *filestream, Fbe *fbe, Rule *rule){

    Action *action = rule->getAction();

    std::map<std::string, Instigation*> *instigations = action->getInstigations();

    for (std::map<std::string, Instigation*>::iterator it = instigations->begin(); it != instigations->end(); ++it) {

        Instigation *instigation = it->second;

        generateCodeInstigation(filestream, fbe, rule, instigation);

    }
}

void CodeGenerationElixir::generateCodeRule(Fbe *fbe, std::string path, Rule *rule){

    std::cout << "\nInterpreting Rule " << rule->getName() << " of FBE " << fbe->getName() << " as an actor" << std::endl;
    // Create Rule File
    std::stringstream filenameStream;
    filenameStream << path << "/" << rule->getName() << ".ex";
    std:string filename = filenameStream.str();
    std::fstream filestream;

    filestream.open(filename, std::fstream::out | std::fstream::trunc);
    filestream << "defmodule "<< elixirNameofRule(fbe, rule) << " do\n";
    filestream << "  use NOP.Element.Rule\n";
    filestream << "\n";
    filestream << "  defp create_element_list([this]) do\n";
    filestream << "\n";

    Condition *condition = rule->getCondition();
    
    iterateOverCondition(&filestream, fbe, rule, condition);

    filestream << "  end\n";
    filestream << "\n";
    filestream << "  defp create_instigation_list([this]) do\n";
    filestream << "\n";
    filestream << "    [\n";

    iterateOverInstigations(&filestream, fbe, rule);

    filestream << "    ]\n";
    filestream << "\n";
    filestream << "  end\n";
    filestream << "\n";
    filestream << "end\n";
    filestream.flush();
    filestream.close();

}
void CodeGenerationElixir::iterateOverRules(Fbe *fbe, std::string path) {

    std::map<std::string, Rule*> *rules = fbe->getRules();

    for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {

        Rule *rule = it->second;

        generateCodeRule(fbe, path, rule);

    }

}

void CodeGenerationElixir::clearFbeList(){

    fbeList.clear();
}

void CodeGenerationElixir::addFbeToList(std::string name,  Fbe *fbe){


    if (fbeList.count(name) == 0) {
        fbeList.insert(std::pair<std::string, Fbe*>(name, fbe));
    }

}

std::map<std::string, Fbe*>* CodeGenerationElixir::getFbeList(){

    return &fbeList;
}

void CodeGenerationElixir::determineMainRuleFbeList(Fbe *fbe, Rule *rule){
    
    Condition *condition = rule->getCondition();

    std::map<std::string, Subcondition*> *subconditions = condition->getSubconditions();
    
    for (std::map<std::string, Subcondition*>::iterator it = subconditions->begin(); it != subconditions->end(); ++it) {

        Subcondition *subcondition = it->second;

        Conjunction *conjunction = subcondition->getConjunction();

        std::map<std::string, Premise*> *premises = subcondition->getPremises();

        for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {

            Premise *premise = it->second;

            Expression *expression = premise->getExpression();
            
            Factor *leftFactor = expression->getLeftFactor();

            if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

                ElementFactor *elementLeft = (ElementFactor*)leftFactor;

                //instance está como Entity - getFbe nao está funcionando
                //addFbeToList(elementLeft->getInstanceName(), elementLeft->getInstance()->getFbe());
                addFbeToList(elementLeft->getInstanceName(), nullptr);

            }

            Factor *rightFactor = expression->getRightFactor();

            if (rightFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

                ElementFactor *elementRight = (ElementFactor*)rightFactor;

                //instance está como Entity - getFbe nao está funcionando
                //addFbeToList(elementRight->getInstanceName(), elementRight->getInstance()->getFbe());
                addFbeToList(elementRight->getInstanceName(), nullptr);

            }

     
        }


    }

    std::map<std::string, Premise*> *premisesCond = condition->getPremises();

    for (std::map<std::string, Premise*>::iterator it = premisesCond->begin(); it != premisesCond->end(); ++it) {

        Premise *premise = it->second;

        Expression *expression = premise->getExpression();
        
        Factor *leftFactor = expression->getLeftFactor();

        if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

            ElementFactor *elementLeft = (ElementFactor*)leftFactor;


            //instance está como Entity - getFbe nao está funcionando
            //addFbeToList(elementLeft->getInstanceName(), instance->getFbe());
            addFbeToList(elementLeft->getInstanceName(), nullptr);

        }

        Factor *rightFactor = expression->getRightFactor();

        if (rightFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

            ElementFactor *elementRight = (ElementFactor*)rightFactor;

            //instance está como Entity - getFbe nao está funcionando
            //addFbeToList(elementRight->getInstanceName(), elementRight->getInstance()->getFbe());
            addFbeToList(elementRight->getInstanceName(), nullptr);

        }


    }

    Action *action = rule->getAction();

    std::map<std::string, Instigation*> *instigations = action->getInstigations();

    for (std::map<std::string, Instigation*>::iterator it = instigations->begin(); it != instigations->end(); ++it) {

        Instigation *instigation = it->second;

        std::list<Call*> *calls = instigation->getCalls();

        for (std::list<Call*>::iterator it = calls->begin(); it != calls->end(); ++it) {

            Call *call = ((Call*)*it);

            //instance está como Entity - getFbe nao está funcionando
            //addFbeToList(call->getInstanceName(), call->getInstance()->getFbe());
            addFbeToList(call->getInstanceName(), nullptr);

        }

    }

}




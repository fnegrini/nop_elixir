#include "generation/namespaces/NamespacesCompiler.h"

#include "NOPGraph.h"

#include "elements/Fbe.h"
#include "elements/Instance.h"
#include "elements/Attribute.h"
#include "elements/Method.h"
#include "elements/Premise.h"
#include "elements/Subcondition.h"
#include "elements/Condition.h"
#include "elements/Rule.h"
#include "elements/Action.h"
#include "elements/Instigation.h"
#include "elements/Expression.h"
#include "elements/Factor.h"
#include "elements/ElementFactor.h"
#include "elements/Symbol.h"
#include "elements/Type.h"
#include "elements/Param.h"
#include "elements/CodeBlock.h"
#include "elements/Attribution.h"
#include "elements/Target.h"
#include "elements/Call.h"
#include "elements/Conjunction.h"
#include "elements/Argument.h"
#include "elements/IncludeBlock.h"
#include "elements/MainBlock.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>

// Function that returns the type of the Attribute
std::string returnType(int attType) {
    std::string ret = "";
    switch (attType) {
	case Type::BOOLEAN_TYPE:
	    ret = "bool";
            break;
        case Type::INTEGER_TYPE:
            ret = "int";
            break;
        case Type::DOUBLE_TYPE:
            ret = "float";
            break;
        case Type::STRING_TYPE:
            ret = "std::string";
            break;
        case Type::CHAR_TYPE:
            ret = "char";
            break;
        default:
            ret = "undefined";
            break;
    }//end switch
    return ret;
}

// Function that returns the default type of the Attribute
std::string returnDefault(short attType) {
    std::string ret = "";
    switch (attType) {
        case Type::BOOLEAN_TYPE:
            ret = "false";
            break;
        case Type::INTEGER_TYPE:
            ret = "0";
            break;
        case Type::DOUBLE_TYPE:
            ret = "0.0";
            break;
        case Type::STRING_TYPE:
            ret = "\"\"";
            break;
        case Type::CHAR_TYPE:
            ret = "\" \"";
            break;
        default:
            ret = "null";
            break;
    }//end switch
    return ret;
}    

// Function that returns the operator
std::string returnOperation(short attOp) {
    std::string ret = "";
    switch (attOp) {
        case Symbol::EQUAL_SYMBOL:
            ret = " == ";
            break;
        case Symbol::NOT_EQUAL_SYMBOL:
            ret = " != ";
            break;
        case Symbol::LESSER_THAN_SYMBOL:
            ret = " < ";
            break;
        case Symbol::GREATER_THAN_SYMBOL:
            ret = " > ";
            break;
        case Symbol::LESS_OR_EQUAL_SYMBOL:
            ret = " <= ";
            break;
        case Symbol::GREATER_OR_EQUAL_SYMBOL:
            ret = " >= ";
            break;
        default:
            ret = "undefined";
            break;
    }//end switch
    return ret;
}

std::string NamespacesCompiler::getInstanceCompleteName(Instance* instance) {

    std::string cmpInstName = instance->getName();

    if (cmpInstName == "this") {
        
        cmpInstName = "main";

    } else {

        while (instance->getParentInstance() != 0) {

            if (instance->getParentInstance()->getName() == "this") {
                cmpInstName = "main::" + cmpInstName;
                break;
            }

            cmpInstName = instance->getParentInstance()->getName() + "::" + cmpInstName;

            instance = instance->getParentInstance();

        }

    }

    return cmpInstName;

}

void NamespacesCompiler::generateCode() {

    Instance *mainInstance = graph->getFbeMainInstance();

    std::string includeBlock = "";
    Fbe *fbe = mainInstance->getFbe();

    system("rmdir /Q /S Generated");
    system("mkdir Generated");

    std::map<std::string, IncludeBlock*> *includes = fbe->getIncludes();
    for (std::map<std::string, IncludeBlock*>::iterator it = includes->begin(); it != includes->end(); ++it) {
        
        IncludeBlock *include = it->second;

	if (include->getTarget()->getTargetId() == Target::NAMESPACES_TARGET) {

		includeBlock = include->getCode();

	}

    }

    generateCodeInstance(mainInstance, includeBlock);

    generateBody();
    generateBatch();
    
}

void NamespacesCompiler::generateCodeInstance(Instance *mainInstance, std::string includeBlock) {

    // Attributes
    generateAllAttributes(mainInstance, includeBlock);
    
    // Methods
    generateAllMethods(mainInstance, includeBlock);
    
    // Rules
    generateAllRules(mainInstance, includeBlock);
    
    // Premises
    generateAllPremises(mainInstance, includeBlock);

}

void NamespacesCompiler::generateAllAttributes(Instance *mainInstance, std::string includeBlock) {

    //creation instances.h file
    ofstream fileH;
    std::string filenameH = "Generated/instances.h";
    fileH.open(filenameH.c_str(), ios::out);
    fileH << "#pragma once" << std::endl;
    fileH << "#include <string>" << std::endl;
    fileH << "namespace instance {" << std::endl;

    //Creation of instances.cpp file
    ofstream fileCPP;
    std::string filenameCPP = "Generated/instances.cpp";
    fileCPP.open(filenameCPP.c_str(), ios::out);
    fileCPP << "#include \"instances.h\"" << std::endl;
    fileCPP << "#include \"premises.h\"" << std::endl;
    fileCPP << "#include <string>" << std::endl;
    fileCPP << "#include <iostream>" << std::endl;

    fileCPP << std::endl;
    fileCPP << "//#define _DEBUG_" << std::endl;
    fileCPP << std::endl;
    
    fileCPP << includeBlock << std::endl;

    fileCPP << "namespace instance {" << std::endl;

    generateCodeAttribute(fileCPP, fileH, mainInstance, 0);
    
    fileH << "}"<< std::endl;// end of at namespace
    fileH.close();
    fileCPP << "}"<< std::endl;// end of at namespace
    fileCPP.close();
}

void NamespacesCompiler::generateCodeAttribute(ofstream& fileCPP, ofstream& fileH, Instance *instance, int level) {

    std::string tab = getLevel(level);

    std::string instName = "";

    if (instance->getName() != "this") {
        instName = instance->getName();
    } else {
        instName = "main"; 
    }

    fileH << tab << "\tnamespace "<< instName <<" {" << std::endl;
    fileCPP << tab << "\tnamespace " << instName << " {" << std::endl;

    std::map<std::string, Attribute*> *attributes = instance->getAttributes();
    for (std::map<std::string, Attribute*>::iterator it = attributes->begin(); it != attributes->end(); ++it) {

        Attribute *attribute = it->second;

        fileH << tab << "\t\tnamespace at {" << std::endl;
        fileH << tab << "\t\t\tnamespace "<< attribute->getName()<<" {" << std::endl;
        fileH << tab << "\t\t\t\textern "<< returnType(attribute->getType()->getTypeId()) <<" value;" << std::endl;
        fileH << tab << "\t\t\t\textern void setValue("<< returnType(attribute->getType()->getTypeId()) <<" newValue);" << std::endl;
        fileH << tab << "\t\t\t}"<< std::endl;
        fileH << tab << "\t\t}"<< std::endl;

        fileCPP << tab << "\t\tnamespace at {" << std::endl;
        fileCPP << tab << "\t\t\tnamespace " << attribute->getName() << " {" << std::endl;
        fileCPP << tab << "\t\t\t\t"<< returnType(attribute->getType()->getTypeId()) <<" value = "<<attribute->getFactor()->getStringValue()<<";" << std::endl;
        fileCPP << tab << "\t\t\t\tvoid setValue("<< returnType(attribute->getType()->getTypeId()) <<" newValue) {" << std::endl;
        fileCPP << tab << "\t\t\t\t\tif (value != newValue) {" << std::endl;
        fileCPP << tab << "\t\t\t\t\t\tvalue = newValue;" << std::endl;

        std::map<std::string, Premise*> *premises = attribute->getPremises();
        for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {
            
            Premise *premise = it->second;
            
            std::string cmpInstName = getInstanceCompleteName(premise->getParentInstance());
            std::string cmp1st_att = "";

            Expression *expression = premise->getExpression();
            Factor *leftFactor = expression->getLeftFactor();

            if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

                ElementFactor *element = (ElementFactor*)leftFactor;
                cmp1st_att = element->getInstance()->getName();
                cmp1st_att = cmp1st_att + "_";
                cmp1st_att = cmp1st_att + element->getAttribute()->getName();
                
                //debug
                fileCPP << tab << "\t\t\t\t\t\t#ifdef _DEBUG_" << std::endl;
                fileCPP << tab << "\t\t\t\t\t\tstd::cout << \"premise::"<<cmpInstName<<"::"<<premise->getName()<<"::notify_"<<cmp1st_att<<"(newValue)\" << std::endl;"<< std::endl;
                fileCPP << tab << "\t\t\t\t\t\t#endif" << std::endl;
                fileCPP << tab << "\t\t\t\t\t\tpremise::"<<cmpInstName<<"::"<<premise->getName()<<"::notify_"<<cmp1st_att<<"(newValue);"<< std::endl;

            }

        }

        fileCPP << tab << "\t\t\t\t\t}" << std::endl;//end if
        fileCPP << tab << "\t\t\t\t}" << std::endl;// end setValue function
        fileCPP << tab << "\t\t\t}" << std::endl;// end of attribute
        fileCPP << tab << "\t\t}"<< std::endl;// end of at
        
    }

    level++;

    std::map<std::string, Instance*> *otherInsts = instance->getInstances();

    if (otherInsts->size() > 0) {

        // loop para serem implementadas as premises das instances
        for (std::map<std::string, Instance*>::iterator it = otherInsts->begin(); it != otherInsts->end(); ++it) {

            Instance *otherInst = it->second;      // otherInst -> corresponde a "instances das instances"
            
            if (otherInst->getName() != "this") {

                generateCodeAttribute(fileCPP, fileH, otherInst, level);

            }

        }

    }

    fileH << tab << "\t}"<< std::endl;// end of instance
    fileCPP << tab << "\t}"<< std::endl;// end of instance
    
}

void NamespacesCompiler::generateAllPremises(Instance *mainInstance, std::string includeBlock) {
	
    //creation premises.h file
    ofstream fileH;
    std::string filenameH = "Generated/premises.h";
    fileH.open(filenameH.c_str(), ios::out);
    fileH << "#pragma once" << std::endl;
    fileH << "#include <string>" << std::endl;
    fileH << "namespace premise {" << std::endl;

    //Creation of premises.cpp file
    ofstream fileCPP;
    std::string filenameCPP = "Generated/premises.cpp";
    fileCPP.open(filenameCPP.c_str(), ios::out);
    fileCPP << "#include \"premises.h\"" << std::endl;
    fileCPP << "#include \"rules.h\"" << std::endl;
    fileCPP << "#include <string>" << std::endl;
    fileCPP << "#include <iostream>" << std::endl;
    fileCPP << includeBlock << std::endl;

    fileCPP << std::endl;
    fileCPP << "//#define _DEBUG_" << std::endl;
    fileCPP << std::endl;

    fileCPP << "namespace premise {" << std::endl;

    // implementation of fbe main premises
    generateCodePremise(fileCPP, fileH, mainInstance, 0);
    
    fileCPP << "}"<<std::endl; // end of pr namespace
    fileCPP.close();
    
    fileH << "}"<<std::endl; // end of pr namespace
    fileH.close();

}

void NamespacesCompiler::generateCodePremise(ofstream& fileCPP, ofstream& fileH, Instance *instance, int level) {
	
    std::string tab = getLevel(level);

    int subConds = 0;
    std::string ruleName = "";
    std::string instName = "";

    if (instance->getName() != "this") {
        instName = instance->getName();
    } else {
        instName = "main"; 
    }
    
    std::map<std::string, Rule*> *rules = instance->getRules();
    std::map<std::string, Instance*> *otherInsts = instance->getInstances();

    if (rules->size() > 0 || otherInsts->size() > 0) {

        fileH << tab << "\tnamespace "<< instName <<" {" << std::endl;
        fileCPP << tab << "\tnamespace " << instName << " {" << std::endl;

        std::string cmpInstName = getInstanceCompleteName(instance);
        
        for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) {

            Rule *rule = it->second;

            subConds = 0;
            ruleName = rule->getName();

            Condition *condition = rule->getCondition();

            std::map<std::string, Subcondition*> *subConditions = condition->getSubconditions();

            if (subConditions->size() > 0) {

                for (std::map<std::string, Subcondition*>::iterator it = subConditions->begin(); it != subConditions->end(); ++it) {
                    
                    subConds++;

                    Subcondition *subCondition = it->second;

                    std::map<std::string, Premise*> *premises = subCondition->getPremises();
                    for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {
                    
                        Premise *premise = it->second;

                        generatePremise(fileCPP, fileH, premise, cmpInstName, ruleName, subConds, level);

                    }
                }    

            } else {

                std::map<std::string, Premise*> *premises = condition->getPremises();
                for (std::map<std::string, Premise*>::iterator it = premises->begin(); it != premises->end(); ++it) {
                    
                    Premise *premise = it->second;

                    generatePremise(fileCPP, fileH, premise, cmpInstName, ruleName, subConds, level);

                }

            }

        }

        level++;

        // loop para serem implementadas as premises das instances
        for (std::map<std::string, Instance*>::iterator it = otherInsts->begin(); it != otherInsts->end(); ++it) {

            Instance *otherInst = it->second;      // otherInst -> corresponde a "instances das instances"

            if (otherInst->getName() != "this") {

                generateCodePremise(fileCPP, fileH, otherInst, level);

            }

        }

        fileH << tab << "\t}" << std::endl;
        fileCPP << tab << "\t}" << std::endl;

    }

}

void NamespacesCompiler::generatePremise(ofstream& fileCPP, ofstream& fileH, Premise *premise, std::string instName, std::string ruleName, int subConds, int level) {
	
    std::string tab = getLevel(level);

    fileCPP << tab << "\t\tnamespace " << premise->getName() << " {" << std::endl;
    fileH << tab << "\t\tnamespace " << premise->getName() << " {" << std::endl;
	
    std::string methodCallInc = "";
    std::string methodCallDec = "";
    
	std::string leftValue = "";
    std::string rightValue = "";
    std::string cmpOp = "";

	std::string preLogicalValue = "";

    std::string cmpAttType = "";
    std::string cmp1st_att = "";
    std::string cmpInstName = "";
	
    Expression *expression = premise->getExpression();
    Factor *leftFactor = expression->getLeftFactor();
	
    if (leftFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

        ElementFactor *element = (ElementFactor*)leftFactor;

        cmp1st_att = element->getInstance()->getName(); 

        cmp1st_att = cmp1st_att + "_";
        cmp1st_att = cmp1st_att + element->getAttribute()->getName();
        cmpAttType = returnType(element->getAttribute()->getType()->getTypeId());

		leftValue = element->getAttribute()->getFactor()->getStringValue();

    } else {

		leftValue = leftFactor->getStringValue();

	}

	Factor *rightFactor = expression->getRightFactor();

	if (rightFactor->getFactorId() == Factor::ELEMENT_FACTOR) {

        ElementFactor *element = (ElementFactor*)rightFactor;

		rightValue = element->getAttribute()->getFactor()->getStringValue();

    } else {

		rightValue = rightFactor->getStringValue();

	}

    cmpOp = returnOperation(expression->getSymbol()->getSymbolId());

	preLogicalValue = (premise->getInitialLogicalValue() ? "true" : "false");

    // Construção da chamada do namespace das rules
    methodCallInc = methodCallInc + "rule::" + instName + "::" + ruleName + "::inc";
    methodCallDec = methodCallDec + "rule::" + instName + "::" + ruleName + "::dec";

    // quando (subConds != 0) significa que há subConditions: neste caso, para cada subCondition é criado um método inc() e dec()
    // enumerando esses métodos de acordo com o valor do contador subConds
    // quando (subConds = 0) significa que na estrutura do fbe, sas conditions segue direto para as premises... sem subcondition
    if (subConds != 0) {
        char buf[3];

        sprintf(buf, "%i", subConds);  // convertendo o valor de subConds para string

        methodCallInc = methodCallInc + buf;
        methodCallDec = methodCallDec + buf;  
    }

    methodCallInc = methodCallInc + "();";
    methodCallDec = methodCallDec + "();";
    
    fileH << tab << "\t\t\textern bool state;" << std::endl;
    fileH << tab << "\t\t\textern " << cmpAttType << " cpy1st, cpy2nd;" << std::endl;
    
    fileH << tab << "\t\t\textern void compare();" << std::endl;
    fileH << tab << "\t\t\textern void notify_"<<cmp1st_att<<"("<<cmpAttType<< " newValue);" << std::endl;

    fileCPP << tab << "\t\t\tbool state = (" << preLogicalValue << ");" << std::endl;
    
	fileCPP << tab << "\t\t\t" << cmpAttType << " cpy1st = " << leftValue << ";" << std::endl;
	fileCPP << tab << "\t\t\t" << cmpAttType << " cpy2nd = " << rightValue << ";" << std::endl;
    
    fileCPP << tab << "\t\t\tvoid compare() {"<< std::endl;
    fileCPP << tab << "\t\t\t\tif (cpy1st"<< cmpOp <<"cpy2nd) {"<< std::endl;
    fileCPP << tab << "\t\t\t\t\tif (state == false) {"<< std::endl;
    fileCPP << tab << "\t\t\t\t\t\tstate = true;"<< std::endl;
    fileCPP << tab << "\t\t\t\t\t#ifdef _DEBUG_" << std::endl;
    fileCPP << tab << "\t\t\t\t\t\tstd::cout << \""<< methodCallInc << "\" << std::endl;" << std::endl;
    fileCPP << tab << "\t\t\t\t\t#endif" << std::endl;
    fileCPP << tab << "\t\t\t\t\t\t"<< methodCallInc << std::endl;
    fileCPP << tab << "\t\t\t\t\t}" << std::endl;
    fileCPP << tab << "\t\t\t\t} else {" << std::endl;
    fileCPP << tab << "\t\t\t\t\tif (state == true) {"<< std::endl;
    fileCPP << tab << "\t\t\t\t\t\tstate = false;"<< std::endl;
    fileCPP << tab << "\t\t\t\t\t#ifdef _DEBUG_" << std::endl;
    fileCPP << tab << "\t\t\t\t\t\tstd::cout << \""<< methodCallDec << "\" << std::endl;" << std::endl;
    fileCPP << tab << "\t\t\t\t\t#endif" << std::endl;
    fileCPP << tab << "\t\t\t\t\t\t"<< methodCallDec << std::endl;
    fileCPP << tab << "\t\t\t\t\t}" << std::endl;
    fileCPP << tab << "\t\t\t\t}" << std::endl;
    fileCPP << tab << "\t\t\t}" << std::endl;
    fileCPP << tab << "\t\t\tvoid notify_"<<cmp1st_att<<"("<<cmpAttType<< " newValue) {" << std::endl;
    fileCPP << tab << "\t\t\t\tcpy1st = newValue;" << std::endl;
    fileCPP << tab << "\t\t\t\tcompare();" << std::endl;
    fileCPP << tab << "\t\t\t}" << std::endl;
    fileCPP << tab << "\t\t}" << std::endl;
    fileH << tab << "\t\t}" << std::endl;
}

void NamespacesCompiler::generateAllMethods(Instance *mainInstance, std::string includeBlock) {
	
    //creation methods.h file
    ofstream fileH;
    std::string filenameH = "Generated/methods.h";
    fileH.open(filenameH.c_str(), ios::out);
    fileH << "#pragma once" << std::endl;
    fileH << "#include <string>" << std::endl;
    fileH << "namespace method {" << std::endl;

    //Creation of methods.cpp file
    ofstream fileCPP;
    std::string filenameCPP = "Generated/methods.cpp";
    fileCPP.open(filenameCPP.c_str(), ios::out);
    fileCPP << "#include \"methods.h\"" << std::endl;
    fileCPP << "#include \"instances.h\"" << std::endl;
    fileCPP << "#include <string>" << std::endl;
    fileCPP << "#include <iostream>" << std::endl;
    fileCPP << includeBlock << std::endl;

    fileCPP << std::endl;
    fileCPP << "//#define _DEBUG_" << std::endl;
    fileCPP << std::endl;

    fileCPP << "namespace method {" << std::endl;

    fileH << "\tnamespace main {" << std::endl;
	fileCPP << "\tnamespace main {" << std::endl;

    // implementação dos method do fbe main
    generateCodeMethod(fileCPP, fileH, mainInstance, 0);

    fileCPP << "\t}"<< std::endl;// end of method
    fileH << "\t}"<< std::endl;// end of method

    fileH << "}"<< std::endl;// end of mt namespace
    fileH.close();
    fileCPP << "}"<< std::endl;// end of mt namespace
    fileCPP.close();

}

void NamespacesCompiler::generateCodeMethod(ofstream& fileCPP, ofstream& fileH, Instance *instance, int level) {
	
	std::string tab = getLevel(level);

    std::map<std::string, Method*> *methods = instance->getMethods();

    if (methods->size() > 0) {

		if (instance->getName() != "this") {
			fileH << tab << "\tnamespace " << instance->getName()<<" {" << std::endl;
			fileCPP << tab << "\tnamespace " << instance->getName() << " {" << std::endl;
		}/* else {
			fileH << tab << "\tnamespace main {" << std::endl;
			fileCPP << tab << "\tnamespace main {" << std::endl;
		}*/
		
        for (std::map<std::string, Method*>::iterator it = methods->begin(); it != methods->end(); ++it) {
            
            Method *method = it->second;

			fileH << tab << "\t\tnamespace "<< method->getName()<<" {" << std::endl;
			fileCPP << tab << "\t\tnamespace " << method->getName() << " {" << std::endl;
			
            std::string paramsCode = "";

            std::map<std::string, Param*> *params = method->getParams();
            for (std::map<std::string, Param*>::iterator it = params->begin(); it != params->end(); ++it) {

                Param *param = it->second;
                paramsCode = paramsCode + returnType(param->getType()->getTypeId()) + " " + param->getName();
                            
            } 

            std::map<std::string, CodeBlock*> *codeBlocks = method->getCodeBlocks();
            for (std::map<std::string, CodeBlock*>::iterator it = codeBlocks->begin(); it != codeBlocks->end(); ++it) {
                
                CodeBlock *codeBlock = it->second;
                
                fileH << tab << "\t\t\tvoid " << method->getName()<<"("<<paramsCode<<");" << std::endl;
                fileCPP << tab << "\t\t\tvoid " << method->getName()<<"("<<paramsCode<<") {" << std::endl;
                
                fileCPP << tab << "\t\t\t\t" << codeBlock->getCode() << std::endl;

                fileCPP << tab << "\t\t\t}" << std::endl;

            }

            std::map<std::string, Attribution*> *attributions = method->getAttributions();
            for (std::map<std::string, Attribution*>::iterator it = attributions->begin(); it != attributions->end(); ++it) {

                Attribution *attribution = it->second;
                Factor *factor = attribution->getFactor();
                
                ElementFactor *element =  attribution->getElement();

                Attribute *att = element->getAttribute();

                std::string typeName = returnType(att->getType()->getTypeId());

                std::string cmpInstName = getInstanceCompleteName(att->getParentInstance());

                fileCPP << tab << "\t\t\tvoid " << method->getName() << "() {" << std::endl;

                fileCPP << tab << "\t\t\t\t#ifdef _DEBUG_" << std::endl;
                fileCPP << tab << "\t\t\t\tstd::cout << \"" << method->getName() << "\" << std::endl;" << std::endl;
                fileCPP << tab << "\t\t\t\t#endif" << std::endl;
                fileCPP << tab << "\t\t\t\tinstance::" << cmpInstName << "::at::" << element->getAttributeName() << "::setValue(" << factor->getStringValue() << ");" << std::endl;
                fileCPP << tab << "\t\t\t}" << std::endl;

                fileH << tab << "\t\t\textern void " << method->getName() << "();" << std::endl;
                
            }

            fileCPP << tab << "\t\t}"<< std::endl;// end of at namespace
            fileH << tab << "\t\t}"<< std::endl;

        }
        
        level++;

        // loop para serem implementadas os methods das instances
        std::map<std::string, Instance*> *otherInsts = instance->getInstances();
        for (std::map<std::string, Instance*>::iterator it = otherInsts->begin(); it != otherInsts->end(); ++it) {

            Instance *otherInst = it->second;      // otherInst -> corresponde a "instances das instances"
            if (otherInst->getName() != "this") {

                generateCodeMethod(fileCPP, fileH, otherInst, level);

            }

        }

        if (instance->getName() != "this") {
			fileCPP << tab << "\t}"<< std::endl;// end of method
            fileH << tab << "\t}"<< std::endl;// end of method
		}

    }else{
        // loop para serem implementadas os methods das instances
        std::map<std::string, Instance*> *otherInsts = instance->getInstances();
        for (std::map<std::string, Instance*>::iterator it = otherInsts->begin(); it != otherInsts->end(); ++it) {

            Instance *otherInst = it->second;      // otherInst -> corresponde a "instances das instances"
            if (otherInst->getName() != "this") {

                generateCodeMethod(fileCPP, fileH, otherInst, 1);

            }

        }
    }

}

void NamespacesCompiler::generateAllRules(Instance *mainInstance, std::string includeBlock) {
    
    //creation rules.h file
    ofstream fileH;
    std::string filenameH = "Generated/rules.h";
    fileH.open(filenameH.c_str(), ios::out);
    fileH << "#pragma once" << std::endl;
    fileH << "#include <string>" << std::endl;
    fileH << "namespace rule {" << std::endl;

    //Creation of rules.cpp file
    ofstream fileCPP;
    std::string filenameCPP = "Generated/rules.cpp";
    fileCPP.open(filenameCPP.c_str(), ios::out);
    fileCPP << "#include \"rules.h\"" << std::endl;
    fileCPP << "#include \"methods.h\"" << std::endl;
    fileCPP << "#include \"instances.h\"" << std::endl;
    fileCPP << "#include <string>" << std::endl;
    fileCPP << "#include <iostream>" << std::endl; 
    fileCPP << std::endl;
    fileCPP << "//#define _DEBUG_" << std::endl;
    fileCPP << std::endl;
       
    fileCPP << includeBlock << std::endl;
    fileCPP << "namespace rule {" << std::endl;

    fileH << "\tnamespace main {" << std::endl;
    fileCPP << "\tnamespace main {" << std::endl;

    // implementação das rules do fbe main
    generateCodeRule(fileCPP, fileH, mainInstance, 0);

    fileH << "\t}" << std::endl;
    fileCPP << "\t}" << std::endl;

    fileH << "}"<<std::endl;// end of rl namespace
    fileH.close();
    fileCPP << "}"<<std::endl;// end of rl namespace
    fileCPP.close();

}

void NamespacesCompiler::generateCodeRule(ofstream& fileCPP, ofstream& fileH, Instance *instance, int level) {

    std::string tab = getLevel(level);

    std::map<std::string, Rule*> *rules = instance->getRules();

    if (rules->size() > 0) {

        if (instance->getName() != "this") {
            fileH << tab << "\tnamespace "<< instance->getName()<<" {" << std::endl;
            fileCPP << tab << "\tnamespace " << instance->getName() << " {" << std::endl;
        }
        /*else{
            fileH << tab << "\tnamespace main {" << std::endl;
            fileCPP << tab << "\tnamespace main {" << std::endl;
        }*/

        for (std::map<std::string, Rule*>::iterator it = rules->begin(); it != rules->end(); ++it) { 

            Rule *rule = it->second;

            fileH << tab << "\t\tnamespace " << rule->getName() << " {" << std::endl;
            fileCPP << tab << "\t\tnamespace " << rule->getName() << " {" << std::endl;

            Condition *condition = rule->getCondition();
                
            std::map<std::string, Subcondition*> *subconditions = condition->getSubconditions();
            if (subconditions->size() > 0) {

                int i = 0;
                std::vector<std::string> list_exprConjunction;
                std::vector<int> list_numPremises;

                // declaração das variaveis para cada subcondition
                for (std::map<std::string, Subcondition*>::iterator it = subconditions->begin(); it != subconditions->end(); ++it) {

					Subcondition *subcondition = it->second;

                    i++;

                    fileH << tab << "\t\t\textern int count" << i << ";" << std::endl;
                    fileH << tab << "\t\t\textern bool status" << i << ";" << std::endl;
                    fileCPP << tab << "\t\t\tint count" << i << " = " << subcondition->getNumberApprovedPremises() << ";" << std::endl;
                    fileCPP << tab << "\t\t\tbool status" << i << ";" << std::endl;

                    Conjunction* conjunction = subcondition->getConjunction();
                    std::string conjunctionName = conjunction->getConjunctionName(); 

                    std::map<std::string, Premise*> *premises = subcondition->getPremises();
                    if (conjunctionName == "AND") {
                        list_exprConjunction.push_back("==");
                        list_numPremises.push_back(premises->size());
                    } else {
                        list_exprConjunction.push_back(">=");
                        list_numPremises.push_back(1);
                    } 
                }

                //implementação dos métodos para cada subcondition
                for (int it = 1; it <= subconditions->size(); ++it) {
                    
                    fileH << tab << "\t\t\textern void inc" << it << "();" << std::endl;
                    fileH << tab << "\t\t\textern void dec" << it << "();" << std::endl;                
                    
                    fileCPP << tab << "\t\t\tvoid inc" << it << "() {" << std::endl;
                    fileCPP << tab << "\t\t\t\tcount" << it << "++;" << std::endl;
                    fileCPP << tab << "\t\t\t\t#ifdef _DEBUG_" << std::endl;
                    fileCPP << tab << "\t\t\t\tstd::cout << count" << it << " << std::endl;" << std::endl;
                    fileCPP << tab << "\t\t\t\t#endif" << std::endl;
                    fileCPP << tab << "\t\t\t\tstatus" << it << " = false;" << std::endl;
                    fileCPP << tab << "\t\t\t\tif (count" << it << " " << list_exprConjunction[it-1] << " " <<list_numPremises[it-1]<<") {" << std::endl;
                    fileCPP << tab << "\t\t\t\t\tstatus" << it << " = true;" << std::endl;
                    fileCPP << tab << "\t\t\t\t\tcompareStatusSubConditions();" << std::endl;
                    fileCPP << tab << "\t\t\t\t}"<< std::endl;
                    fileCPP << tab << "\t\t\t}" << std::endl;
                    fileCPP << tab << "\t\t\tvoid dec" << it << "() {" << std::endl;
                    fileCPP << tab << "\t\t\t\tcount" << it << "--;" << std::endl;
                    fileCPP << tab << "\t\t\t\t#ifdef _DEBUG_" << std::endl;
                    fileCPP << tab << "\t\t\t\tstd::cout << count" << it << " << std::endl;" << std::endl;
                    fileCPP << tab << "\t\t\t\t#endif" << std::endl;
                    fileCPP << tab << "\t\t\t}" << std::endl;               

                }

                fileH << tab << "\t\t\textern void compareStatusSubConditions();" << std::endl;
                fileCPP << tab << "\t\t\tvoid compareStatusSubConditions() {" << std::endl; 
                fileCPP << tab << "\t\t\t\tif (";

                int conjunctionCondId = condition->getConjunction()->getConjunctionId();
                std::string conjunctionCondValue = "";

                if (conjunctionCondId == Conjunction::AND_CONJUNCTION) {
                    conjunctionCondValue = " && ";
                } else if (conjunctionCondId == Conjunction::OR_CONJUNCTION) {
                    conjunctionCondValue = " || ";
                }

                for (int it = 1; it <= subconditions->size(); ++it) {
                    fileCPP << "(status" << it << " == true)";   
                    if (it != subconditions->size()) {
                        fileCPP << conjunctionCondValue; 
                    } else {
                        fileCPP << ") {" << std::endl;    
                    }
                }

                // Instigations
                generateInstigation(fileCPP, fileH, instance, rule, level);

                fileCPP << tab << "\t\t\t\t}" << std::endl;
                fileCPP << tab << "\t\t\t}" << std::endl;
                fileCPP << tab << "\t\t}" << std::endl;
                fileH << tab << "\t\t}" << std::endl;

            } else {     // não há subconditions

                //Premises
                std::map<std::string, Premise*> *premises = condition->getPremises();

                int numPremises = 0;
                std::string expressionConjunction = "";
                std::string subConditionName = "";

                Conjunction* conjunction = condition->getConjunction();
                std::string conjunctionName = conjunction->getConjunctionName();
                
                if (conjunctionName == "AND") {
                    expressionConjunction = "==";
                    numPremises = premises->size();
                } else {
                    expressionConjunction = ">=";
                    numPremises = 1;
                } 
                
                // declaração da variável
                fileH << tab << "\t\t\textern int count;" << std::endl;
                fileCPP << tab << "\t\t\tint count = " << condition->getNumberApprovedPremises() << ";" << std::endl;

                // implementação dos métodos
                fileH << tab << "\t\t\textern void inc();" << std::endl;
                fileH << tab << "\t\t\textern void dec();" << std::endl;
                fileCPP << tab << "\t\t\tvoid inc() {" << std::endl;
                fileCPP << tab << "\t\t\t\tcount++;" << std::endl;
                fileCPP << tab << "\t\t\t\t#ifdef _DEBUG_" << std::endl;
                fileCPP << tab << "\t\t\t\tstd::cout << count << std::endl;" << std::endl;
                fileCPP << tab << "\t\t\t\t#endif" << std::endl;
                fileCPP << tab << "\t\t\t\tif (count "<< expressionConjunction << " " <<numPremises<<") {" << std::endl;

                //instigations            
                generateInstigation(fileCPP, fileH, instance, rule, level);        

                fileCPP << tab << "\t\t\t\t}" << std::endl;
                fileCPP << tab << "\t\t\t}" << std::endl;
                fileCPP << tab << "\t\t\tvoid dec() {" << std::endl;
                fileCPP << tab << "\t\t\t\tcount--;" << std::endl;
                fileCPP << tab << "\t\t\t\t#ifdef _DEBUG_" << std::endl;
                fileCPP << tab << "\t\t\t\tstd::cout << count << std::endl;" << std::endl;
                fileCPP << tab << "\t\t\t\t#endif" << std::endl;
                fileCPP << tab << "\t\t\t}" << std::endl;
                fileCPP << tab << "\t\t}" << std::endl;
                fileH << tab << "\t\t}" << std::endl;

            }
            
        }

        level++;

        // loop para serem implementadas os methods das instances
        std::map<std::string, Instance*> *otherInsts = instance->getInstances();
        for (std::map<std::string, Instance*>::iterator it = otherInsts->begin(); it != otherInsts->end(); ++it) {

            Instance *otherInst = it->second;      // otherInst -> corresponde a "instances das instances"
            if (otherInst->getName() != "this") {

                generateCodeRule(fileCPP, fileH, otherInst, level);

            }

        }

        if (instance->getName() != "this") {
            fileH << tab << "\t}" << std::endl;
            fileCPP << tab << "\t}" << std::endl;
        }
        
    }else{
        // loop para serem implementadas os methods das instances
        std::map<std::string, Instance*> *otherInsts = instance->getInstances();
        for (std::map<std::string, Instance*>::iterator it = otherInsts->begin(); it != otherInsts->end(); ++it) {

            Instance *otherInst = it->second;      // otherInst -> corresponde a "instances das instances"
            if (otherInst->getName() != "this") {

                generateCodeRule(fileCPP, fileH, otherInst, 1);

            }

        }
    }

}

void NamespacesCompiler::generateInstigation(ofstream& fileCPP, ofstream& fileH, Instance *instance, Rule *rule, int level) {

    std::string tab = getLevel(level);

    Action *action = rule->getAction(); 

    std::map<std::string, Instigation*> *instigations = action->getInstigations();
    for (std::map<std::string, Instigation*>::iterator it = instigations->begin(); it != instigations->end(); ++it) {      
        
        Instigation *instigation = it->second;
		
		std::list<Call*> *calls = instigation->getCalls();
		for (std::list<Call*>::iterator it = calls->begin(); it != calls->end(); ++it) {

			Call *call = (*it);

			Method *method = call->getMethod();

            std::string cmpInstName = getInstanceCompleteName(method->getParentInstance());

            std::list<Argument*> *arguments = call->getArguments();
			
            if (arguments->size() > 0) {

                for (std::list<Argument*>::iterator it = arguments->begin(); it != arguments->end(); ++it) {

					Argument *argument = (*it);

                    Factor *factor = argument->getFactor();
                    
                    fileCPP << tab << "\t\t\t\t\t#ifdef _DEBUG_" << std::endl;
					fileCPP << tab << "\t\t\t\t\tstd::cout << \"method::" << cmpInstName << "::" << method->getName() << "::" << method->getName() << ")\" << std::endl;" << std::endl;
                    fileCPP << tab << "\t\t\t\t\t#endif" << std::endl;
                    fileCPP << tab << "\t\t\t\t\tmethod::" << cmpInstName << "::" << method->getName() << "::" << method->getName() << "(" << factor->getStringValue() << ");" << std::endl;

                }

            } else {
                
                fileCPP << tab << "\t\t\t\t\t#ifdef _DEBUG_" << std::endl;
                fileCPP << tab << "\t\t\t\t\tstd::cout << \"method::" << cmpInstName << "::" << method->getName() << "::" << method->getName() << "()\" << std::endl;" << std::endl;
                fileCPP << tab << "\t\t\t\t\t#endif" << std::endl;
                fileCPP << tab << "\t\t\t\t\tmethod::" << cmpInstName << "::" << method->getName() << "::" << method->getName() << "();" << std::endl;

            }            
        }
    }
}

void NamespacesCompiler::generateBatch() {

    ofstream file;
    std::string filename = "Generated/Build.bat";
    file.open(filename.c_str(), ios::out);
    file << "@echo off" << std::endl;
    file << "" << std::endl;
    file << "echo \"Compiling Generated compiler...\"" << std::endl;
    file << "g++ -std=c++11 -Wno-deprecated -Wno-deprecated-register -I./ ./*.cpp -ggdb -o Generated.exe > log.txt 2>&1" << std::endl;
    file << "" << std::endl;
    file << "" << std::endl;
    file << "if exist Generated.exe (" << std::endl;
    file << "   echo \"Generated has compiled with success\"" << std::endl;
    file << ") else (" << std::endl;
    file << "    echo \"Generated has not been compiled =(\"" << std::endl;
    file << ")" << std::endl;
    file << "" << std::endl;
    file << "pause" << std::endl;

    file.close();

}

void NamespacesCompiler::generateBody() {

    ofstream file;
    std::string filename = "Generated/Main.cpp";
    file.open(filename.c_str(), ios::out);
    file << "#include <stdio.h>" << std::endl;
    file << "#include <sys/time.h>" << std::endl;
    file << "#include <iostream>" << std::endl;
    file << "#include \"premises.h\"" << std::endl;
    file << "#include \"instances.h\"" << std::endl;

    file << std::endl;
    file << "//#define _DEBUG_" << std::endl;
    file << std::endl;
    file << "using namespace std;" << std::endl;
    file << std::endl;

    file << "int main() {" << std::endl;
	
    /*file << "\ttimeval time;" << std::endl;
    file << "\tdouble initial;" << std::endl;
    file << "\tdouble final;" << std::endl;
    file << "\tgettimeofday(&time,0);" << std::endl;
    file << "\tinitial = (time.tv_sec * 1000.0) + (time.tv_usec / 1000.0);" << std::endl;*/

    file << "\tstd::cout << \"Inicio\" << endl;" << std::endl;

    file << std::endl;

	if (graph->getMainBlock() != 0) {

		std::map<std::string, Attribution*> *attributions = graph->getMainBlock()->getAttributions();
		for (std::map<std::string, Attribution*>::iterator it = attributions->begin(); it != attributions->end(); ++it) {

			Attribution *attribution = it->second;
			Factor *factor = attribution->getFactor();
			
			ElementFactor *element =  attribution->getElement();
			
			Attribute *att = element->getAttribute();
			
			Instance *instance = att->getParentInstance();
			
			std::string typeName = returnType(att->getType()->getTypeId());

			std::string cmpInstName = getInstanceCompleteName(instance);

			file << "\tinstance::" << cmpInstName << "::at::" << att->getName() << "::setValue(" << factor->getStringValue() << ");" << std::endl;

		}

	}
	
    file << std::endl;
    
    /* CODIGO DE TESTES PARA CTA
    file << "\tint count = 2;" << std::endl;
    file << "\tfor(int i = 0;i<2000;i++){" << std::endl;
    file << "\t\tif(count == 90)" << std::endl;
    file << "\t\t\tcount = 0;" << std::endl;
    file << "\t\telse" << std::endl;
    file << "\t\t\tcount++;" << std::endl;
	file << "\t}" << std::endl;
    file << "}" << std::endl;
    file << "\tgettimeofday(&time,0);" << std::endl;
    file << "\tfinal = (time.tv_sec * 1000.0) + (time.tv_usec / 1000.0);" << std::endl;
    file << "\tdouble resultado = final - initial;" << std::endl;
    file << "\tcout << resultado << endl;" << std::endl;
    */

    file << "\treturn 0;" << std::endl;
    file << std::endl;
    file << "\tstd::cout << \"Fim\" << endl;" << std::endl;    
    file << "}" << std::endl;

    file.close();
    
}

void NamespacesCompiler::assemble() {
    generateCode();
}

#include "elements/IntegerFactor.h"

#include <iostream>

IntegerFactor::IntegerFactor(int value) : Factor(Factor::INTEGER_FACTOR) {

    this->value = value;

}

IntegerFactor::~IntegerFactor() {

    //

}

int IntegerFactor::getValue() {

    return this->value;

}

std::string IntegerFactor::getStringValue() {

    return std::to_string(this->value);

}

Factor* IntegerFactor::clone() {

    return new IntegerFactor(this->value);

}

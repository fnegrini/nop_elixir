#include "elements/Instance.h"

#include "elements/Visibility.h"
#include "elements/Fbe.h"
#include "elements/Instance.h"
#include "elements/Attribute.h"
#include "elements/Method.h"

#include "elements/Rule.h"

#include <iostream>

Instance::Instance(Instance *instance, Visibility *visibility, std::string fbeName, std::string name)
	: Entity(instance, name) {

    this->visibility = visibility;

    this->fbe = 0;

    this->fbeName = fbeName;

}

Instance::~Instance() {

    std::cout << "~Instance()" << std::endl;

}

void Instance::print() {

    std::cout << "\t# Instance: ";

    this->visibility->print();

    std::cout << " ";

    std::cout << this->getName();

    std::cout << " of ";

    std::cout << this->fbeName;

    std::cout << std::endl;

}

Visibility* Instance::getVisibility() {

    return this->visibility;

}

void Instance::setFbe(Fbe *fbe) {

    this->fbe = fbe;

}

Fbe* Instance::getFbe() {

    return this->fbe;

}

std::string Instance::getFbeName() {

    return this->fbeName;

}

Instance* Instance::getInstance(std::string name) {

    if (this->instances->count(name) > 0) {

        std::map<std::string, Instance*>::iterator it = this->instances->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

Attribute* Instance::getAttribute(std::string name) {

    if (this->attributes->count(name) > 0) {

        std::map<std::string, Attribute*>::iterator it = this->attributes->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

Method* Instance::getMethod(std::string name) {

    if (this->methods->count(name) > 0) {

        std::map<std::string, Method*>::iterator it = this->methods->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

Rule* Instance::getRule(std::string name) {

    if (this->rules->count(name) > 0) {

        std::map<std::string, Rule*>::iterator it = this->rules->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

void Instance::instantiateElements() {

	std::cout << "instantiateElements" << std::endl;

    this->instances = new std::map<std::string, Instance*>();

	for (std::map<std::string, Instance*>::iterator it = this->fbe->getInstances()->begin(); it != this->fbe->getInstances()->end(); ++it) {

        Instance *instance = it->second;

        Instance *instanceClone = instance->clone(this);

        this->instances->insert(std::pair<std::string, Instance*>(instanceClone->getName(), instanceClone));

    }

	std::cout << "attributes" << std::endl;
	
    //////////////////////////////////////////////////////////////////////

    this->attributes = new std::map<std::string, Attribute*>();

    for (std::map<std::string, Attribute*>::iterator it = this->fbe->getAttributes()->begin(); it != this->fbe->getAttributes()->end(); ++it) {

        Attribute *attribute = it->second;

        Attribute *attributeClone = attribute->clone(this);

        this->attributes->insert(std::pair<std::string, Attribute*>(attributeClone->getName(), attributeClone));

    }

	std::cout << "methods" << std::endl;

    //////////////////////////////////////////////////////////////////////
    
    this->methods = new std::map<std::string, Method*>();

    for (std::map<std::string, Method*>::iterator it = this->fbe->getMethods()->begin(); it != this->fbe->getMethods()->end(); ++it) {

        Method *method = it->second;

        Method *methodClone = method->clone(this);
	
        this->methods->insert(std::pair<std::string, Method*>(methodClone->getName(), methodClone));

    }

	std::cout << "rules" << std::endl;

    //////////////////////////////////////////////////////////////////////

    this->rules = new std::map<std::string, Rule*>();

    for (std::map<std::string, Rule*>::iterator it = this->fbe->getRules()->begin(); it != this->fbe->getRules()->end(); ++it) {

        Rule *rule = it->second;

        Rule *ruleClone = rule->clone(this);

        this->rules->insert(std::pair<std::string, Rule*>(ruleClone->getName(), ruleClone));

    }

}

std::map<std::string, Instance*>* Instance::getInstances() {

    return this->instances;

}

std::map<std::string, Attribute*>* Instance::getAttributes() {

    return this->attributes;

}

std::map<std::string, Method*>* Instance::getMethods() {

    return this->methods;

}

std::map<std::string, Rule*>* Instance::getRules() {

    return this->rules;

}

Instance* Instance::clone(Instance *instance) {

    return new Instance(instance, this->getVisibility(), this->getFbeName(), this->getName());

}

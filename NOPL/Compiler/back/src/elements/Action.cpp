#include "elements/Action.h"

#include "elements/Execution.h"
#include "elements/Instigation.h"
#include "elements/Rule.h"
#include "elements/Instance.h"

#include <iostream>

Action::Action(Instance *instance, std::string name, Execution *execution, std::map<std::string, Instigation*> *instigations = 0)
    : Entity(instance, name) {

    this->execution = execution;

    this->instigations = instigations;

    this->rule = 0;

}

Action::~Action() {

    std::cout << "~Action()" << std::endl;

}

void Action::print() {

    std::cout << "\t# Action ";
    
    std::cout << this->getName();

    std::cout << " [";

    this->execution->print();

    std::cout << "]:\n";

    if (this->instigations != 0 && this->instigations->size() > 0) {

        for (std::map<std::string, Instigation*>::iterator it = this->instigations->begin(); it != this->instigations->end(); ++it) {

            ((Instigation*)it->second)->print();

        }

    }

    std::cout << std::endl;

}

Execution* Action::getExecution() {

    return this->execution;

}

std::map<std::string, Instigation*>* Action::getInstigations() {

    return this->instigations;

}

void Action::setRule(Rule *rule) {

    this->rule = rule;

}

Rule* Action::getRule() {

    return this->rule;

}

Action* Action::clone(Instance *instance) {

    std::map<std::string, Instigation*> *instigations = new std::map<std::string, Instigation*>();

    for (std::map<std::string, Instigation*>::iterator it = this->getInstigations()->begin(); it != this->getInstigations()->end(); ++it) {

        Instigation *instigation = it->second;

        Instigation *instigationClone = instigation->clone(instance);

	instigationClone->setAction(this);

        instigations->insert(std::pair<std::string, Instigation*>(instigationClone->getName(), instigationClone));

    }

    return new Action(instance, this->getName(), this->execution, instigations);

}

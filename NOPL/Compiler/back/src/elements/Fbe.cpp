#include "elements/Fbe.h"

#include "elements/IncludeBlock.h"
#include "elements/Property.h"
#include "elements/Instance.h"
#include "elements/Attribute.h"
#include "elements/Method.h"
#include "elements/Rule.h"

#include "elements/Visibility.h"

#include <iostream>

Fbe::Fbe(std::string name,
         std::map<std::string, IncludeBlock*> *includes = 0,
         std::map<std::string, Property*> *properties = 0,
         std::map<std::string, Instance*> *instances = 0,
         std::map<std::string, Attribute*> *attributes = 0,
         std::map<std::string, Method*> *methods = 0,
         std::map<std::string, Rule*> *rules = 0) {

    this->name = name;

    this->includes = includes;
    
    this->properties = properties;

    this->instances = instances;

    this->attributes = attributes;
    
    this->methods = methods;

    this->rules = rules;
    
	//TODO???
	if (name == "Main") {

		Instance *instance = new Instance(0, new Visibility(Visibility::PRIVATE_VISIBILITY), this->name, "this");
		instance->setFbe(this);
		
		this->instances->insert(std::pair<std::string, Instance*>("this", instance));

	}

    print();

}

Fbe::~Fbe() {

    std::cout << "~Fbe()" << std::endl;

}

void Fbe::print() {

    std::cout << "\n\n* Fbe ";
    
    std::cout << this->getName();

    std::cout << " {\n";

    if (this->includes != 0 && this->includes->size() > 0) {

        std::cout << "\n\t> Include blocks:\n\n";

        for (std::map<std::string, IncludeBlock*>::iterator it = this->includes->begin(); it != this->includes->end(); ++it) {

            ((IncludeBlock*)it->second)->print();

        }

    }

    if (this->properties != 0 && this->properties->size() > 0) {

        std::cout << "\n\t> Properties:\n\n";

        for (std::map<std::string, Property*>::iterator it = this->properties->begin(); it != this->properties->end(); ++it) {

            ((Property*)it->second)->print();

        }

    }

    if (this->instances != 0 && this->instances->size() > 0) {

        std::cout << "\n\t> Instances:\n\n";

        for (std::map<std::string, Instance*>::iterator it = this->instances->begin(); it != this->instances->end(); ++it) {

            ((Instance*)it->second)->print();

        }

    }

    if (this->attributes != 0 && this->attributes->size() > 0) {

        std::cout << "\n\t> Attributes:\n\n";

        for (std::map<std::string, Attribute*>::iterator it = this->attributes->begin(); it != this->attributes->end(); ++it) {

            ((Attribute*)it->second)->print();

        }

    }

    if (this->methods != 0 && this->methods->size() > 0) {

        std::cout << "\n\t> Methods:\n\n";

        for (std::map<std::string, Method*>::iterator it = this->methods->begin(); it != this->methods->end(); ++it) {

            ((Method*)it->second)->print();

        }

    }

    if (this->rules != 0 && this->rules->size() > 0) {

        std::cout << "\n\t> Rules:\n\n";

        for (std::map<std::string, Rule*>::iterator it = this->rules->begin(); it != this->rules->end(); ++it) {

            ((Rule*)it->second)->print();

        }

    }

    std::cout << "}" << std::endl;

}

std::string Fbe::getName() {

    return this->name;

}

std::map<std::string, IncludeBlock*>* Fbe::getIncludes() {

    return this->includes;

}

std::map<std::string, Instance*>* Fbe::getInstances() {

    return this->instances;

}

std::map<std::string, Attribute*>* Fbe::getAttributes() {

    return this->attributes;

}

std::map<std::string, Method*>* Fbe::getMethods() {

    return this->methods;

}

std::map<std::string, Rule*>* Fbe::getRules() {

    return this->rules;

}

Instance* Fbe::getInstance(std::string name) {

    if (this->instances->count(name) > 0) {

        std::map<std::string, Instance*>::iterator it = this->instances->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

Attribute* Fbe::getAttribute(std::string name) {

    if (this->attributes->count(name) > 0) {

        std::map<std::string, Attribute*>::iterator it = this->attributes->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

Method* Fbe::getMethod(std::string name) {

    if (this->methods->count(name) > 0) {

        std::map<std::string, Method*>::iterator it = this->methods->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

Rule* Fbe::getRule(std::string name) {

    if (this->rules->count(name) > 0) {

        std::map<std::string, Rule*>::iterator it = this->rules->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

#include "elements/DoubleFactor.h"

#include <iostream>

DoubleFactor::DoubleFactor(double value) : Factor(Factor::DOUBLE_FACTOR) {

    this->value = value;

}

DoubleFactor::~DoubleFactor() {

    //

}

double DoubleFactor::getValue() {

    return this->value;

}

std::string DoubleFactor::getStringValue() {

    return std::to_string(this->value);

}

Factor* DoubleFactor::clone() {

    return new DoubleFactor(this->value);

}

#include "elements/Call.h"

#include "elements/Instance.h"
#include "elements/Method.h"
#include "elements/Argument.h"

#include <iostream>

Call::Call(std::string instanceName, std::string methodName, std::list<Argument*> *arguments = 0) {

    this->instanceName = instanceName;

    this->methodName = methodName;

    this->arguments = arguments;

}

Call::~Call() {

    //

}

void Call::print() {

    std::cout << "\t\t# Call [";
    
    std::cout << this->getStringValue();

    std::cout << "]:\n";

    if (this->arguments != 0 && this->arguments->size() > 0) {

        std::cout << "\t\t  arguments:\n";

        for (std::list<Argument*>::iterator it = this->arguments->begin(); it != this->arguments->end(); ++it) {

            ((Argument*)*it)->print();

        }

    }

}

void Call::setInstance(Instance *instance) {

    this->instance = instance;

}

Instance* Call::getInstance() {

    return this->instance;

}

void Call::setMethod(Method *method) {

    this->method = method;

}

Method* Call::getMethod() {

    return this->method;

}

std::string Call::getInstanceName() {

    return this->instanceName;

}

std::string Call::getMethodName() {

    return this->methodName;

}

std::string Call::getStringValue() {

    return this->instanceName + "." + this->methodName;

}

std::list<Argument*>* Call::getArguments(){

    return this->arguments;
    
}

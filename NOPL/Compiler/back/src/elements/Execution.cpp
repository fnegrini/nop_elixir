#include "elements/Execution.h"

#include <iostream>

Execution::Execution(int execution) {

    this->execution = execution;

}

Execution::~Execution() {

    //

}

int Execution::getExecution() {
    return this->execution;
}

void Execution::print() {

    switch(this->execution) {
        
        case Execution::SEQUENTIAL_EXECUTION: {
            std::cout << "SEQUENTIAL";
        } break;

        case Execution::PARALLEL_EXECUTION: {
            std::cout << "PARALLEL";
        } break;

        default: {
            std::cout << "error";
        } break;

    }
    
}

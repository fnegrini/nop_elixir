#include "elements/FormationRuleIndex.h"

#include <iostream>

FormationRuleIndex::FormationRuleIndex(std::string id, int from, int to) {

    this->id = id;
    this->from = from;
    this->to = to;

}

FormationRuleIndex::~FormationRuleIndex() {
    std::cout << "~FormationRuleIndex()" << std::endl;
}

void FormationRuleIndex::print() {

    std::cout << "\n\t\t";

    std::cout << " " << this->id;
    std::cout << " from " << this->from;
    std::cout << " to " << this->to;
    std::cout << "\n";

    if (this->nextFormationRuleIndex != NULL) {
        this->nextFormationRuleIndex->print();
    }

    std::cout << std::endl;
    
}

std::string FormationRuleIndex::getId() {
    return this->id;
}

int FormationRuleIndex::getFrom() {
    return this->from;
}


int FormationRuleIndex::getTo() {
    return this->to;
}

FormationRuleIndex *FormationRuleIndex::getNextFormationRuleIndex() {
    return this->nextFormationRuleIndex;
}

void FormationRuleIndex::addFormationRuleIndex(FormationRuleIndex *nextFormationRuleIndex) {
    if (this->nextFormationRuleIndex == NULL) {
        this->nextFormationRuleIndex = nextFormationRuleIndex;
    } else {
        this->nextFormationRuleIndex->addFormationRuleIndex(nextFormationRuleIndex);
    }
}
#include "elements/Visibility.h"

#include <iostream>

Visibility::Visibility(int visibility) {

    this->visibility = visibility;

}

Visibility::~Visibility() {

    //

}

void Visibility::print() {

    std::cout << this->getVisibilityName();

}

int Visibility::getVisibilityId() {

    return this->visibility;

}

std::string Visibility::getVisibilityName() {

    switch(this->visibility) {
        
        case Visibility::PRIVATE_VISIBILITY: {
            return "private";
        } break;

        case Visibility::PUBLIC_VISIBILITY: {
            return "public";
        } break;

        default: {
            return "error";
        } break;

    }

}

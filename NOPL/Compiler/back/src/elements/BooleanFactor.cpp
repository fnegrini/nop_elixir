#include "elements/BooleanFactor.h"

#include <iostream>

BooleanFactor::BooleanFactor(bool value) : Factor(Factor::BOOLEAN_FACTOR) {

    this->value = value;

}

BooleanFactor::~BooleanFactor() {

    //

}

bool BooleanFactor::getValue() {

    return this->value;

}

std::string BooleanFactor::getStringValue() {

    if (this->value) {
        return "true";
    } else {
        return "false";
    }
    
}

Factor* BooleanFactor::clone() {

    return new BooleanFactor(this->value);

}

#include "elements/Property.h"

#include <iostream>

Property::Property(int type) {

    this->type = type;

}

Property::~Property() {

    std::cout << "~Property()" << std::endl;

}

void Property::print() {

    std::cout << "\t# Property: ";

    std::cout << " ";

    std::cout << this->getTypeName();

    std::cout << std::endl;

}

int Property::getType() {

    return this->type;

}

std::string Property::getTypeName() {

    switch(this->type) {
        
        case TARGET_PROPERTY: {
            return "Target";
        } break;

        case STRATEGY_PROPERTY: {
            return "Strategy";
        } break;
        
        default: {
            return "undefined";
        } break;

    }

}

#include "elements/Attribute.h"

#include "elements/Type.h"
#include "elements/Visibility.h"
#include "elements/Factor.h"

#include "elements/Instance.h"
#include "elements/Premise.h"

#include <iostream>

Attribute::Attribute(Instance *instance, Visibility *visibility, Type *type, std::string name, Factor *factor)
    : Entity(instance, name) {

    this->visibility = visibility;
    this->type = type;
    this->factor = factor;

    this->premises = new std::map<std::string, Premise*>();

}

Attribute::~Attribute() {

    std::cout << "~Attribute()" << std::endl;

}

void Attribute::print() {

    std::cout << "\t# Attribute: ";
    
    this->visibility->print();
    
    std::cout << " ";
         
    this->type->print();

    std::cout << " ";
    std::cout << this->getName();

    if (this->getFactor() != NULL && this->getFactor()->getStringValue() != "") {
        std::cout << " = ";
        std::cout << this->getFactor()->getStringValue();
    }

    std::cout << std::endl;

}

Visibility* Attribute::getVisibility() {

    return this->visibility;

}

Type* Attribute::getType() {

    return this->type;

}

Factor* Attribute::getFactor() {

    return this->factor;

}

void Attribute::addPremise(Premise *premise) {

    if (this->premises->count(premise->getName()) == 0) {

        this->premises->insert(std::pair<std::string, Premise*>(premise->getName(), premise));

    }

}

Premise* Attribute::getPremise(std::string name) {

    if (this->premises->count(name) > 0) {

        std::map<std::string, Premise*>::iterator it = this->premises->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

std::map<std::string, Premise*>* Attribute::getPremises() {

    return this->premises;

}

Attribute* Attribute::clone(Instance *instance) {

    return new Attribute(instance, this->visibility, this->type, this->getName(), this->factor);

}

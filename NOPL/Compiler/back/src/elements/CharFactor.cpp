#include "elements/CharFactor.h"

#include <iostream>

CharFactor::CharFactor(char value) : Factor(Factor::CHAR_FACTOR) {

    this->value = value;

}

CharFactor::~CharFactor() {

    //

}

char CharFactor::getValue() {

    return this->value;

}

std::string CharFactor::getStringValue() {

    return std::to_string(this->value);

}

Factor* CharFactor::clone() {

    return new CharFactor(this->value);

}

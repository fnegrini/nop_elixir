#include "elements/Type.h"

#include <iostream>

Type::Type(int basicType, int numBytes) {

    this->basicType = basicType;
    this->numBytes = numBytes;

}

Type::~Type() {

    //

}

void Type::print() {

    std::cout << this->getTypeName();

}

int Type::getTypeId() {

    return this->basicType;

}

std::string Type::getTypeName() {

    std::string rType = "";

    switch(this->basicType) {
        
        case BOOLEAN_TYPE: {
            rType += "boolean";
        } break;

        case INTEGER_TYPE: {
            rType += "integer";
        } break;

        case DOUBLE_TYPE: {
            rType += "double";
        } break;

        case STRING_TYPE: {
            rType += "string";
        } break;

        case CHAR_TYPE: {
            rType += "char";
        } break;

        default: {
            rType += "undefined";
        } break;

    }

    if (this->numBytes > 0) {
        rType += "<";
        rType += this->numBytes;
        rType += ">";
    }

    return rType;

}

int Type::getNumBytes() {

    return this->numBytes;

}

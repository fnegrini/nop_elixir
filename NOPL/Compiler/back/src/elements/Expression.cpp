#include "elements/Expression.h"

#include "elements/Factor.h"
#include "elements/IntegerFactor.h"
#include "elements/Symbol.h"
#include "elements/Instance.h"

#include <iostream>

Expression::Expression(Factor *leftFactor, Symbol *symbol, Factor *rightFactor) {

    this->leftFactor = leftFactor;

    this->symbol = symbol;

    this->rightFactor = rightFactor;

}

Expression::~Expression() {

    //

}

void Expression::print() {

    this->getLeftFactor()->print();

    std::cout << " ";

    this->getSymbol()->print();

    std::cout << " ";

    this->getRightFactor()->print();

}

Factor* Expression::getLeftFactor() {

    return this->leftFactor;

}

Symbol* Expression::getSymbol() {

    return this->symbol;

}

Factor* Expression::getRightFactor() {

    return this->rightFactor;

}

std::string Expression::getVectorIndex() {
    if (this->getLeftFactor()->getFactorId() == Factor::STRING_FACTOR) {
        return this->getLeftFactor()->getStringValue();
    } else if (this->getRightFactor()->getFactorId() == Factor::STRING_FACTOR) { 
        return this->getRightFactor()->getStringValue();
    } else {
        throw new std::invalid_argument("This doesn't seems to be a vector expression");
    }
}

int Expression::evaluateVectorExpression(int indexValue) {
    this->print();
    int leftValue;
    int rightValue;
    if (this->getLeftFactor()->getFactorId() == Factor::STRING_FACTOR) {
        leftValue = indexValue;
        rightValue = ((IntegerFactor *)this->getRightFactor())->getValue();
    } else {
        leftValue = ((IntegerFactor *)this->getLeftFactor())->getValue();
        rightValue = indexValue;
    }

    switch (this->getSymbol()->getSymbolId()) {
        case Symbol::PLUS_SYMBOL: {
            return leftValue + rightValue;
        } break;

        case Symbol::MINUS_SYMBOL: {
            return leftValue - rightValue;
        } break;

        default: {
            throw new std::invalid_argument("Operation " + this->getSymbol()->getSymbolName()  +" not supported for vector expression");
        } break;
    }

}

Expression* Expression::clone() {

	Factor *leftFactorClone = this->leftFactor->clone();
	Factor *rightFactorClone = this->rightFactor->clone();
	
	return new Expression(leftFactorClone, this->symbol, rightFactorClone);

}

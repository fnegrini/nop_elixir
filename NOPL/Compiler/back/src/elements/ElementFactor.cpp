#include "elements/ElementFactor.h"

#include "elements/Instance.h"
#include "elements/Attribute.h"

#include <iostream>

ElementFactor::ElementFactor(std::string instanceName, std::string attributeName) : Factor(Factor::ELEMENT_FACTOR) {

    this->instanceName = instanceName;

    this->attributeName = attributeName;

}

ElementFactor::ElementFactor(std::string instanceName, std::string attributeName, int factor) : Factor(factor) {

    this->instanceName = instanceName;

    this->attributeName = attributeName;

}

ElementFactor::~ElementFactor() {

    //

}

void ElementFactor::setInstance(Instance *instance) {

    this->instance = instance;

}

Instance* ElementFactor::getInstance() {

    return this->instance;

}

void ElementFactor::setAttribute(Attribute *attribute) {

    this->attribute = attribute;

}

Attribute* ElementFactor::getAttribute() {

    return this->attribute;

}

std::string ElementFactor::getInstanceName() {

    return this->instanceName;

}

std::string ElementFactor::getAttributeName() {

    return this->attributeName;

}

std::string ElementFactor::getStringValue() {

    return this->instanceName + "." + this->attributeName;

}

ElementFactor* ElementFactor::clone() {

    return new ElementFactor(this->instanceName, this->attributeName);

}

#include "elements/Method.h"

#include "elements/Visibility.h"

#include "elements/Param.h"
#include "elements/CodeBlock.h"
#include "elements/Attribution.h"

#include "elements/ElementFactor.h"

#include <iostream>

Method::Method(Instance *instance, Visibility *visibility, std::string name, std::map<std::string, Param*> *params = 0, std::map<std::string, CodeBlock*> *codeBlocks = 0, std::map<std::string, Attribution*> *attributions = 0)
    : Entity(instance, name) {

    this->visibility = visibility;

    this->params = params;

    this->codeBlocks = codeBlocks;

    this->attributions = attributions;

}

Method::~Method() {

    std::cout << "~Method()" << std::endl;

}

void Method::print() {

    std::cout << "\t# Method: ";
    
    this->visibility->print();
    
    std::cout << " ";
    std::cout << this->getName();

    if (this->params != 0 && this->params->size() > 0) {

        std::cout << "\n\t  params:\n";

        for (std::map<std::string, Param*>::iterator it = this->params->begin(); it != this->params->end(); ++it) {

            ((Param*)it->second)->print();

        }

    }

    if (this->codeBlocks != 0 && this->codeBlocks->size() > 0) {

        std::cout << "\n\t  code blocks:\n";

        for (std::map<std::string, CodeBlock*>::iterator it = this->codeBlocks->begin(); it != this->codeBlocks->end(); ++it) {

            ((CodeBlock*)it->second)->print();

        }

    }

    if (this->attributions != 0 && this->attributions->size() > 0) {

        std::cout << "\n\t  attributions:\n";

        for (std::map<std::string, Attribution*>::iterator it = this->attributions->begin(); it != this->attributions->end(); ++it) {

            ((Attribution*)it->second)->print();

        }

    }
    
    std::cout << std::endl;

}

Visibility* Method::getVisibility() {

    return this->visibility;

}

std::map<std::string, Param*>* Method::getParams() {

    return this->params;

}

std::map<std::string, CodeBlock*>* Method::getCodeBlocks() {

    return this->codeBlocks;

}

std::map<std::string, Attribution*>* Method::getAttributions() {

    return this->attributions;

}

Method* Method::clone(Instance *instance) {

	std::map<std::string, Attribution*> *attributionsClone = new std::map<std::string, Attribution*>();

    for (std::map<std::string, Attribution*>::iterator it = this->getAttributions()->begin(); it != this->getAttributions()->end(); ++it) {

        Attribution *attribution = it->second;

        Attribution *attributionClone = attribution->clone(instance);

		attributionsClone->insert(std::pair<std::string, Attribution*>(attributionClone->getElement()->getStringValue(), attributionClone));

    }

    return new Method(instance, this->visibility, this->getName(), this->params, this->codeBlocks, attributionsClone);

}

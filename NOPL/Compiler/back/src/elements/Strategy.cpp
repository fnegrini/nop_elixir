#include "elements/Strategy.h"

#include <iostream>

Strategy::Strategy(int strategy) : Property(Property::STRATEGY_PROPERTY) {

    this->strategy = strategy;

}

Strategy::~Strategy() {

    //

}

void Strategy::print() {

    std::cout << this->getStrategyName();

}

int Strategy::getStrategyId() {

    return this->strategy;

}

std::string Strategy::getStrategyName() {

    switch(this->strategy) {
        
        case NO_ONE_STRATEGY: {
            return "NO_ONE";
        } break;

        case BREADTH_STRATEGY: {
            return "BREADTH";
        } break;

        case DEPTH_STRATEGY: {
            return "DEPTH";
        } break;

        case PRIORITY_STRATEGY: {
            return "PRIORITY";
        } break;

        default: {
            return "undefined";
        } break;

    }

}
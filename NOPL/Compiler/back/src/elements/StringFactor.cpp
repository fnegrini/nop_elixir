#include "elements/StringFactor.h"

#include <iostream>

StringFactor::StringFactor(std::string value) : Factor(Factor::STRING_FACTOR) {

    this->value = value;

}

StringFactor::~StringFactor() {

    //

}

std::string StringFactor::getValue() {

    return this->value;

}

std::string StringFactor::getStringValue() {

    return this->value;

}

Factor* StringFactor::clone() {

    return new StringFactor(this->value);

}

#include "elements/Entity.h"

#include "elements/Instance.h"

#include <iostream>

Entity::Entity(Instance *instance, std::string name) {

    this->parentInstance = instance;

    this->name = name;

}

Entity::~Entity() {

    std::cout << "~Entity()" << std::endl;

}

Instance* Entity::getParentInstance() {

    return this->parentInstance;

}

std::string Entity::getName() {

    return this->name;

}

#include "elements/Instigation.h"

#include "elements/Execution.h"
#include "elements/Call.h"
#include "elements/Action.h"

#include <iostream>

Instigation::Instigation(Instance *instance, std::string name, Execution *execution, std::list<Call*> *calls = 0)
    : Entity(instance, name) {

    this->execution = execution;

    this->calls = calls;

    this->action = 0;

}

Instigation::~Instigation() {

    std::cout << "~Instigation()" << std::endl;

}

void Instigation::print() {

    std::cout << "\t# Instigation ";
    
    std::cout << this->getName();

    std::cout << " [";

    this->execution->print();

    std::cout << "]:\n";

    if (this->calls != 0 && this->calls->size() > 0) {

        for (std::list<Call*>::iterator it = this->calls->begin(); it != this->calls->end(); ++it) {

            ((Call*)*it)->print();

        }

    }

}

Execution* Instigation::getExecution() {

    return this->execution;

}

std::list<Call*>* Instigation::getCalls() {

    return this->calls;

}

void Instigation::setAction(Action *action) {

    this->action = action;

}

Action* Instigation::getAction() {

    return this->action;

}

Instigation* Instigation::clone(Instance *instance) {

    return new Instigation(instance, this->getName(), this->execution, this->calls);

}

#include "elements/Attribution.h"

#include "elements/ElementFactor.h"
#include "elements/Factor.h"

#include <iostream>

Attribution::Attribution(ElementFactor *element, Factor *factor) {

    this->element = element;
    this->factor = factor;

}

Attribution::~Attribution() {

    std::cout << "~Attribution()" << std::endl;

}

void Attribution::print() {

    std::cout << "\n\t\t";

    this->element->print();
    std::cout << " = ";

    this->factor->print();

    std::cout << std::endl;
    
}

ElementFactor* Attribution::getElement() {

    return this->element;

}

Factor* Attribution::getFactor() {

    return this->factor;

}

Attribution* Attribution::clone(Instance *instance) {

    ElementFactor *cloneElement = this->element->clone();
    
    return new Attribution(cloneElement, this->factor);

}

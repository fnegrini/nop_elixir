#include "elements/VectorDto.h"

VectorDto::VectorDto(std::string name, std::string index) {
    this->name = name;
    this->index = index;
    this->expression = NULL;
}

VectorDto::VectorDto(std::string name, Expression *expression) {
    this->name = name;
    this->index = "";
    this->expression = expression;
}

VectorDto::~VectorDto() {}

std::string VectorDto::getName() {
    return this->name;
}
std::string VectorDto::getIndex() {
    return this->index;
}
Expression *VectorDto::getExpression() {
    return this->expression;
}
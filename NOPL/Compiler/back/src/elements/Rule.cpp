#include "elements/Rule.h"

#include "elements/Condition.h"
#include "elements/Action.h"

#include <iostream>

Rule::Rule(Instance *instance, std::string name, Condition *condition, Action *action)
    : Entity(instance, name) {

    this->condition = condition;

    this->action = action;

}

Rule::~Rule() {

    std::cout << "~Rule()" << std::endl;

}

void Rule::print() {

    std::cout << "\t# Rule ";
    
    std::cout << this->getName();

    std::cout << "\n";

    this->condition->print();

    std::cout << "\n";

    this->action->print();

    std::cout << std::endl;

}

Condition* Rule::getCondition() {

    return this->condition;

}

Action* Rule::getAction() {

    return this->action;

}

Rule* Rule::clone(Instance *instance) {

    Condition *conditionClone = this->condition->clone(instance);
    conditionClone->setRule(this);

    Action *actionClone = this->action->clone(instance);
    actionClone->setRule(this);

    return new Rule(instance, this->getName(), conditionClone, actionClone);

}

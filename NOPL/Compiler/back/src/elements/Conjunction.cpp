#include "elements/Conjunction.h"

#include <iostream>

Conjunction::Conjunction(int conjunction) {

    this->conjunction = conjunction;

}

Conjunction::~Conjunction() {

    //

}

void Conjunction::print() {

    std::cout << this->getConjunctionName();

}

int Conjunction::getConjunctionId() {

    return this->conjunction;

}

std::string Conjunction::getConjunctionName() {

    switch(this->conjunction) {
        
        case NO_CONJUNCTION: {
            return "SINGLE";
        } break;

        case AND_CONJUNCTION: {
            return "AND";
        } break;

        case OR_CONJUNCTION: {
            return "OR";
        } break;

        default: {
            return "undefined";
        } break;

    }

}

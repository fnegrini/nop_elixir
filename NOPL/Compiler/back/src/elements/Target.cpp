#include "elements/Target.h"

#include <iostream>

Target::Target(int target) : Property(Property::TARGET_PROPERTY) {

    this->target = target;

}

Target::~Target() {

    //

}

void Target::print() {

    std::cout << this->getTargetName();

}

int Target::getTargetId() {

    return target;

}

std::string Target::getTargetName() {

    switch(this->target) {
        
        case CODE_GENERATION_EXAMPLE_TARGET: { 
            return "CODE_GENERATION_EXAMPLE";
        } break;

        case NAMESPACES_TARGET: {
            return "NAMESPACES";
        } break;

        case ELIXIR_TARGET: {
            return "ELIXIR";
        } break;

        default: {
            return "UNDEFINED";
        } break;

    }

}

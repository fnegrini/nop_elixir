#include "elements/Condition.h"

#include "elements/Conjunction.h"
#include "elements/Premise.h"
#include "elements/Subcondition.h"
#include "elements/Rule.h"

#include <iostream>

Condition::Condition(Instance *instance, std::string name, Conjunction *conjunction, std::map<std::string, Premise*> *premises = 0, std::map<std::string, Subcondition*> *subconditions = 0)
    : Entity(instance, name) {

    if (conjunction != 0) {
        this->conjunction = conjunction;
    } else {
        this->conjunction = new Conjunction(Conjunction::NO_CONJUNCTION);
    }

    this->premises = premises;

    this->subconditions = subconditions;

    this->rule = 0;

}

Condition::~Condition() {

    std::cout << "~Condition()" << std::endl;

}

void Condition::print() {

    std::cout << "\t# Condition ";
    
    std::cout << this->getName();

    if (this->conjunction != 0) {

        std::cout << " [";

        this->conjunction->print();

        std::cout << "]:\n";

    } else {
        
        std::cout << ":\n";

    }

    if (this->premises != 0 && this->premises->size() > 0) {

        for (std::map<std::string, Premise*>::iterator it = this->premises->begin(); it != this->premises->end(); ++it) {

            ((Premise*)it->second)->print();

        }

    }

    if (this->subconditions != 0 && this->subconditions->size() > 0) {

        for (std::map<std::string, Subcondition*>::iterator it = this->subconditions->begin(); it != this->subconditions->end(); ++it) {

            ((Subcondition*)it->second)->print();

        }

    }

    std::cout << std::endl;

}

Conjunction* Condition::getConjunction() {

    return this->conjunction;

}

void Condition::setNumberApprovedPremises(int numberApprovedPremises) {

	this->numberApprovedPremises = numberApprovedPremises;

}

int Condition::getNumberApprovedPremises() {

	return this->numberApprovedPremises;

}

std::map<std::string, Premise*>* Condition::getPremises() {

    return this->premises;

}

std::map<std::string, Subcondition*>* Condition::getSubconditions() {

    return this->subconditions;

}

void Condition::setRule(Rule *rule) {

    this->rule = rule;

}

Rule* Condition::getRule() {

    return this->rule;

}

Condition* Condition::clone(Instance *instance) {

    std::map<std::string, Premise*> *premises = new std::map<std::string, Premise*>();

    for (std::map<std::string, Premise*>::iterator it = this->getPremises()->begin(); it != this->getPremises()->end(); ++it) {

        Premise *premise = it->second;

        Premise *premiseClone = premise->clone(instance);

		premiseClone->setCondition(this);

        premises->insert(std::pair<std::string, Premise*>(premiseClone->getName(), premiseClone));

    }

    std::map<std::string, Subcondition*> *subconditions = new std::map<std::string, Subcondition*>();

    for (std::map<std::string, Subcondition*>::iterator it = this->getSubconditions()->begin(); it != this->getSubconditions()->end(); ++it) {

        Subcondition *subcondition = it->second;

        Subcondition *subconditionClone = subcondition->clone(instance);

		subconditionClone->setCondition(this);

        subconditions->insert(std::pair<std::string, Subcondition*>(subconditionClone->getName(), subconditionClone));

    }

    return new Condition(instance, this->getName(), this->conjunction, premises, subconditions);

}

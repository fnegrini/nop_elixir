#include "elements/Factor.h"

#include <iostream>

Factor::Factor(int factor) {

    this->factor = factor;

}

Factor::~Factor() {

    //

}

void Factor::print() {

    std::cout << this->getFactorName();

}

int Factor::getFactorId() {

    return this->factor;

}

std::string Factor::getFactorName() {

    switch(this->factor) {

        case VECTOR_ELEMENT_FACTOR: {
            return "VECTOR_ELEMENT (" + this->getStringValue() + ")";
        } break;
        
        case ELEMENT_FACTOR: {
            return "ELEMENT (" + this->getStringValue() + ")";
        } break;

        case BOOLEAN_FACTOR: {
            return "BOOLEAN (" + this->getStringValue() + ")";
        } break;

        case INTEGER_FACTOR: {
            return "INTEGER (" + this->getStringValue() + ")";
        } break;

        case DOUBLE_FACTOR: {
            return "DOUBLE (" + this->getStringValue() + ")";
        } break;

        case CHAR_FACTOR: {
            return "CHAR (" + this->getStringValue() + ")";
        } break;

        case STRING_FACTOR: {
            return "STRING (" + this->getStringValue() + ")";
        } break;

        default: {
            return "undefined";
        } break;

    }

}

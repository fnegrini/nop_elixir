#include "elements/Subcondition.h"

#include "elements/Conjunction.h"
#include "elements/Premise.h"
#include "elements/Condition.h"

#include <iostream>

Subcondition::Subcondition(Instance *instance, std::string name, Conjunction *conjunction, std::map<std::string, Premise*> *premises = 0)
    : Entity(instance, name) {

    if (conjunction != 0) {
        this->conjunction = conjunction;
    } else {
        this->conjunction = new Conjunction(Conjunction::NO_CONJUNCTION);
    }

    this->premises = premises;

}

Subcondition::~Subcondition() {

    std::cout << "~Subcondition()" << std::endl;

}

void Subcondition::print() {

    std::cout << "\t# Subcondition ";
    
    std::cout << this->getName();

    std::cout << " [";

    this->conjunction->print();

    std::cout << "]:\n";

    if (this->premises != 0 && this->premises->size() > 0) {

        for (std::map<std::string, Premise*>::iterator it = this->premises->begin(); it != this->premises->end(); ++it) {

            ((Premise*)it->second)->print();

        }

    }

}

Conjunction* Subcondition::getConjunction() {

    return this->conjunction;

}

void Subcondition::setNumberApprovedPremises(int numberApprovedPremises) {

	this->numberApprovedPremises = numberApprovedPremises;

}

int Subcondition::getNumberApprovedPremises() {

	return this->numberApprovedPremises;

}

std::map<std::string, Premise*>* Subcondition::getPremises() {

    return this->premises;

}

void Subcondition::setCondition(Condition *condition) {

    this->condition = condition;

}

Condition* Subcondition::getCondition() {

    return this->condition;

}

Subcondition* Subcondition::clone(Instance *instance) {

    std::map<std::string, Premise*> *premises = new std::map<std::string, Premise*>();

    for (std::map<std::string, Premise*>::iterator it = this->getPremises()->begin(); it != this->getPremises()->end(); ++it) {

        Premise *premise = it->second;

        Premise *premiseClone = premise->clone(instance);

	premiseClone->setSubcondition(this);

        premises->insert(std::pair<std::string, Premise*>(premiseClone->getName(), premiseClone));

    }

    return new Subcondition(instance, this->getName(), this->conjunction, premises);

}

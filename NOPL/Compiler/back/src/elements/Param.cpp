#include "elements/Param.h"

#include "elements/Type.h"

#include <iostream>

Param::Param(Type *type, std::string name) {

    this->type = type;
    this->name = name;

}

Param::~Param() {

    std::cout << "~Param()" << std::endl;

}

void Param::print() {

    std::cout << "\n\t\t";

    this->type->print();
    std::cout << " " << this->name;

    std::cout << std::endl;
    
}

Type* Param::getType() {

    return this->type;

}

std::string Param::getName() {

    return this->name;

}
#include "elements/Argument.h"

#include "elements/Factor.h"

#include <iostream>

Argument::Argument(Factor *factor) {

    this->factor = factor;

}

Argument::~Argument() {

    std::cout << "~Argument()" << std::endl;

}

void Argument::print() {

    std::cout << "\t\t\t";

    this->factor->print();

    std::cout << std::endl;
    
}

Factor* Argument::getFactor() {

    return this->factor;

}

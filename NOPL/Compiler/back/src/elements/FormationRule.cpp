#include "elements/FormationRule.h"
#include "elements/FormationRuleIndex.h"

#include <iostream>

FormationRule::FormationRule() {
}

FormationRule::~FormationRule() {
    this->valueMaps.clear();
    std::cout << "~FormationRule()" << std::endl;
}

void FormationRule::print() {

    std::cout << "\t# FormationRule ";
    std::cout << this->name;
    std::cout << "\n";
    
    this->formationRuleIndex->print();

    std::cout << std::endl;

}


void FormationRule::setName(std::string name) {
    this->name = name;
}

std::string FormationRule::addFormationRuleIndex(FormationRuleIndex *index) {
     std::string errorMessage = "";

     if (index == NULL) {
         return "FORMATION_RULE index can't be null";
     }

     if (index->getFrom() >= index->getTo()) {
        errorMessage += "FormationRule [";
        errorMessage += index->getId();
        errorMessage += "] ";
        errorMessage +=  "To index needs to be higher than from index";
        return errorMessage;
     }

    if  (this->formationRuleIndex == NULL) {
        this->formationRuleIndex = index;
        this->valueMaps.clear();
    } else {
        this->formationRuleIndex->addFormationRuleIndex(index);
    }

     this->valueMaps[index->getId()] = index->getFrom();

     return errorMessage;

}

void FormationRule::updateIndex(std::string id, int position) {
    this->valueMaps[id] = position;

}

int FormationRule::getCurrentPositionForIndex(std::string id) {
    return this->valueMaps.at(id);
}

FormationRuleIndex* FormationRule::getFormationRuleIndex() {
    return this->formationRuleIndex;
}
#include "elements/Premise.h"

#include "elements/Expression.h"
#include "elements/Attribute.h"
#include "elements/Instance.h"
#include "elements/Condition.h"
#include "elements/Subcondition.h"
#include "elements/ElementFactor.h"

#include <iostream>

Premise::Premise(Instance *instance, std::string name, Expression *expression, bool impertinent)
	: Entity(instance, name) {

    this->expression = expression;
    this->impertinent = impertinent;

	this->initialLogicalValue = false;

    this->attributes = new std::map<std::string, Attribute*>();
    
    this->condition = 0;
    this->subcondition = 0;
    
}

Premise::~Premise() {

    std::cout << "~Premise()" << std::endl;

}

void Premise::print() {

    std::cout << "\t# Premise: ";
    
    std::cout << this->getName();

    std::cout << (this->getImpertinent() ? " <impertinent> " : " ");

    this->expression->print();
    
    std::cout << std::endl;

}

Expression* Premise::getExpression() {

    return this->expression;

}

bool Premise::getImpertinent() {

    return this->impertinent;

}

void Premise::setInitialLogicalValue(bool initiallogicalValue) {

	this->initialLogicalValue = initiallogicalValue;

}

bool Premise::getInitialLogicalValue() {

	return this->initialLogicalValue;

}

void Premise::addAttribute(Attribute *attribute) {

    if (this->attributes->count(attribute->getName()) == 0) {

        this->attributes->insert(std::pair<std::string, Attribute*>(attribute->getName(), attribute));

    }

}

Attribute* Premise::getAttribute(std::string name) {

    if (this->attributes->count(name) > 0) {

        std::map<std::string, Attribute*>::iterator it = this->attributes->find(name);

        //std::cout << "first: " << it->first << std::endl;
        //std::cout << "second: " << it->second << std::endl;

        if (it->first != "") {
            return it->second;
        }

    }

    return 0;

}

std::map<std::string, Attribute*>* Premise::getAttributes() {

    return this->attributes;

}

void Premise::setCondition(Condition *condition) {

    this->condition = condition;

}

Condition* Premise::getCondition() {

    return this->condition;

}

void Premise::setSubcondition(Subcondition *subcondition) {

    this->subcondition = subcondition;

}

Subcondition* Premise::getSubcondition() {

    return this->subcondition;

}

Premise* Premise::clone(Instance *instance) {

    Expression *expression = this->expression->clone();
    
    return new Premise(instance, this->getName(), expression, this->impertinent);

}

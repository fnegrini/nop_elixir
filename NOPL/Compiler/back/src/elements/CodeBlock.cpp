#include "elements/CodeBlock.h"

#include "elements/Target.h"

#include <iostream>

CodeBlock::CodeBlock(Target *target, std::string code) {

    this->target = target;
    this->code = code;

}

CodeBlock::~CodeBlock() {

    std::cout << "~CodeBlock()" << std::endl;

}

void CodeBlock::print() {

    std::cout << "\n\t    Code block [";

    this->target->print();

    std::cout << "] " << std::endl;
    
    std::cout << this->code;

    std::cout << std::endl;
    
}

Target* CodeBlock::getTarget() {

    return this->target;

}

std::string CodeBlock::getCode() {

    return this->code;

}
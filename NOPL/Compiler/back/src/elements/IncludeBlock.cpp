#include "elements/IncludeBlock.h"

#include "elements/Target.h"

#include <iostream>

IncludeBlock::IncludeBlock(Target *target, std::string code) {

    this->target = target;
    this->code = code;

}

IncludeBlock::~IncludeBlock() {

    std::cout << "~IncludeBlock()" << std::endl;

}

void IncludeBlock::print() {

    std::cout << "\t# Include block [";

    this->target->print();

    std::cout << "] " << std::endl;
    
    std::cout << this->code;

    std::cout << std::endl;
    
}

Target* IncludeBlock::getTarget() {

    return this->target;

}

std::string IncludeBlock::getCode() {

    return this->code;

}
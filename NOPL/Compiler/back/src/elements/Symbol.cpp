#include "elements/Symbol.h"

#include <iostream>

Symbol::Symbol(int symbol) {

    this->symbol = symbol;

}

Symbol::~Symbol() {

    //

}

void Symbol::print() {

    std::cout << this->getSymbolName();

}

int Symbol::getSymbolId() {

    return this->symbol;

}

std::string Symbol::getSymbolName() {

    switch(this->symbol) {
        
        case EQUAL_SYMBOL: {
            return "EQUAL";
        } break;

        case NOT_EQUAL_SYMBOL: {
            return "NOT_EQUAL";
        } break;

        case LESSER_THAN_SYMBOL: {
            return "LESSER_THAN";
        } break;

        case GREATER_THAN_SYMBOL: {
            return "GREATER_THAN";
        } break;

        case LESS_OR_EQUAL_SYMBOL: {
            return "LESS_OR_EQUAL";
        } break;

        case GREATER_OR_EQUAL_SYMBOL: {
            return "GREATER_OR_EQUAL";
        } break;

        case PLUS_SYMBOL: {
            return "PLUS_SYMBOL";
        } break;

        case MINUS_SYMBOL: {
            return "MINUS_SYMBOL";
        } break;

        default: {
            return "undefined";
        } break;

    }

}

#include "elements/VectorElementFactor.h"

VectorElementFactor::VectorElementFactor(std::string instanceName, VectorDto *instanceDto, std::string attributeName, VectorDto *attributeDto)
    : ElementFactor(instanceName, attributeName, Factor::VECTOR_ELEMENT_FACTOR) {
        if (instanceDto != NULL) {
            this->instanceIndex = instanceDto->getIndex();
            this->instanceExpression = instanceDto->getExpression();
        } else {
            this->instanceIndex = "";
            this->instanceExpression = NULL;
        }
    
        if (attributeDto != NULL) {
            this->attributeIndex = attributeDto->getIndex();
            this->attributeExpression = attributeDto->getExpression();
        } else {
            this->attributeIndex = "";
            this->attributeExpression = NULL;
        }
}

VectorElementFactor::~VectorElementFactor() {
}

std::string VectorElementFactor::getInstanceIndex() {

    return this->instanceIndex;
}

std::string VectorElementFactor::getAttributeIndex() {

    return this->attributeIndex;

}

Expression *VectorElementFactor::getInstanceExpression() {
    return this->instanceExpression;
}

Expression *VectorElementFactor::getAttributeExpression() {
    return this->attributeExpression;
}

std::string VectorElementFactor::getStringValue() {

    return this->getInstanceName() + (!this->instanceIndex.empty() ? "[" + this->instanceIndex + "]" : "") +  "." 
    + this->getAttributeName() + (!this->attributeIndex.empty() ? "[" + this->attributeIndex + "]" : "");

}

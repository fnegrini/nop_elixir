#include "elements/MainBlock.h"

#include "elements/Attribution.h"

#include <iostream>

MainBlock::MainBlock(std::map<std::string, Attribution*> *attributions) {

	this->attributions = attributions;

}

MainBlock::~MainBlock() {

    std::cout << "~MainBlock()" << std::endl;

}

void MainBlock::print() {

    std::cout << "\n\t    Main block [";

	if (this->attributions != 0 && this->attributions->size() > 0) {

        std::cout << "\n\t  attributions:\n";

        for (std::map<std::string, Attribution*>::iterator it = this->attributions->begin(); it != this->attributions->end(); ++it) {

            ((Attribution*)it->second)->print();

        }

    }

    std::cout << std::endl;
    
}

std::map<std::string, Attribution*>* MainBlock::getAttributions() {

    return this->attributions;

}

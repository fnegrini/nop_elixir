#include "Compiler.h"

#include "NOPGraph.h"

#include <iostream>

using namespace std;

Compiler::Compiler() {

    cout << "Compiler()" << endl;

    graph = new NOPGraph();

}

Compiler::~Compiler() {

    cout << "~Compiler()" << endl;

    delete graph;

}

std::string Compiler::getLevel(int level) {

	std::string str = "";

	for (int i = 0; i < level; i++) {
		str += "\t";
	}

	return str;

}

defmodule NOPTest do
  use ExUnit.Case

  doctest NOP

  test "Create first FBE" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    assert NOP.Service.FBE.get_name(fbe) == "NOP.element.MyFBE"
  end

  test "Create Semaphore reading attributes" do
    fbe =
      NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.Element.FBE")

    assert NOP.Service.FBE.get_attribute(fbe, :value) == 0
  end

  test "Create FBE reading and writing attributes" do
    fbe =
      NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    assert NOP.Service.FBE.get_attribute(fbe, :value) == 5
  end

  test "Link Premise to FBE" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise =
      NOP.Service.Premise.create_premise("NOP.element.Premise", fbe, :value, :GE, 10)

    assert NOP.Service.Premise.evaluate_premise(premise) == false
  end

  test "Link Premise to FBE with initial assert" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise =
      NOP.Service.Premise.create_premise("NOP.element.Premise", fbe, :value, :EQ, 0)

    assert NOP.Service.Premise.evaluate_premise(premise) == true
  end

  test "FBE changing attribute value and notificating premise" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.Premise", fbe, :value, :EQ, 5)

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    :timer.sleep(100)

    assert NOP.Service.Premise.evaluate_premise(premise1) == true
  end

  test "FBE changing attribute value and notificating multiple premises" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.Premise1", fbe, :value, :EQ, 5)

    premise2 =
      NOP.Service.Premise.create_premise("NOP.element.Premise2", fbe, :value, :GT, 0)

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    :timer.sleep(100)

    val_premise1 = NOP.Service.Premise.evaluate_premise(premise1)

    val_premise2 = NOP.Service.Premise.evaluate_premise(premise2)

    assert (val_premise1 and val_premise2) == true
  end

  test "Premise linked to two FBE" do
    fbe1 = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE1")

    fbe2 = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE2")

    premise =
      NOP.Service.Premise.create_premise("NOP.element.Premise", fbe1, :value, :EQ, fbe2, :value)

    assert NOP.Service.Premise.evaluate_premise(premise) == true
  end

  test "Premise linked to two FBE with notification" do
    fbe1 = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE1")

    fbe2 = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE2")

    premise =
      NOP.Service.Premise.create_premise("NOP.element.Premise", fbe1, :value, :EQ, fbe2, :value)

    NOP.Service.FBE.set_attribute(fbe1, :value, 100)

    NOP.Service.FBE.set_attribute(fbe2, :value, 200)

    :timer.sleep(100)

    assert NOP.Service.Premise.evaluate_premise(premise) == false
  end

  test "Create first Condition" do
    condition = NOP.Service.Condition.create_condition("NOP.element.Condition", [], "true")

    assert NOP.Service.Condition.get_name(condition) == "NOP.element.Condition"
  end

  test "Condition linked to two premises" do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)

    premise2 =
      NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 0)

    condition = NOP.Service.Condition.create_condition("NOP.element.Condition", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")

    assert  NOP.Service.Condition.evaluate_condition(condition) == false
  end

  test "Condition linked to two premises with notification by attribute change" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)

    premise2 =
      NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 1)

    condition = NOP.Service.Condition.create_condition("NOP.element.Condition", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    assert  NOP.Service.Condition.evaluate_condition(condition) == false
  end

  test "Condition linked condition" do

    condition1 = NOP.Service.Condition.create_condition("NOP.element.Condition1", [], "( true )")

    condition2 = NOP.Service.Condition.create_condition("NOP.element.Condition2", [condition1], "( not NOP.element.Condition1 )")

    assert  NOP.Service.Condition.evaluate_condition(condition2) == false
  end

  test "Condition linked to premises and another condition" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)

    premise2 =
      NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 1)

    condition1 = NOP.Service.Condition.create_condition("NOP.element.Condition1", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")

    condition2 = NOP.Service.Condition.create_condition("NOP.element.Condition2", [condition1], "( not NOP.element.Condition1 )")

    assert  NOP.Service.Condition.evaluate_condition(condition2) == true
  end

  test "Condition linked to premises and another condition with change attribute" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)

    premise2 =
      NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 1)

    condition1 = NOP.Service.Condition.create_condition("NOP.element.Condition1", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")

    condition2 = NOP.Service.Condition.create_condition("NOP.element.Condition2", [condition1], "( not NOP.element.Condition1 )")

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    :timer.sleep(100)

    assert  NOP.Service.Condition.evaluate_condition(condition2) == false

  end

  test "Create first Rule" do
    rule = NOP.Service.Rule.create_rule(NOP.Element.Rule_dummy, "NOP.element.Rule", [])

    assert NOP.Service.Rule.get_name(rule) == "NOP.element.Rule"
  end

  test "Rule linked to Condition, linked to premises and another condition" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    rule = NOP.Service.Rule.create_rule(NOP.Element.Rule_dummy2, "NOP.element.Rule", [fbe])

    assert  NOP.Service.Rule.evaluate_rule(rule) == false

  end

  test "Rule running instigation after assert true" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    NOP.Service.Rule.create_rule(NOP.Element.Rule_dummy3, "NOP.element.Rule",  [fbe])

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    :timer.sleep(100)

    assert  assert NOP.Service.FBE.get_attribute(fbe, :value) == 3

  end


  test "FBE Statistics reset test" do

    NOP.Application.reset_element_list_total()

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    premise1 = NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)

    premise2 = NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 1)

    condition1 = NOP.Service.Condition.create_condition("NOP.element.Condition1", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")

    condition2 = NOP.Service.Condition.create_condition("NOP.element.Condition2", [condition1], "( not NOP.element.Condition1 )")

    Enum.map(1..1000,
      fn(x) ->
        NOP.Service.FBE.set_attribute(fbe, :value, rem(x,6))
      end)

    assert NOP.Application.get_statistics_from_total() > 0

  end

  test "Agregated FBE test" do

    fbe_agregated = NOP.Service.FBE.create_fbe(NOP.Element.FBE_Agregate, "NOP.element.MyFBE")

    assert NOP.Service.FBE.get_attribute(fbe_agregated, :dummy, :value) == 0

  end


  test "Agregated FBE test with changing attribute" do

    fbe_agregated = NOP.Service.FBE.create_fbe(NOP.Element.FBE_Agregate, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe_agregated, :dummy, :value, 5)

    assert NOP.Service.FBE.get_attribute(fbe_agregated, :dummy, :value) == 5

  end

  test "Agregated FBE test with changing attribute and assert premise" do

    fbe_agregated = NOP.Service.FBE.create_fbe(NOP.Element.FBE_Agregate, "NOP.element.MyFBE")

    premise =
      NOP.Service.Premise.create_premise("NOP.element.Premise", fbe_agregated, {:dummy, :value}, :EQ, 5)

    NOP.Service.FBE.set_attribute(fbe_agregated, :dummy, :value, 5)

    :timer.sleep(100)

    assert NOP.Service.Premise.evaluate_premise(premise) == true

  end

  test "Testin attribution with expression" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value, 5)

    NOP.Service.FBE.set_attribute_expr(fbe, :value, "( @value * 2)")

    assert NOP.Service.FBE.get_attribute(fbe, :value) == 10

  end

  test "Testin attribution with expression in aggregation" do

    fbe_agregated = NOP.Service.FBE.create_fbe(NOP.Element.FBE_Agregate, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe_agregated, :dummy, :value, 5)

    NOP.Service.FBE.set_attribute_expr(fbe_agregated, :dummy,  :value, "( @value * 2 )")

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(fbe_agregated, :dummy, :value) == 10

  end

  test "Testing attribution with expression between attributes" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value1, 10)

    NOP.Service.FBE.set_attribute_expr(fbe, :value2, "@value1")

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(fbe, :value2) == 10

  end

  test "Testing attribution with atom expression between attributes" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value1, :yes)

    NOP.Service.FBE.set_attribute_expr(fbe, :value2, "@value1")

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(fbe, :value2) == :yes

  end

  test "Testing attribution with atom expression between attributes with logic expression" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value1, :yes)

    NOP.Service.FBE.set_attribute_expr(fbe, :value2, "if @value1 == :yes do :no else :yes end")

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(fbe, :value2) == :no

  end

  test "Testing attribution with atom expression between attributes with string concatenation" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value1, "Hello ")

    NOP.Service.FBE.set_attribute_expr(fbe, :value2, "@value1 <> \"world\"")

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(fbe, :value2) == "Hello world"

  end

  test "Testing getting multiple attributes at once" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value1, 10)

    NOP.Service.FBE.set_attribute(fbe, :value2, 20)

    attr_map = NOP.Service.FBE.get_attribute(fbe, [:value1, :value2])

    :timer.sleep(100)

    assert (attr_map[:value1] + attr_map[:value2]) == 30

  end

  test "Testing set attribute chain" do

    fbe1 = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE1")

    fbe2 = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE1")

    NOP.Service.FBE.set_attribute(fbe1, :value1, 10)
    NOP.Service.FBE.set_attribute(fbe2, :value2, 10)

    #Expression: fbe2.value1 = (fbe1.value1 * 3 + 10) + value2
    NOP.Service.FBE.set_attribute_chain(fbe1, "@value1 * 3 + 100", {fbe2, :value1, "@result + @value2"})

    :timer.sleep(200)

    assert NOP.Service.FBE.get_attribute(fbe2, :value1) == 140

  end

  test "Testing a division by zero" do

    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_multi_attr, "NOP.element.MyFBE")

    NOP.Service.FBE.set_attribute(fbe, :value1, 0)

    NOP.Service.FBE.set_attribute_expr(fbe, :value2, "10 / @value1")

    :timer.sleep(100)

    assert NOP.Service.FBE.get_attribute(fbe, :value2) == 0

  end

end

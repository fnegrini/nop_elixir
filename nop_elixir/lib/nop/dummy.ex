# ---
# Excerpted from "Programming Elixir ≥ 1.6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/elixir16 for more book information.
# ---
defmodule NOP.Element.FBE_dummy do
  use NOP.Element.FBE

  defp int_attributes() do
    %{:value => 0}
  end

  def change_value_to_3(fbe) do
    NOP.Service.FBE.set_attribute(fbe, :value, 3)
  end

end

defmodule NOP.Element.FBE_Agregate do
  use NOP.Element.FBE

  defp int_attributes() do
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "dummy")
    %{:dummy => fbe}
  end

  def change_value_to_3(fbe) do
    NOP.Service.FBE.set_attribute(fbe, :value, 3)
  end

end

defmodule NOP.Element.FBE_multi_attr do
  use NOP.Element.FBE

  defp int_attributes() do
    %{:value1 => 0,
      :value2 => 0}
  end

end

defmodule NOP.Element.Rule_dummy do
  use NOP.Element.Rule

  defp create_element_list(_fbe_list) do
    []
  end

  defp create_instigation_list(_fbe_list) do
    []
  end

end

defmodule NOP.Element.Rule_dummy2 do
  use NOP.Element.Rule

  defp create_element_list([fbe]) do

    premise1 =
      NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)

    premise2 =
      NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 1)

    condition1 = NOP.Service.Condition.create_condition("NOP.element.Condition1", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")

    condition2 = NOP.Service.Condition.create_condition("NOP.element.Condition2", [condition1], "( not NOP.element.Condition1 )")

    [condition1, condition2]

  end

  defp create_instigation_list(_fbe_list) do
    []
  end

end

defmodule NOP.Element.Rule_dummy3 do
  use NOP.Element.Rule

  defp create_element_list([fbe]) do

    premise = NOP.Service.Premise.create_premise("NOP.element.premise", fbe, :value, :EQ, 5)

    [premise]

  end

  defp create_instigation_list([fbe]) do
    [{NOP.Element.FBE_dummy, :change_value_to_3, [fbe]}]
  end

end

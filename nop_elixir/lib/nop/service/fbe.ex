#---
# Excerpted from "Programming Elixir ≥ 1.6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/elixir16 for more book information.
#---
defmodule NOP.Service.FBE do
  use GenServer
  use NOP.Service.ServiceBase

  def start_link(_) do
    state = %{}
    state = reset_element_to_state(state)
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(state) do
    { :ok, state}
  end

  def create_fbe(fbe_type, fbe_name) do
    {:ok, pid} = fbe_type.start_link(fbe_name)
    GenServer.cast(NOP.Service.FBE, {:add_element, fbe_name, pid})
    pid
  end

  def set_attribute(fbe, attr_name, attr_value) do
    GenServer.cast(fbe, {:set_attr, attr_name, attr_value})
  end

  def set_attribute(fbe, fbe_name, attr_name, attr_value) do
    GenServer.cast(fbe, {:set_attr, {fbe_name, attr_name}, attr_value})
  end

  def set_attribute_expr(fbe, attr_name, expression) do
    GenServer.cast(fbe, {:set_attr_expr, attr_name, expression})
  end

  def set_attribute_expr(fbe, fbe_name, attr_name, expression) do
    GenServer.cast(fbe, {:set_attr_expr, {fbe_name, attr_name}, expression})
  end

  def get_attribute(fbe, attr_name) do
    GenServer.call(fbe, {:get_attr, attr_name}, :infinity)
  end

  def get_attribute(fbe, fbe_name, attr_name) do
    GenServer.call(fbe, {:get_attr, {fbe_name, attr_name}}, :infinity)
  end

  def set_attribute_chain(fbe, expression, chain) do
    GenServer.cast(fbe, {:set_attr_chain, expression, chain})
  end

end

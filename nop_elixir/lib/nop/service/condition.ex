#---
# Excerpted from "Programming Elixir ≥ 1.6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/elixir16 for more book information.
#---
defmodule NOP.Service.Condition do
  use GenServer
  use NOP.Service.ServiceBase

  def start_link(_) do
    state = %{}
    state = reset_element_to_state(state)
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(args) do
    { :ok, args }
  end

  def create_condition(condition_name, premise_list, expression) do
    {:ok, pid} = NOP.Element.Condition.start_link(condition_name, premise_list, expression)
    GenServer.cast(NOP.Service.Condition, {:add_element, condition_name, pid})
    pid
  end

  def evaluate_condition(condition) do
    GenServer.call(condition, :evaluate)
  end

end

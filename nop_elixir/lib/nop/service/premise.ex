#---
# Excerpted from "Programming Elixir ≥ 1.6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/elixir16 for more book information.
#---
defmodule NOP.Service.Premise do
  use GenServer
  use NOP.Service.ServiceBase

  def start_link(_) do
    state = %{}
    state = reset_element_to_state(state)
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(args) do
    { :ok, args }
  end

  def create_premise(premise_name, fbe, fbe_attr, operator, operand) do
    {:ok, pid} = NOP.Element.Premise.start_link(premise_name, fbe, fbe_attr, operator, operand)
    GenServer.cast(NOP.Service.Premise, {:add_element, premise_name, pid})
    pid
  end

  def create_premise(premise_name, fbe_left, fbe_left_attr, operator, fbe_right, fbe_right_attr) do
    {:ok, pid} = NOP.Element.Premise.start_link(premise_name, fbe_left, fbe_left_attr, operator, fbe_right, fbe_right_attr)
    GenServer.cast(NOP.Service.Premise, {:add_element, premise_name, pid})
    pid
  end

  def evaluate_premise(premise) do
    GenServer.call(premise, :evaluate)
  end

end

defmodule NOP.Service.ServiceBase do
  defmacro __using__([]) do
    quote do

      def get_name(element) do
        GenServer.call(element, :get_name, :infinity)
      end

      def get_statistics(element) do
        GenServer.call(element, :get_statistics, :infinity)
      end

      def reset_statistics(element) do
        GenServer.cast(element, :reset_statistics)
      end

      defp get_elements_from_state(state) do
        Map.get(state, :elements)
      end

      defp add_element_to_state(state, name, pid) do
        old = get_elements_from_state(state)
        Map.put(state, :elements, old ++ [{name, pid}])
      end

      defp reset_element_to_state(state) do
        Map.put(state, :elements, [])
      end

      def handle_cast({:add_element, name, pid}, state) do
        state = add_element_to_state(state, name, pid)
        {:noreply, state}
      end

      def handle_cast(:reset_element_list, state) do
        {:noreply, reset_element_to_state(state)}
      end

      def handle_cast(:reset_statistics, state) do
        elements = get_elements_from_state(state)
        reset_statisticis_all(elements)
        {:noreply, state}
      end

      defp reset_statisticis_all([]) do
        #Finished!
      end

      defp reset_statisticis_all([{_name, pid} | elements]) do
        reset_statistics(pid)
        reset_statisticis_all(elements)
      end

      def handle_call(:get_elements, _from, state) do
        {:reply, get_elements_from_state(state), state}
      end

      def handle_call(:get_statistics_count, _from, state) do
        {:reply, get_statistics_count(state), state}
      end


      defp get_statistics_count(state) do
        elements = get_elements_from_state(state)
        statistic_count(elements, 0)
      end

      defp statistic_count([], stat_count) do
        stat_count
      end

      defp statistic_count([{_name, pid} | elements], stat_count) do
        count = GenServer.call(pid, :get_statistics, :infinity)
        statistic_count(elements, count + stat_count)
      end

    end
  end
end

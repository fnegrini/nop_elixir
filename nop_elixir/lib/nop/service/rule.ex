#---
# Excerpted from "Programming Elixir ≥ 1.6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/elixir16 for more book information.
#---
defmodule NOP.Service.Rule do
  use GenServer
  use NOP.Service.ServiceBase

  def start_link(_) do
    state = %{}
    state = reset_element_to_state(state)
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(args) do
    { :ok, args }
  end

  def create_rule(rule_type, rule_name, fbe_list) do
    {:ok, pid} = rule_type.start_link(rule_name, fbe_list)
    GenServer.cast(NOP.Service.Rule, {:add_element, rule_name, pid})
    pid
  end

  def evaluate_rule(rule) do
    GenServer.call(rule, :evaluate, :infinity)
  end

  def run_instigations(rule) do
    GenServer.cast(rule, :run_manual)
  end
end

defmodule NOP.Element.Condition do
  use GenServer
  use NOP.Element.LogicElement


  def start_link(condition_name, premise_list, expression) do

    state = init_state(condition_name, premise_list, expression)

    state = init_condition_list(state)

    state = reset_statistics_to_state(state)

    state = set_name(state, condition_name)

    #{:ok, pid} = GenServer.start_link(__MODULE__, state, name: String.to_atom(condition_name))
    {:ok, pid} = GenServer.start_link(__MODULE__, state)

    link_to_elements(premise_list, pid, condition_name)

    {:ok, pid}

  end

  def init(state) do
    {:ok, state}
  end


  defp init_state(condition_name, premise_list, expression) do

    premise_values = evaluate_elements(condition_name, premise_list)

    result = evaluate_condition(condition_name, premise_values, expression)

    %{:name   => condition_name,
      :premises => premise_list,
      :expression => expression,
      :values => premise_values,
      :result => result}

  end

  defp evaluate_condition(_, _, "") do
    true
  end

  defp evaluate_condition(condition_name, premise_values, expression) do

    Logger.debug("Evaluating condition #{condition_name} with expression: << #{expression} >>")

    expression_converted = convert_expression(premise_values, expression)

    {result, []} = Code.eval_string(expression_converted)

    Logger.debug("Condition #{condition_name} expression converted: << #{expression_converted} >> ? #{result}" )

    result
  end

  defp get_expression_from_state(state) do
    Map.get(state, :expression)
  end

  defp update_my_state(state, name, _element_name, values, _old_result) do

    expression = get_expression_from_state(state)

    evaluate_condition(name, values, expression)

  end

  defp convert_expression_int([], expression) do
    expression
  end

  defp convert_expression_int([{atom, assert} | premise_list], expression) do

    atom_string = to_string(atom)
    assert_string = to_string(assert)

    expression = String.replace(expression, atom_string, assert_string)

    convert_expression_int(premise_list, expression)

  end

  defp convert_expression(premise_values, expression) do
    #Convert To List
    premise_list = Map.to_list(premise_values)

    convert_expression_int(premise_list, expression)

  end

end

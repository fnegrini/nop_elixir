defmodule NOP.Element.ElementBase do
  defmacro __using__([]) do
    quote do

      defp get_name(state) do
        Map.get(state, :name)
      end

      defp set_name(state, name) do
        Map.put(state, :name, name)
      end

      defp get_statistics_from_state(state) do
        Map.get(state, :statistics)
      end

      defp add_statistics_to_state(state, count) do
        old = get_statistics_from_state(state)
        Map.put(state, :statistics, old + count)
      end

      defp reset_statistics_to_state(state) do
        Map.put(state, :statistics, 0)
      end

      defp get_timestamp() do
        :erlang.system_time(:millisecond)
      end

      def diff_between_timestamp(beg_time, end_time) do
        end_time - beg_time
      end

      def handle_call(:get_statistics, _from, state) do
        {:reply, get_statistics_from_state(state), state}
      end

      def handle_call(:get_name, _from, state) do
        {:reply, get_name(state), state}
      end

      def handle_cast(:reset_statistics, state) do
        {:noreply, reset_statistics_to_state(state)}
      end

    end
  end
end

#---
# Excerpted from "Programming Elixir ≥ 1.6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/elixir16 for more book information.
#---
defmodule NOP.Application do
  @moduledoc false

  use Application

  def start(_type, nil) do
    import Supervisor.Spec, warn: false

    children = [
      worker(NOP.Service.FBE,  [NOP.Service.FBE]),
      worker(NOP.Service.Premise, [NOP.Service.Premise]),
      worker(NOP.Service.Condition, [NOP.Service.Condition]),
      worker(NOP.Service.Rule, [NOP.Service.Rule]),
    ]

    opts = [strategy: :one_for_one, name: NOP.Supervisor]

    Supervisor.start_link(children, opts)

  end

  def get_statistics_from_total() do
    get_statistics_from_FBE() +
    get_statistics_from_Premise() +
    get_statistics_from_Condition() +
    get_statistics_from_Rule()

  end

  def reset_element_list_total() do
    reset_element_list_FBE()
    reset_element_list_Premise()
    reset_element_list_Condition()
    reset_element_list_Rule()
  end

  def reset_statistics_total() do
    reset_statistics_FBE()
    reset_statistics_Premise()
    reset_statistics_Condition()
    reset_statistics_Rule()
  end

  def get_statistics_from_FBE() do
    GenServer.call(NOP.Service.FBE, :get_statistics_count, :infinity)
  end

  def reset_element_list_FBE() do
    GenServer.cast(NOP.Service.FBE, :reset_element_list)
  end

  def reset_statistics_FBE() do
    GenServer.cast(NOP.Service.FBE, :reset_statistics)
  end

  def get_statistics_from_Premise() do
    GenServer.call(NOP.Service.Premise, :get_statistics_count, :infinity)
  end

  def reset_element_list_Premise() do
    GenServer.cast(NOP.Service.Premise, :reset_element_list)
  end

  def reset_statistics_Premise() do
    GenServer.cast(NOP.Service.Premise, :reset_statistics)
  end

  def get_statistics_from_Condition() do
    GenServer.call(NOP.Service.Condition, :get_statistics_count, :infinity)
  end

  def reset_element_list_Condition() do
    GenServer.cast(NOP.Service.Condition, :reset_element_list)
  end

  def reset_statistics_Condition() do
    GenServer.cast(NOP.Service.Condition, :reset_statistics)
  end

  def get_statistics_from_Rule() do
    GenServer.call(NOP.Service.Rule, :get_statistics_count, :infinity)
  end

  def reset_element_list_Rule() do
    GenServer.cast(NOP.Service.Rule, :reset_element_list)
  end

  def reset_statistics_Rule() do
    GenServer.cast(NOP.Service.Rule, :reset_statistics)
  end

  defp get_element_list_total() do
    GenServer.call(NOP.Service.FBE, :get_elements) ++
    GenServer.call(NOP.Service.Premise, :get_elements) ++
    GenServer.call(NOP.Service.Condition, :get_elements) ++
    GenServer.call(NOP.Service.Rule, :get_elements)
  end

  defp check_message_queue([]) do
    0 #List empty - all elements are queue empty
  end

  defp check_message_queue([{_name, pid} | elements]) do

    info = Process.info(pid)

    count =
      if info[:message_queue_len] == 0 and info[:status] == :waiting do
        0
      else 
        1
      end

    count + check_message_queue(elements)
  end

  defp check_message_queue_loop(elements) do

    if check_message_queue(elements) > 0 do

      :timer.sleep(10)

      check_message_queue_loop(elements)

    end

  end

  def wait_up_to_end_all_process() do

    # wait until all process have message queue empty
    elements = get_element_list_total()

    check_message_queue_loop(elements)

  end

end

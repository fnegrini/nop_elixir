defmodule NOP do
  @moduledoc """
  Documentation for NopCta.
  """

  @doc """
  NOP (Notification oriented programing) simulator for Elixir
  UTFPR - Universidade Federal do Paraná
  """
end

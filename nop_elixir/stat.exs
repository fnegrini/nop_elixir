{time_in_microseconds, ret_val} =  :timer.tc(
  fn ->
    NOP.Application.reset_element_list_FBE()
    fbe = NOP.Service.FBE.create_fbe(NOP.Element.FBE_dummy, "NOP.element.MyFBE")
    premise1 = NOP.Service.Premise.create_premise("NOP.element.premise1", fbe, :value, :EQ, 5)
    premise2 = NOP.Service.Premise.create_premise("NOP.element.premise2", fbe, :value, :GT, 1)
    condition1 = NOP.Service.Condition.create_condition("NOP.element.Condition1", [premise1, premise2], "( NOP.element.premise1 and NOP.element.premise2 )")
    condition2 = NOP.Service.Condition.create_condition("NOP.element.Condition2", [condition1], "( not NOP.element.Condition1 )")
    Enum.map(1..1000,
      fn(x) ->
	    NOP.Service.FBE.set_attribute(fbe, :value, rem(x,6))
      end)
	NOP.Application.wait_up_to_end_all_process()
  end)
  
  IO.puts("The liquid time is #{time_in_microseconds / 1000.0}")
  IO.puts("The gross time is #{NOP.Application.get_statistics_from_total()}")
  
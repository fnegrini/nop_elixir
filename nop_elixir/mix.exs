defmodule NOP.MixProject do
  use Mix.Project

  defp package() do
    [
      # This option is only needed when you don't want to use the OTP application name
      name: "nop_elixir",
      # These are the default files included in the package
      files: ["lib", "mix.exs"],
      licenses: ["Apache 2.0"],
      links: %{"Bitbucket" => "https://bitbucket.org/fnegrini/nop_elixir"}
    ]
  end

  def project do
    [
      app: :nop_elixir,
      version: "0.0.24",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "NOP Elixir",
      description: "NOP (Notification Oriented programing) simulator for Elixir",
      source_url: "https://bitbucket.org/fnegrini/nop_elixir",
      licenses: "Apache 2.0",
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {
        NOP.Application, nil
      },
      registered: [
        NOP.Server,
      ],
      extra_applications: [:logger],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:distillery, "~> 2.0.12", runtime: false},
      #{:ex_doc, ">= 0.0.0", only: :dev},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
